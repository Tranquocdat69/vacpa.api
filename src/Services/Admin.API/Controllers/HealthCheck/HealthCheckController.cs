﻿using Business;
using CommonUtils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Admin.API.Controllers
{
   [ApiVersion("1.0")]
   [ApiController]
   [Route("api/healthcheck")]
   //[ApiExplorerSettings(GroupName = "01: System - JWT")]
   public class HealthCheckController : ControllerBase
   {
       private readonly IConfiguration _config;

      //  public AdminTokenController(IConfiguration config, IUserHandler userHandler, IApplicationHandler applicationHandler)
       public HealthCheckController(IConfiguration config)
       {
           _config = config;
       }

       /// <summary>
       /// check trạng thái hoạt động của api
       /// </summary>
       /// <param name="healthcheck"></param>
       /// <returns></returns>
       [AllowAnonymous, HttpGet]
       public HealthCheckResult GetHealth()
       {
            var flag = UserCollectionOld.Instance.Check();

            if (flag)
            {
                return HealthCheckResult.Healthy();
            }

            return HealthCheckResult.Unhealthy();
       }

   }
}