﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using CommonUtils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Admin.API.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class ApplicationController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<ApplicationController> _logger;
        private readonly IApplicationHandler _handler;
        private readonly ILogHandler _logHanlder;

        public ApplicationController(ILogger<ApplicationController> logger, IApplicationHandler hanlder, ILogHandler logHanlder)
        {
            _logger = logger;
            _handler = hanlder;
            _logHanlder = logHanlder;
        }

        #region Get
        /// <summary>
        /// Lấy toàn bộ danh sách WFState
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet, Route("")]
        // [Authorize, HttpGet, Route("")]
        //[ProducesResponseType(typeof(Response<>), StatusCodes.Status200OK)]
        public Task<Response> GetAll()
        {
            return _handler.GetAllFromCache();
        }

        /// <summary>
        /// Lấy danh sách ApplicationId theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [ HttpGet, Route("filter")]
        public Task<Response> GetByFilterASync(string filter)
        {
            var filterModel = JsonConvert.DeserializeObject<ApplicationQueryModel>(filter);
            return _handler.GetByFilterAsync(filterModel);
        }

        /// <summary>
        /// Lấy thông tin Application theo Id
        /// </summary>
        /// <param name="commandId">Id bản ghi</param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("{applicationId}")]
        public Task<Response> GetByIdAsync(Guid applicationId)
        {
            return _handler.GetByIdAsync(applicationId);
        }
        #endregion Get

        #region Post
        [Authorize, HttpPost, Route("")]
        public Task<Response> CreateAsync([FromBody] ApplicationCreateModel model)
        {
            return _handler.CreateAsync(model);
        }

        #endregion Post


        #region Update
        /// <summary>
        /// Cập nhật thông tin Application
        /// </summary>
        /// <param name="">Id bản ghi</param>
        /// <returns></returns>
        [Authorize, HttpPut, Route("")]
        public Task<Response> UpdateAsync([FromBody] ApplicationUpdateModel model)
        {
            return _handler.UpdateAsync(model);
        }
        #endregion Update

        #region Delete
        [Authorize, HttpDelete, Route("delete/{applicationId}")]
        public Task<Response> DeleteAsync(Guid applicationId)
        {
            return _handler.DeleteAsync(applicationId);
        }

        [Authorize, HttpDelete, Route("delete/many")]
        public Task<Response> DeleteMany(List<Guid> ids)
        {
            return _handler.DeleteManyAsync(ids);
        }
        #endregion Delete


        /// <summary>
        /// Lấy danh sách logs theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("log")]
        public async Task<Response> GetLogASync(string filter)
        {
            var requestInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            var appId = requestInfo.ApplicationId;
            var filterModel = JsonConvert.DeserializeObject<LogFilterModel>(filter);
            //if (!filterModel.ApplicationId.HasValue)
            //{
            //    filterModel.ApplicationId = appId;
            //}
            //if (!filterModel.UserId.HasValue)
            //{
            //    filterModel.UserId = requestInfo.UserId;
            //}
            return await _logHanlder.GetLogByFilter(filterModel);
        }

        /// <summary>
        /// add log when upload start
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("upload-start")]
        public async Task<Response> LogUploadStart()
        {
            var requestInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            var appId = requestInfo.ApplicationId;
            var filterModel = JsonConvert.DeserializeObject<LogCreateModel>("{ }");
            filterModel.ApplicationId = appId;
            filterModel.UserId = requestInfo.UserId;
            filterModel.Content = "Bắt đầu upload";
            filterModel.ObjectType = "Satrt upload - Tin bài";
            filterModel.Exception = "Success";
            return await _logHanlder.CreateAsync(filterModel);
        }

        /// <summary>
        /// add log when upload end
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("upload-end")]
        public async Task<Response> LogUploadEnd()
        {
            var requestInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            var appId = requestInfo.ApplicationId;
            var filterModel = JsonConvert.DeserializeObject<LogCreateModel>("{ }");
            filterModel.ApplicationId = appId;
            filterModel.UserId = requestInfo.UserId;
            filterModel.Content = "Kết thúc upload";
            filterModel.ObjectType = "End upload - Tin bài";
            filterModel.Exception = "Success";
            return await _logHanlder.CreateAsync(filterModel);
        }

        /// <summary>
        /// add log when download start
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("download-start")]
        public async Task<Response> LogDownloadStart()
        {
            var requestInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            var appId = requestInfo.ApplicationId;
            var filterModel = JsonConvert.DeserializeObject<LogCreateModel>("{ }");
            filterModel.ApplicationId = appId;
            filterModel.UserId = requestInfo.UserId;
            filterModel.Content = "Bắt đầu download";
            filterModel.ObjectType = "Start download - Tin bài";
            filterModel.Exception = "Success";
            return await _logHanlder.CreateAsync(filterModel);
        }

        /// <summary>
        /// add log when download end
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("download-end")]
        public async Task<Response> LogDownloadEnd()
        {
            var requestInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            var appId = requestInfo.ApplicationId;
            var filterModel = JsonConvert.DeserializeObject<LogCreateModel>("{ }");
            filterModel.ApplicationId = appId;
            filterModel.UserId = requestInfo.UserId;
            filterModel.Content = "Kết thúc download";
            filterModel.ObjectType = "End download - Tin bài";
            filterModel.Exception = "Success";
            return await _logHanlder.CreateAsync(filterModel);
        }

        /// <summary>
        /// add log when delete end
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("delete-files")]
        public async Task<Response> LogDeleteFile()
        {
            var requestInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            var appId = requestInfo.ApplicationId;
            var filterModel = JsonConvert.DeserializeObject<LogCreateModel>("{ }");
            filterModel.ApplicationId = appId;
            filterModel.UserId = requestInfo.UserId;
            filterModel.Content = "Xóa file";
            filterModel.ObjectType = "Delete file - Tin bài";
            filterModel.Exception = "Success";
            return await _logHanlder.CreateAsync(filterModel);
        }
    }
}
