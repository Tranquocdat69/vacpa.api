﻿using Business;
using CommonUtils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Admin.API.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class OrganizationController
    {
        private IOrganizationHandler _handler;
        private ILogger<IOrganizationHandler> _logger;

        public OrganizationController(IOrganizationHandler handler, ILogger<IOrganizationHandler> logger)
        {
            this._handler = handler;
            this._logger = logger;
        }

        [Authorize, HttpPost, Route("")]
        public Task<Response> CreateAsync([FromBody] OrganizationModel model)
        {
            return _handler.CreateOrganization(model);
        }

        //Load list đơn vị cha
        [Authorize, HttpGet, Route("currentId")]
        public Task<Response> GetAllParentASync(Guid? currentId)
        {
            return _handler.GetAllOrganization(currentId);
        }

        [Authorize, HttpGet, Route("{id}")]
        public Task<Response> GetByIdAsync(Guid id)
        {
            return _handler.GetOrganizationById(id);
        }

        [Authorize, HttpGet, Route("filter")]
        public Task<Response> GetByFilterASync(string filter)
        {
            var filterModel = JsonConvert.DeserializeObject<OrganizationQueryModel>(filter);
            return _handler.GetOrganizationByFilter(filterModel);
        }

        [Authorize, HttpPut, Route("")]
        public Task<Response> UpdateOrganizationAsync([FromBody] OrganizationModel model)
        {
            return _handler.UpdateOrganization(model);
        }

        [Authorize, HttpDelete, Route("{id}")]
        public Task<Response> DeleteOrganizationAsync(Guid id)
        {
            return _handler.DeleteOrganization(id);
        }

        [Authorize, HttpDelete, Route("multi")]
        public async Task<Response> DeleteMultiAsync([FromBody] List<Guid> listId)
        {
            return await _handler.DeleteMultiAsync(listId);
        }


        [Authorize, HttpGet, Route("")]
        public Task<List<OrganizationModel>> GetAllOrganization()
        {
            return _handler.GetAll();
        }

        [Authorize, HttpGet, Route("allbycache")]
        public Task<Response> GetAllFromCache()
        {
            return _handler.GetAllFromCache();
        }
    }
}
