﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using CommonUtils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Admin.API.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly ILogger<RoleController> _logger;
        private readonly IRoleHandler _hanlder;
        public RoleController(ILogger<RoleController> logger, IRoleHandler hanlder)
        {
            _logger = logger;
            _hanlder = hanlder;
        }   

        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseList<RoleCreateModel>), StatusCodes.Status200OK)]
        public async Task<Response> GetAllAsync()
        {
            return await _hanlder.GetAllAsync();
        }

        [Authorize, HttpGet, Route("filter")]
        [ProducesResponseType(typeof(ResponsePagination<RoleModel>), StatusCodes.Status200OK)]
        public async Task<Response> GetFilter(string filter)
        {
            var filterModel = JsonConvert.DeserializeObject<RoleQueryModel>(filter);
            return  await _hanlder.GetByFilterAsync(filterModel);
        }

        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<Response> CreateAsync([FromBody] RoleCreateModel model)
        {
            return await _hanlder.CreateAsync(model);
        }

        [Authorize, HttpPut, Route("")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<Response> UpdateAsync([FromBody] RoleUpdateModel model)
        {
            return await _hanlder.UpdateAsync(model);
        }
        [Authorize, HttpDelete, Route("{roleId}")]
        public async Task<Response> DeleteByIdAsync(Guid roleId)
        {
            return await _hanlder.DeleteAsync(roleId);
        }       


        [Authorize, HttpDelete, Route("multi")]
        [ProducesResponseType(typeof(ResponseDeleteMulti), StatusCodes.Status200OK)]
        public async Task<Response> DeleteMultiAsync([FromBody] List<Guid> listId)
        {
            return await _hanlder.DeleteMultiAsync(listId);
        }


        [Authorize, HttpGet, Route("{roleId}")]
        [ProducesResponseType(typeof(ResponseObject<RoleCreateModel>), StatusCodes.Status200OK)]
        public async Task<Response> GetByIdAsync(Guid roleId)
        {
            return await _hanlder.GetByIdAsync(roleId);
        }
    }

}