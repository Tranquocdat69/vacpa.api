﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CommonUtils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Admin.API.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class DownloadExcelController : ControllerBase
    {
        [Authorize, HttpGet, Route("create")]
        public string CreateDirectory()
        {
            try
            {
                #region Xác định thư mục đích

                var rootPath = Utils.GetConfig("AppSettings:Folder:RootPath");
                //var sourcefolder = Utils.GetConfig("AppSettings:Folder:source");
                var sectionName = Utils.GetConfig("AppSettings:Folder:sectionName");
                // Append section if existed
                if (string.IsNullOrEmpty(sectionName))
                {
                    sectionName = "sectionNamedefault";
                }
                var sourcefolder = sectionName + "/Download";
                var folderPath = DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("dd");
                var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder, folderPath));
                if (!isExistDirectory)
                {
                    var directoryInfo = Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder, folderPath));
                }
                #endregion    
                //var directory = Path.Combine(sourcefolder, folderPath);
                var directory = rootPath + "/" + sourcefolder + "/" + folderPath;
                return directory;

            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return ("Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        [Authorize, HttpGet, Route("")]
        public string CreateFileName(string typeName)
        {
            try
            {
                var filePrefix = DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") +
                        DateTime.Now.ToString("ss") + new Random(DateTime.Now.Millisecond).Next(10, 99);
                var filePostFix = DateTime.Now.ToString("yy-MM-dd");
                var fileName = typeName + "_" + filePrefix + "_" +
                        filePostFix + ".xlsx";
                return fileName;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return ("Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public class ExportModel
        {
            public string TypeName { get; set; }
            public List<ColumnHeader> ColumnHeader { get; set; }
        }

        public class ColumnHeader
        {
            public string Alphabet { get; set; }
            public string ColumnName { get; set; }
            public string Value { get; set; }
        }
    }
}