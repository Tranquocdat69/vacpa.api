﻿using Business;
using CommonUtils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Admin.API.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class WorkflowConfigureController : ControllerBase
    {
        private readonly IWorkflowConfigureHandler _handler;
        public WorkflowConfigureController(IWorkflowConfigureHandler handler)
        {
            _handler = handler;
        }
        #region Get
        /// <summary>
        /// Lấy toàn bộ ds wf configure
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponseList<WorkflowConfigureModel>), StatusCodes.Status200OK)]
        public async Task<Response> GetAll()
        {
            return await _handler.GetAll();
        }
        /// <summary>
        /// Lấy wf configure theo id    
        /// </summary>
        /// <param name="workflowConfigureId"></param>
        /// <returns></returns>
        [HttpGet, Route("{workflowConfigureId}")]
        [ProducesResponseType(typeof(ResponseObject<WorkflowConfigureModel>), StatusCodes.Status200OK)]
        public async Task<Response> GetById(Guid workflowConfigureId)
        {
            return await _handler.GetById(workflowConfigureId);
        }
        /// <summary>
        /// Lấy wf configure theo bộ lọc   
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet, Route("filter")]
        public Task<Response> GetByFilter(string filter)
        {
            var filterModel = JsonConvert.DeserializeObject<WorkflowConfigureQueryModel>(filter);
            return _handler.GetByFilter(filterModel);
        }
        /// <summary>
        ///    Lay wf name
        /// </summary>
        /// <param name="wfname"></param>
        /// <returns></returns>
        [HttpGet, Route("wfname")]
        public Task<Response> GetWFName()
        {
            //var filterModel = JsonConvert.DeserializeObject<WorkflowConfigureQueryModel>(filter);
            return _handler.GetAllWF();
        }
        /// <summary>
        /// Lấy wf theo chuc nang input: navId, area, donVi   
        /// </summary>
        /// <param name="getWFForFunction"></param>
        /// <returns></returns>
        [HttpGet, Route("getWFForFunction_navId")]
        public Task<Response> getWFForFunction_navId(Guid? navId, Guid? provinceId, Guid? districtId, Guid? wardId, Guid? donViId)
        {
            return _handler.GetWFForFunction_navId(navId,provinceId,districtId,wardId,donViId);
        }
        /// <summary>
        /// Lấy wf theo chuc nang input: url, area, donVi   
        /// </summary>
        /// <param name="getWFForFunction"></param>
        /// <returns></returns>
        [HttpGet, Route("getWFForFunction_url")]
        public Task<Response> getWFForFunction_url(string url, Guid? provinceId, Guid? districtId, Guid? wardId, Guid? donViId)
        {
            return _handler.GetWFForFunction_url(url, provinceId, districtId, wardId, donViId);
        }
        /// <summary>
        /// Kiem tra user co trong nhom quyen QTHT hay khong?   
        /// </summary>
        /// <param name="IsAdministratorRole"></param>
        /// <returns></returns>
        [HttpGet, Route("isAdministratorRole")]
        public Response IsAdministratorRole(Guid? id)
        {
            //var filterModel = JsonConvert.DeserializeObject<WorkflowConfigureQueryModel>(filter);
            return _handler.IsAdministratorRole(id);
        }
        #endregion
        #region Post
        [HttpPost, Route("create")]
        public Task<Response> Create([FromBody] WorkflowConfigureCreateModel model)
        {
            return _handler.Create(model);
        }
        #endregion
        #region Update
        [HttpPut, Route("{workflowConfigureId}")]
        public async Task<Response> Update(Guid workflowConfigureId, [FromBody] WorkflowConfigureUpdateModel model)
        {
            return await _handler.Update(workflowConfigureId, model);
        }
        #endregion
        #region Delete
        [HttpDelete, Route("{workflowConfigureId}")]
        public async Task<Response> Delete(Guid workflowConfigureId)
        {
            return await _handler.Delete(workflowConfigureId);
        }

        [HttpDelete, Route("many")]
        public async Task<Response> DeleteMany(List<Guid> ids)
        {
            return await _handler.DeleteMany(ids);
        }
        #endregion


    }

}
