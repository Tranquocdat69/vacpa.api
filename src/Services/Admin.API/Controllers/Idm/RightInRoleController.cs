﻿using Business;
using CommonUtils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin.API.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class RightInRoleController : ControllerBase
    {
        private readonly ILogger<RightInRoleController> _logger;

        private readonly IRightInRoleHandler _handler;
        public RightInRoleController(ILogger<RightInRoleController> logger, IRightInRoleHandler handler)
        {
            _logger = logger;
            _handler = handler;
        }

        #region CRUD

        [Authorize, HttpPost, Route("{rightCode}/{roleId}")]
        public async Task<Response> CreateAsync(string rightCode, Guid roleId)
        {
            try
            {
                return await _handler.CreateAsync(rightCode, roleId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new ResponseObject<RightInRoleModel>(null, ex.Message, Code.ServerError);
            }
        }

        [Authorize, HttpGet, Route("{id}")]
        public async Task<Response> GetByRoleIdAsync(Guid id)
        {
            try
            {
                return await _handler.GetByRoleIdAsync(id);
            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                return new ResponseList<RightInRoleModel>(null, ex.Message, Code.ServerError);
            }
        }

        [Authorize, HttpPut, Route("")]
        public async Task<Response> UpdateAsync([FromBody]RightInRoleModel model)
        {
            try
            {
                return await _handler.UpdateAsync(model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new ResponseObject<RightInRoleModel>(null, ex.Message, Code.ServerError);
            }
        }

        [Authorize, HttpDelete, Route("{id}")]
        public async Task<Response> DeleteById(long id)
        {
            try
            {
                return await _handler.DeleteById(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new ResponseObject<RightInRoleModel>(null, ex.Message, Code.ServerError);
            }
        }

        /// <summary>
        /// Thêm nhiều quyền cho role
        /// </summary>
        /// <param name="rightCodes">Danh sách quyền</param>
        /// <param name="roleId">RoleId</param>
        /// <returns></returns>
        [Authorize, HttpPost, Route("{id}/right/many")]
        public async Task<Response> AddRightsToRole(List<string> rightCodes, Guid roleId)
        {
            try
            {
                return await _handler.AddRightsToRole(rightCodes, roleId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        #endregion

    }
}
