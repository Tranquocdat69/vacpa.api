﻿using Business;
using CommonUtils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin.API.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class RightController : ControllerBase
    {
        private readonly ILogger<RightController> _logger;
        private readonly IRightHandler _handler;
        public RightController(ILogger<RightController> logger, IRightHandler handler)
        {
            _logger = logger;
            _handler = handler;
        }

        #region CRUD
        /// <summary>
        /// Thêm mới right
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        [Authorize, HttpPost, Route("")]
        public async Task<Response> CreateAsync([FromBody] RightCreateModel model)
        {
            return await _handler.CreateAsync(model);
        }

        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="rightCode">Id bản ghi</param>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        [Authorize, HttpPut, Route("")]
        public async Task<Response> UpdateAsync(string rightCode, [FromBody] RightUpdateModel model)
        {
            return await _handler.UpdateAsync(model);
        }

        /// <summary>
        /// Lấy về tất cả
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        [Authorize, HttpGet, Route("all")]
        public async Task<Response> GetAllAsync()
        {
            return await _handler.GetAllAsync();
        }

        /// <summary>
        /// Xoá theo Id
        /// </summary>
        /// <param name="rightCode">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        [Authorize, HttpDelete, Route("{rightCode}")]
        public async Task<Response> DeleteByIdAsync(string rightCode)
        {
            return await _handler.DeleteByIdAsync(rightCode);
        }
        #endregion

        /// <summary>
        /// Lấy về theo Id
        /// </summary>
        /// <param name="rightCode">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        [Authorize, HttpGet, Route("{rightCode}")]
        public async Task<Response> GetByCodeAsync(string rightCode)
        {
            return await _handler.GetByCodeAsync(rightCode);
        }

        /// <summary>
        /// Lấy về theo filter
        /// </summary>
        /// <param name="filterModel"></param>
        /// <param name="size">Số lượng bản ghi lấy về</param>
        /// <param name="page">Số trang</param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("filter")]
        public async Task<Response> GetByFilterAsync(string filter)
        {
            var filterModel = JsonConvert.DeserializeObject<RightFilterModel>(filter);
            return await _handler.GetByFilterAsync(filterModel);
        }
        /// <summary>
        /// Lấy về theo ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [Authorize, HttpDelete, Route("many")]
        public Task<Response> DeleteMany(List<String> ids)
        {
            return _handler.DeleteMany(ids);
        }

        /// <summary>
        /// Lấy về theo groups
        /// </summary>
        /// <param name="rightCode">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        [Authorize, HttpGet, Route("groups")]
        public async Task<Response> GetByGroupRight()
        {
            return await _handler.GetByGroupRight();
        }

    }
}
