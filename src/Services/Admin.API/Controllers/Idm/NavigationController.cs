﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using CommonUtils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Admin.API.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class NavigationController : ControllerBase
    {
        private readonly ILogger<NavigationController> _logger;
        private readonly INavigationHandler _handler;
        public NavigationController(ILogger<NavigationController> logger, INavigationHandler handlder)
        {
            _logger = logger;
            _handler = handlder;
        }

        [Authorize, HttpGet, Route("")]
        public async Task<Response> Get(Guid userId)
        {
            var requestInfo = CommonUtils.Helper.GetRequestInfo(Request, HttpContext.User);
            var appId = requestInfo.ApplicationId;

            return await _handler.GetNavigationsByUserId(userId, appId);
        }

        /// <summary>
        /// Check
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="navigationUrl"></param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("check")]
        public async Task<Response> Check(string userId, string navigationUrl)
        {
            var requestInfo = CommonUtils.Helper.GetRequestInfo(Request, HttpContext.User);
            var appId = requestInfo.ApplicationId;
            return await _handler.CheckNavigationsByUserID(userId, navigationUrl, appId.ToString());
        }

        /// <summary>
        /// get by condition
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="navigationId"></param>
        /// <param name="navigationUrl"></param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("rights")]
        //[EnableCors(origins: "*", headers: "*", methods: "GET")]
        public async Task<Response> Get(string userId, string navigationId, string navigationUrl)
        {
            var requestInfo = CommonUtils.Helper.GetRequestInfo(Request, HttpContext.User);
            var appId = requestInfo.ApplicationId;
            return await _handler.GetAllRightsInNavigations(userId, navigationId, navigationUrl, appId.ToString());
        }

        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet, Route("all")]
        //[EnableCors(origins: "*", headers: "*", methods: "GET")]
        public async Task<ResponseObject<List<NavigationModel>>> GetAll(string textSearch)
        {

            //return await _handler.GetNavigations(null, true);
            return await _handler.GetNavigations(textSearch, true);

        }

        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet, Route("allnav")]
        //[EnableCors(origins: "*", headers: "*", methods: "GET")]        
        public async Task<Response> GetAllNav()
        {
            var requestInfo = CommonUtils.Helper.GetRequestInfo(Request, HttpContext.User);
            var appId = requestInfo.ApplicationId;
            return await _handler.GetAllNavigations(null);
        }
        /// <summary>
        /// get all subchild 
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet, Route("allSubchildNav")]
        //[EnableCors(origins: "*", headers: "*", methods: "GET")]        
        public async Task<Response> GetNavSubChild()
        {
            //var requestInfo = CommonUtils.Helper.GetRequestInfo(Request, HttpContext.User);
            //var appId = requestInfo.ApplicationId;
            return await _handler.GetNavSubChild();
        }

        /// <summary>
        /// update roles of navigation
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [Authorize, HttpPut, Route("role/update")]
        public async Task<Response> UpdateRoleInNavigation([FromBody] NavigationRoleUpdateModel value)
        {

            return await _handler.UpdateRoleInNavigations(value);
        }

        /// <summary>
        /// create update
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>        
        [Authorize, HttpPost, Route("role")]
        public async Task<Response> CreateRoleInNavigation([FromBody] NavigationRoleUpdateModel value)
        {

            //var requestData = ApiUtils.GetRequestFields(Request);
            //var appId = requestData.ApplicationId;
            //value.ApplicationId = appId;
            return await _handler.CreateRoleInNavigations(value);
        }

        /// <summary>
        /// created
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>      
        [Authorize, HttpPost, Route("")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Add([FromBody] NavigationCreateRequestModel value)
        {

            var requestInfo = CommonUtils.Helper.GetRequestInfo(Request, HttpContext.User);
            var appId = requestInfo.ApplicationId;
            value.ApplicationId = appId;
            return await _handler.Create(value);
        }

        /// <summary>
        /// update
        /// </summary>
        /// <param name="value"></param>
        /// <param name="id"></param>
        /// <returns></returns>      
        [Authorize, HttpPut, Route("")]
        //[EnableCors(origins: "*", headers: "*", methods: "PUT")]
        public async Task<Response> Update([FromBody] NavigationUpdateRequestModel value)
        {

            value.NavigationId = value.NavigationId;
            return await _handler.Update(value);
        }

        /// <summary>
        /// delete a navigation
        /// </summary>
        /// <param name="navigationId"></param>
        /// <returns></returns>    
        [Authorize, HttpDelete, Route("{navigationId}")]
        //[EnableCors(origins: "*", headers: "*", methods: "DELETE")]
        public async Task<Response> Delete(Guid navigationId)
        {

            return await _handler.Delete(navigationId);
        }

        //public class DeleteNavigation2
        //{
        //    public List<TaxonomyTermModel> ListItemDelete { get; set; }
        //}

        //hoant -  27/09/2017
        /// <summary>
        /// get list Navigation by role id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("{roleId}/navigation")]
        public async Task<Response> GetNavigationByRole(Guid roleId)
        {
            return await _handler.GetNavigationByRole(roleId);
        }
    }

}