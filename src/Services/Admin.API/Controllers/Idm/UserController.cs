﻿using Admin.API.Business.NodeUpload;
using ApiUtils;
using Business;
using CommonUtils;
using DataUtils;
using Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static Admin.API.Controllers.DownloadExcelController;

namespace Admin.API.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserHandler _hanlder;

        public UserController(ILogger<UserController> logger, IUserHandler hanlder)
        {
            _logger = logger;
            _hanlder = hanlder;
        }

        #region CRUD
        [ HttpGet, Route("all")]
        //[Authorize]
        public Task<Response> GetAll()
        {
            return _hanlder.GetAll();
        }

        [ HttpGet, Route("allbycache")]
        public async Task<Response> GetAllFromCache()
        {
            var userInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            var appId = (userInfo.ApplicationId.ToString() != "00000000-0000-0000-0000-000000000000") ? userInfo.ApplicationId.ToString() : "48ed5b71-66dc-4725-9604-4c042e45fa3f";
            var res = ApiUtils.UserCollection.Instance.GetAllUserByAppId(appId);
            return new ResponseObject<List<SystemUserModel>>(res);
        }

        [Authorize, HttpGet, Route("filter")]
        [ProducesResponseType(typeof(ResponsePagination<UserModel>), StatusCodes.Status200OK)]
        public async Task<Response> GetFilter(string filter="")
        {
            var filterModel = JsonConvert.DeserializeObject<UserFilterModel>(filter);
            return await _hanlder.GetFilter(filterModel);
        }
        //ManhNX
        [Authorize, HttpGet, Route("filterUserRegister")]
        [ProducesResponseType(typeof(ResponsePagination<UserModel>), StatusCodes.Status200OK)]
        public async Task<Response> GetFilterUserRegister(string filter = "")
        {
            var filterModel = JsonConvert.DeserializeObject<UserRegisterFilterModel>(filter);
            return await _hanlder.GetUserRegisterFilter(filterModel);
        }

        [HttpGet]
        [Route("user-export")]
        public ResponseObject<FileUploadResult> UserExportExcel(string export)
        {
            var exportModel = JsonConvert.DeserializeObject<ExportModel>(export);
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            try
            {
                DownloadExcelController downloadExcelCtrl = new DownloadExcelController();
                string directory = downloadExcelCtrl.CreateDirectory();
                string typeName = exportModel.TypeName;
                string fileName = downloadExcelCtrl.CreateFileName(typeName);
                FileInfo file = new FileInfo(Path.Combine(directory, fileName));
                if (file.Exists)
                {
                    file.Delete();
                    file = new FileInfo(Path.Combine(directory, fileName));
                }

                var columnHeaders = exportModel.ColumnHeader;

                using (ExcelPackage package = new ExcelPackage(file))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");

                    // add Header     
                    for (int i = 0; i < columnHeaders.Count(); i++)
                    {
                        worksheet.Cells[1, i + 1].Value = columnHeaders[i].ColumnName;
                        worksheet.Cells[1, i + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Cells[1, i + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                    }
                    using (var headers = worksheet.Cells[1, 1, 1, columnHeaders.Count()]) //(1,1) (1,7)
                    {
                        headers.AutoFilter = true;
                    }
                    int j = 2;
                    int stt = 1;

                    using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                    {
                        var lstUser = unitOfWork.GetRepository<IdmUsers>().GetAll().Where(a => a.ApplicationId == appId).ToList();

                        foreach (var data in lstUser)
                        {
                            foreach (var item in exportModel.ColumnHeader)
                            {
                                if (item.ColumnName == "STT")
                                {
                                    worksheet.Cells[item.Alphabet + j].Value = stt;
                                }
                                else
                                {
                                    worksheet.Cells[item.Alphabet + j].Value = data.GetPropValue(item.Value);
                                }
                            }

                            j++;

                            stt++;
                        }

                        package.Save();
                        var rootpath = Utils.GetConfig("AppSettings:Folder:RootPath");
                        var fileUploadResult = new FileUploadResult
                        {
                            Status = 1,
                            Message = "Xử lý file thành công",
                            PhysicalPath = (directory + "/" + fileName).Substring(rootpath.Length + 1),// path to become an url for downloading
                            Name = "File excel" + exportModel.TypeName,
                            PhysicalName = directory + "/" + fileName,
                            Size = 1,
                            Extension = "xlsx",
                            AbsolutePath = directory + "/" + fileName,
                            RelativePath = directory + "/" + fileName
                        };
                        return new ResponseObject<FileUploadResult>(fileUploadResult, "Xử lý thành công", Code.Success, 1, 1, 1);
                    }

                }

            }
            catch (Exception ex)
            {
                Serilog.Log.Error("", ex.Message);
                return new ResponseObject<FileUploadResult>(null, ex.Message, Code.ServerError, 0, 0, 0);
            }
        }

        [Authorize, HttpPost, Route("")]
        public Task<Response> Create([FromBody] SystemUserModel model)
        {
            var userInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            var userId = userInfo.UserId;
            var applicationId = userInfo.ApplicationId;
            return _hanlder.Create(model);
        }
        //ManhNX
        [ HttpPost, Route("Register")]
        //[Authorize]
        public Task<Response> Register([FromBody] UserRegisterModel model)
        {
            //var userInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            //var userId = userInfo.UserId;
            //var applicationId = userInfo.ApplicationId;
            return _hanlder.Register(model);
        }
        [ HttpPost, Route("CreateUserByQTHT")]
        //[Authorize]
        public Task<Response> CreateUserByQTHT([FromBody] UserCreateByQTHTModel model)
        {
            //var userInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            //var userId = userInfo.UserId;
            //var applicationId = userInfo.ApplicationId;
            return _hanlder.CreateUserByQTHT(model);
        }
        [Authorize, HttpPut, Route("updateUserByQTHT")]
        public Task<Response> UpdateUserByQTHT([FromBody] UserUpdateByQTHTModel model)
        {
            var userInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            var userId = userInfo.UserId;
            var applicationId = userInfo.ApplicationId;
            return _hanlder.UpdateUserByQTHT(model);
        }
        [Authorize, HttpPost, Route("checkMailAlready")]
        public Response CheckMailAlready([FromBody] CheckMailModel model)
        {
            var userInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            var userId = userInfo.UserId;
            var applicationId = userInfo.ApplicationId;
            return _hanlder.CheckMailAlready(model);
        }
        [HttpPut, Route("forgetPassword")]
        public Task<Response> ForgotPassword(string username, string email)
        {
            return _hanlder.ForgotPassword(username, email);
        }

        [Authorize, HttpPut, Route("")]
        public Task<Response> Update([FromBody] SystemUserModel model)
        {
            var userInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            var userId = userInfo.UserId;
            var applicationId = userInfo.ApplicationId;
            return _hanlder.Update(model);
        }

        [Authorize, HttpGet, Route("{userId}")]
        public Task<Response> GetById(Guid userId)
        {
            return _hanlder.GetById(userId);
        }

        [Authorize, HttpGet, Route("{userName}/username")] 
        public Task<Response> GetByUserName(string userName)
        {
            return _hanlder.GetByUserName(userName);
        }

        [Authorize, HttpPut, Route("locking")]
        public Task<Response> LockUser([FromBody] UserLockingModel model)
        {
            return _hanlder.LockUser(model);
        }

        [Authorize, HttpPut, Route("password")]
        public Task<Response> UpdateUserPassword([FromBody] UpdateUserPasswordModel model)
        {
            return _hanlder.UpdateUserPassword(model);
        }
        #endregion

        #region User with Role and Right
        [Authorize, HttpGet, Route("roles-and-rights/{userId}")]
        public Task<Response> GetRolesAndRightsOfuser(Guid userId)
        {
            return _hanlder.GetRolesAndRightsOfuser(userId);
        }

        [Authorize, HttpPost, Route("role")]
        public Task<Response> AddRoleIntoUser([FromBody] RoleOfUser model)
        {
            return _hanlder.AddRoleIntoUser(model);
        }

        [Authorize, HttpDelete, Route("role/{userId}/{roleId}")]
        public Task<Response> DeleteRoleOfUser(Guid userId, Guid roleId)
        {
            return _hanlder.DeleteRoleOfUser(userId, roleId);
        }

        #endregion

        #region user with Right
        /// <summary>
        /// Thêm mới nhiều quyền cho user
        /// </summary>
        /// <param name="rightId">Danh sách quyền</param>
        /// <param name="userId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [Authorize, HttpPost, Route("{id}/right/many")]
        public Task<Response> AddRightsASync([FromBody] List<string> rightId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            var requestInfo = CommonUtils.Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;

            return _hanlder.AddUserRights(id, rightId, appId, actorId);
        }

        /// <summary>
        /// Thêm mới một quyền cho user
        /// </summary>
        /// <param name="rightId">Mã quyền </param>
        /// <param name="userId">UserId</param>
        /// <param name="applicationId">ApplicationId</param>
        /// <returns></returns>
        [Authorize, HttpPost, Route("{id}/right")]
        public Task<Response> AddRightASync([FromBody] RightInUserModel model)
        {
            var requestInfo = CommonUtils.Helper.GetRequestInfo(Request, HttpContext.User);
            var appId = requestInfo.ApplicationId;

            return _hanlder.AddUserRight(model.UserId, model.RightCode, model.ApplicationId.Value);
        }

        [Authorize, HttpDelete, Route("{id}/right")]
        public Task<Response> DeleteRightsAsync([FromBody] RightInUserModel model)
        {
            var requestInfo = CommonUtils.Helper.GetRequestInfo(Request, HttpContext.User);
            var appId = model.ApplicationId ?? requestInfo.ApplicationId;

            return _hanlder.DeleteUserRights(model.UserId, model.LstRightCode, appId);
        }

        [Authorize, HttpPut, Route("{id}/right/{rightCode}")]
        public Task<Response> UpdateRightOfUser(Guid id, string rightCode)
        {
            var requestInfo = CommonUtils.Helper.GetRequestInfo(Request, HttpContext.User);
            var appId = requestInfo.ApplicationId;

            return _hanlder.UpdateRightOfUser(id, rightCode, appId);
        }

        #endregion

        #region GetBirthdayUser
        [ HttpGet, Route("birthday")]
        //[Authorize]
        public Task<Response> GetBirthday()
        {
            return _hanlder.GetBirthday();
        }

        #endregion


        [Authorize, HttpPut, Route("organization")]
        public Task<Response> AddAllOrganization()
        {
            return _hanlder.AddAllOrganization();
        }
        #region WF
        [HttpPost, Route("commands")]
        public Task<Response> GetCommands(InputCommand model)
        {
            //var userInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            //var userId = userInfo.UserId;
            //var applicationId = userInfo.ApplicationId;
            //model.UserId = userId.ToString();
            return _hanlder.GetCommands(model);
        }
        [ HttpPost, Route("execute")]
        public Task<Response> ExecuteCommand(InputCommand model)
        {
            //var userInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            //var userId = userInfo.UserId;
            //var applicationId = userInfo.ApplicationId;
            //model.UserId = userId.ToString();
            return _hanlder.ExecuteCommand(model);
        }

        [HttpPost, Route("registerexecute")]
        public Task<Response> ExecuteRegisterCommand(InputCommand model)
        {
            //var userInfo = CommonUtils.ApiUtils.GetRequestFields(Request);
            //var userId = userInfo.UserId;
            //var applicationId = userInfo.ApplicationId;
            //model.UserId = userId.ToString();
            return _hanlder.ExecuteRegisterCommand(model);
        }
        #endregion
    }
}