﻿using Business;
using CommonUtils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Admin.API.Controllers
{
    /// <summary>
    /// JWT cho hệ thống
    /// </summary>
   [ApiVersion("1.0")]
   [ApiController]
   [Route("api/v-1/jwt/token")]
   //[ApiExplorerSettings(GroupName = "01: System - JWT")]
   public class JwtController : ControllerBase
   {
       private readonly IConfiguration _config;
       private readonly IUserHandler _userHandler;
       private readonly IApplicationHandler _applicationHandler;

      //  public AdminTokenController(IConfiguration config, IUserHandler userHandler, IApplicationHandler applicationHandler)
       public JwtController(IConfiguration config, IUserHandler userHandler, IApplicationHandler applicationHandler)
       {
           _config = config;
           _userHandler = userHandler;
           _applicationHandler = applicationHandler;
       }

       /// <summary>
       /// Đăng nhập và lấy kết quả JWT token
       /// </summary>
       /// <param name="login"></param>
       /// <returns></returns>
       [AllowAnonymous, HttpPost]
       public async Task<IActionResult> SignInJwt([FromBody] LoginModel login)
       {
            IActionResult response = Unauthorized();
            try
            {
                
                var accessToken = string.Empty;

                if (login.Username == _config["Authentication:AdminUser"] &&
                     login.Password == _config["Authentication:AdminPassWord"])
                {
                    return await BuildToken(UserConstants.AdministratorId, login.RememberMe);
                }
                if (login.Username == _config["Authentication:GuestUser"] && login.Password == _config["Authentication:GuestPassWord"])
                {
                    return await BuildToken(UserConstants.UserId, login.RememberMe);
                }
                var user = _userHandler.Authentication(login.Username, login.Password).Result;
                if (user.Code == Code.Success && user is ResponseObject<UserModel> userData)
                {
                    return await BuildToken(userData.Data.Id, login.RememberMe);
                }
                return response;
            }
            catch (Exception ex)
            {
                return response;
            }
       }

        private async Task<LoginResponse> BuildTokenModel(Guid userId, bool isRememberMe)
        {
            // get curren User
            var currentUser = UserCollectionOld.Instance.GetModel(userId);
            List<ApplicationModel> listApplication = ApplicationCollection.Instance.Collection.ToList();
            //List<BaseRoleModel> listRole = RoleCollection.Instance.Collection.Select(x => AutoMapperUtils.AutoMap<RoleModel, BaseRoleModel>(x)).ToList();
            //List<BaseRightModelOfUser> listRight = RightCollection.Instance.Collection.Select(x => AutoMapperUtils.AutoMap<RightModel, BaseRightModelOfUser>(x)).ToList();
            // if (currentUser.Type != UserConstants.IsSuperAdmin)
            // {
            // get data
            //var getApp = await _applicationHandler.GetAllByUserIdAsync(userId);
            //if (getApp.Code == Code.Success && getApp is ResponseObject<List<ApplicationModel>> getAppData)
            //{
            //    listApplication = getAppData.Data;
            //    var applicationModel = listApplication.FirstOrDefault();
            //    var getRoles = await _userMapRoleHandler.GetRoleMapUserAsync(userId, applicationModel.Id);
            //    var getRights = await _rightMapUserHandler.GetRightMapUserAsync(userId, applicationModel.Id);
            //    if (getRoles.Code == Code.Success && getRights.Code == Code.Success &&
            //       getRoles is ResponseObject<List<BaseRoleModel>> getRolesData &&
            //       getRights is ResponseObject<List<BaseRightModelOfUser>> getRightsData)
            //    {
            //        //listRole = getRolesData.Data.ToList();
            //        //listRight = getRightsData.Data;

            //    }
            //    else
            //    {
            //        return null;
            //    }

            //}
            //else
            //{
            //    return null;
            //}

            // }

            var iat = DateTime.Now;
            var exp = iat.AddMinutes(Convert.ToDouble(_config["Authentication:Jwt:TimeToLive"]));
            var claims = new[]
            {
                            new Claim(ClaimConstants.USER_NAME, currentUser.Email??""),
                            new Claim(ClaimConstants.FULL_NAME, currentUser.Name??""),
                            new Claim(ClaimConstants.AVATAR, currentUser.Avatar??""),
                            new Claim(ClaimConstants.USER_ID, userId.ToString()),
                            //new Claim(ClaimConstants.APP_ID, applicationModel.Id.ToString()),
                            //new Claim(ClaimConstants.APPS, JsonConvert.SerializeObject(listApplication.Select(x=>x.Code))),
                            //new Claim(ClaimConstants.ROLES, JsonConvert.SerializeObject(listRole.Select(x=>x.Code))),
                            //new Claim(ClaimConstants.RIGHTS, JsonConvert.SerializeObject(listRight.Where(x=> x.Enable).Select(x=>x.Code))),
                            new Claim(ClaimConstants.EXPIRES_AT, iat.ToUnixTime().ToString()),
                            new Claim(ClaimConstants.ISSUED_AT,  exp.ToUnixTime().ToString()),
                    };




            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Authentication:Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Authentication:Jwt:Issuer"],
                           _config["Authentication:Jwt:Issuer"],
                           claims,
                           expires: DateTime.Now.AddMinutes(Convert.ToDouble(_config["Authentication:Jwt:TimeToLive"])),
                           signingCredentials: creds);
            if (isRememberMe)
            {
                token = new JwtSecurityToken(_config["Authentication:Jwt:Issuer"],
                             _config["Authentication:Jwt:Issuer"],
                             claims,
                             expires: DateTime.Now.AddMinutes(Convert.ToDouble(99999)),
                             signingCredentials: creds);
            }
            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            return new LoginResponse()
            {
                //ApplicationId = applicationModel.Id,
                TokenString = tokenString,
                UserId = userId,
                //ApplicationModel = applicationModel,
                //ListApplication = listApplication.ToList(),
                //ListRight = listRight.ToList(),
                //ListRole = listRole.ToList(),
                //ExpiresAt = exp,
                //IssuedAt = iat
            };
        }
        private async Task<IActionResult> BuildToken(Guid userId, bool isRememberMe)
        {

            var result = await BuildTokenModel(userId, isRememberMe);
            if (result == null)
            {
                return Helper.TransformData(new Response(Code.Forbidden,
                    "Người dùng chưa được cấp quyền nào"));
            }
            else
            {
                return Helper.TransformData(new ResponseObject<LoginResponse>(result));
            }
        }

        public class LoginModel
       {
           public string Username { get; set; }
           public string Password { get; set; }
           public bool RememberMe { get; set; }
       }

       public class LoginResponse
       {
           public Guid UserId { get; set; }
         //   public BaseUserModel UserModel => UserCollection.Instance.GetModel(UserId);
           public string TokenString { get; set; }
           public DateTime TimeExpride { get; set; }
         //   public BaseApplicationModel ApplicationModel { get; set; }
           public Guid ApplicationId { get; set; }
       }
   }
}