﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using CommonUtils;
using Business;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Linq;
using Microsoft.AspNetCore.Authentication;
using System.Collections.Generic;
using ApiUtils;

namespace Admin.API.Controllers
{
    /// <summary>
    /// Tích hợp Wso2
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/wso2")]
    //[ApiExplorerSettings(GroupName = "01: System - Wso2")]
    public class AdminWso2Controller : ControllerBase
    {
        private readonly string _serviceValidate = Utils.GetConfig("Authentication:WSO2:Uri");
        private readonly string _clientId = Utils.GetConfig("Authentication:WSO2:Clientid");
        private readonly string _clientSecret = Utils.GetConfig("Authentication:WSO2:Secret");
        private string _redirectUri = Utils.GetConfig("Authentication:WSO2:Redirecturi");

        private readonly string _key = Utils.GetConfig("Authentication:Jwt:Key");
        private readonly string _timeToLive = Utils.GetConfig("Authentication:Jwt:TimeToLive");
        private readonly string _issuer = Utils.GetConfig("Authentication:Jwt:Issuer");

        private readonly IUserHandler _userHandler;
        //private readonly ILogHandler _logHandler;

        public AdminWso2Controller(IUserHandler userHandler)
        {
            _userHandler = userHandler;
            //_logHandler = logHandler;
        }

        //CmsSiteHandler siteHandler = new CmsSiteHandler();
        //SystemUserHandler userHandler = new SystemUserHandler();

        /// <summary>
        /// API lấy về các thông tin xác thực lấy từ Identity Server theo Authentication Code
        /// </summary>
        /// <param name="authorizationCode"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("{authorizationCode}/authenticationinfo")]
        [ProducesResponseType(typeof(ResponseObject<Wso2Result>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAuthenticationInfoByCode(string authorizationCode)
        {

            var refreshTokenUri = new Uri(_serviceValidate + "oauth2/token")
            .AddQuery("grant_type", "authorization_code")
            .AddQuery("redirect_uri", _redirectUri)
            .AddQuery("code", authorizationCode);

            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            var webRequest = (HttpWebRequest)WebRequest.Create(refreshTokenUri);
            webRequest.Method = "POST";
            var encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(_clientId + ":" + _clientSecret));
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Accept = "application/json, text/javascript, */*";
            webRequest.Headers.Add("Authorization", "Basic " + encoded);

            bool isValid = Guid.TryParse(authorizationCode, out _);

            if (isValid)
            {
                try
                {
                    Console.WriteLine("GetUserInfoByAccessToken: " + refreshTokenUri);
                    using (var jsonResponse = await webRequest.GetResponseAsync())
                    {
                        var jsonStream = jsonResponse.GetResponseStream();

                        var ms = new MemoryStream();
                        jsonStream?.CopyTo(ms);
                        ms.Position = 0;
                        var response = new HttpResponseMessage(HttpStatusCode.OK)
                        {
                            Content = new StreamContent(ms)
                        };
                        response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                        var responseBody = await response.Content.ReadAsStringAsync();
                        var wso2Result = JsonConvert.DeserializeObject<Wso2Result>(responseBody);

                        // Caculate expires time
                        var dt = DateTime.Now;
                        wso2Result.start_time = dt.ToString(CultureInfo.InvariantCulture);
                        wso2Result.expires_time = dt.AddSeconds(wso2Result.expires_in).ToString(CultureInfo.InvariantCulture);
                        // Get user info
                        if (wso2Result.access_token != null)
                        {
                            var getUserInfo = await GetUserInfoByAccessToken(wso2Result.access_token);



                            if (getUserInfo.Code == Code.Success)
                            {
                                if (getUserInfo is ResponseObject<Wso2UserInfo> getUserInfoData)
                                {
                                    wso2Result.UserInfo = getUserInfoData.Data;
                                    var act = wso2Result.access_token;
                                    wso2Result.access_token = BuildToken(new XUser()
                                    {
                                        UserId = new Guid(wso2Result.UserInfo.user_id),
                                        ApplicationId = new Guid(wso2Result.UserInfo.site_id),
                                        AccessToken = act,
                                        RefreshToken = wso2Result.refresh_token
                                    }, wso2Result.expires_in);
                                }
                            }
                        }
                        var result = new ResponseObject<Wso2Result>(wso2Result);
                        return Helper.TransformData(result);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    var result = new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message, null);
                    return Helper.TransformData(result);
                }
            }
            else
            {
                try
                {
                    string authHeader = Request.Headers["Authorization"];

                    string accessToken = Guid.NewGuid().ToString();
                    // Get the token
                    var token = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                    accessToken = token;


                    var pertokenstring = accessToken.ToString().Trim();

                    #region check jwt
                    var atoken = "";
                    // validatetoken
                    var handerJwt = new JwtSecurityTokenHandler();
                    var tokenInfo = handerJwt.ReadJwtToken(pertokenstring);

                    var issuer = Utils.GetConfig("Authentication:Jwt:Issuer");
                    var key = Utils.GetConfig("Authentication:Jwt:Key");
                    #endregion
                    SecurityToken validatedToken;
                    handerJwt.ValidateToken(pertokenstring, new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = issuer,
                        ValidAudience = issuer,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key))
                    }, out validatedToken);
                    if (validatedToken != null
                    && validatedToken.Issuer == issuer
                    && validatedToken.ValidFrom.CompareTo(DateTime.Now) != 1
                    && validatedToken.ValidTo.CompareTo(DateTime.Now) != -1)
                        {
                            var claimsJwt = new List<Claim>();
                            atoken = tokenInfo.Claims.Where(x => x.Type == "ACCESS_TOKEN").FirstOrDefault().Value;
                            var userId = tokenInfo.Claims.Where(x => x.Type == "USER_ID").FirstOrDefault().Value;
                            var refreshToken = tokenInfo.Claims.Where(x => x.Type == "REFRESH_TOKEN").FirstOrDefault()?.Value;
                            var wso2Result = new Wso2Result();
                            wso2Result.access_token = atoken;

                            var dt = DateTime.Now;

                            wso2Result.expires_in = int.Parse(_timeToLive);
                            wso2Result.start_time = dt.ToString(CultureInfo.InvariantCulture);
                            wso2Result.expires_time = dt.AddMinutes(wso2Result.expires_in).ToString(CultureInfo.InvariantCulture);
                            wso2Result.refresh_token = refreshToken;

                            var getUserInfo = await GetUserInfoByUserId(new Guid(userId));


                            if (getUserInfo.Code == Code.Success)
                            {
                                if (getUserInfo is ResponseObject<Wso2UserInfo> getUserInfoData)
                                {
                                    wso2Result.UserInfo = getUserInfoData.Data;
                                    var act = wso2Result.access_token;
                                    wso2Result.access_token = BuildToken(new XUser()
                                    {
                                        UserId = new Guid(wso2Result.UserInfo.user_id),
                                        ApplicationId = new Guid(wso2Result.UserInfo.site_id),
                                        AccessToken = act,
                                        RefreshToken = refreshToken
                                    }, wso2Result.expires_in * 60);
                                }
                            }
                            var result = new ResponseObject<Wso2Result>(wso2Result);
                            return Helper.TransformData(result);
                        }
                    else
                    {
                        var result = new ResponseError(Code.ServerError, "Hết phiên: ", null);
                        return Helper.TransformData(result);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    var result = new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message, null);
                    return Helper.TransformData(result);
                }
                
            }
        }

        /// <summary>
        /// API làm mới access token dựa vào refresh token
        /// </summary>
        /// <param name="token"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("{token}/refreshtoken")]
        [ProducesResponseType(typeof(ResponseObject<Wso2Result>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetNewAccessTokenByRefreshToken(string token)
        {
            string authHeader = Request.Headers["Authorization"];
            var requestField = Helper.GetRequestInfo(Request, HttpContext.User);

            if (!string.IsNullOrEmpty(authHeader))
            {
                // Get the token
                var atoken = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();

                var handerJwt = new JwtSecurityTokenHandler();
                var tokenInfo = handerJwt.ReadJwtToken(atoken);

                requestField.UserId = new Guid(tokenInfo.Claims.Where(x => x.Type == "USER_ID").FirstOrDefault().Value);
                requestField.ApplicationId = new Guid(tokenInfo.Claims.Where(x => x.Type == "APPLICATION_ID").FirstOrDefault().Value);
            }


            var refreshTokenUri = new Uri(_serviceValidate + "oauth2/token")
            .AddQuery("grant_type", "refresh_token")
            .AddQuery("refresh_token", token);
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            var webRequest = (HttpWebRequest)WebRequest.Create(refreshTokenUri);
            webRequest.Method = "POST";
            var encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(_clientId + ":" + _clientSecret));
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Accept = "application/json, text/javascript, */*";
            webRequest.Headers.Add("Authorization", "Basic " + encoded);
            // webRequest.Headers.Add("Authorization", "Bearer " + token);
            try
            {
                using (var jsonResponse = await webRequest.GetResponseAsync())
                {
                    var jsonStream = jsonResponse.GetResponseStream();

                    var ms = new MemoryStream();
                    jsonStream?.CopyTo(ms);
                    ms.Position = 0;
                    var response = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StreamContent(ms)
                    };
                    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    var responseBody = await response.Content.ReadAsStringAsync();
                    var wso2Result = JsonConvert.DeserializeObject<Wso2Result>(responseBody);

                    // Caculate expires time
                    var dt = DateTime.Now;
                    wso2Result.start_time = dt.ToString(CultureInfo.InvariantCulture);
                    wso2Result.expires_time = dt.AddSeconds(wso2Result.expires_in).ToString(CultureInfo.InvariantCulture);
                    wso2Result.expires_in = int.Parse(_timeToLive);
                    wso2Result.expires_time = dt.AddMinutes(wso2Result.expires_in).ToString(CultureInfo.InvariantCulture);

                    var act = wso2Result.access_token;
                    wso2Result.access_token = BuildToken(new XUser()
                    {
                        AccessToken = act,
                        UserId = requestField.UserId,
                        ApplicationId = requestField.ApplicationId,
                        RefreshToken = wso2Result.refresh_token
                    }, wso2Result.expires_in * 60);

                    var result = new ResponseObject<Wso2Result>(wso2Result);
                    return Helper.TransformData(result);
                }
            }
            catch (Exception ex)
            {

                Serilog.Log.Error(ex, "");
                var result = new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message, null);
                return Helper.TransformData(result);
            }
        }

        /// <summary>
        /// API làm mới access token dựa vào refresh token
        /// </summary>
        /// <param name="token"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("logout")]
        [ProducesResponseType(typeof(ResponseObject<Wso2Result>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Logout()
        {
            try
            {
                var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
                //var result = await _logHandler.CreateAsync(new LogCreateModel()
                //{
                //    ActionByUserId = requestInfo.UserId.ToString(),
                //    ApplicationId = requestInfo.ApplicationId,
                //    Content = "Đăng xuất",
                //    Date = DateTime.Now,
                //    Exception = "Success",
                //    Level = "1",
                //    Logger = "Đăng xuất",
                //    ModuleId = Guid.Empty,
                //    ObjectId = Guid.Empty,
                //    ObjectType = "LOGOUT",
                //    Thread = null,
                //    UserId = requestInfo.UserId,
                //    UserName = UserCollection.Instance.GetModel(requestInfo.UserId).UserName
                //});
                return Ok();
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                var result = new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message, null);
                return Helper.TransformData(result);
            }
        }

        /// <summary>
        /// API lấy claim user dựa vào access token
        /// </summary>
        /// <param name="token"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("{token}")]
        [ProducesResponseType(typeof(ResponseObject<Wso2UserInfo>), StatusCodes.Status200OK)]
        public async Task<Response> GetUserInfoByAccessToken(string token)
        {

            var userInfoUri = new Uri(_serviceValidate + "oauth2/userinfo")
            .AddQuery("schema", "openid");
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            var webRequest = (HttpWebRequest)WebRequest.Create(userInfoUri);

            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Accept = "application/json, text/javascript, */*";
            webRequest.Headers.Add("Authorization", "Bearer " + token);
            try
            {
                using (var jsonResponse = await webRequest.GetResponseAsync())
                {
                    var jsonStream = jsonResponse.GetResponseStream();

                    var ms = new MemoryStream();
                    jsonStream?.CopyTo(ms);
                    ms.Position = 0;
                    var response = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StreamContent(ms)
                    };
                    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    var responseBody = await response.Content.ReadAsStringAsync();

                    var wso2Result = JsonConvert.DeserializeObject<Wso2UserInfo>(responseBody);

                    wso2Result.user_name = wso2Result.sub;

                    // Get UserId
                    wso2Result.user_id = Guid.Empty.ToString();
                    if (!string.IsNullOrEmpty(wso2Result.sub))
                    {

                        // UserCollection.Instance.LoadToHashSet();

                        // OrganizationCollection.Instance.LoadToHashSet();
                        try
                        {
                            var object1 = wso2Result.sub.Split("/")[1].Split("@")[0];
                            wso2Result.sub = object1;
                        }
                        catch (Exception)
                        {
                            wso2Result.sub = wso2Result.sub;
                        }


                        var userExist = ApiUtils.UserCollection.Instance.GetUserByUserName(wso2Result.sub);
                        if (userExist != null)
                        {
                            wso2Result.user_id = userExist.Id.ToString() != Guid.Empty.ToString() ? userExist.Id.ToString() : userExist.UserId.ToString();
                        }

                        // Check access site and active status
                        if (wso2Result.user_id != Guid.Empty.ToString())
                        {
                            wso2Result.IsAccessible = false;
                            wso2Result.IsActiveAndExist = false;
                            var userGuidId = new Guid(wso2Result.user_id);
                            //0. Check user active and exist
                            // var userType = 0; //(type=0 => Internal user)
                            var userStatus = ApiUtils.UserCollection.Instance.CheckUserActiveV2(wso2Result.sub);
                            if (userStatus)
                            {
                                wso2Result.IsActiveAndExist = true;
                                wso2Result.display_name = userExist.FullName;
                                //1. Check site access
                                wso2Result.site_id = userExist.ApplicationId.ToString();
                                wso2Result.site_name = ApplicationCollection.Instance.GetModel(new Guid(userExist.ApplicationId)).ApplicationName;
                                wso2Result.IsAccessible = true;

                                wso2Result.Properties = new UserModel()
                                {
                                    UserRights = new RoleAndRightOfUser()
                                    {
                                        Roles = AutoMapperUtils.AutoMap<SystemRoleModel, RoleOfUser>(userExist.Roles),
                                        FreeRights = AutoMapperUtils.AutoMap<SystemRightModel, RightsOfUser>(userExist.Rights)
                                    },
                                    Avatar = userExist.Avatar,
                                    OrganizationId = new Guid(userExist.OrganizationId),
                                    OrganizationName = userExist.OrganizationName,

                                };
                                if (userExist.UserOrganization != null)
                                {
                                    wso2Result.Properties.UserOrganization = AutoMapperUtils.AutoMap<UserOrganization, UsersInOrganizationModel>(userExist.UserOrganization);
                                }

                                var marktime = DateTime.Now;
                                //var RolesRights = await _userHandler.GetRolesAndRightsOfuser(userGuidId) as ResponseObject<RoleAndRightOfUser>;
                                Serilog.Log.Information("GetRolesAndRightsOfuser time: " + (DateTime.Now - marktime).TotalMilliseconds);
                                Console.WriteLine("GetRolesAndRightsOfuser time: " + (DateTime.Now - marktime).TotalMilliseconds);
                                // var roles = RolesRights.Data.Roles;
                                // var rights = RolesRights.Data.FreeRights;

                                //wso2Result.Properties.UserRights = RolesRights.Data;

                                var jwtHandler = new JWTHelper();
                                var permissionString = jwtHandler.BuildToken(wso2Result.Properties);
                                Serilog.Log.Information("BuildToken time: " + (DateTime.Now - marktime).TotalMilliseconds);
                                Console.WriteLine("BuildToken time: " + (DateTime.Now - marktime).TotalMilliseconds);
                                wso2Result.PermissionToken = permissionString;

                                //    var siteAccessDefault = siteHandler.GetDefaultSite(userGuidId);
                                //    if (siteAccessDefault.Status == 1)
                                //    {
                                //        wso2Result.site_id = siteAccessDefault.Data.SiteId.ToString();
                                //        wso2Result.site_name = siteAccessDefault.Data.SiteName;
                                //        wso2Result.IsAccessible = true;
                                //    }
                                //    else
                                //    {
                                //        var siteAccess = siteHandler.GetAccessibleSites(userGuidId);
                                //        if (siteAccess.Status == 1)
                                //        {
                                //            var siteDefault = siteAccess.Data[0];
                                //            wso2Result.site_id = siteAccessDefault.Data.SiteId.ToString();
                                //            wso2Result.site_name = siteAccessDefault.Data.SiteName;
                                //            wso2Result.IsAccessible = true;
                                //        }
                                //    }
                                //await _logHandler.CreateAsync(new LogCreateModel()
                                //{
                                //    ActionByUserId = wso2Result.user_id,
                                //    ApplicationId = userExist.ApplicationId,
                                //    Content = "Đăng nhập",
                                //    Date = DateTime.Now,
                                //    Exception = "Success",
                                //    Level = "1",
                                //    Logger = "Đăng nhập",
                                //    ModuleId = Guid.Empty,
                                //    ObjectId = Guid.Empty,
                                //    ObjectType = "LOGIN",
                                //    Thread = null,
                                //    UserId = new Guid(wso2Result.user_id),
                                //    UserName = userExist.UserName
                                //});

                            }
                            else
                            {
                                return new ResponseError(Code.BadRequest, "Tài khoản đã bị khóa: ", null);
                            }
                        }
                    }

                    var result = new ResponseObject<Wso2UserInfo>(wso2Result);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Serilog.Log.Error(ex, "");
                var result = new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message, null);
                return result;
            }
        }

        private async Task<Response> GetUserInfoByUserId(Guid userId)
        {
            try
            {
                var wso2Result = new Wso2UserInfo();
                wso2Result.user_id = userId.ToString();
                var userExist = UserCollection.Instance.GetUserById(userId);
                wso2Result.sub = userExist.UserName;
                // Check access site and active status
                if (wso2Result.user_id != Guid.Empty.ToString())
                {
                    wso2Result.IsAccessible = false;
                    wso2Result.IsActiveAndExist = false;
                    var userGuidId = new Guid(wso2Result.user_id);
                    //0. Check user active and exist
                    // var userType = 0; //(type=0 => Internal user)
                    var userStatus = ApiUtils.UserCollection.Instance.CheckUserActiveV2(wso2Result.sub);
                    if (userStatus)
                    {
                        wso2Result.IsActiveAndExist = true;
                        wso2Result.display_name = userExist.FullName;
                        //1. Check site access
                        wso2Result.site_id = userExist.ApplicationId.ToString();
                        wso2Result.site_name = ApplicationCollection.Instance.GetModel(new Guid(userExist.ApplicationId)).ApplicationName;
                        wso2Result.IsAccessible = true;

                        wso2Result.Properties = new UserModel()
                        {
                            UserRights = new RoleAndRightOfUser()
                            {
                                Roles = AutoMapperUtils.AutoMap<SystemRoleModel, RoleOfUser>(userExist.Roles),
                                FreeRights = AutoMapperUtils.AutoMap<SystemRightModel, RightsOfUser>(userExist.Rights)
                            },
                            Avatar = userExist.Avatar,
                            OrganizationId = new Guid(userExist.OrganizationId),
                            OrganizationName = userExist.OrganizationName,

                        };
                        if (userExist.UserOrganization != null)
                        {
                            wso2Result.Properties.UserOrganization = AutoMapperUtils.AutoMap<UserOrganization, UsersInOrganizationModel>(userExist.UserOrganization);
                        }

                        var marktime = DateTime.Now;
                        //var RolesRights = await _userHandler.GetRolesAndRightsOfuser(userGuidId) as ResponseObject<RoleAndRightOfUser>;
                        Serilog.Log.Information("GetRolesAndRightsOfuser time: " + (DateTime.Now - marktime).TotalMilliseconds);
                        Console.WriteLine("GetRolesAndRightsOfuser time: " + (DateTime.Now - marktime).TotalMilliseconds);
                        // var roles = RolesRights.Data.Roles;
                        // var rights = RolesRights.Data.FreeRights;

                        //wso2Result.Properties.UserRights = RolesRights.Data;

                        //var jwtHandler = new JWTHelper();
                        //if (wso2Result.Properties.UserRights.Roles)
                        //{

                        //}
                        //var permissionString = jwtHandler.BuildToken(wso2Result.Properties);
                        Serilog.Log.Information("BuildToken time: " + (DateTime.Now - marktime).TotalMilliseconds);
                        Console.WriteLine("BuildToken time: " + (DateTime.Now - marktime).TotalMilliseconds);
                        wso2Result.PermissionToken = "";

                    }
                    else
                    {
                        return new ResponseError(Code.BadRequest, "Tài khoản đã bị khóa: ", null);
                    }
                }

                var result = new ResponseObject<Wso2UserInfo>(wso2Result);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Serilog.Log.Error(ex, "");
                var result = new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message, null);
                return result;
            }
        }

        private string BuildToken(XUser user, int expires_in)
        {
            var claims = new[]
            {
                new Claim("REFRESH_TOKEN", user.RefreshToken),
               new Claim("ACCESS_TOKEN", user.AccessToken),
               new Claim("USER_ID", user.UserId.ToString()),
               new Claim("APPLICATION_ID", user.ApplicationId.ToString()),
               new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
           };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var eTime = DateTime.Now;
            var m = Convert.ToDouble(expires_in / 60);
            eTime = eTime.AddMinutes(m);

            // Serilog.Log.Information("Mobile note BuildToken " + eTime.ToString("HH:mm:ss dd/MM/yyy"), "");

            var token = new JwtSecurityToken(_issuer,
               _issuer,
               claims,
               expires: eTime.AddHours(7),
               signingCredentials: creds);

            // Serilog.Log.Information("Mobile note BuildToken +7 " + eTime.ToString("HH:mm:ss dd/MM/yyy"), "");

            return new JwtSecurityTokenHandler().WriteToken(token);

        }
    }

    public class Wso2Result
    {
#pragma warning disable IDE1006 // Naming Styles
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public string id_token { get; set; }
        public int expires_in { get; set; }
        public string start_time { get; set; }
        public string expires_time { get; set; }
        public string token_type { get; set; }
#pragma warning restore IDE1006 // Naming Styles
        public Wso2UserInfo UserInfo { get; set; }
    }
    public class Wso2UserInfo
    {
#pragma warning disable IDE1006 // Naming Styles
        public string sub { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string display_name { get; set; }
        public string email { get; set; }
        public string site_id { get; set; }
        public string site_name { get; set; }
        public bool IsActiveAndExist { get; set; }
        public bool IsAccessible { get; set; }

        public UserModel Properties { get; set; }
        public string PermissionToken { get; set; }

#pragma warning restore IDE1006 // Naming Styles
    }
    public class XUser
    {
        public Guid UserId { get; set; }
        public Guid ApplicationId { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}



