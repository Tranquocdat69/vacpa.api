﻿using Business;
using CommonUtils;
using Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Admin.API.Controllers.Cat
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/cat/[controller]")]
    public class WorkflowStateController : ControllerBase
    {
        private readonly ILogger<WorkflowStateController> _logger;
        private readonly IWorkflowStateHandler _handler;

        public WorkflowStateController(ILogger<WorkflowStateController> logger, IWorkflowStateHandler handler)
        {
            _logger = logger;
            _handler = handler;
        }


        #region Post
        [Authorize, HttpPost, Route("")]
        public Task<Response> CreateAsync([FromBody] WorkflowStateCreateModel model)
        {
            return _handler.CreateAsync(model);
        }

        #endregion Post

        #region Get
        /// <summary>
        /// Lấy toàn bộ danh sách WFState
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet, Route("")]
        // [Authorize, HttpGet, Route("")]
        //[ProducesResponseType(typeof(Response<>), StatusCodes.Status200OK)]
        public Task<Response> GetAll()
        {
            return _handler.GetAllFromCache();
        }

        /// <summary>
        /// Lấy danh sách WFState theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("filter")]
        public Task<Response> GetByFilterASync(string filter)
        {
            var filterModel = JsonConvert.DeserializeObject<WorkflowStateQueryModel>(filter);
            return _handler.GetByFilterASync(filterModel);
        }

        /// <summary>
        /// Lấy thông tin WFState theo Id
        /// </summary>
        /// <param name="commandId">Id bản ghi</param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("{stateId}")]
        public Task<Response> GetByIdAsync(Guid stateId)
        {
            return _handler.GetByIdAsync(stateId);
        }

        #endregion Get

        #region Update
        /// <summary>
        /// Cập nhật thông tin WFState
        /// </summary>
        /// <param name="">Id bản ghi</param>
        /// <returns></returns>
        [Authorize, HttpPut, Route("")]
        public Task<Response> UpdateAsync([FromBody] WorkflowStateUpdateModel model)
        {
            return _handler.UpdateAsync(model);
        }
        #endregion Update

        #region Delete
        [Authorize, HttpDelete, Route("delete/{stateId}")]
        public Task<Response> DeleteAsync(Guid stateId)
        {
            return _handler.DeleteAsync(stateId);
        }

        [Authorize, HttpDelete, Route("delete/many")]
        public Task<Response> DeleteMany(List<Guid> ids)
        {
            return _handler.DeleteMany(ids);
        }
        #endregion Delete


    }
}
