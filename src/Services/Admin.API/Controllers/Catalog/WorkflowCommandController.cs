﻿using Business;
using CommonUtils;
using Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Admin.API.Controllers.Cat
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/cat/[controller]")]
    public class WorkflowCommandController : ControllerBase
    {
        private readonly ILogger<WorkflowCommandController> _logger;
        private readonly IWorkflowCommandHandler _handler;

        public WorkflowCommandController(ILogger<WorkflowCommandController> logger, IWorkflowCommandHandler handler)
        {
            _logger = logger;
            _handler = handler; 
        }


        #region Post
        [Authorize, HttpPost, Route("")]
        public Task<Response> CreateAsync([FromBody] WorkflowCommandCreateModel model)
        {
            return _handler.CreateAsync(model);
        }

        #endregion Post

        #region Get
        /// <summary>
        /// Lấy toàn bộ danh sách WFCommand
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet, Route("")]
        // [Authorize, HttpGet, Route("")]
        //[ProducesResponseType(typeof(Response<>), StatusCodes.Status200OK)]
        public Task<Response> GetAll()
        {
            return _handler.GetAllFromCache();
        }

        /// <summary>
        /// Lấy danh sách WFCommand theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("filter")]
        public Task<Response> GetByFilterASync(string filter)
        {
            var filterModel = JsonConvert.DeserializeObject<WorkflowCommandQueryModel>(filter);
            return _handler.GetByFilterASync(filterModel);
        }

        /// <summary>
        /// Lấy thông tin WFCommand theo Id
        /// </summary>
        /// <param name="commandId">Id bản ghi</param>
        /// <returns></returns>
        [Authorize, HttpGet, Route("{commandId}")]
        public Task<Response> GetByIdAsync(Guid commandId)
        {
            return _handler.GetByIdAsync(commandId);
        }

        #endregion Get

        #region Update
        /// <summary>
        /// Cập nhật thông tin WFCommand
        /// </summary>
        /// <param name="">Id bản ghi</param>
        /// <returns></returns>
        [Authorize, HttpPut, Route("")]
        public Task<Response> UpdateAsync([FromBody] WorkflowCommandUpdateModel model)
        {
            return _handler.UpdateAsync(model);
        }
        #endregion Update

        #region Delete
        [Authorize, HttpDelete, Route("delete/{commandId}")]
        public Task<Response> DeleteAsync(Guid commandId)
        {
            return _handler.DeleteAsync(commandId);
        }

        [Authorize, HttpDelete, Route("delete/many")]
        public Task<Response> DeleteMany(List<Guid> ids)
        {
            return _handler.DeleteMany(ids);
        }
        #endregion Delete


    }
}
