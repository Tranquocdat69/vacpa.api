namespace Business
{
    using ApiUtils;
    using Microsoft.Extensions.DependencyInjection;

    /// <summary>
    /// <see cref="IServiceCollection"/> extension methods add project services.
    /// </summary>
    /// <remarks>
    /// AddSingleton - Only one instance is ever created and returned.
    /// AddScoped - A new instance is created and returned for each request/response cycle.
    /// AddTransient - A new instance is created and returned each time.
    /// </remarks>
    public static class ProjectServiceCollectionExtensions
    {
        public static IServiceCollection AddProjectServices(this IServiceCollection services) =>
            services
        #region SYS
                //SonTH
                .AddSingleton<IApplicationHandler, ApplicationHandler>()
                .AddSingleton<IUserHandler, UserHandler>()
                .AddSingleton<IRoleHandler, RoleHandler>()
                .AddSingleton<IRightHandler, RightHandler>()
                .AddSingleton<INavigationHandler, NavigationHandler>()
                .AddSingleton<IWorkflowConfigureHandler, WorkflowConfigureHandler>()
                .AddSingleton<ILogHandler, LogHandler>()
                .AddSingleton<IBootstrapHandler, DbBootstrapHandler>()

                //HaNH
                .AddSingleton<IRightInRoleHandler, RightInRoleHandler>()
                .AddSingleton<INodeUploadHandler, NodeUploadHandler>()
                .AddSingleton<IOrganizationHandler, OrganizationHandler>()
                //HaNT
                .AddSingleton<IWorkflowCommandHandler, WorkflowCommandHandler>()
                .AddSingleton<IWorkflowStateHandler, WorkflowStateHandler>()
                .AddSingleton<IWorkflowHandler, DbWorkflowHandler>()

                //.AddSingleton<IBus, IBus>()
                .AddSingleton<ICacheService, RedisCacheService>()
        #endregion
            ;
    }
}