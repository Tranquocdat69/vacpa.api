﻿using ApiUtils;
using CommonUtils;
using DataUtils;
using Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Admin.API
{
    public class EventHandling
    {
        // private static ITieuChiDanhGiaLaoDongHandler _h;
        private static IBus _eventBus;
        public static void ConfigureEventBus(IApplicationBuilder app)
        {
            // _h = app.ApplicationServices.GetRequiredService<ITieuChiDanhGiaLaoDongHandler>();
            _eventBus = app.ApplicationServices.GetRequiredService<IBus>();

            // hứng dữ liệu cho bảng application từ soa sang msa
            _eventBus.ReceiveAsync<List<ApplicationSOAModel>>(SyncDataSOAToMSA.APPLICATION, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {
                        var appData = unit.GetRepository<IdmApplications>().GetAll().ToList();

                        foreach (var item in x)
                        {
                            var detailApp = (from s in appData where s.ApplicationId == item.ApplicationId select s).FirstOrDefault();
                            if (detailApp == null)
                            {
                                var data = new IdmApplications
                                {
                                    ApplicationId = item.ApplicationId,
                                    ApplicationName = item.ApplicationName,
                                    Description = item.ApplicationName,
                                    LoweredApplicationName = item.LoweredApplicationName
                                };
                                unit.GetRepository<IdmApplications>().Add(data);
                            }
                            else
                            {
                                detailApp.ApplicationId = item.ApplicationId;
                                detailApp.ApplicationName = item.ApplicationName;
                                detailApp.Description = item.ApplicationName;
                                detailApp.LoweredApplicationName = item.LoweredApplicationName;

                                unit.GetRepository<IdmApplications>().Update(detailApp);

                            }
                        }

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }


            });

            // hứng dữ liệu cho bảng Organization từ soa sang msa
            _eventBus.ReceiveAsync<List<OrganizationSOAModel>>(SyncDataSOAToMSA.ORGANIZATION, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {
                        var orgData = unit.GetRepository<IdmOrganizations>().GetAll().ToList();

                        foreach (var item in x)
                        {
                            var detailOrg = (from s in orgData where s.OrganizationId == item.OrganizationId select s).FirstOrDefault();
                            if (detailOrg == null)
                            {
                                var data = new IdmOrganizations
                                {
                                    ApplicationId = item.ApplicationId,
                                    OrganizationId = item.OrganizationId,
                                    OrganizationName = item.Name,

                                    ParentOrganizationId = item.ParentId,
                                    //ParentOrganizationName = item.ParentName,
                                    Status = 1,
                                    Loai = item.Loai,
                                    Type = item.Type,
                                    ThuTu = item.STT,
                                    Description = item.Description,
                                    Address = item.DiaChi,
                                    PhoneNumber = item.SoDienThoai,
                                    Fax = item.SoFax,
                                    Email = item.Email,
                                    Path = item.Path,
                                    IdPath = item.IdPath,
                                    Level = item.Level,
                                    CreatedByUserId = item.CreatedByUserID,
                                    CreatedOnDate = item.CreatedOnDate,
                                    //LastModifiedByFullName = item.LastModifiedByUserName,
                                    LastModifiedByUserId = item.LastModifiedByUserID,
                                    LastModifiedOnDate = item.LastModifiedOnDate,
                                    IsPublic = true,
                                };
                                data.Code = !String.IsNullOrEmpty(item.Code) ? item.Code : "";
                                unit.GetRepository<IdmOrganizations>().Add(data);
                            }
                            else
                            {
                                detailOrg.ApplicationId = item.ApplicationId;
                                detailOrg.OrganizationId = item.OrganizationId;
                                detailOrg.OrganizationName = item.Name;

                                detailOrg.ParentOrganizationId = item.ParentId;
                                //detailOrg.ParentOrganizationName = item.ParentName;
                                detailOrg.Status = 1;
                                detailOrg.Loai = item.Loai;
                                detailOrg.Type = item.Type;
                                detailOrg.ThuTu = item.STT;
                                detailOrg.Description = item.Description;
                                detailOrg.Address = item.DiaChi;
                                detailOrg.PhoneNumber = item.SoDienThoai;
                                detailOrg.Fax = item.SoFax;
                                detailOrg.Email = item.Email;
                                detailOrg.Path = item.Path;
                                detailOrg.IdPath = item.IdPath;
                                detailOrg.Level = item.Level;
                                detailOrg.CreatedByUserId = item.CreatedByUserID;
                                detailOrg.CreatedOnDate = item.CreatedOnDate;
                                //detailOrg.LastModifiedByFullName = item.LastModifiedByUserName;
                                detailOrg.LastModifiedByUserId = item.LastModifiedByUserID;
                                detailOrg.LastModifiedOnDate = item.LastModifiedOnDate;
                                detailOrg.IsPublic = true;

                                detailOrg.Code = !String.IsNullOrEmpty(item.Code) ? item.Code : "";
                                unit.GetRepository<IdmOrganizations>().Update(detailOrg);
                            }
                        }

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }
            });

            // hứng dữ liệu cho bảng Rights từ soa sang msa
            _eventBus.ReceiveAsync<List<RightSOAModel>>(SyncDataSOAToMSA.RIGHT, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {
                        var rightData = unit.GetRepository<IdmRight>().GetAll().ToList();

                        foreach (var item in x)
                        {
                            var detailRight = (from s in rightData where s.RightCode == item.RightCode select s).FirstOrDefault();
                            if (detailRight == null)
                            {
                                var data = new IdmRight
                                {
                                    RightCode = item.RightCode,
                                    RightName = item.RightName,
                                    Status = item.Status,
                                    Description = item.Description,
                                    GroupCode = item.GroupCode,
                                    IsGroup = item.IsGroup.Value,
                                    Level = item.Level.Value,
                                    Order = item.Order,
                                };
                                unit.GetRepository<IdmRight>().Add(data);
                            }
                            else
                            {
                                detailRight.RightCode = item.RightCode;
                                detailRight.RightName = item.RightName;
                                detailRight.Status = item.Status;
                                detailRight.Description = item.Description;
                                detailRight.GroupCode = item.GroupCode;
                                detailRight.IsGroup = item.IsGroup.Value;
                                detailRight.Level = item.Level.Value;
                                detailRight.Order = item.Order;


                                unit.GetRepository<IdmRight>().Update(detailRight);
                            }
                        }

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }
            });

            // hứng dữ liệu cho bảng Role từ soa sang msa
            _eventBus.ReceiveAsync<List<RoleSOAModel>>(SyncDataSOAToMSA.ROLE, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {
                        var roleData = unit.GetRepository<IdmRoles>().GetAll().ToList();

                        foreach (var item in x)
                        {
                            var detailRoles = (from s in roleData where s.RoleId == item.RoleId select s).FirstOrDefault();
                            if (detailRoles == null)
                            {
                                var data = new IdmRoles
                                {
                                    ApplicationId = item.ApplicationId,
                                    RoleId = item.RoleId,
                                    RoleCode = item.RoleCode,
                                    RoleName = item.RoleName,
                                    LoweredRoleName = item.RoleName.ToLower(),
                                    Description = item.Description,

                                    CreatedByUserId = item.CreatedByUserId,
                                    CreatedOnDate = item.CreatedOnDate,
                                    LastModifiedByUserId = item.LastModifiedByUserId,
                                    LastModifiedOnDate = item.LastModifiedOnDate,
                                };
                                unit.GetRepository<IdmRoles>().Add(data);
                            }
                            else
                            {
                                detailRoles.ApplicationId = item.ApplicationId;
                                detailRoles.RoleId = item.RoleId;
                                detailRoles.RoleCode = item.RoleCode;
                                detailRoles.RoleName = item.RoleName;
                                detailRoles.LoweredRoleName = item.RoleName.ToLower();
                                detailRoles.Description = item.Description;

                                detailRoles.CreatedByUserId = item.CreatedByUserId;
                                detailRoles.CreatedOnDate = item.CreatedOnDate;
                                detailRoles.LastModifiedByUserId = item.LastModifiedByUserId;
                                detailRoles.LastModifiedOnDate = item.LastModifiedOnDate;

                                unit.GetRepository<IdmRoles>().Update(detailRoles);
                            }
                        }

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }
            });

            // hứng dữ liệu cho bảng Right In Role từ soa sang msa
            _eventBus.ReceiveAsync<List<RightInRoleSOAModel>>(SyncDataSOAToMSA.RIGHTINROLE, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {
                        var rightInRoleData = unit.GetRepository<IdmRightsInRole>().GetAll().ToList();

                        foreach (var item in x)
                        {
                            var detailRIR = (from s in rightInRoleData where s.Id == item.Id select s).FirstOrDefault();
                            if (detailRIR == null)
                            {
                                var data = new IdmRightsInRole
                                {
                                    Id = item.Id,
                                    RoleId = item.RoleId,
                                    RightCode = item.RightCode,
                                    CreatedDate = item.CreatedDate,
                                    ModifiedDate = item.ModifiedDate,
                                };
                                unit.GetRepository<IdmRightsInRole>().Add(data);
                            }
                            else
                            {
                                detailRIR.Id = item.Id;
                                detailRIR.RoleId = item.RoleId;
                                detailRIR.RightCode = item.RightCode;
                                detailRIR.CreatedDate = item.CreatedDate;
                                detailRIR.ModifiedDate = item.ModifiedDate;

                                unit.GetRepository<IdmRightsInRole>().Update(detailRIR);
                            }
                        }

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }
            });

            // hứng dữ liệu cho bảng User từ soa sang msa
            _eventBus.ReceiveAsync<List<UserSOAModel>>(SyncDataSOAToMSA.USER, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {

                        var userData = unit.GetRepository<IdmUsers>().GetAll().ToList();

                        foreach (var item in x)
                        {
                            var detailUser = (from s in userData where s.UserId == item.UserId select s).FirstOrDefault();
                            if (detailUser == null)
                            {
                                var data = new IdmUsers
                                {
                                    UserId = item.UserId,
                                    ApplicationId = item.ApplicationId,
                                    UserName = item.UserName,
                                    NickName = item.NickName,
                                    MobilePin = item.MobilePin,
                                    Email = item.Email,
                                    Type = item.Type,
                                    FullName = item.FullName,
                                    LoweredUserName = item.LoweredUserName,
                                    Password = item.Password,
                                    PasswordFormat = item.PasswordFormat,
                                    PasswordSalt = item.PasswordSalt,
                                    IsLockedOut = item.IsLockedOut,
                                    MobileAlias = item.MobileAlias,
                                    IsAnonymous = item.IsAnonymous,
                                    LastActivityDate = item.LastActivityDate,
                                    CharacterManagerId = item.CharacterManagerId,
                                    Birthday = item.Birthday,
                                    CreatedByUserId = item.CreatedByUserId,
                                    CreatedOnDate = item.CreatedOnDate,
                                    LastModifiedByUserId = item.LastModifiedByUserId,
                                    LastModifiedOnDate = item.LastModifiedOnDate,
                                    Address = item.Address,
                                    Avatar = item.Avatar,
                                    FinanceCode = item.FinanceCode,
                                };
                                data.SchemeName = !String.IsNullOrEmpty(item.SchemeName) ? item.SchemeName : "";
                                unit.GetRepository<IdmUsers>().Add(data);
                            }
                            else
                            {
                                detailUser.UserId = item.UserId;
                                detailUser.ApplicationId = item.ApplicationId;
                                detailUser.UserName = item.UserName;
                                detailUser.NickName = item.NickName;
                                detailUser.MobilePin = item.MobilePin;
                                detailUser.Email = item.Email;
                                detailUser.Type = item.Type;
                                detailUser.FullName = item.FullName;
                                detailUser.LoweredUserName = item.LoweredUserName;
                                detailUser.Password = item.Password;
                                detailUser.PasswordFormat = item.PasswordFormat;
                                detailUser.PasswordSalt = item.PasswordSalt;
                                detailUser.IsLockedOut = item.IsLockedOut;
                                detailUser.MobileAlias = item.MobileAlias;
                                detailUser.IsAnonymous = item.IsAnonymous;
                                detailUser.LastActivityDate = item.LastActivityDate;
                                detailUser.CharacterManagerId = item.CharacterManagerId;
                                detailUser.Birthday = item.Birthday;
                                detailUser.CreatedByUserId = item.CreatedByUserId;
                                detailUser.CreatedOnDate = item.CreatedOnDate;
                                detailUser.LastModifiedByUserId = item.LastModifiedByUserId;
                                detailUser.LastModifiedOnDate = item.LastModifiedOnDate;
                                detailUser.Address = item.Address;
                                detailUser.Avatar = item.Avatar;
                                detailUser.FinanceCode = item.FinanceCode;

                                detailUser.SchemeName = !String.IsNullOrEmpty(item.SchemeName) ? item.SchemeName : "";
                                unit.GetRepository<IdmUsers>().Update(detailUser);
                            }
                           
                        }

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }
            });

            // hứng dữ liệu cho bảng Right Of User từ soa sang msa
            _eventBus.ReceiveAsync<List<RightOfUserSOAModel>>(SyncDataSOAToMSA.RIGHTOFUSER, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {

                        var rouData = unit.GetRepository<IdmRightsOfUser>().GetAll().ToList();

                        foreach (var item in x)
                        {
                            var detailROU = (from s in rouData where s.Id == item.Id select s).FirstOrDefault();
                            if (detailROU == null)
                            {
                                var data = new IdmRightsOfUser
                                {
                                    Id = item.Id,
                                    UserId = item.UserId,
                                    InheritedFromRoles = item.InheritedFromRoles,
                                    Inherited = item.Inherited,
                                    CreatedDate = item.CreatedDate,
                                    ModifiedDate = item.ModifiedDate,
                                    Enable = item.Enable,
                                    ApplicationId = item.ApplicationId,

                                };
                                data.RightCode = !String.IsNullOrEmpty(item.RightCode) ? item.RightCode : " ";
                                unit.GetRepository<IdmRightsOfUser>().Add(data);
                            }
                            else
                            {
                                detailROU.Id = item.Id;
                                detailROU.UserId = item.UserId;
                                detailROU.RightCode = !String.IsNullOrEmpty(item.RightCode) ? item.RightCode : " ";
                                detailROU.InheritedFromRoles = item.InheritedFromRoles;
                                detailROU.Inherited = item.Inherited;
                                detailROU.CreatedDate = item.CreatedDate;
                                detailROU.ModifiedDate = item.ModifiedDate;
                                detailROU.Enable = item.Enable;
                                detailROU.ApplicationId = item.ApplicationId;

                                unit.GetRepository<IdmRightsOfUser>().Update(detailROU);
                            }

                        }

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }
            });

            // hứng dữ liệu cho bảng User In Organization từ soa sang msa
            _eventBus.ReceiveAsync<List<UserInOrganizationSOAModel>>(SyncDataSOAToMSA.USERINORGANIZATION, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {

                        var uioData = unit.GetRepository<IdmUsersInOrganization>().GetAll().ToList();

                        var orgData = unit.GetRepository<IdmOrganizations>().GetAll().ToList();

                        foreach (var item in x)
                        {
                            var detailUIO = (from s in uioData where s.Id == item.Id select s).FirstOrDefault();
                            if (detailUIO == null)
                            {
                                var data = new IdmUsersInOrganization
                                {
                                    Id = item.Id,
                                    UserId = item.UserId,
                                    OrganizationId = item.OrganizationId,
                                    CreatedByUserId = item.CreatedByUserId,
                                    CreatedOnDate = item.CreatedOnDate,
                                    LastModifiedByUserId = item.LastModifiedByUserId,
                                    LastModifiedOnDate = item.LastModifiedOnDate,
                                    Role = item.Role,
                                    OrganizationName = (from s in orgData where s.OrganizationId == item.OrganizationId select s).FirstOrDefault().OrganizationName,
                                };

                                unit.GetRepository<IdmUsersInOrganization>().Add(data);
                            }
                            else
                            {
                                detailUIO.Id = item.Id;
                                detailUIO.UserId = item.UserId;
                                detailUIO.OrganizationId = item.OrganizationId;
                                detailUIO.CreatedByUserId = item.CreatedByUserId;
                                detailUIO.CreatedOnDate = item.CreatedOnDate;
                                detailUIO.LastModifiedByUserId = item.LastModifiedByUserId;
                                detailUIO.LastModifiedOnDate = item.LastModifiedOnDate;
                                detailUIO.Role = item.Role;
                                detailUIO.OrganizationName = (from s in orgData where s.OrganizationId == item.OrganizationId select s).FirstOrDefault().OrganizationName;

                                unit.GetRepository<IdmUsersInOrganization>().Update(detailUIO);
                            }

                        }

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }
            });

            // hứng dữ liệu cho bảng User In Role từ soa sang msa
            _eventBus.ReceiveAsync<List<UserInRoleSOAModel>>(SyncDataSOAToMSA.USERINROLE, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {

                        var uirData = unit.GetRepository<IdmUsersInRoles>().GetAll().ToList();

                        foreach (var item in x)
                        {
                            var detailUIR = (from s in uirData where s.UserId == item.UserId && s.RoleId == item.RoleId select s).FirstOrDefault();
                            if (detailUIR == null)
                            {
                                var data = new IdmUsersInRoles
                                {
                                    Id = Guid.NewGuid(),
                                    UserId = item.UserId,
                                    RoleId = item.RoleId,
                                    CreateDate = item.CreateDate,
                                    DeleteDate = item.DeleteDate,
                                    ApplicationId = item.ApplicationId,
                                };

                                unit.GetRepository<IdmUsersInRoles>().Add(data);
                            }
                            else
                            {
                                detailUIR.UserId = item.UserId;
                                detailUIR.RoleId = item.RoleId;
                                detailUIR.CreateDate = item.CreateDate;
                                detailUIR.DeleteDate = item.DeleteDate;
                                detailUIR.ApplicationId = item.ApplicationId;

                                unit.GetRepository<IdmUsersInRoles>().Update(detailUIR);
                            }

                        }

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }
            });

            // hứng dữ liệu cho bảng Navigation từ soa sang msa
            _eventBus.ReceiveAsync<List<NavigationSOAModel>>(SyncDataSOAToMSA.NAVIGATION, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {

                        var navData = unit.GetRepository<Navigation>().GetAll().ToList();

                        foreach (var item in x)
                        {
                            var detailNAV = (from s in navData where s.NavigationId == item.NavigationId select s).FirstOrDefault();
                            if (detailNAV == null)
                            {
                                var data = new Navigation
                                {
                                    NavigationId = item.NavigationId,
                                    ParentId = item.ParentId,
                                    ApplicationId = item.ApplicationId,
                                    Code = item.Code,
                                    Name = item.Name,
                                    Status = item.Status.Value,
                                    Order = item.Order,
                                    HasChild = item.HasChild,
                                    UrlRewrite = item.UrlRewrite,
                                    IconClass = item.IconClass,
                                    NavigationNameEn = item.NavigationNameEn,
                                    CreatedByUserId = item.CreatedByUserId,
                                    CreatedOnDate = item.CreatedOnDate,
                                    LastModifiedByUserId = item.LastModifiedByUserId,
                                    LastModifiedOnDate = item.LastModifiedOnDate,
                                    IdPath = item.IdPath,
                                    Path = item.Path,
                                    Level = item.Level,
                                    SubUrl = item.SubUrl,
                                };

                                unit.GetRepository<Navigation>().Add(data);
                            }
                            else
                            {
                                detailNAV.NavigationId = item.NavigationId;
                                detailNAV.ParentId = item.ParentId;
                                detailNAV.ApplicationId = item.ApplicationId;
                                detailNAV.Code = item.Code;
                                detailNAV.Name = item.Name;
                                detailNAV.Status = item.Status.Value;
                                detailNAV.Order = item.Order;
                                detailNAV.HasChild = item.HasChild;
                                detailNAV.UrlRewrite = item.UrlRewrite;
                                detailNAV.IconClass = item.IconClass;
                                detailNAV.NavigationNameEn = item.NavigationNameEn;
                                detailNAV.CreatedByUserId = item.CreatedByUserId;
                                detailNAV.CreatedOnDate = item.CreatedOnDate;
                                detailNAV.LastModifiedByUserId = item.LastModifiedByUserId;
                                detailNAV.LastModifiedOnDate = item.LastModifiedOnDate;
                                detailNAV.IdPath = item.IdPath;
                                detailNAV.Path = item.Path;
                                detailNAV.Level = item.Level;
                                detailNAV.SubUrl = item.SubUrl;

                                unit.GetRepository<Navigation>().Update(detailNAV);
                            }

                        }

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }
            });

            // hứng dữ liệu cho bảng NavigationRole từ soa sang msa
            _eventBus.ReceiveAsync<List<NavigationRoleSOAModel>>(SyncDataSOAToMSA.NAVIGATIONROLE, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {

                        var navroleData = unit.GetRepository<NavigationRole>().GetAll().ToList();

                        foreach (var item in x)
                        {
                            var detailNAVROLE = (from s in navroleData where s.NavigationId == item.NavigationId select s).FirstOrDefault();
                            if (detailNAVROLE == null)
                            {
                                var data = new NavigationRole
                                {
                                    NavigationRoleId = item.NavigationRoleId,
                                    RoleId = item.RoleId,
                                    NavigationId = item.NavigationId,
                                    FromSubNavigation = item.FromSubNavigation,
                                };

                                unit.GetRepository<NavigationRole>().Add(data);
                            }
                            else
                            {
                                detailNAVROLE.NavigationRoleId = item.NavigationRoleId;
                                detailNAVROLE.RoleId = item.RoleId;
                                detailNAVROLE.NavigationId = item.NavigationId;
                                detailNAVROLE.FromSubNavigation = item.FromSubNavigation;

                                unit.GetRepository<NavigationRole>().Update(detailNAVROLE);
                            }

                        }

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }
            });

            // hứng dữ liệu cho bảng NavigationRole từ soa sang msa
            _eventBus.ReceiveAsync<SystemUserModel>(SyncDataSOAToMSA.CREATE_USER, x =>
            {
                try
                {
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {

                        

                        if (unit.Save() > 0)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                }
            });
        }
    }
}
