﻿using ApiUtils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.IO.Compression;
using System.Linq;

namespace Business
{
    public static class CustomServiceCollectionExtensions
    {
        //public static IServiceCollection AddAppInsight(this IServiceCollection services, IConfiguration configuration)
        //{
        //    services.AddApplicationInsightsTelemetry(configuration);
        //    services.AddApplicationInsightsKubernetesEnricher();

        //    return services;
        //}

        public static IServiceCollection AddCustomMVC(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddControllers(options =>
            {
                options.EnableEndpointRouting = false;
            });

            //services.AddControllers();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .SetIsOriginAllowed((host) => true)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            return services;
        }

        //public static IServiceCollection AddCustomHealthCheck(this IServiceCollection services, IConfiguration configuration)
        //{
        //    var accountName = configuration.GetValue<string>("AzureStorageAccountName");
        //    var accountKey = configuration.GetValue<string>("AzureStorageAccountKey");

        //    var hcBuilder = services.AddHealthChecks();

        //    hcBuilder
        //        .AddCheck("self", () => HealthCheckResult.Healthy())
        //        .AddSqlServer(
        //            configuration["ConnectionString"],
        //            name: "CatalogDB-check",
        //            tags: new string[] { "catalogdb" });

        //    if (!string.IsNullOrEmpty(accountName) && !string.IsNullOrEmpty(accountKey))
        //    {
        //        hcBuilder
        //            .AddAzureBlobStorage(
        //                $"DefaultEndpointsProtocol=https;AccountName={accountName};AccountKey={accountKey};EndpointSuffix=core.windows.net",
        //                name: "catalog-storage-check",
        //                tags: new string[] { "catalogstorage" });
        //    }

        //    if (configuration.GetValue<bool>("AzureServiceBusEnabled"))
        //    {
        //        hcBuilder
        //            .AddAzureServiceBusTopic(
        //                configuration["EventBusConnection"],
        //                topicName: "msa_event_bus",
        //                name: "catalog-servicebus-check",
        //                tags: new string[] { "servicebus" });
        //    }
        //    else
        //    {
        //        hcBuilder
        //            .AddRabbitMQ(
        //                $"amqp://{configuration["EventBusConnection"]}",
        //                name: "catalog-rabbitmqbus-check",
        //                tags: new string[] { "rabbitmqbus" });
        //    }

        //    return services;
        //}

        public static IServiceCollection AddCustomDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            //services.AddEntityFrameworkSqlServer()
            //    .AddDbContext<DataContext>(options =>
            //    {
            //        options.UseSqlServer(configuration["ConnectionString"],
            //                             sqlServerOptionsAction: sqlOptions =>
            //                             {
            //                                 sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
            //                             //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
            //                             sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
            //                             });
            //    });

            //services.AddDbContext<IntegrationEventLogContext>(options =>
            //{
            //    options.UseSqlServer(configuration["ConnectionString"],
            //                         sqlServerOptionsAction: sqlOptions =>
            //                         {
            //                             sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
            //                             //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
            //                             sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
            //                         });
            //});

            return services;
        }

        public static IServiceCollection AddCustomOptions(this IServiceCollection services, IConfiguration configuration)
        {
            //services.Configure<CatalogSettings>(configuration);
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetails = new ValidationProblemDetails(context.ModelState)
                    {
                        Instance = context.HttpContext.Request.Path,
                        Status = StatusCodes.Status400BadRequest,
                        Detail = "Please refer to the errors property for additional details."
                    };

                    return new BadRequestObjectResult(problemDetails)
                    {
                        ContentTypes = { "application/problem+json", "application/problem+xml" }
                    };
                };
            });

            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "S MSA - Admin HTTP API",
                    Version = "v1",
                    Description = "The S MSA - Admin Microservice HTTP API. This is a Data-Driven/CRUD microservice sample"
                });
                options.OrderActionsBy((apiDesc) => $"{apiDesc.ActionDescriptor.RouteValues["controller"]}_{apiDesc.HttpMethod}");
            });

            return services;

        }

        public static IServiceCollection AddIntegrationServices(this IServiceCollection services, IConfiguration configuration)
        {
            //services.AddTransient<Func<DbConnection, IIntegrationEventLogService>>(
            //    sp => (DbConnection c) => new IntegrationEventLogService(c));

            // services.AddTransient<ICatalogIntegrationEventService, CatalogIntegrationEventService>();

            //if (configuration.GetValue<bool>("AzureServiceBusEnabled"))
            //{
            //    services.AddSingleton<IServiceBusPersisterConnection>(sp =>
            //    {
            //        var settings = sp.GetRequiredService<IOptions<CatalogSettings>>().Value;
            //        var logger = sp.GetRequiredService<ILogger<DefaultServiceBusPersisterConnection>>();

            //        var serviceBusConnection = new ServiceBusConnectionStringBuilder(settings.EventBusConnection);

            //        return new DefaultServiceBusPersisterConnection(serviceBusConnection, logger);
            //    });
            //}
            //else
            //{

            //}

            //services.AddSingleton<IRabbitMQPersistentConnection>(sp =>
            //{
            //    //var settings = sp.GetRequiredService<IOptions<CatalogSettings>>().Value;
            //    var logger = sp.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();

            //    var factory = new ConnectionFactory()
            //    {
            //        HostName = configuration["EventBusConnection"],
            //        DispatchConsumersAsync = true
            //    };

            //    if (!string.IsNullOrEmpty(configuration["EventBusPort"]))
            //    {
            //        factory.Port = int.Parse(configuration["EventBusPort"]);
            //    }

            //    if (!string.IsNullOrEmpty(configuration["EventBusUserName"]))
            //    {
            //        factory.UserName = configuration["EventBusUserName"];
            //    }

            //    if (!string.IsNullOrEmpty(configuration["EventBusPassword"]))
            //    {
            //        factory.Password = configuration["EventBusPassword"];
            //    }

            //    var retryCount = 5;
            //    if (!string.IsNullOrEmpty(configuration["EventBusRetryCount"]))
            //    {
            //        retryCount = int.Parse(configuration["EventBusRetryCount"]);
            //    }

            //    return new DefaultRabbitMQPersistentConnection(factory, logger, retryCount);
            //});

            var enableBus = configuration["EnableBus"];
            if (enableBus == "1")
            {
                var x = configuration["EventBusConnection"];
                services.AddSingleton(sp => RabbitHutch.CreateBus(configuration["EventBusConnection"], ushort.Parse(configuration["EventBusPort"]), "/", configuration["EventBusUserName"], configuration["EventBusPassword"]));

            }

            return services;
        }

        /// <summary>
        /// Configures caching for the application. Registers the <see cref="IDistributedCache"/> and
        /// <see cref="IMemoryCache"/> types with the services collection or IoC container. The
        /// <see cref="IDistributedCache"/> is intended to be used in cloud hosted scenarios where there is a shared
        /// cache, which is shared between multiple instances of the application. Use the <see cref="IMemoryCache"/>
        /// otherwise.
        /// </summary>
        public static IServiceCollection AddCustomCaching(this IServiceCollection services) =>
            services
                // Adds IMemoryCache which is a simple in-memory cache.
                .AddMemoryCache()
                // Adds IDistributedCache which is a distributed cache shared between multiple servers. This adds a
                // default implementation of IDistributedCache which is not distributed. See below:
                .AddDistributedMemoryCache();

        /// <summary>
        /// Add custom routing settings which determines how URL's are generated.
        /// </summary>
        public static IServiceCollection AddCustomRouting(this IServiceCollection services) =>
            services.AddRouting(
                options =>
                {
                    // All generated URL's should be lower-case.
                    options.LowercaseUrls = true;
                });

        /// <summary>
        /// Adds the Strict-Transport-Security HTTP header to responses. This HTTP header is only relevant if you are
        /// using TLS. It ensures that content is loaded over HTTPS and refuses to connect in case of certificate
        /// errors and warnings.
        /// See https://developer.mozilla.org/en-US/docs/Web/Security/HTTP_strict_transport_security and
        /// http://www.troyhunt.com/2015/06/understanding-http-strict-transport.html
        /// Note: Including subdomains and a minimum maxage of 18 weeks is required for preloading.
        /// Note: You can refer to the following article to clear the HSTS cache in your browser:
        /// http://classically.me/blogs/how-clear-hsts-settings-major-browsers
        /// </summary>
        public static IServiceCollection AddCustomStrictTransportSecurity(this IServiceCollection services) =>
            services
                .AddHsts(
                    options =>
                    {
                        // Preload the HSTS HTTP header for better security. See https://hstspreload.org/
                        // options.IncludeSubDomains = true;
                        // options.MaxAge = TimeSpan.FromSeconds(31536000); // 1 Year
                        // options.Preload = true;
                    });

        public static IServiceCollection AddCustomApiVersioning(this IServiceCollection services) =>
            services.AddApiVersioning(
                options =>
                {
                    options.AssumeDefaultVersionWhenUnspecified = true;
                    options.ReportApiVersions = true;
                });

        public static IServiceCollection AddCustomResponseCompression(this IServiceCollection services) =>
            services
                .AddResponseCompression(
                    options =>
                    {
                        // Add additional MIME types (other than the built in defaults) to enable GZIP compression for.
                        var customMimeTypes = services
                            .BuildServiceProvider()
                            .GetRequiredService<CompressionOptions>()
                            .MimeTypes ?? Enumerable.Empty<string>();
                        options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(customMimeTypes);
                    })
                .Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Optimal);


        public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            var subscriptionClientName = configuration["SubscriptionClientName"];

            //if (configuration.GetValue<bool>("AzureServiceBusEnabled"))
            //{
            //    services.AddSingleton<IEventBus, EventBusServiceBus>(sp =>
            //    {
            //        var serviceBusPersisterConnection = sp.GetRequiredService<IServiceBusPersisterConnection>();
            //        var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
            //        var logger = sp.GetRequiredService<ILogger<EventBusServiceBus>>();
            //        var eventBusSubcriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();

            //        return new EventBusServiceBus(serviceBusPersisterConnection, logger,
            //            eventBusSubcriptionsManager, subscriptionClientName, iLifetimeScope);
            //    });

            //}
            //else
            //{

            //}
            //services.AddSingleton<IEventBus, EventBusRabbitMQ>(sp =>
            //{
            //    var rabbitMQPersistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
            //    var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
            //    var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ>>();
            //    var eventBusSubcriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();

            //    var retryCount = 5;
            //    if (!string.IsNullOrEmpty(configuration["EventBusRetryCount"]))
            //    {
            //        retryCount = int.Parse(configuration["EventBusRetryCount"]);
            //    }

            //    return new EventBusRabbitMQ(rabbitMQPersistentConnection, logger, iLifetimeScope, eventBusSubcriptionsManager, subscriptionClientName, retryCount);
            //});

            //services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();
            //services.AddTransient<OrderStatusChangedToAwaitingValidationIntegrationEventHandler>();
            //services.AddTransient<OrderStatusChangedToPaidIntegrationEventHandler>();

            return services;
        }
    }
}
