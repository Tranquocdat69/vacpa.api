﻿using System;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using CommonUtils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Debug;

namespace Infrastructure
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {

            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            return new DataContext("", databaseType, connectionString, isTesting);
        }
    }
    public class DataContext : DbContext
    {
        private string prefix;


        public static readonly ILoggerFactory loggerFactory = new LoggerFactory(new[] {
              new DebugLoggerProvider()
        });

        public DataContext()
        {
            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            DatabaseType = databaseType;
            ConnectionString = connectionString;
            IsTesting = isTesting;
        }

        public DataContext(string prefix, string databaseType, string connectionString, bool isTesting = false)
        {
            Prefix = prefix;
            DatabaseType = databaseType;
            ConnectionString = connectionString;
            IsTesting = isTesting;
        }

        #region sys
        public virtual DbSet<IdmApplications> IdmApplications { get; set; }
        #endregion

        #region bsd
        public DbSet<bsd_Logs> bsd_Logs { get; set; }
        public virtual DbSet<Navigation> Navigation { get; set; }
        public virtual DbSet<NavigationRole> NavigationRole { get; set; }
        #endregion

        #region idm
        public virtual DbSet<IdmRoles> IdmRoles { get; set; }
        public virtual DbSet<IdmUsers> IdmUsers { get; set; }
        public virtual DbSet<IdmUsersInRoles> IdmUsersInRoles { get; set; }
        public virtual DbSet<IdmOrganizations> IdmOrganizations { get; set; }
        public virtual DbSet<IdmUsersInOrganization> IdmUsersInOrganization { get; set; }
        public virtual DbSet<IdmRight> IdmRight { get; set; }
        public virtual DbSet<IdmRightsInRole> IdmRightsInRole { get; set; }
        public virtual DbSet<IdmRightsOfUser> IdmRightsOfUser { get; set; }
        public virtual DbSet<WorkflowConfigure> WorkflowConfigure { get; set; }

        public virtual DbSet<IdmUserInArea> IdmUserInArea { get; set; }

        #endregion

        #region catalog
        //public DbSet<BsdParameter> BsdParameter { get; set; }
        public virtual DbSet<catalog_WorkflowCommand> catalog_WorkflowCommand { get; set; }
        public virtual DbSet<catalog_WorkflowState> catalog_WorkflowState { get; set; }
        //public virtual DbSet<catalog_Province> catalog_Province { get; set; }
        //public virtual DbSet<catalog_District> catalog_District { get; set; }
        //public virtual DbSet<catalog_Ward> catalog_Ward { get; set; }
        #endregion

        public string Prefix { get; set; }
        public string DatabaseType { get; set; }
        public string ConnectionString { get; set; }
        public bool IsTesting { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            if (!optionsBuilder.IsConfigured)
            {
                #region Config database
                if (IsTesting)
                {
                    optionsBuilder.UseInMemoryDatabase("testDatabase");
                }
                else
                {
                    switch (DatabaseType)
                    {
                        //case "MySqlPomeloDatabase":
                        //    optionsBuilder.UseMySql(ConnectionString);
                        //    break;
                        //case "MSSQLDatabase":
                        //    optionsBuilder.UseSqlServer(ConnectionString);
                        //    break;
                        //case "OracleDatabase":
                        //    optionsBuilder.UseOracle(ConnectionString);
                        //    break;
                        case "PostgreSQLDatabase":
                            optionsBuilder.UseNpgsql(ConnectionString, builder => builder.EnableRetryOnFailure(
                                maxRetryCount: 10,
                                maxRetryDelay: TimeSpan.FromSeconds(5),
                                errorCodesToAdd: null
                                ));
                            break;
                        //case "Sqlite":
                        //    optionsBuilder.UseSqlite(ConnectionString);
                        //    break;
                        default:
                            optionsBuilder.UseSqlServer(ConnectionString);
                            break;
                    }
                }
                #endregion

                optionsBuilder.ReplaceService<IModelCacheKeyFactory, DynamicModelCacheKeyFactory>();
                optionsBuilder.UseLoggerFactory(loggerFactory).EnableSensitiveDataLogging();
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }

    }
    public class DynamicModelCacheKeyFactory : IModelCacheKeyFactory
    {
        public object Create(DbContext context)
        {

            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            if (context is DataContext dynamicContext)
            {
                return (context.GetType(), dynamicContext.Prefix, databaseType, connectionString, isTesting);
            }

            return context.GetType();
        }
    }
}