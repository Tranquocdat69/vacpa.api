﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure
{
    [Table("idm_UsersInOrganization")]
    public partial class IdmUsersInOrganization
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public Guid UserId { get; set; }
        [Required]
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public Guid? CreatedByUserId { get; set; }
        
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        
        public DateTime? LastModifiedOnDate { get; set; }
        [StringLength(256)]
        public string Role { get; set; }

        [ForeignKey("OrganizationId")]
        public virtual IdmOrganizations Organization { get; set; }
        [ForeignKey("UserId")]
        public virtual IdmUsers User { get; set; }
    }
}
