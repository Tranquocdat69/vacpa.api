﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure
{
    [Table("idm_RightsInRole")]
    public partial class IdmRightsInRole
    {
        public long Id { get; set; }
        public Guid RoleId { get; set; }
        [Required]
        [StringLength(256)]
        public string RightCode { get; set; }
        
        public DateTime CreatedDate { get; set; }
        
        public DateTime ModifiedDate { get; set; }

        [ForeignKey("RightCode")]
        public virtual IdmRight RightCodeNavigation { get; set; }
        [ForeignKey("RoleId")]
        public virtual IdmRoles Role { get; set; }
    }
}
