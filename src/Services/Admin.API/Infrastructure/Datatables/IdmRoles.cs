﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure
{
    [Table("idm_Roles")]
    public partial class IdmRoles
    {
        public IdmRoles()
        {
            //IdmUsersInRoles = new HashSet<IdmUsersInRoles>();
            // IdmRightsInRole = new HashSet<IdmRightsInRole>();
        }

        public Guid ApplicationId { get; set; }
        [Key]
        public Guid RoleId { get; set; }
        [Required]
        [StringLength(256)]
        public string RoleName { get; set; }
        [Required]
        [StringLength(256)]
        public string LoweredRoleName { get; set; }
        [StringLength(256)]
        public string Description { get; set; }
        public bool EnableDelete { get; set; }
        [Column("CreatedByUserID")]
        public Guid CreatedByUserId { get; set; }
        
        public DateTime CreatedOnDate { get; set; }
        [Column("LastModifiedByUserID")]
        public Guid LastModifiedByUserId { get; set; }
        
        public DateTime LastModifiedOnDate { get; set; }
        [StringLength(256)]
        public string RoleCode { get; set; }

        [ForeignKey("ApplicationId")]
        public virtual IdmApplications Application { get; set; }
        //public ICollection<IdmUsersInRoles> IdmUsersInRoles { get; set; }
        public virtual ICollection<IdmRightsInRole> IdmRightsInRole { get; set; }
    }
}
