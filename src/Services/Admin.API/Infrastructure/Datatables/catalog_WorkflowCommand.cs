namespace Infrastructure
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("catalog_WorkflowCommand")]
    public partial class catalog_WorkflowCommand
    {
        [Key]
        [Column("CommandId")]
        public Guid CommandId { get; set; }
        [StringLength(256)]
        [Column("CommandCode")]
        public string CommandCode { get; set; }
        [StringLength(1024)]
        [Column("CommandName")]
        public string CommandName { get; set; }
        public int Order { get; set; }
    }
}
