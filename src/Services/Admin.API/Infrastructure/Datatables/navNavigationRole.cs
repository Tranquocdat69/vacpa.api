﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure
{
    [Table("nav_Navigation_Role")]
    public partial class NavigationRole
    {
        public Guid NavigationRoleId { get; set; }
        public Guid RoleId { get; set; }
        public Guid NavigationId { get; set; }
        public Guid? FromSubNavigation { get; set; }

        [ForeignKey("NavigationId")]
        public virtual Navigation Navigation { get; set; }
    }
}
