﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure
{
    [Table("idm_Applications")]
    public partial class IdmApplications
    {
        public IdmApplications()
        {
            IdmRoles = new HashSet<IdmRoles>();
            IdmUsers = new HashSet<IdmUsers>();
        }

        [Required]
        [StringLength(256)]
        public string ApplicationName { get; set; }
        [Required]
        [StringLength(256)]
        public string LoweredApplicationName { get; set; }
        [Key]
        public Guid ApplicationId { get; set; }
        [StringLength(256)]
        public string Description { get; set; }
        public virtual  ICollection<IdmRoles> IdmRoles { get; set; }
        public virtual  ICollection<IdmUsers> IdmUsers { get; set; }
    }
}
