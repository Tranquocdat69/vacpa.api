﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure
{
    [Table("catalog_Province")]
    public partial class catalog_Province
    {
        public catalog_Province()
        {
        }

        [Key]
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string Issue_modification_decision { get; set; }
        public string Issued_decision_date { get; set; }
        public string Issued_department { get; set; }
        public string Id_regions { get; set; }
        public string Status { get; set; }
        public string Version { get; set; }
        public string Created_by { get; set; }
        public DateTime Created_time { get; set; }
        public string Updated_by { get; set; }
        public DateTime Updated_time { get; set; }
    }
}
