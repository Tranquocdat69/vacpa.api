﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure
{
    [Table("idm_RightsOfUser")]
    public partial class IdmRightsOfUser
    {
        public long Id { get; set; }
        public Guid UserId { get; set; }
        [Required]
        [StringLength(256)]
        public string RightCode { get; set; }
        public string InheritedFromRoles { get; set; }
        
        public DateTime CreatedDate { get; set; }
        
        public DateTime ModifiedDate { get; set; }
        public bool Inherited { get; set; }
        public bool Enable { get; set; }
        public Guid ApplicationId { get; set; }

        [ForeignKey("RightCode")]
        public virtual IdmRight RightCodeNavigation { get; set; }
        [ForeignKey("UserId")]
        public virtual IdmUsers User { get; set; }
    }
}
