﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure
{
    [Table("idm_Users")]
    public partial class IdmUsers
    {
        public IdmUsers()
        {
            //IdmUsersInOrganization = new HashSet<IdmUsersInOrganization>();
            //IdmUsersInRoles = new HashSet<IdmUsersInRoles>();
            //IdmRightsOfUser = new HashSet<IdmRightsOfUser>();
        }

        public Guid ApplicationId { get; set; }
        [Key]
        public Guid UserId { get; set; } 
        [Required]
        [StringLength(256)]
        public string UserName { get; set; } 
        [StringLength(512)]
        public string NickName { get; set; } 
        public string MobilePin { get; set; } 
        [StringLength(256)]
        public string Email { get; set; }
        public string OtherEmail { get; set; }
        public int Type { get; set; } 
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public DateTime Birthday { get; set; }
        [Required]
        [StringLength(256)]
        public string LoweredUserName { get; set; }

        [StringLength(128)]
        public string Password { get; set; }
        public int PasswordFormat { get; set; }
        [Required]
        [StringLength(128)]
        public string PasswordSalt { get; set; }
        public bool IsLockedOut { get; set; }
        [StringLength(16)]
        public string MobileAlias { get; set; }
        public bool IsAnonymous { get; set; }
        public string Avatar { get; set; } 
        public string Address { get; set; } 
        
        public DateTime LastActivityDate { get; set; }
        public Guid? CharacterManagerId { get; set; }

        public string FinanceCode { get; set; }
        public string ChucVu { get; set; }
        //base
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        //Workflow
        public string SchemeName { get; set; }
        public string WorkFlowCurrentState { get; set; }
        public DateTime? WorkFlowProcessTime { get; set; }
        public string WorkFlowNextState { get; set; }
        public string WorkFlowPreviousState { get; set; }
        public string WorkFlowAsignUserName { get; set; }
        public Guid? WorkflowProcessUserId { get; set; }
        public string WorkflowProcessUserName { get; set; }

        [ForeignKey("ApplicationId")]
        public virtual  IdmApplications Application { get; set; }
        public virtual  ICollection<IdmUsersInOrganization> IdmUsersInOrganization { get; set; }
        public virtual  ICollection<IdmUsersInRoles> IdmUsersInRoles { get; set; }
        public virtual  ICollection<IdmRightsOfUser> IdmRightsOfUser { get; set; }
    }
}
