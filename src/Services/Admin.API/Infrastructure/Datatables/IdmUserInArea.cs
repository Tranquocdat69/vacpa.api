﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace Infrastructure
{
    [Table("idm_UserInArea")]
    public partial class IdmUserInArea
    {
        [Key]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid ProvinceId { get; set; }
        public string ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        [AllowNull]
        public Guid DistrictId { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
        [AllowNull]
        public Guid WardId { get; set; }
        public string WardCode { get; set; }
        public string WardName { get; set; }
        [AllowNull]
        public Guid AddressId { get; set; }
        public string AddressCode { get; set; }
        public string AddressName { get; set; }
        public int Level { get; set; }
        public string FullAddress { get; set; }

        public Guid ApplicationId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid CreatedByUserId { get; set; }
        public string CreatedByUserFullName { get; set; }
    }
}

