﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure
{
    [Table("idm_Right")]
    public partial class IdmRight
    {
        public IdmRight()
        {
        }

        [Key]
        [StringLength(256)]
        public string RightCode { get; set; }
        [Required]
        [StringLength(1024)]
        public string RightName { get; set; }
        [Required]
        public string Description { get; set; }
        public bool? Status { get; set; }
        public int? Order { get; set; }
        public bool IsGroup { get; set; }
        public int Level { get; set; }
        [StringLength(256)]
        public string GroupCode { get; set; }
        public virtual ICollection<IdmRightsInRole> IdmRightsInRole { get; set; }
        public virtual ICollection<IdmRightsOfUser> IdmRightsOfUser { get; set; }
        public virtual ICollection<IdmRight> InverseGroupCodeNavigation { get; set; }
    }
}
