﻿namespace Infrastructure
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("catalog_WorkflowConfigure")]
    public partial class WorkflowConfigure
    {              
        public Guid Id { get; set; }
        public bool IsCenter { get; set; } = true; //true: WF cho cấp Trung ương. false: cho Tỉnh/Quận Huyện     
        public Guid? ProvinceId { get; set; }
        public Guid? DistrictId { get; set; }
        public Guid? WardId { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public Guid? DonViId { get; set; }
        public string WorkflowCode { get; set; }
        public string FunctionList { get; set; }
        public bool Status { get; set; } = true;
        public Guid? CreatedByUserId { get; set; }     
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public string ListFunctionName { get; set; }

    }
}
