﻿namespace Infrastructure
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("bsd_Logs")]
    public partial class bsd_Logs
    {          
        [Key]    
        public int Id { get; set; }
        public Guid? ApplicationId { get; set; }
        public DateTime Date { get; set; }
        [StringLength(256)]
        public string Thread { get; set; }
        [StringLength(64)]
        public string Level { get; set; }
        [StringLength(255)]
        public string Logger { get; set; }
        [StringLength(512)]
        public string ActionByUserId { get; set; }
        [StringLength(1024)]
        public string Exception { get; set; }
        public Guid ModuleId { get; set; }
        [StringLength(1024)]
        public string Content { get; set; }
        public Guid UserId { get; set; }
        [StringLength(256)]
        public string UserName { get; set; }
        [StringLength(64)]
        public string ObjectType { get; set; }
        public Guid? ObjectId { get; set; }

    }
}
