namespace Infrastructure
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("catalog_WorkflowState")]
    public partial class catalog_WorkflowState
    {
        [Key]
        [Column("StateId")]
        public Guid StateId { get; set; }
        [StringLength(256)]
        [Column("StateCode")]
        public string StateCode { get; set; }
        [StringLength(1024)]
        [Column("StateName")]
        public string StateName { get; set; }
    }
}
