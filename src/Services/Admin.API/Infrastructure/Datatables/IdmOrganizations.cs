﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure
{
    [Table("idm_Organizations")]
    public partial class IdmOrganizations
    {
        public IdmOrganizations()
        {
            IdmUsersInOrganization = new HashSet<IdmUsersInOrganization>();
        }
        public Guid ApplicationId { get; set; }
        [Key]
        public Guid OrganizationId { get; set; }
        [Required]
        public string OrganizationName { get; set; }
        [Required]
        public string Code { get; set; }
        // đơn vị phòng ban cha
        public Guid ParentOrganizationId { get; set; }
        public string ParentOrganizationName { get; set; }
        public int Status { get; set; }
        public int? Loai { get; set; } // 1. Đơn vị, 2. Phòng ban

        //Organization detail
        public string Type { get; set; }
        public bool? IsPublic { get; set; }
        public int? ThuTu { get; set; }
        public string Description { get; set; }
        public string Address { get; set; } // địa chỉ inews
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string IdPath { get; set; }
        public string Path { get; set; } 
        public int? Level { get; set; }
        public bool? HasChild { get; set; }

        // base
        public string CreatedByFullName { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public string LastModifiedByFullName { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }

        public virtual IdmOrganizations InverseOrganization { get; set; }
        public virtual ICollection<IdmUsersInOrganization> IdmUsersInOrganization { get; set; }
    }
}
