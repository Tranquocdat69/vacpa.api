﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class addwfcatalogtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "catalog_WorkflowCommand",
                columns: table => new
                {
                    CommandId = table.Column<Guid>(nullable: false),
                    CommandCode = table.Column<string>(maxLength: 256, nullable: true),
                    CommandName = table.Column<string>(maxLength: 1024, nullable: true),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_catalog_WorkflowCommand", x => x.CommandId);
                });

            migrationBuilder.CreateTable(
                name: "catalog_WorkflowState",
                columns: table => new
                {
                    StateId = table.Column<Guid>(nullable: false),
                    StateCode = table.Column<string>(maxLength: 256, nullable: true),
                    StateName = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_catalog_WorkflowState", x => x.StateId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "catalog_WorkflowCommand");

            migrationBuilder.DropTable(
                name: "catalog_WorkflowState");
        }
    }
}
