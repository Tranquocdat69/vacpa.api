﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class _UpdateOrgTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreatedByFullName",
                table: "idm_Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastModifiedByFullName",
                table: "idm_Organizations",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ParentOrganizationId",
                table: "idm_Organizations",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "ParentOrganizationName",
                table: "idm_Organizations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedByFullName",
                table: "idm_Organizations");

            migrationBuilder.DropColumn(
                name: "LastModifiedByFullName",
                table: "idm_Organizations");

            migrationBuilder.DropColumn(
                name: "ParentOrganizationId",
                table: "idm_Organizations");

            migrationBuilder.DropColumn(
                name: "ParentOrganizationName",
                table: "idm_Organizations");
        }
    }
}
