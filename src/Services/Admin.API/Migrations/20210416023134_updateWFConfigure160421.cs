﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class updateWFConfigure160421 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DistrictName",
                table: "catalog_WorkflowConfigure",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProvinceName",
                table: "catalog_WorkflowConfigure",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WardName",
                table: "catalog_WorkflowConfigure",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DistrictName",
                table: "catalog_WorkflowConfigure");

            migrationBuilder.DropColumn(
                name: "ProvinceName",
                table: "catalog_WorkflowConfigure");

            migrationBuilder.DropColumn(
                name: "WardName",
                table: "catalog_WorkflowConfigure");
        }
    }
}
