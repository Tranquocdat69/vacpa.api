﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class _17032021_UpdateOrganization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "ParentOrganizationId",
                table: "idm_Organizations",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "idm_Organizations",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "DistrictId",
                table: "idm_Organizations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsCentered",
                table: "idm_Organizations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "ProvinceId",
                table: "idm_Organizations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "idm_Organizations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "WardId",
                table: "idm_Organizations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DistrictId",
                table: "idm_Organizations");

            migrationBuilder.DropColumn(
                name: "IsCentered",
                table: "idm_Organizations");

            migrationBuilder.DropColumn(
                name: "ProvinceId",
                table: "idm_Organizations");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "idm_Organizations");

            migrationBuilder.DropColumn(
                name: "WardId",
                table: "idm_Organizations");

            migrationBuilder.AlterColumn<Guid>(
                name: "ParentOrganizationId",
                table: "idm_Organizations",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "idm_Organizations",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
