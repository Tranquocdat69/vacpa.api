﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class updateWFConfigure010421 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "DistrictId",
                table: "catalog_WorkflowConfigure",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ProvinceId",
                table: "catalog_WorkflowConfigure",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "WardId",
                table: "catalog_WorkflowConfigure",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DistrictId",
                table: "catalog_WorkflowConfigure");

            migrationBuilder.DropColumn(
                name: "ProvinceId",
                table: "catalog_WorkflowConfigure");

            migrationBuilder.DropColumn(
                name: "WardId",
                table: "catalog_WorkflowConfigure");
        }
    }
}
