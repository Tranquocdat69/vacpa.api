﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class editWorkflowConfigure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "catalog_WorkflowConfigure",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsCenter = table.Column<bool>(nullable: false),
                    DonViId = table.Column<Guid>(nullable: true),
                    DonViName = table.Column<string>(nullable: true),
                    WorkflowCode = table.Column<string>(nullable: true),
                    WorkflowName = table.Column<string>(nullable: true),
                    Route = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    CreatedByUserId = table.Column<Guid>(nullable: false),
                    CreatedByUser = table.Column<string>(nullable: true),
                    CreatedOnDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_catalog_WorkflowConfigure", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "catalog_WorkflowConfigure");
        }
    }
}
