﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class updateforienkeycasede : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_idm_RightsInRole_idm_Right_RightCode",
                table: "idm_RightsInRole");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_RightsInRole_idm_Roles_RoleId",
                table: "idm_RightsInRole");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_RightsOfUser_idm_Right_RightCode",
                table: "idm_RightsOfUser");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_RightsOfUser_idm_Users_UserId",
                table: "idm_RightsOfUser");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_Roles_idm_Applications_ApplicationId",
                table: "idm_Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_Users_idm_Applications_ApplicationId",
                table: "idm_Users");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_UsersInOrganization_idm_Organizations_OrganizationId",
                table: "idm_UsersInOrganization");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_UsersInOrganization_idm_Users_UserId",
                table: "idm_UsersInOrganization");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_UsersInRoles_idm_Roles_RoleId",
                table: "idm_UsersInRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_UsersInRoles_idm_Users_UserId",
                table: "idm_UsersInRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_nav_Navigation_Role_nav_Navigation_NavigationId",
                table: "nav_Navigation_Role");

            migrationBuilder.AddForeignKey(
                name: "FK_idm_RightsInRole_idm_Right_RightCode",
                table: "idm_RightsInRole",
                column: "RightCode",
                principalTable: "idm_Right",
                principalColumn: "RightCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_RightsInRole_idm_Roles_RoleId",
                table: "idm_RightsInRole",
                column: "RoleId",
                principalTable: "idm_Roles",
                principalColumn: "RoleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_RightsOfUser_idm_Right_RightCode",
                table: "idm_RightsOfUser",
                column: "RightCode",
                principalTable: "idm_Right",
                principalColumn: "RightCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_RightsOfUser_idm_Users_UserId",
                table: "idm_RightsOfUser",
                column: "UserId",
                principalTable: "idm_Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_Roles_idm_Applications_ApplicationId",
                table: "idm_Roles",
                column: "ApplicationId",
                principalTable: "idm_Applications",
                principalColumn: "ApplicationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_Users_idm_Applications_ApplicationId",
                table: "idm_Users",
                column: "ApplicationId",
                principalTable: "idm_Applications",
                principalColumn: "ApplicationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_UsersInOrganization_idm_Organizations_OrganizationId",
                table: "idm_UsersInOrganization",
                column: "OrganizationId",
                principalTable: "idm_Organizations",
                principalColumn: "OrganizationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_UsersInOrganization_idm_Users_UserId",
                table: "idm_UsersInOrganization",
                column: "UserId",
                principalTable: "idm_Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_UsersInRoles_idm_Roles_RoleId",
                table: "idm_UsersInRoles",
                column: "RoleId",
                principalTable: "idm_Roles",
                principalColumn: "RoleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_UsersInRoles_idm_Users_UserId",
                table: "idm_UsersInRoles",
                column: "UserId",
                principalTable: "idm_Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_nav_Navigation_Role_nav_Navigation_NavigationId",
                table: "nav_Navigation_Role",
                column: "NavigationId",
                principalTable: "nav_Navigation",
                principalColumn: "NavigationId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_idm_RightsInRole_idm_Right_RightCode",
                table: "idm_RightsInRole");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_RightsInRole_idm_Roles_RoleId",
                table: "idm_RightsInRole");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_RightsOfUser_idm_Right_RightCode",
                table: "idm_RightsOfUser");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_RightsOfUser_idm_Users_UserId",
                table: "idm_RightsOfUser");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_Roles_idm_Applications_ApplicationId",
                table: "idm_Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_Users_idm_Applications_ApplicationId",
                table: "idm_Users");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_UsersInOrganization_idm_Organizations_OrganizationId",
                table: "idm_UsersInOrganization");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_UsersInOrganization_idm_Users_UserId",
                table: "idm_UsersInOrganization");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_UsersInRoles_idm_Roles_RoleId",
                table: "idm_UsersInRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_idm_UsersInRoles_idm_Users_UserId",
                table: "idm_UsersInRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_nav_Navigation_Role_nav_Navigation_NavigationId",
                table: "nav_Navigation_Role");

            migrationBuilder.AddForeignKey(
                name: "FK_idm_RightsInRole_idm_Right_RightCode",
                table: "idm_RightsInRole",
                column: "RightCode",
                principalTable: "idm_Right",
                principalColumn: "RightCode",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_RightsInRole_idm_Roles_RoleId",
                table: "idm_RightsInRole",
                column: "RoleId",
                principalTable: "idm_Roles",
                principalColumn: "RoleId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_RightsOfUser_idm_Right_RightCode",
                table: "idm_RightsOfUser",
                column: "RightCode",
                principalTable: "idm_Right",
                principalColumn: "RightCode",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_RightsOfUser_idm_Users_UserId",
                table: "idm_RightsOfUser",
                column: "UserId",
                principalTable: "idm_Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_Roles_idm_Applications_ApplicationId",
                table: "idm_Roles",
                column: "ApplicationId",
                principalTable: "idm_Applications",
                principalColumn: "ApplicationId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_Users_idm_Applications_ApplicationId",
                table: "idm_Users",
                column: "ApplicationId",
                principalTable: "idm_Applications",
                principalColumn: "ApplicationId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_UsersInOrganization_idm_Organizations_OrganizationId",
                table: "idm_UsersInOrganization",
                column: "OrganizationId",
                principalTable: "idm_Organizations",
                principalColumn: "OrganizationId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_UsersInOrganization_idm_Users_UserId",
                table: "idm_UsersInOrganization",
                column: "UserId",
                principalTable: "idm_Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_UsersInRoles_idm_Roles_RoleId",
                table: "idm_UsersInRoles",
                column: "RoleId",
                principalTable: "idm_Roles",
                principalColumn: "RoleId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_idm_UsersInRoles_idm_Users_UserId",
                table: "idm_UsersInRoles",
                column: "UserId",
                principalTable: "idm_Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_nav_Navigation_Role_nav_Navigation_NavigationId",
                table: "nav_Navigation_Role",
                column: "NavigationId",
                principalTable: "nav_Navigation",
                principalColumn: "NavigationId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
