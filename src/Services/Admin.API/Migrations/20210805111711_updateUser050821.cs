﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class updateUser050821 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Approved",
                table: "idm_Users");

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedByUserId",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnDate",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "LastModifiedByUserId",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModifiedOnDate",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SchemeName",
                table: "idm_Users",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "WorkFlowAsignUserName",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkFlowCurrentState",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkFlowNextState",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkFlowPreviousState",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "WorkFlowProcessTime",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "WorkflowProcessUserId",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkflowProcessUserName",
                table: "idm_Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedByUserId",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "CreatedOnDate",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "LastModifiedByUserId",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "LastModifiedOnDate",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "SchemeName",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "WorkFlowAsignUserName",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "WorkFlowCurrentState",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "WorkFlowNextState",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "WorkFlowPreviousState",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "WorkFlowProcessTime",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "WorkflowProcessUserId",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "WorkflowProcessUserName",
                table: "idm_Users");

            migrationBuilder.AddColumn<int>(
                name: "Approved",
                table: "idm_Users",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
