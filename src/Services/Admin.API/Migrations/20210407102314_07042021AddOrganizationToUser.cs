﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class _07042021AddOrganizationToUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedByUser",
                table: "catalog_WorkflowConfigure");

            migrationBuilder.DropColumn(
                name: "DonViName",
                table: "catalog_WorkflowConfigure");

            migrationBuilder.DropColumn(
                name: "Route",
                table: "catalog_WorkflowConfigure");

            migrationBuilder.DropColumn(
                name: "WorkflowName",
                table: "catalog_WorkflowConfigure");

            migrationBuilder.AddColumn<Guid>(
                name: "OrganizationId",
                table: "idm_UserInArea",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrganizationName",
                table: "idm_UserInArea",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "WardId",
                table: "catalog_WorkflowConfigure",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "ProvinceId",
                table: "catalog_WorkflowConfigure",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "DonViId",
                table: "catalog_WorkflowConfigure",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "DistrictId",
                table: "catalog_WorkflowConfigure",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOnDate",
                table: "catalog_WorkflowConfigure",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<Guid>(
                name: "CreatedByUserId",
                table: "catalog_WorkflowConfigure",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<string>(
                name: "FunctionList",
                table: "catalog_WorkflowConfigure",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "LastModifiedByUserId",
                table: "catalog_WorkflowConfigure",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModifiedOnDate",
                table: "catalog_WorkflowConfigure",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrganizationId",
                table: "idm_UserInArea");

            migrationBuilder.DropColumn(
                name: "OrganizationName",
                table: "idm_UserInArea");

            migrationBuilder.DropColumn(
                name: "FunctionList",
                table: "catalog_WorkflowConfigure");

            migrationBuilder.DropColumn(
                name: "LastModifiedByUserId",
                table: "catalog_WorkflowConfigure");

            migrationBuilder.DropColumn(
                name: "LastModifiedOnDate",
                table: "catalog_WorkflowConfigure");

            migrationBuilder.AlterColumn<Guid>(
                name: "WardId",
                table: "catalog_WorkflowConfigure",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "ProvinceId",
                table: "catalog_WorkflowConfigure",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "DonViId",
                table: "catalog_WorkflowConfigure",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "DistrictId",
                table: "catalog_WorkflowConfigure",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOnDate",
                table: "catalog_WorkflowConfigure",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "CreatedByUserId",
                table: "catalog_WorkflowConfigure",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedByUser",
                table: "catalog_WorkflowConfigure",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DonViName",
                table: "catalog_WorkflowConfigure",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Route",
                table: "catalog_WorkflowConfigure",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkflowName",
                table: "catalog_WorkflowConfigure",
                type: "text",
                nullable: true);
        }
    }
}
