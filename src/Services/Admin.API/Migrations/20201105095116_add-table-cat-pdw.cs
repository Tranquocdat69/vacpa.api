﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class addtablecatpdw : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "catalog_District",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    EnglishName = table.Column<string>(nullable: true),
                    Issue_modification_decision = table.Column<string>(nullable: true),
                    Issued_decision_date = table.Column<string>(nullable: true),
                    Issued_department = table.Column<string>(nullable: true),
                    ProvinceId = table.Column<Guid>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true),
                    Created_by = table.Column<string>(nullable: true),
                    Created_time = table.Column<DateTime>(nullable: false),
                    Updated_by = table.Column<string>(nullable: true),
                    Updated_time = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_catalog_District", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "catalog_Province",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    EnglishName = table.Column<string>(nullable: true),
                    Issue_modification_decision = table.Column<string>(nullable: true),
                    Issued_decision_date = table.Column<string>(nullable: true),
                    Issued_department = table.Column<string>(nullable: true),
                    Id_regions = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true),
                    Created_by = table.Column<string>(nullable: true),
                    Created_time = table.Column<DateTime>(nullable: false),
                    Updated_by = table.Column<string>(nullable: true),
                    Updated_time = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_catalog_Province", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "catalog_Ward",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    EnglishName = table.Column<string>(nullable: true),
                    Issue_modification_decision = table.Column<string>(nullable: true),
                    Issued_decision_date = table.Column<string>(nullable: true),
                    Issued_department = table.Column<string>(nullable: true),
                    DistrictId = table.Column<Guid>(nullable: false),
                    ProvinceId = table.Column<Guid>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true),
                    Created_by = table.Column<string>(nullable: true),
                    Created_time = table.Column<DateTime>(nullable: false),
                    Updated_by = table.Column<string>(nullable: true),
                    Updated_time = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_catalog_Ward", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "catalog_District");

            migrationBuilder.DropTable(
                name: "catalog_Province");

            migrationBuilder.DropTable(
                name: "catalog_Ward");
        }
    }
}
