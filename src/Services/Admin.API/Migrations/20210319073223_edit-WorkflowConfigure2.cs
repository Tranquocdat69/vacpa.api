﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class editWorkflowConfigure2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "DonViId",
                table: "catalog_WorkflowConfigure",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "DonViId",
                table: "catalog_WorkflowConfigure",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid));
        }
    }
}
