﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class _updateUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "SchemeName",
                table: "idm_Users",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<string>(
                name: "ChucVu",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OtherEmail",
                table: "idm_Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShortName",
                table: "idm_Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChucVu",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "OtherEmail",
                table: "idm_Users");

            migrationBuilder.DropColumn(
                name: "ShortName",
                table: "idm_Users");

            migrationBuilder.AlterColumn<string>(
                name: "SchemeName",
                table: "idm_Users",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
