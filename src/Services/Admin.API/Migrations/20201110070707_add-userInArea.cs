﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class adduserInArea : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "catalog_District");

            migrationBuilder.DropTable(
                name: "catalog_Province");

            migrationBuilder.DropTable(
                name: "catalog_Ward");

            migrationBuilder.CreateTable(
                name: "idm_UserInArea",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    ProvinceId = table.Column<Guid>(nullable: false),
                    ProvinceCode = table.Column<string>(nullable: true),
                    ProvinceName = table.Column<string>(nullable: true),
                    DistrictId = table.Column<Guid>(nullable: false),
                    DistrictCode = table.Column<string>(nullable: true),
                    DistrictName = table.Column<string>(nullable: true),
                    WardId = table.Column<Guid>(nullable: false),
                    WardCode = table.Column<string>(nullable: true),
                    WardName = table.Column<string>(nullable: true),
                    AddressId = table.Column<Guid>(nullable: false),
                    AddressCode = table.Column<string>(nullable: true),
                    AddressName = table.Column<string>(nullable: true),
                    Level = table.Column<int>(nullable: false),
                    FullAddress = table.Column<string>(nullable: true),
                    ApplicationId = table.Column<Guid>(nullable: false),
                    CreatedOnDate = table.Column<DateTime>(nullable: false),
                    CreatedByUserId = table.Column<Guid>(nullable: false),
                    CreatedByUserFullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_UserInArea", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "idm_UserInArea");

            migrationBuilder.CreateTable(
                name: "catalog_District",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Code = table.Column<string>(type: "text", nullable: true),
                    Created_by = table.Column<string>(type: "text", nullable: true),
                    Created_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    EnglishName = table.Column<string>(type: "text", nullable: true),
                    Issue_modification_decision = table.Column<string>(type: "text", nullable: true),
                    Issued_decision_date = table.Column<string>(type: "text", nullable: true),
                    Issued_department = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: true),
                    ProvinceId = table.Column<Guid>(type: "uuid", nullable: false),
                    Status = table.Column<string>(type: "text", nullable: true),
                    Updated_by = table.Column<string>(type: "text", nullable: true),
                    Updated_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Version = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_catalog_District", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "catalog_Province",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Code = table.Column<string>(type: "text", nullable: true),
                    Created_by = table.Column<string>(type: "text", nullable: true),
                    Created_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    EnglishName = table.Column<string>(type: "text", nullable: true),
                    Id_regions = table.Column<string>(type: "text", nullable: true),
                    Issue_modification_decision = table.Column<string>(type: "text", nullable: true),
                    Issued_decision_date = table.Column<string>(type: "text", nullable: true),
                    Issued_department = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<string>(type: "text", nullable: true),
                    Updated_by = table.Column<string>(type: "text", nullable: true),
                    Updated_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Version = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_catalog_Province", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "catalog_Ward",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Code = table.Column<string>(type: "text", nullable: true),
                    Created_by = table.Column<string>(type: "text", nullable: true),
                    Created_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DistrictId = table.Column<Guid>(type: "uuid", nullable: false),
                    EnglishName = table.Column<string>(type: "text", nullable: true),
                    Issue_modification_decision = table.Column<string>(type: "text", nullable: true),
                    Issued_decision_date = table.Column<string>(type: "text", nullable: true),
                    Issued_department = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: true),
                    ProvinceId = table.Column<Guid>(type: "uuid", nullable: false),
                    Status = table.Column<string>(type: "text", nullable: true),
                    Updated_by = table.Column<string>(type: "text", nullable: true),
                    Updated_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Version = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_catalog_Ward", x => x.Id);
                });
        }
    }
}
