﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Admin.API.Migrations
{
    public partial class add_tbl_log : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "bsd_Logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ApplicationId = table.Column<Guid>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    Thread = table.Column<string>(maxLength: 256, nullable: true),
                    Level = table.Column<string>(maxLength: 64, nullable: true),
                    Logger = table.Column<string>(maxLength: 255, nullable: true),
                    ActionByUserId = table.Column<string>(maxLength: 512, nullable: true),
                    Exception = table.Column<string>(maxLength: 1024, nullable: true),
                    ModuleId = table.Column<Guid>(nullable: false),
                    Content = table.Column<string>(maxLength: 1024, nullable: true),
                    UserId = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    ObjectType = table.Column<string>(maxLength: 64, nullable: true),
                    ObjectId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_Logs", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "bsd_Logs");
        }
    }
}
