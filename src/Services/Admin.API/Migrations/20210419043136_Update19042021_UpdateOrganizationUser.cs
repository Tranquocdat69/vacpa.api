﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class Update19042021_UpdateOrganizationUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrganizationId",
                table: "idm_UserInArea");

            migrationBuilder.DropColumn(
                name: "OrganizationName",
                table: "idm_UserInArea");

            migrationBuilder.AlterColumn<DateTime>(
                name: "LastModifiedOnDate",
                table: "idm_UsersInOrganization",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<Guid>(
                name: "LastModifiedByUserId",
                table: "idm_UsersInOrganization",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOnDate",
                table: "idm_UsersInOrganization",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<Guid>(
                name: "CreatedByUserId",
                table: "idm_UsersInOrganization",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<string>(
                name: "OrganizationName",
                table: "idm_UsersInOrganization",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrganizationName",
                table: "idm_UsersInOrganization");

            migrationBuilder.AlterColumn<DateTime>(
                name: "LastModifiedOnDate",
                table: "idm_UsersInOrganization",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "LastModifiedByUserId",
                table: "idm_UsersInOrganization",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOnDate",
                table: "idm_UsersInOrganization",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "CreatedByUserId",
                table: "idm_UsersInOrganization",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "OrganizationId",
                table: "idm_UserInArea",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrganizationName",
                table: "idm_UserInArea",
                type: "text",
                nullable: true);
        }
    }
}
