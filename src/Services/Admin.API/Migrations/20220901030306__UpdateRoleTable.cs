﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.API.Migrations
{
    public partial class _UpdateRoleTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_idm_Roles_idm_Applications_IdmApplicationsApplicationId",
                table: "idm_Roles");

            migrationBuilder.DropIndex(
                name: "IX_idm_Roles_IdmApplicationsApplicationId",
                table: "idm_Roles");

            migrationBuilder.DropColumn(
                name: "IdmApplicationsApplicationId",
                table: "idm_Roles");

            migrationBuilder.CreateIndex(
                name: "IX_idm_Roles_ApplicationId",
                table: "idm_Roles",
                column: "ApplicationId");

            migrationBuilder.AddForeignKey(
                name: "FK_idm_Roles_idm_Applications_ApplicationId",
                table: "idm_Roles",
                column: "ApplicationId",
                principalTable: "idm_Applications",
                principalColumn: "ApplicationId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_idm_Roles_idm_Applications_ApplicationId",
                table: "idm_Roles");

            migrationBuilder.DropIndex(
                name: "IX_idm_Roles_ApplicationId",
                table: "idm_Roles");

            migrationBuilder.AddColumn<Guid>(
                name: "IdmApplicationsApplicationId",
                table: "idm_Roles",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_idm_Roles_IdmApplicationsApplicationId",
                table: "idm_Roles",
                column: "IdmApplicationsApplicationId");

            migrationBuilder.AddForeignKey(
                name: "FK_idm_Roles_idm_Applications_IdmApplicationsApplicationId",
                table: "idm_Roles",
                column: "IdmApplicationsApplicationId",
                principalTable: "idm_Applications",
                principalColumn: "ApplicationId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
