﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Admin.API.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "idm_Applications",
                columns: table => new
                {
                    ApplicationId = table.Column<Guid>(nullable: false),
                    ApplicationName = table.Column<string>(maxLength: 256, nullable: false),
                    LoweredApplicationName = table.Column<string>(maxLength: 256, nullable: false),
                    Description = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_Applications", x => x.ApplicationId);
                });

            migrationBuilder.CreateTable(
                name: "idm_Organizations",
                columns: table => new
                {
                    OrganizationId = table.Column<Guid>(nullable: false),
                    ApplicationId = table.Column<Guid>(nullable: false),
                    ParentOrganizationId = table.Column<Guid>(nullable: false),
                    OrganizationName = table.Column<string>(nullable: false),
                    CreatedByUserId = table.Column<Guid>(nullable: false),
                    CreatedOnDate = table.Column<DateTime>(nullable: false),
                    LastModifiedByUserId = table.Column<Guid>(nullable: false),
                    LastModifiedOnDate = table.Column<DateTime>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    IsPublic = table.Column<bool>(nullable: true),
                    ThuTu = table.Column<int>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Loai = table.Column<int>(nullable: true),
                    IdPath = table.Column<string>(nullable: true),
                    Path = table.Column<string>(nullable: true),
                    Level = table.Column<int>(nullable: true),
                    HasChild = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_Organizations", x => x.OrganizationId);
                });

            migrationBuilder.CreateTable(
                name: "idm_Right",
                columns: table => new
                {
                    RightCode = table.Column<string>(maxLength: 256, nullable: false),
                    RightName = table.Column<string>(maxLength: 1024, nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Status = table.Column<bool>(nullable: true),
                    Order = table.Column<int>(nullable: true),
                    IsGroup = table.Column<bool>(nullable: false),
                    Level = table.Column<int>(nullable: false),
                    GroupCode = table.Column<string>(maxLength: 256, nullable: true),
                    IdmRightRightCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_Right", x => x.RightCode);
                    table.ForeignKey(
                        name: "FK_idm_Right_idm_Right_IdmRightRightCode",
                        column: x => x.IdmRightRightCode,
                        principalTable: "idm_Right",
                        principalColumn: "RightCode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "nav_Navigation",
                columns: table => new
                {
                    NavigationId = table.Column<Guid>(nullable: false),
                    ParentId = table.Column<Guid>(nullable: true),
                    ApplicationId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(maxLength: 256, nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    Order = table.Column<int>(nullable: true),
                    HasChild = table.Column<bool>(nullable: false),
                    UrlRewrite = table.Column<string>(maxLength: 512, nullable: true),
                    IconClass = table.Column<string>(maxLength: 50, nullable: true),
                    NavigationName_En = table.Column<string>(maxLength: 256, nullable: true),
                    CreatedByUserId = table.Column<Guid>(nullable: true),
                    CreatedOnDate = table.Column<DateTime>(nullable: true),
                    LastModifiedByUserId = table.Column<Guid>(nullable: true),
                    LastModifiedOnDate = table.Column<DateTime>(nullable: true),
                    IdPath = table.Column<string>(maxLength: 450, nullable: true),
                    Path = table.Column<string>(maxLength: 900, nullable: true),
                    Level = table.Column<int>(nullable: false),
                    SubUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_nav_Navigation", x => x.NavigationId);
                });

            migrationBuilder.CreateTable(
                name: "idm_Roles",
                columns: table => new
                {
                    RoleId = table.Column<Guid>(nullable: false),
                    ApplicationId = table.Column<Guid>(nullable: false),
                    RoleName = table.Column<string>(maxLength: 256, nullable: false),
                    LoweredRoleName = table.Column<string>(maxLength: 256, nullable: false),
                    Description = table.Column<string>(maxLength: 256, nullable: true),
                    EnableDelete = table.Column<bool>(nullable: false),
                    CreatedByUserID = table.Column<Guid>(nullable: false),
                    CreatedOnDate = table.Column<DateTime>(nullable: false),
                    LastModifiedByUserID = table.Column<Guid>(nullable: false),
                    LastModifiedOnDate = table.Column<DateTime>(nullable: false),
                    RoleCode = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_Roles", x => x.RoleId);
                    table.ForeignKey(
                        name: "FK_idm_Roles_idm_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "idm_Applications",
                        principalColumn: "ApplicationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "idm_Users",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    ApplicationId = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: false),
                    NickName = table.Column<string>(maxLength: 512, nullable: true),
                    MobilePin = table.Column<string>(nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(nullable: true),
                    LoweredUserName = table.Column<string>(maxLength: 256, nullable: false),
                    Password = table.Column<string>(maxLength: 128, nullable: true),
                    PasswordFormat = table.Column<int>(nullable: false),
                    PasswordSalt = table.Column<string>(maxLength: 128, nullable: false),
                    IsLockedOut = table.Column<bool>(nullable: false),
                    MobileAlias = table.Column<string>(maxLength: 16, nullable: true),
                    IsAnonymous = table.Column<bool>(nullable: false),
                    LastActivityDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_Users", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_idm_Users_idm_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "idm_Applications",
                        principalColumn: "ApplicationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "nav_Navigation_Role",
                columns: table => new
                {
                    NavigationRoleId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false),
                    NavigationId = table.Column<Guid>(nullable: false),
                    FromSubNavigation = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_nav_Navigation_Role", x => x.NavigationRoleId);
                    table.ForeignKey(
                        name: "FK_nav_Navigation_Role_nav_Navigation_NavigationId",
                        column: x => x.NavigationId,
                        principalTable: "nav_Navigation",
                        principalColumn: "NavigationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "idm_RightsInRole",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<Guid>(nullable: false),
                    RightCode = table.Column<string>(maxLength: 256, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_RightsInRole", x => x.Id);
                    table.ForeignKey(
                        name: "FK_idm_RightsInRole_idm_Right_RightCode",
                        column: x => x.RightCode,
                        principalTable: "idm_Right",
                        principalColumn: "RightCode",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_idm_RightsInRole_idm_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "idm_Roles",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "idm_RightsOfUser",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<Guid>(nullable: false),
                    RightCode = table.Column<string>(maxLength: 256, nullable: false),
                    InheritedFromRoles = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Inherited = table.Column<bool>(nullable: false),
                    Enable = table.Column<bool>(nullable: false),
                    ApplicationId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_RightsOfUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_idm_RightsOfUser_idm_Right_RightCode",
                        column: x => x.RightCode,
                        principalTable: "idm_Right",
                        principalColumn: "RightCode",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_idm_RightsOfUser_idm_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "idm_Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "idm_UsersInOrganization",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    OrganizationId = table.Column<Guid>(nullable: false),
                    CreatedByUserId = table.Column<Guid>(nullable: false),
                    CreatedOnDate = table.Column<DateTime>(nullable: false),
                    LastModifiedByUserId = table.Column<Guid>(nullable: false),
                    LastModifiedOnDate = table.Column<DateTime>(nullable: false),
                    Role = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_UsersInOrganization", x => x.Id);
                    table.ForeignKey(
                        name: "FK_idm_UsersInOrganization_idm_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "idm_Organizations",
                        principalColumn: "OrganizationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_idm_UsersInOrganization_idm_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "idm_Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "idm_UsersInRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: true),
                    DeleteDate = table.Column<DateTime>(nullable: true),
                    ApplicationId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_UsersInRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_idm_UsersInRoles_idm_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "idm_Roles",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_idm_UsersInRoles_idm_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "idm_Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_idm_Right_IdmRightRightCode",
                table: "idm_Right",
                column: "IdmRightRightCode");

            migrationBuilder.CreateIndex(
                name: "IX_idm_RightsInRole_RightCode",
                table: "idm_RightsInRole",
                column: "RightCode");

            migrationBuilder.CreateIndex(
                name: "IX_idm_RightsInRole_RoleId",
                table: "idm_RightsInRole",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_idm_RightsOfUser_RightCode",
                table: "idm_RightsOfUser",
                column: "RightCode");

            migrationBuilder.CreateIndex(
                name: "IX_idm_RightsOfUser_UserId",
                table: "idm_RightsOfUser",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_idm_Roles_ApplicationId",
                table: "idm_Roles",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_idm_Users_ApplicationId",
                table: "idm_Users",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_idm_UsersInOrganization_OrganizationId",
                table: "idm_UsersInOrganization",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_idm_UsersInOrganization_UserId",
                table: "idm_UsersInOrganization",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_idm_UsersInRoles_RoleId",
                table: "idm_UsersInRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_idm_UsersInRoles_UserId",
                table: "idm_UsersInRoles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_nav_Navigation_Role_NavigationId",
                table: "nav_Navigation_Role",
                column: "NavigationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "idm_RightsInRole");

            migrationBuilder.DropTable(
                name: "idm_RightsOfUser");

            migrationBuilder.DropTable(
                name: "idm_UsersInOrganization");

            migrationBuilder.DropTable(
                name: "idm_UsersInRoles");

            migrationBuilder.DropTable(
                name: "nav_Navigation_Role");

            migrationBuilder.DropTable(
                name: "idm_Right");

            migrationBuilder.DropTable(
                name: "idm_Organizations");

            migrationBuilder.DropTable(
                name: "idm_Roles");

            migrationBuilder.DropTable(
                name: "idm_Users");

            migrationBuilder.DropTable(
                name: "nav_Navigation");

            migrationBuilder.DropTable(
                name: "idm_Applications");
        }
    }
}
