using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Business;
using ApiUtils;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Versioning;
using StackExchange.Redis;
using Microsoft.OpenApi.Models;
using CommonUtils;

namespace Admin.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "Custom Scheme";
                options.DefaultChallengeScheme = "Custom Scheme";
            })
            .AddCustomAuth(o => { });

            services.AddAutoMapper(typeof(Startup));
            services.AddCustomRouting();
            services.AddResponseCaching();
            services.AddCustomResponseCompression();
            services.AddCustomStrictTransportSecurity();

            if (Utils.GetConfig("Redis:Enable") == "true")
            {
                var redisCNN = Configuration.GetSection("Redis")["ConnectionString"];
                var redisIsntanceName = Configuration.GetSection("Redis")["InstanceName"];
                // Register the RedisCache service
                services.AddStackExchangeRedisCache(options =>
                {
                    options.Configuration = redisCNN;
                    options.InstanceName = !string.IsNullOrEmpty(redisIsntanceName) ? redisIsntanceName : "Instance-";
                });
            }

            services.AddApiVersioning(apiVersioningOptions =>
            {
                apiVersioningOptions.ReportApiVersions = true;
                apiVersioningOptions.ApiVersionReader = new HeaderApiVersionReader("API");
            });


            // services.AddControllers();
            services
                //.AddAppInsight(Configuration)
                //.AddGrpc().Services
                .AddCustomMVC(Configuration)
                .AddCustomDbContext(Configuration)
                .AddCustomOptions(Configuration)
                .AddIntegrationServices(Configuration)
                .AddEventBus(Configuration)
                .AddProjectServices()
                .AddCustomRouting()
                .AddCustomApiVersioning()
                // .AddAuthorization()
                //.AddSwaggerGen(c =>
                //{
                //    c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebApi", Version = "v1" });
                //})
                .AddSwaggerGen()
                .AddCors();
            services.AddControllers();
            //.AddCustomHealthCheck(Configuration);

            var container = new ContainerBuilder();
            container.Populate(services);

            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            var pathBase = Configuration["PATH_BASE"];

            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger<Startup>().LogDebug("Using PATH BASE '{pathBase}'", pathBase);
                app.UsePathBase(pathBase);
            }

            app.UseSwagger()
             .UseSwaggerUI(c =>
             {
                 c.SwaggerEndpoint($"{ (!string.IsNullOrEmpty(pathBase) ? pathBase : string.Empty) }/swagger/v1/swagger.json", "Admin.API Version 1");
             });

            app.UseDefaultFiles(new DefaultFilesOptions()
            {
                DefaultFileNames = new List<string>() { "index.html" },
                RequestPath = new PathString("")
            });
            app.UseHttpsRedirection();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseDeveloperExceptionPage();
            app.UseCors(
                //options => options.WithOrigins("http://localhost:4200").AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin()

                options => options.WithOrigins("*").AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin()
            );
            app.UseMvc();

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            // app.UseEndpoints(endpoints => endpoints.MapControllers());

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapDefaultControllerRoute();
            //    endpoints.MapControllers();
            //    endpoints.MapGet("/_proto/", async ctx =>
            //    {
            //        ctx.Response.ContentType = "text/plain";
            //        using var fs = new FileStream(Path.Combine(env.ContentRootPath, "Proto", "catalog.proto"), FileMode.Open, FileAccess.Read);
            //        using var sr = new StreamReader(fs);
            //        while (!sr.EndOfStream)
            //        {
            //            var line = await sr.ReadLineAsync();
            //            if (line != "/* >>" || line != "<< */")
            //            {
            //                await ctx.Response.WriteAsync(line);
            //            }
            //        }
            //    });
            //    //endpoints.MapGrpcService<CatalogService>();
            //    //endpoints.MapHealthChecks("/hc", new HealthCheckOptions()
            //    //{
            //    //    Predicate = _ => true,
            //    //    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            //    //});
            //    //endpoints.MapHealthChecks("/liveness", new HealthCheckOptions
            //    //{
            //    //    Predicate = r => r.Name.Contains("self")
            //    //});
            //});

            var enableBus = Configuration["EnableBus"];
            if (enableBus == "1")
            {
                EventHandling.ConfigureEventBus(app);
            }
            // ke thua cache
            Bootstrapping.ConfigureBootstrapping(app);
            //ConfigureSocketIo(app);
        }
        protected virtual void ConfigureEventBus(IApplicationBuilder app)
        {
            //var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            //eventBus.Subscribe<OrderStatusChangedToAwaitingValidationIntegrationEvent, OrderStatusChangedToAwaitingValidationIntegrationEventHandler>();
            //eventBus.Subscribe<OrderStatusChangedToPaidIntegrationEvent, OrderStatusChangedToPaidIntegrationEventHandler>();
        }
    }
}
