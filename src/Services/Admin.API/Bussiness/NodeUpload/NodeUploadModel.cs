﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin.API.Business.NodeUpload
{
    /// <summary>
    /// Dữ liệu kết quả upload vào tạo node
    /// </summary>
    public class NodeUploadResult
    {
        /// <summary>
        /// Id của file
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Tên file
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Tên vật lý
        /// </summary>
        public string PhysicalName { get; set; }
        /// <summary>
        /// Kích cỡ tập tin
        /// </summary>
        public long Size { get; set; }
        /// <summary>
        /// Định dạng tập tin
        /// </summary>
        public string Extension { get; set; }
        /// <summary>
        /// Đường đẫn logic
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Đường đẫn vật lý
        /// </summary>
        public string PhysicalPath { get; set; }
    }
    /// <summary>
    /// Dữ liệu kết quả upload file vật lý
    /// </summary>
    public class FileUploadResult
    {
        public int Status { get; set; }
        public string Message { get; set; }

        /// <summary>
        /// Tên vật lý
        /// </summary>
        public string PhysicalName { get; set; }
        /// <summary>
        /// Tên file vật lý lưu trên ổ đĩa
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Kích thước của file upload lên
        /// </summary>
        public long Size { get; set; }
        /// <summary>
        /// Đuôi file
        /// </summary>
        public string Extension { get; set; }
        /// <summary>
        /// Đường dẫn tuyệt đối vật lý
        /// </summary>
        public string PhysicalPath { get; set; }
        /// <summary>
        /// Đường dẫn tương đối vật lý
        /// </summary>
        public string RelativePath { get; set; }
        public string AbsolutePath { get; set; }
    }
}
