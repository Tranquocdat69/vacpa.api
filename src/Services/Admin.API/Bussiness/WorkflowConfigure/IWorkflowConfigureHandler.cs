﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonUtils;
using DataUtils;

namespace Business
{
    public interface IWorkflowConfigureHandler
    {
        Task<Response> GetAll();
        Task<Response> GetById(Guid id);
        Task<Response> GetByFilter(WorkflowConfigureQueryModel model);
        Response IsAdministratorRole(Guid? id);
        Task<Response> GetAllWF();
        Task<Response> GetWFForFunction_navId(Guid? navId, Guid? provinceId, Guid? districtId, Guid? wardId, Guid? donViId);
        Task<Response> GetWFForFunction_url(string url, Guid? provinceId, Guid? districtId, Guid? wardId, Guid? donViId);

        Task<Response> Create(WorkflowConfigureCreateModel model);
        Task<Response> Update(Guid id, WorkflowConfigureUpdateModel model);
        Task<Response> Delete(Guid id);
        Task<Response> DeleteMany(List<Guid> ids);
    }
}
