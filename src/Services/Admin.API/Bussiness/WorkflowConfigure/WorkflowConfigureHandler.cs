﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using DataUtils;
using Microsoft.EntityFrameworkCore;
using ApiUtils;

namespace Business
{
    public class WorkflowConfigureHandler : IWorkflowConfigureHandler
    {
        private readonly IWorkflowHandler _workflowHandler;
        private readonly string playoutService = Utils.GetConfig("playout:apiPlayoutUri");
        //private readonly IDeviceHandler _deviceHandler;
        private readonly string _ipfmUri = Utils.GetConfig("Authentication:IPFM:Uri");
        public WorkflowConfigureHandler(IWorkflowHandler workflowHandler)
        {
            _workflowHandler = workflowHandler;
            _ipfmUri = Utils.GetConfig("Authentication:IPFM:Uri");

        }

        public async Task<Response> GetAll()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = unitOfWork.GetRepository<WorkflowConfigure>().GetAll();

                    data = data.OrderByDescending(s => s.CreatedOnDate);

                    var queryResult = await data.ToListAsync();

                    if (queryResult.Count != 0)
                    {
                        var retData = new List<WorkflowConfigureModel>();
                        foreach (var item in queryResult)
                        {
                            var convertItem = AutoMapperUtils.AutoMap<WorkflowConfigure, WorkflowConfigureModel>(item);
                            retData.Add(convertItem);
                        }
                        return new ResponseList<WorkflowConfigureModel>(retData, "Get data successfully", Code.Success);
                    }
                    else
                    {
                        return new ResponseList<WorkflowConfigureModel>(null, "No data found", Code.NotFound);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<WorkflowConfigureModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> GetById(Guid id)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unitOfWork.GetRepository<WorkflowConfigure>().Get(x => x.Id == id).FirstOrDefaultAsync();
                    if (data != null)
                    {
                        var convertItem = AutoMapperUtils.AutoMap<WorkflowConfigure, WorkflowConfigureModel>(data);
                        return new ResponseObject<WorkflowConfigureModel>(convertItem, "Get data successfully", Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<WorkflowConfigureModel>(null, "Id not found", Code.NotFound);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        public async Task<Response> GetByFilter(WorkflowConfigureQueryModel filter)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = unitOfWork.GetRepository<WorkflowConfigure>().GetAll();
                    var donvi = unitOfWork.GetRepository<IdmOrganizations>().GetAll();
                    var navigation = unitOfWork.GetRepository<Navigation>().GetAll();
                    // get data from Provice (PlayoutDB)
                    //var resApiHandler = new ResApiHandler<ProviceResponseModel, dynamic>(CommonUtils.ResApiTarget.Ipfm);
                    //ProviceResponseModel getProvinceResponse = await resApiHandler.Get(playoutService + "cat/province");

                    // Lọc theo DonViKhuVuc, Chức năng và wf name
                    if (filter.DonViId.HasValue)
                    {
                        data = data.Where(x => x.DonViId == filter.DonViId.Value);
                    }
                    if (filter.ProvinceId.HasValue)
                    {
                        data = data.Where(x => x.ProvinceId == filter.ProvinceId.Value);
                    }
                    if (filter.DistrictId.HasValue)
                    {
                        data = data.Where(x => x.DistrictId == filter.DistrictId.Value);
                    }
                    if (filter.WardId.HasValue)
                    {
                        data = data.Where(x => x.WardId == filter.WardId.Value);
                    }
                    if (!string.IsNullOrEmpty(filter.FullTextSearch))
                    {
                        var text = filter.FullTextSearch.ToLower().Trim();
                        data = data.Where(x => x.ListFunctionName.ToLower().Contains(text) || x.WorkflowCode.ToLower().Contains(text));
                    }
                    // filter by date sort
                    if (filter.Sort == "-LastModifiedOnDate")
                        data = data.OrderByDescending(x => x.CreatedOnDate);

                    // tính tổng số bản ghi trả về theo filter
                    var totalCount = data.Count();

                    if (!filter.IsGetAll)
                    {
                        if (!filter.Size.HasValue)
                        {
                            filter.Size = 20;
                        }
                        filter.Page = filter.Page - 1;
                        int excludedRows = ((int)filter.Page) * ((int)filter.Size);
                        if (excludedRows <= 0)
                        {
                            excludedRows = 0;
                        }
                        data = data.Skip(excludedRows).Take((int)filter.Size);
                    }

                    var fullQueryData = AutoMapperUtils.AutoMap<WorkflowConfigure, WorkflowConfigureModel>(await data.ToListAsync());
                    //Get Provice 

                    foreach (var item in fullQueryData)
                    {
                        var khuVucDonVi = "";
                        if (item.IsCenter)
                        {
                            var donViName = donvi.Where(x => x.OrganizationId == item.DonViId).FirstOrDefault();
                            if (donViName != null)
                            {
                                khuVucDonVi = donViName.OrganizationName;
                            }
                            else
                            {
                                khuVucDonVi = "Chưa xác định";
                            }

                        }
                        else
                        {
                            //var findProvice = getProvinceResponse.Data.Where(x => x.Id == item.ProvinceId.ToString()).Select(x=>x.Name).FirstOrDefault();
                            var area = "";

                            if (item.ProvinceName != null && item.ProvinceId != Guid.Empty &&
                               item.DistrictName != null && item.DistrictId != Guid.Empty &&
                               item.WardName != null && item.WardId != Guid.Empty)
                            {
                                area = item.ProvinceName + ", " + item.DistrictName + ", " + item.WardName;
                            }
                            else if (item.ProvinceName != null && item.ProvinceId != Guid.Empty &&
                               item.DistrictName != null && item.DistrictId != Guid.Empty)
                            {
                                area = item.ProvinceName + ", " + item.DistrictName;
                            }
                            else if (item.ProvinceName != null && item.ProvinceId != Guid.Empty)
                            {
                                area = item.ProvinceName;
                            }
                            else
                            {
                                area = null;
                            }

                            khuVucDonVi = area;
                        }
                        item.DonViKhuVuc = khuVucDonVi;
                    }
                    // Get Navigation

                    foreach (var item in fullQueryData)
                    {
                        var listNav = "";
                        // Gán nav bằng chuỗi ban đầu
                        var nav = item.FunctionList;
                        //Replace ký tự dể thành mảng chuỗi đơn giản nhất. Các ký tự phân tách nhau bằng dấu ","
                        nav = nav.Replace("[\"", "");
                        nav = nav.Replace("\"]", "");
                        nav = nav.Replace("\"", "");

                        string[] arrListStr = nav.Split(new char[] { ',' });
                        for (int i = 0; i < arrListStr.Length; i++)
                        {
                            var navName = navigation.Where(x => x.NavigationId.ToString() == arrListStr[i]).FirstOrDefault();
                            if (navName != null)
                            {
                                listNav += navName.Name + ",";
                            }
                            else
                            {
                                listNav += "Ko xác định" + ",";
                            }
                        }
                        listNav = listNav.Substring(0, listNav.LastIndexOf(','));
                        //int a = listNav.LastIndexOf(',');
                        //listNav.Remove(a);
                        //listNav.TrimEnd(',');
                        item.FunctionList = listNav;
                    }

                    var response = new Pagination<WorkflowConfigureModel>();
                    response.Content = fullQueryData;
                    response.NumberOfElements = totalCount;
                    response.Page = filter.Page.HasValue ? filter.Page.Value : 0;
                    response.Size = filter.Size.HasValue ? filter.Size.Value : 0;
                    response.TotalElements = totalCount;
                    response.TotalPages = totalCount / (filter.Size.HasValue ? (filter.Size.Value + 1) : 1);

                    var r = new ResponsePagination<WorkflowConfigureModel>(response);

                    return r;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<WorkflowConfigureModel>(null, ex.Message, Code.ServerError);
            }
        }
        public async Task<Response> Create(WorkflowConfigureCreateModel model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var navigation = unitOfWork.GetRepository<Navigation>().GetAll();
                    // Check chức năng mới truyền vào đã tôn tại chưa?
                    var listNav = "";
                    // Gán nav bằng chuỗi ban đầu
                    var nav = model.FunctionList;
                    //Replace ký tự dể thành mảng chuỗi đơn giản nhất. Các ký tự phân tách nhau bằng dấu ","
                    nav = nav.Replace("[\"", "");
                    nav = nav.Replace("\"]", "");
                    nav = nav.Replace("\"", "");

                    string[] arrListStr = nav.Split(new char[] { ',' });
                    for (int i = 0; i < arrListStr.Length; i++)
                    {
                        var nav1 = unitOfWork.GetRepository<WorkflowConfigure>().Get(a => a.DonViId == model.DonViId && a.FunctionList.Contains(arrListStr[i])).FirstOrDefault();
                        var nav2 = unitOfWork.GetRepository<WorkflowConfigure>().Get(a => a.ProvinceId == model.ProvinceId && a.DistrictId == model.DistrictId && 
                        a.WardId == model.WardId && a.FunctionList.Contains(arrListStr[i])).FirstOrDefault();

                        if (nav1 != null && nav1.DonViId != null)
                        {
                            return new ResponseObject<WorkflowConfigureModel>(null, "Quy trình đã tồn tại cho chức năng, đơn vị này!", Code.BadRequest);
                        }
                        if (nav2 != null && nav2.ProvinceId != null)
                        {
                            return new ResponseObject<WorkflowConfigureModel>(null, "Quy trình đã tồn tại cho chức năng, khu vực này!", Code.BadRequest);
                        }
                    }
                    var listFunctionName = "";
                    // Gán nav bằng chuỗi ban đầu
                    var navi = model.FunctionList;
                    //Replace ký tự dể thành mảng chuỗi đơn giản nhất. Các ký tự phân tách nhau bằng dấu ","
                    navi = navi.Replace("[\"", "");
                    navi = navi.Replace("\"]", "");
                    navi = navi.Replace("\"", "");
                    string[] arrListStr1 = nav.Split(new char[] { ',' });
                    for (int i = 0; i < arrListStr1.Length; i++)
                    {
                        var navName = navigation.Where(x => x.NavigationId.ToString() == arrListStr1[i]).FirstOrDefault();
                        if (navName != null)
                        {
                            listNav += navName.Name + ",";
                        }
                        else
                        {
                            listNav += "Ko xác định" + ",";
                        }
                    }
                    listNav = listNav.Substring(0, listNav.LastIndexOf(','));

                    var insertItem = AutoMapperUtils.AutoMap<WorkflowConfigureCreateModel, WorkflowConfigure>(model);
                    insertItem.Id = Guid.NewGuid();
                    insertItem.CreatedByUserId = model.CreatedByUserId;
                    insertItem.CreatedOnDate = DateTime.Now;
                    insertItem.ListFunctionName = listNav;
                    unitOfWork.GetRepository<WorkflowConfigure>().Add(insertItem);
                    if (unitOfWork.Save() >= 1)
                    {
                        var insertedItem = unitOfWork.GetRepository<WorkflowConfigure>().Get(x => x.Id == insertItem.Id).FirstOrDefault();
                        return new ResponseObject<WorkflowConfigureModel>(AutoMapperUtils.AutoMap<WorkflowConfigure, WorkflowConfigureModel>(insertedItem), "Create successfully", Code.Success);
                    }
                    return new ResponseObject<WorkflowConfigureModel>(null, "Error with saving", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<WorkflowConfigureModel>(null, ex.Message, Code.ServerError);
            }
        }
        public async Task<Response> Update(Guid id, WorkflowConfigureUpdateModel model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var navigation = unitOfWork.GetRepository<Navigation>().GetAll();
                    // Check chức năng mới truyền vào đã tôn tại chưa?
                    var listNav = "";
                    var existItem = await unitOfWork.GetRepository<WorkflowConfigure>().Get(x => x.Id == id).FirstOrDefaultAsync();
                    if (existItem != null)
                    {
                        var listFunctionName = "";
                        // Gán nav bằng chuỗi ban đầu
                        var navi = model.FunctionList;
                        //Replace ký tự dể thành mảng chuỗi đơn giản nhất. Các ký tự phân tách nhau bằng dấu ","
                        navi = navi.Replace("[\"", "");
                        navi = navi.Replace("\"]", "");
                        navi = navi.Replace("\"", "");
                        string[] arrListStr1 = navi.Split(new char[] { ',' });
                        for (int i = 0; i < arrListStr1.Length; i++)
                        {
                            var navName = navigation.Where(x => x.NavigationId.ToString() == arrListStr1[i]).FirstOrDefault();
                            if (navName != null)
                            {
                                listNav += navName.Name + ",";
                            }
                            else
                            {
                                listNav += "Ko xác định" + ",";
                            }
                        }
                        listNav = listNav.Substring(0, listNav.LastIndexOf(','));

                        existItem.IsCenter = model.IsCenter;
                        existItem.WorkflowCode = model.WorkflowCode;
                        existItem.ProvinceId = model.ProvinceId;
                        existItem.DistrictId = model.DistrictId;
                        existItem.WardId = model.WardId;
                        existItem.ProvinceName = model.ProvinceName;
                        existItem.DistrictName = model.DistrictName;
                        existItem.WardName = model.WardName;
                        existItem.DonViId = model.DonViId;
                        existItem.FunctionList = model.FunctionList;
                        existItem.Status = model.Status;
                        existItem.LastModifiedByUserId = model.LastModifiedByUserId;
                        existItem.LastModifiedOnDate = DateTime.Now;
                        existItem.ListFunctionName = listNav;
                        unitOfWork.GetRepository<WorkflowConfigure>().Update(existItem);
                        if (unitOfWork.Save() > 0)
                        {
                            var convertItem = AutoMapperUtils.AutoMap<WorkflowConfigure, WorkflowConfigureModel>(existItem);
                            return new ResponseObject<WorkflowConfigureModel>(convertItem, "Update successfully", Code.Success);
                        }
                        return new ResponseObject<WorkflowConfigureModel>(null, "Update error", Code.ServerError);
                    }
                    else
                    {
                        return new ResponseObject<WorkflowConfigureModel>(null, "Id not found", Code.NotFound);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<WorkflowConfigureModel>(null, ex.Message, Code.ServerError);
            }
        }
        public async Task<Response> Delete(Guid id)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var existItem = await unitOfWork.GetRepository<WorkflowConfigure>().Get(x => x.Id == id).FirstOrDefaultAsync();
                    if (existItem != null)
                    {
                        unitOfWork.GetRepository<WorkflowConfigure>().Delete(existItem);
                        if (unitOfWork.Save() >= 0)
                        {
                            var deletedItem = AutoMapperUtils.AutoMap<WorkflowConfigure, WorkflowConfigureModel>(existItem);
                            return new ResponseObject<WorkflowConfigureModel>(deletedItem, "Delete successfully!", Code.Success);
                        }
                        else
                        {
                            return new ResponseObject<WorkflowConfigureModel>(null, "Delete failed!", Code.ServerError);
                        }
                    }
                    else
                    {
                        return new ResponseObject<WorkflowConfigureModel>(null, "Id not found", Code.NotFound);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<WorkflowConfigureModel>(null, ex.Message, Code.ServerError);
            }
        }
        public async Task<Response> DeleteMany(List<Guid> ids)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    unitOfWork.GetRepository<WorkflowConfigure>().DeleteRange(x => ids.Contains(x.Id));
                    if (unitOfWork.Save() >= 0)
                    {
                        return new ResponseDeleteMulti() { Code = Code.Success, Message = "Delete successfully" };
                    }
                    else
                    {
                        return new Response(Code.ServerError, "Delete failed!");
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<WorkflowConfigure>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> GetAllWF()
        {
            try
            {
                var result = await _workflowHandler.GetWFName();
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        // Lấy wf cho chức năng với input: Id chức năng, Id khu vực, Id đơn vị
        public async Task<Response> GetWFForFunction_navId(Guid? navId, Guid? provinceId, Guid? districtId, Guid? wardId, Guid? donViId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    // Kiểm tra xem co ton tai area hay donvi ko?
                    if (donViId == null && provinceId == null)
                    {
                        return new ResponseObject<string>(null, "WF di cung voi khu vưc hoac don vi", Code.Success);
                    }
                    else
                    {
                        // Kiem tra xem navId co ton tai ko
                        var isNav = unitOfWork.GetRepository<Navigation>().Get(a => a.NavigationId == navId).FirstOrDefault();
                        if (isNav != null)
                        {
                            //check wf theo khu vực hay đơn vị
                            if (donViId != null)
                            {
                                var wfDonVi = unitOfWork.GetRepository<WorkflowConfigure>().Get(a => a.DonViId == donViId && a.FunctionList.Contains(navId.ToString())).FirstOrDefault();
                                if (wfDonVi != null)
                                {
                                    return new ResponseObject<string>(AutoMapperUtils.AutoMap<WorkflowConfigure, WorkflowConfigureModel>(wfDonVi).WorkflowCode, "Ton tai wf cho Don vi", Code.Success);
                                }
                                else
                                {
                                    // Check theo cac chuc nang dc cau hinh WF mac dinh
                                    if (navId.ToString() == "e88159f0-5398-4493-bccf-2f2989b1eb09")//Quản lý lịch phát chương trình
                                    {
                                        return new ResponseObject<string>("WF_FMIP_DATLICH_DEFAULT", "Dung wf mac dinh nay nhe", Code.Success);
                                    }
                                    if (navId.ToString() == "1c6bfa0e-d37a-4013-8a7d-e20304fcb5f9")//D/S đăng ký nội dung chương trình
                                    {
                                        return new ResponseObject<string>("WF_FMIP_BIENTAPNOIDUNG_DEFAULT", "Dung wf mac dinh nay nhe", Code.Success);
                                    }
                                }
                                return new ResponseObject<string>(null, "Ko dinh nghia wf cho chuc nang nay!", Code.Success);
                            }
                            else
                            {
                                var wfArea = unitOfWork.GetRepository<WorkflowConfigure>().Get(a => a.ProvinceId == provinceId && a.DistrictId == districtId && a.WardId == wardId && a.FunctionList.Contains(navId.ToString())).FirstOrDefault();
                                if (wfArea != null)
                                {
                                    return new ResponseObject<string>(AutoMapperUtils.AutoMap<WorkflowConfigure, WorkflowConfigureModel>(wfArea).WorkflowCode, "Ton tai wf cho Khu vuc", Code.Success);
                                }
                                else
                                {
                                    // Check theo cac chuc nang dc cau hinh WF mac dinh
                                    if (navId.ToString() == "e88159f0-5398-4493-bccf-2f2989b1eb09")//Quản lý lịch phát chương trình
                                    {
                                        return new ResponseObject<string>("WF_FMIP_DATLICH_DEFAULT", "Dung wf mac dinh nay nhe", Code.Success);
                                    }
                                    if (navId.ToString() == "1c6bfa0e-d37a-4013-8a7d-e20304fcb5f9")//D/S đăng ký nội dung chương trình
                                    {
                                        return new ResponseObject<string>("WF_FMIP_BIENTAPNOIDUNG_DEFAULT", "Dung wf mac dinh nay nhe", Code.Success);
                                    }
                                }
                                return new ResponseObject<string>(null, "Ko dinh nghia wf cho chuc nang nay!", Code.Success);


                            }
                        }
                        return new ResponseObject<string>(null, "Ko ton tai id chuc nang nay", Code.Success);

                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        // Lấy wf cho chức năng với input: url chức năng, Id khu vực, Id đơn vị
        public async Task<Response> GetWFForFunction_url(string url, Guid? provinceId, Guid? districtId, Guid? wardId, Guid? donViId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    // Kiểm tra xem co ton tai area hay donvi ko?
                    if (donViId == null && provinceId == null)
                    {
                        return new ResponseObject<string>(null, "WF di cung voi khu vưc hoac don vi", Code.Success);
                    }
                    else
                    {
                        //Kiểm tra xem url đó có tồn tại ko và là chức năng nào
                        var nav = unitOfWork.GetRepository<Navigation>().Get(a => a.UrlRewrite == url).FirstOrDefault();

                        if (nav != null)
                        {
                            //Id chức năng 
                            var navId = nav.NavigationId;
                            //check wf theo khu vực hay đơn vị
                            if (donViId != null)
                            {
                                var wfDonVi = unitOfWork.GetRepository<WorkflowConfigure>().Get(a => a.DonViId == donViId && a.FunctionList.Contains(navId.ToString())).FirstOrDefault();
                                if (wfDonVi != null)
                                {
                                    return new ResponseObject<string>(AutoMapperUtils.AutoMap<WorkflowConfigure, WorkflowConfigureModel>(wfDonVi).WorkflowCode, "Ton tai wf cho Don vi", Code.Success);
                                }
                                else
                                {
                                    // Check theo cac chuc nang dc cau hinh WF mac dinh
                                    if (navId.ToString() == "e88159f0-5398-4493-bccf-2f2989b1eb09")//Quản lý lịch phát chương trình
                                    {
                                        return new ResponseObject<string>("WF_FMIP_DATLICH_DEFAULT", "Dung wf mac dinh nay nhe", Code.Success);
                                    }
                                    if (navId.ToString() == "1c6bfa0e-d37a-4013-8a7d-e20304fcb5f9")//D/S đăng ký nội dung chương trình
                                    {
                                        return new ResponseObject<string>("WF_FMIP_BIENTAPNOIDUNG_DEFAULT", "Dung wf mac dinh nay nhe", Code.Success);
                                    }
                                }
                                return new ResponseObject<string>(null, "Ko dinh nghia wf cho chuc nang nay!", Code.Success);
                            }
                            else
                            {
                                var wfArea = unitOfWork.GetRepository<WorkflowConfigure>().Get(a => a.ProvinceId == provinceId && a.DistrictId == districtId && a.WardId == wardId && a.FunctionList.Contains(navId.ToString())).FirstOrDefault();
                                if (wfArea != null)
                                {
                                    return new ResponseObject<string>(AutoMapperUtils.AutoMap<WorkflowConfigure, WorkflowConfigureModel>(wfArea).WorkflowCode, "Ton tai wf cho Khu vuc", Code.Success);
                                }
                                else
                                {
                                    // Check theo cac chuc nang dc cau hinh WF mac dinh
                                    if (navId.ToString() == "e88159f0-5398-4493-bccf-2f2989b1eb09")//Quản lý lịch phát chương trình
                                    {
                                        return new ResponseObject<string>("WF_FMIP_DATLICH_DEFAULT", "Dung wf mac dinh nay nhe", Code.Success);
                                    }
                                    if (navId.ToString() == "1c6bfa0e-d37a-4013-8a7d-e20304fcb5f9")//D/S đăng ký nội dung chương trình
                                    {
                                        return new ResponseObject<string>("WF_FMIP_BIENTAPNOIDUNG_DEFAULT", "Dung wf mac dinh nay nhe", Code.Success);
                                    }
                                }
                                return new ResponseObject<string>(null, "Ko dinh nghia wf cho chuc nang nay!", Code.Success);
                            }
                        }
                        else
                        {
                            return new ResponseObject<string>(null, "Ko ton tai duong dan nay", Code.Success);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        // Kiem tra user co trong nhom quyen QTHT hay ko ?
        public Response IsAdministratorRole(Guid? userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = from a in unitOfWork.GetRepository<IdmRoles>().GetAll()
                               join b in unitOfWork.GetRepository<IdmUsersInRoles>().GetAll() on a.RoleId equals b.RoleId
                               join c in unitOfWork.GetRepository<IdmUsers>().GetAll() on b.UserId equals c.UserId
                               where c.UserId == userId && a.RoleCode == "QTHT"
                               select new
                               {
                                   UserID = c.UserId,
                               };
                    if (data.Count() > 0)
                    {
                        return new ResponseObject<bool>(true);
                    }
                    else
                    {
                        return new ResponseObject<bool>(false);
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResponseObject<bool>(false);
            }
        }
    }
}
