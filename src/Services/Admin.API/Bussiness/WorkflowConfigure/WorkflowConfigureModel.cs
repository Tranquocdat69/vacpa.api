﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class WorkflowConfigureModel
    {
        public Guid Id { get; set; }
        public bool IsCenter { get; set; } = true;//true: WF cho cấp Trung ương. false: cho Tỉnh/Quận Huyện   
        public Guid? ProvinceId { get; set; }
        public Guid? DistrictId { get; set; }
        public Guid? WardId { get; set; }
        public Guid? DonViId { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string WorkflowCode { get; set; }
        public string FunctionList { get; set; }
        public bool Status { get; set; } = true;
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        // Khác 
        public string DonViKhuVuc { get; set; }
        public string WorkflowName { get; set; }
        public string ListFunctionName { get; set; }
    }

    public class WorkflowConfigureQueryModel : PaginationRequest
    {
        public Guid? ProvinceId { get; set; }
        public Guid? DistrictId { get; set; }
        public Guid? WardId { get; set; }
        public bool IsGetAll { get; set; } = false;
        public Guid Id { get; set; }
        public Guid? DonViId { get; set; }
        public string WorkflowCode { get; set; }
        public string WorkflowName { get; set; }
        public string ListFunctionName { get; set; }
    }
    public class WorkflowConfigureCreateModel
    {
        public bool IsCenter { get; set; } = true;//true: WF cho cấp Trung ương. false: cho Tỉnh/Quận Huyện   
        public Guid? ProvinceId { get; set; }
        public Guid? DistrictId { get; set; }
        public Guid? WardId { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public Guid? DonViId { get; set; }
        public string WorkflowCode { get; set; }
        public string FunctionList { get; set; }
        public bool Status { get; set; } = true;
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public string ListFunctionName { get; set; }
    }
    public class WorkflowConfigureUpdateModel
    {
        public bool IsCenter { get; set; } = true;//true: WF cho cấp Trung ương. false: cho Tỉnh/Quận Huyện   
        public Guid? ProvinceId { get; set; }
        public Guid? DistrictId { get; set; }
        public Guid? WardId { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public Guid? DonViId { get; set; }
        public string WorkflowCode { get; set; }
        public string FunctionList { get; set; }
        public bool Status { get; set; } = true;
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public string ListFunctionName { get; set; }
    }

    public class ProvinceData
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string IssueModificationDecision { get; set; }
        public DateTime? IssuedDecisionDate { get; set; }
        public string IssuedDepartment { get; set; }
        public int Status { get; set; }
        public int Version { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public string lastModifiedByUserId { get; set; }
        public DateTime? CreateOnDate { get; set; }
        public string CreateByUserId { get; set; }
        public string district { get; set; }

    }
    public class ProviceResponseModel
    {
        public List<ProvinceData> Data { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public int TotalTime { get; set; }
        public string DataCount { get; set; }
        public string Status { get; set; }

    }
    public class WorkflowResponseModel
    {
        public string Code { get; set; }
        public string Scheme { get; set; }
    }
}
