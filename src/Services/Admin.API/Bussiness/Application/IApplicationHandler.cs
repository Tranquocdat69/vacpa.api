﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonUtils;
using DataUtils;

namespace Business
{
    public interface IApplicationHandler
    {
        Task<List<ApplicationModel>> GetAll();
        Task<Response> GetAllFromCache();
        Task<Response> CreateAsync(ApplicationCreateModel model);
        Task<Response> GetByFilterAsync(ApplicationQueryModel model);
        Task<Response> GetByIdAsync(Guid id);
        Task<Response> UpdateAsync(ApplicationUpdateModel model);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteManyAsync(List<Guid> ids);
    }
}