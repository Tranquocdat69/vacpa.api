﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonUtils;
using DataUtils;
using Infrastructure;

namespace Business
{
    public class ApplicationCollection
    {
        private readonly IApplicationHandler _handler;
        public HashSet<ApplicationModel> Collection = new HashSet<ApplicationModel>();

        protected ApplicationCollection(IApplicationHandler handler)
        {
            _handler = handler;
            //LoadToHashSet();
        }
        protected ApplicationCollection()
        {
            _handler = new ApplicationHandler();
            //LoadToHashSet();
        }

        public static ApplicationCollection Instance { get; } = new ApplicationCollection();

        public void LoadToHashSet()
        {
            Collection = new HashSet<ApplicationModel>();
            var listResponse = _handler.GetAll().Result;
            if (listResponse != null)
            {
                // Add to hashset
                foreach (var item in listResponse)
                    Collection.Add(item);
            }
        }

        public string GetName(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.ApplicationId == id);
            return result?.ApplicationName;
        }

        public ApplicationModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.ApplicationId == id);
            if (result == null)
            {
                result = new ApplicationModel();
            }
            return result;
        }

        public List<ApplicationModel> GetAll()
        {
            if (Collection == null || Collection.Count == 0)
            {
                LoadToHashSet();
                return Collection.ToList();
            }
            {
                return Collection.ToList();
            }
        }
    }
}
