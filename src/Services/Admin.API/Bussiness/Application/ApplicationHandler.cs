﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataUtils;
using Microsoft.EntityFrameworkCore;
using Serilog;
using CommonUtils;
using Infrastructure;
using ApiUtils;

namespace Business
{
    public class ApplicationHandler : IApplicationHandler
    {
        //public IEventBus _eventBus;

        public ApplicationHandler()
        {
        }

        // public ApplicationHandler(IEventBus eventBus)
        // {
        //     _eventBus = eventBus;
        // }

        public async Task<List<ApplicationModel>> GetAll()
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = unit.GetRepository<IdmApplications>().GetAll();
                    if (data.Any())
                    {
                        var response = new List<ApplicationModel>();
                        foreach (var item in data)
                        {
                            response.Add(AutoMapperUtils.AutoMap<IdmApplications, ApplicationModel>(item));
                        }
                        return response;
                    }
                    else
                        return new List<ApplicationModel>();
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new List<ApplicationModel>();
            }
        }

        public async Task<Response> GetAllFromCache()
        {
            try
            {
                return new ResponseList<ApplicationModel>(ApplicationCollection.Instance.GetAll(), "Get data successfully", Code.Success);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseList<ApplicationModel>(null, ex.Message, Code.ServerError);

            }
        }

        public async Task<Response> GetByFilterAsync(ApplicationQueryModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = from dt in unit.GetRepository<IdmApplications>().GetAll().OrderBy(x => x.ApplicationId) select dt;
                    if (data.Any())
                    {
                        if (!String.IsNullOrEmpty(model.FullTextSearch))
                        {
                            var txt = model.FullTextSearch.ToLower();
                            data = data.Where(x => x.ApplicationName.ToLower().Contains(txt));
                        }

                        if (!model.Size.HasValue)
                        {
                            model.Size = 20;
                        }
                        model.Page = model.Page - 1;
                        int excludedRows = ((int)model.Page) * ((int)model.Size);
                        if (excludedRows <= 0)
                        {
                            excludedRows = 0;
                        }

                        var totalCount = data.Count();
                        data = data.Skip(excludedRows).Take((int)model.Size);

                        var dataResult = await data.ToListAsync();

                        var fullQueryData = AutoMapperUtils.AutoMap<IdmApplications, ApplicationModel>(dataResult);

                        var response = new Pagination<ApplicationModel>();
                        response.Content = fullQueryData;
                        response.NumberOfElements = totalCount;
                        response.Page = model.Page.HasValue ? model.Page.Value : 0;
                        response.Size = model.Size.HasValue ? model.Size.Value : 0;
                        response.TotalElements = totalCount;
                        response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);


                        return new ResponsePagination<ApplicationModel>(response);
                    }
                    else
                        return new Response(Code.NotFound, "No data found");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetByIdAsync(Guid id)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unit.GetRepository<IdmApplications>().Get(x => x.ApplicationId == id).FirstOrDefaultAsync();
                    if (data != null)
                        return new ResponseObject<ApplicationModel>(AutoMapperUtils.AutoMap<IdmApplications, ApplicationModel>(data), "Get data successfully", Code.Success);
                    return new ResponseError(Code.NotFound, "No data found");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> UpdateAsync(ApplicationUpdateModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unit.GetRepository<IdmApplications>().Get(x => x.ApplicationId == model.ApplicationId).FirstOrDefaultAsync();
                    if (data == null)
                        return new ResponseError(Code.NotFound, "No data found!");
                    else
                    {
                        var record = await unit.GetRepository<IdmApplications>().Get(x => x.ApplicationId == model.ApplicationId).FirstOrDefaultAsync();
                        if (record != null)
                        {
                            record.ApplicationName = model.ApplicationName;
                            record.Description = model.Description;
                            record.LoweredApplicationName = model.LoweredApplicationName;

                            unit.GetRepository<IdmApplications>().Update(record);

                            if (unit.Save() > 0)
                                return new Response(Code.Success, "Update successfully!");
                            return new ResponseError(Code.ServerError, "The SQL statement can not be executed!");
                        }
                        return new ResponseError(Code.NotFound, "No data found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> CreateAsync(ApplicationCreateModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var existData = await unit.GetRepository<IdmApplications>().Get(x => x.ApplicationId == model.ApplicationId).FirstOrDefaultAsync();
                    if (existData != null)
                        return new Response(Code.MethodNotAllowed, "Id exist");
                    var record = AutoMapperUtils.AutoMap<ApplicationCreateModel, IdmApplications>(model);
                    unit.GetRepository<IdmApplications>().Add(record);
                    if (await unit.SaveAsync() > 0)
                    {
                        return new ResponseObject<ApplicationModel>(AutoMapperUtils.AutoMap<IdmApplications, ApplicationModel>(record), "Create Successfully", Code.Success);
                    }
                    return new ResponseError(Code.ServerError, "The sql statement has been terminated!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> DeleteAsync(Guid id)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var record = await unit.GetRepository<IdmApplications>().Get(x => x.ApplicationId == id).FirstOrDefaultAsync();
                    if (record != null)
                    {
                        unit.GetRepository<IdmApplications>().Delete(record);
                        if (await unit.SaveAsync() > 0)
                            return new Response(Code.Success, "Delete successfully!");
                        return new ResponseError(Code.ServerError, "The SQL statement can not be executed!");
                    }
                    return new ResponseError(Code.NotFound, "No data found!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> DeleteManyAsync(List<Guid> ids)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var resultList = new List<WorkflowStateModel>();
                    foreach (var id in ids)
                    {
                        var deleteResult = await DeleteAsync(id) as ResponseObject<ApplicationModel>;
                    }
                    var result = new Response(Code.Success, "Delete multiple success");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.NotFound, "No data found!");
            }
        }
    }
}