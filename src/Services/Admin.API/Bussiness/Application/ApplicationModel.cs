﻿using System;
using CommonUtils;
using DataUtils;

namespace Business
{
    public class BaseApplicationModel
    {
        public Guid ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string Code { get; set; }
    }

    public class ApplicationModel : BaseApplicationModel
    {
        public string Description { get; set; }
        public bool IsDefault { get; set; }
        public string LoweredApplicationName { get; set; }
    }

    public class ApplicationQueryModel : PaginationRequest
    {
        public string ApplicationName { get; set; }
        public string Code { get; set; }
        public string LoweredApplicationName { get; set; }
    }

    public class ApplicationCreateModel
    {
        public Guid ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string LoweredApplicationName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Guid? TemplateApp { get; set; }
    }

    public class ApplicationUpdateModel
    {
        public Guid ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string LoweredApplicationName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}