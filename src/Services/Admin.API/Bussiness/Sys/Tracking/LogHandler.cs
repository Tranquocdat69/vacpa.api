﻿using CommonUtils;
using DataUtils;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class LogHandler : ILogHandler
    {
        public LogHandler()
        {
        }

        public async Task<Response> CreateAsync(LogCreateModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var newRecord = new bsd_Logs()
                    {
                        Exception = model.Exception,
                        ActionByUserId = model.ActionByUserId,
                        Content = model.Content,
                        ApplicationId = model.ApplicationId,
                        Date = DateTime.Now,
                        Level = "1",
                        Logger = "",
                        ModuleId = Guid.Empty,
                        ObjectId = model.ObjectId,
                        ObjectType = model.ObjectType,
                        Thread = "1",
                        UserId = model.UserId,
                        UserName = UserCollectionOld.Instance.GetModel(model.UserId).UserName

                    };
                    unit.GetRepository<bsd_Logs>().Add(newRecord);
                    if (await unit.SaveAsync() > 0)
                        return new ResponseObject<LogModel>(AutoMapperUtils.AutoMap<bsd_Logs, LogModel>(newRecord), "Create successfully!", Code.Success);
                    return new ResponseError(Code.ServerError, "The sql statement can not be executed!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetLogByFilter(LogFilterModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var record = unit.GetRepository<bsd_Logs>().GetAll();

                    if (!String.IsNullOrEmpty(model.FullTextSearch))
                        record = record.Where(x => x.Content.ToLower().Contains(model.FullTextSearch.ToLower())
                    || x.Exception.ToLower().Contains(model.FullTextSearch.ToLower()) 
                    );
                    if (model.ApplicationId.HasValue)
                        record = record.Where(x => x.ApplicationId == model.ApplicationId);

                    if (model.UserId.HasValue)
                        record = record.Where(x => x.UserId == model.UserId.Value);

                    //Pagination
                    int pageNumber = model.Page.HasValue ? model.Page.Value : model.Page.GetValueOrDefault();
                    int pageSize = model.Size.HasValue ? model.Size.Value : model.Size.GetValueOrDefault();
                    var totalRecords = record.Count();

                    var r = await (record.OrderByDescending(s => s.Date).Skip((pageNumber - 1) * pageSize).Take(pageSize)).ToListAsync();

                    var retData = new List<LogModel>();
                    foreach (var item in r)
                    {
                        var convertItem = AutoMapperUtils.AutoMap<bsd_Logs, LogModel>(item);
                        retData.Add(convertItem);
                    }

                    var response = new Pagination<LogModel>();
                    response.Content = retData;
                    response.NumberOfElements = record.Count();
                    response.Page = pageNumber;
                    response.Size = pageSize;
                    response.TotalElements = totalRecords;
                    response.TotalPages = totalRecords / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                    var result = new ResponsePagination<LogModel>(response);

                    return result;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

    }
}