﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class LogModel
    {
        public int Id { get; set; }
        public Guid? ApplicationId { get; set; }
        public DateTime Date { get; set; }
        public string Thread { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string ActionByUserId { get; set; }
        public string Exception { get; set; }
        public Guid ModuleId { get; set; }
        public string Content { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string ObjectType { get; set; }
        public Guid? ObjectId { get; set; }
    }

    public class LogCreateModel
    {
        public Guid? ApplicationId { get; set; }
        public DateTime Date { get; set; }
        public string Thread { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string ActionByUserId { get; set; }
        public string Exception { get; set; }
        public Guid ModuleId { get; set; }
        public string Content { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string ObjectType { get; set; }
        public Guid? ObjectId { get; set; }
    }

    public class LogFilterModel: PaginationRequest
    {
        public Guid? ApplicationId { get; set; }
        public Guid? UserId { get; set; }
    }
}
