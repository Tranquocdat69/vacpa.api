﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public interface ILogHandler
    {
        Task<Response> GetLogByFilter(LogFilterModel model);
        Task<Response> CreateAsync(LogCreateModel model);
    }
}
