﻿using CommonUtils;
//using Business;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using System.Timers;
using UserCollection = ApiUtils.UserCollection;

namespace Business
{
    public class Bootstrapping
    {
        private static ICacheService _cache1;
        public static void ConfigureBootstrapping(IApplicationBuilder app)
        {
            try
            {
                _cache1 = app.ApplicationServices.GetRequiredService<ICacheService>();
                ApiUtils.UserCollection.Instance.setServiceRunService(_cache1);
                var t1 = Task.Factory.StartNew(() => ApiUtils.UserCollection.Instance.LoadToHashSet());

                Task.WaitAll(t1);
                // set timer 30m
                var intervalTime = !string.IsNullOrEmpty(Utils.GetConfig("AppSettings:IntervalTime")) ? Utils.GetConfig("AppSettings:IntervalTime") : "1800000";

                IntervalB.Set(int.Parse(intervalTime));
            }
            catch (System.Exception ex)
            {
                Serilog.Log.Error(ex, "");
            }
           
        }
    }
    public static class IntervalB
    {
        public static System.Timers.Timer Set(int interval)
        {
            var timer = new System.Timers.Timer(interval);

            // Hook up the Elapsed event for the timer. 
            timer.Elapsed += OnTimedEvent;

            // Have the timer fire repeated events (true is the default)
            timer.AutoReset = true;

            // Start the timer
            timer.Enabled = true;

            // timer.Elapsed += (s, e) => {
            //       timer.Enabled = false;
            //        // Hook up the Elapsed event for the timer. 
            //       timer.Elapsed += OnTimedEvent;
            //       timer.AutoReset = true;
            //       // action.Wait();
            //       timer.Enabled = true;
            // };
            // timer.Enabled = true;
            return timer;
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            var t1 = Task.Factory.StartNew(() => ApiUtils.UserCollection.Instance.LoadToHashSet());
            Task.WaitAll(t1);
            Console.WriteLine("The Elapsed reload user cache 2 event was raised at {0:HH:mm:ss.fff}", e.SignalTime);
            Serilog.Log.Information("The Elapsed reload user cache 2 event was raised at {0:HH:mm:ss.fff}", e.SignalTime);
        }

        public static void Stop(System.Timers.Timer timer)
        {
            timer.Stop();
            timer.Dispose();
        }
    }
}
