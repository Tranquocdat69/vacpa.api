﻿using System;
using CommonUtils;
using DataUtils;
using System.Collections.Generic;

namespace Business
{
    public class IPFMAreaModel
    {
        public string AreaName { get; set; }
        public string ParentId { get; set; }
        public string LgspId { get; set; }
        public string LgspName { get; set; }
    }
    public class IPFMAreaReponseModel
    {      
        public string Result { get; set; }
        public int Code { get; set; }
        public AreaModel Area { get; set; }
    }

   
    public class AreaModel
    {
        public int AreaId { get; set; }
    }
    public class UserCreateResponse
    {
        public string Result { get; set; }
        public int UserId { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }

    }
    public class ipfmGetAllUserModel
    {
        public string Result { get; set; }
        public int Code { get; set; }
        public List<ipfmUserModel> Users { get; set; }
    }

    public class ipfmUserModel
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int AreaId { get; set; }
        public string Fullname { get; set; }
        public string Position { get; set; }
        public string EditAccount { get; set; }
        public string EditSpeaker { get; set; }
        public string EditProgram { get; set; }
        public string AcceptProgram { get; set; }
        public string UseTTS { get; set; }
        public string Broadcast { get; set; }





    }

}
