﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataUtils;
using Microsoft.EntityFrameworkCore;
using CommonUtils;
using Infrastructure;
using System.Data;

namespace Business
{
    public class RoleHandler : IRoleHandler
    {
        //public IEventBus _eventBus;
        public RoleHandler()
        {
        }

        //public RoleHandler(IEventBus eventBus)
        //{
        //    _eventBus = eventBus;
        //}

        public async Task<Response> CreateAsync(RoleCreateModel model)
        {
            try
            {               
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    // Check RoleCode Exist
                    var checkRoleCodeExist = await unitOfWork.GetRepository<IdmRoles>().Get(sp => sp.RoleCode.ToLower() == model.RoleCode.ToLower()).FirstOrDefaultAsync();
                    if (checkRoleCodeExist != null)
                    {
                        return new ResponseObject<RoleCreateModel>(null, "Code Existed", Code.ServerError);
                    }
                    var roleDTO = AutoMapperUtils.AutoMap<RoleCreateModel, IdmRoles>(model);
                    roleDTO.RoleId = Guid.NewGuid();
                    roleDTO.CreatedOnDate = DateTime.Now;
                    roleDTO.LastModifiedByUserId = model.CreatedByUserId;
                    roleDTO.LastModifiedOnDate = DateTime.Now;
                    roleDTO.LoweredRoleName = model.RoleName.ToLower();
                    
                    var resultModel = AutoMapperUtils.AutoMap<IdmRoles, RoleCreateModel>(roleDTO);


                    unitOfWork.GetRepository<IdmRoles>().Add(roleDTO);
                    //Sau khi add thành công Role mới, check các rights
                    if (model.RightsOfRole != null && model.RightsOfRole.Count() > 0)
                    {
                        //Nếu có quyền thì thêm vào bảng Right in Roles
                        foreach (var item in model.RightsOfRole)
                        {
                            var rightsInRole = new IdmRightsInRole();
                            rightsInRole.CreatedDate = DateTime.Now;
                            rightsInRole.RightCode = item.RightCode;
                            rightsInRole.RoleId = roleDTO.RoleId;

                            unitOfWork.GetRepository<IdmRightsInRole>().Add(rightsInRole);
                        }
                    }
                    if (await unitOfWork.SaveAsync() >= 1)
                    {
                        return new ResponseObject<RoleCreateModel>(resultModel, "Create successfully", Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<RoleCreateModel>(null, "Failed to save", Code.ServerError);
                    }                    
                }

                //return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<RoleCreateModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> DeleteAsync(Guid roleId)
        {  
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    //lấy thông tin chi tiết role hiện tại
                    var roledata = (from role in unitOfWork.GetRepository<IdmRoles>().GetAll()
                                    where role.RoleId == roleId
                                    select role).FirstOrDefault();
                    if (roledata != null)
                    {
                        //SystemRoleModel SystemRoleModeldata = ConvertData.Convert(roledata);

                        /* Xóa right in role*/
                        var repo_RightsInRole = unitOfWork.GetRepository<IdmRightsInRole>();
                        var ListRightsInRole = from rir in unitOfWork.GetRepository<IdmRightsInRole>().GetAll()
                                               where rir.RoleId == roleId
                                               select rir;
                        foreach (var item in ListRightsInRole)
                        {
                            repo_RightsInRole.Delete(item);
                        }
                        /* Xóa User in role*/
                        var repo_UsersInRoles = unitOfWork.GetRepository<IdmUsersInRoles>();
                        var ListUsersInRoles = from uir in unitOfWork.GetRepository<IdmUsersInRoles>().GetAll()
                                               where uir.RoleId == roleId
                                               select uir;
                        foreach (var item in ListUsersInRoles)
                        {
                            repo_UsersInRoles.Delete(item);
                        }
                        ///* Change Inherrist.or Xóa right in User.*/
                        //var repo_RightsOfUsers = unitOfWork.GetRepository<IdmRightsOfUser>();
                        //var ListRightsOfUsers = from rou in unitOfWork.GetRepository<IdmRightsOfUser>().GetAll()
                        //                        where rou.InheritedFromRoles.Contains(roleId)
                        //                        select rou;
                        //foreach (var item in ListRightsOfUsers)
                        //{

                        //    //Nếu InheritedFromRoles chỉ chứa mình quyền đó =>> xóa
                        //    //Nếu không khai trừ role ra khỏi InheritedFromRoles
                        //    List<SystemRoleModel> ListRole = MembershipHelper.LoadRolesInherited(item.InheritedFromRoles);
                        //    if (ListRole.Count > 1)
                        //    {
                        //        if (ListRole.Contains(MembershipHelper.MakeIndependentPermission())) //nếu có 2 inherist với 1 cái là self 
                        //            item.Inherited = false;
                        //        item.InheritedFromRoles = MembershipHelper.RemoveRolesInherited(item.InheritedFromRoles, SystemRoleModeldata);
                        //        repo_RightsOfUsers.Update(item);
                        //    }
                        //    else
                        //    {
                        //        repo_RightsOfUsers.Delete(item);
                        //    }
                        //}
                        /* Xóa role*/
                        var repo = unitOfWork.GetRepository<IdmRoles>();
                        repo.Delete(roledata);
                        if (await unitOfWork.SaveAsync() >= 1)
                        {
                            return new ResponseObject<RoleModel>(AutoMapperUtils.AutoMap<IdmRoles, RoleModel>(roledata), "Delete successfully", Code.Success);
                        }
                        else
                        {
                            return new ResponseObject<RoleModel>(null, "Failed to delete", Code.ServerError);
                        }
                    }
                    else
                    {
                        return new ResponseObject<RoleModel>(null, "No data found", Code.ServerError);
                    }                        
                }

                //return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<IdmRoles>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> DeleteMultiAsync(List<Guid> listId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var resultList = new List<RoleModel>();
                    foreach(var id in listId)
                    {
                        var deleteResult = await DeleteAsync(id) as ResponseObject<RoleModel>;
                        resultList.Add(deleteResult.Data);
                    }

                    var result = new ResponseList<RoleModel>(resultList, "Delete multiple success", Code.Success);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseList<RoleModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> GetAllAsync()
        {            
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var startTime = DateTime.Now;
                    var roleList = await unitOfWork.GetRepository<IdmRoles>().GetAll().ToListAsync();
                    List<RoleModel> roleModelList = new List<RoleModel>();
                    //foreach (var role in roleList)
                    //{
                    //    var roleModel = AutoMapperUtils.AutoMap<IdmRoles, RoleModel>(role);
                    //    roleModelList.Add(roleModel);
                    //}
                    roleModelList = AutoMapperUtils.AutoMap<IdmRoles, RoleModel>(roleList);

                    var result = new ResponseList<RoleModel>(roleModelList, "Get all success", Code.Success);
                    Console.WriteLine("GetAll:Role: " + (DateTime.Now - startTime).TotalMilliseconds);

                    if (roleList.Count() > 0)
                    {
                        return result;
                    }
                    else
                        return new ResponseList<RoleModel>(null, "No data found", Code.NotFound);

                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseList<RoleModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> GetByFilterAsync(RoleQueryModel filter)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var result = new ResponseObject<List<RoleModel>>();
                    var data = from dt in unitOfWork.GetRepository<IdmRoles>().Get()
                               select dt;
                    if (filter.ApplicationId.HasValue)
                    {
                        data = data.Where(dt => dt.ApplicationId == filter.ApplicationId);

                    }
                    if (!string.IsNullOrEmpty(filter.FullTextSearch))
                    {
                        var text = filter.FullTextSearch.ToLower();
                        data = data.Where(dt =>
                                        dt.RoleName.ToLower().Contains(text) || dt.RoleCode.ToLower().Contains(text) ||
                                        dt.LoweredRoleName.ToLower().Contains(text) 
                                        );
                    }
                    if (!filter.Size.HasValue)
                    {
                        filter.Size = 20;
                    }
                    filter.Page = filter.Page - 1;
                    int excludedRows = ((int)filter.Page) * ((int)filter.Size);
                    if (excludedRows <= 0)
                    {
                        excludedRows = 0;
                    }

                    // tính tổng số bản ghi trả về theo filter
                    var totalCount = data.Count();

                    data = data.OrderByDescending(s => s.LastModifiedOnDate);

                    data = data.Skip(excludedRows).Take((int)filter.Size);

                    var fullQueryData = AutoMapperUtils.AutoMap<IdmRoles, RoleModel>(await data.ToListAsync());

                    var response = new Pagination<RoleModel>();
                    response.Content = fullQueryData;
                    response.NumberOfElements = totalCount;
                    response.Page = filter.Page.HasValue ? filter.Page.Value : 0;
                    response.Size = filter.Size.HasValue ? filter.Size.Value : 0;
                    response.TotalElements = totalCount;
                    response.TotalPages = totalCount / (filter.Size.HasValue ? (filter.Size.Value + 1) : 1);


                    var r = new ResponsePagination<RoleModel>(response);

                    return r;

                }

            }
            catch (System.Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<RoleModel>>(null, ex.Message, Code.ServerError);
            }

        }

        public async Task<Response> GetByIdAsync(Guid roleId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {

                    var role = await unitOfWork.GetRepository<IdmRoles>().Get(a => a.RoleId == roleId).FirstAsync();
                    if (role != null)
                    {
                        var result = AutoMapperUtils.AutoMap<IdmRoles, RoleModel>(role);

                        //HaNH
                        //Get all rights in role
                        var rightsInRole = unitOfWork.GetRepository<IdmRightsInRole>().Get(x => x.RoleId == role.RoleId).ToList();
                        //Nếu có quyền
                        if(rightsInRole.Count() > 0)
                        {
                            var rightHandler = new RightHandler();
                            result.RightsOfRole = new List<RightModel>();
                            //Get quyền
                            foreach (var item in rightsInRole)
                            {
                                var right = rightHandler.GetByCodeAsync(item.RightCode).Result.Data;
                                if (right != null)
                                    result.RightsOfRole.Add(right);
                            }
                        }    
                        return new ResponseObject<RoleModel>(result, "Get successfully", Code.Success);
                    }
                    else
                        return new ResponseObject<RoleModel>(null, "No Data found", Code.Success);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<RoleModel>(null, ex.Message, Code.ServerError);
            }
            
        }

        public async Task<Response> UpdateAsync(RoleUpdateModel model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var exist = unitOfWork.GetRepository<IdmRoles>().Get(r => r.RoleId == model.RoleId).AsNoTracking().FirstOrDefault();
                    if(exist == null)
                    {
                        return new ResponseObject<RoleUpdateModel>(null, "No data found", Code.NotFound);
                    }
                    // Check RoleCode Exist
                    var checkRoleCodeExist = await unitOfWork.GetRepository<IdmRoles>().Get(r => r.RoleCode.ToLower() == model.RoleCode.ToLower() && r.RoleId != model.RoleId).FirstOrDefaultAsync();
                    if (checkRoleCodeExist != null)
                    {
                        return new ResponseObject<RoleUpdateModel>(null, "Code Existed", Code.ServerError);
                    }
                    exist = AutoMapperUtils.AutoMap<RoleUpdateModel, IdmRoles>(model);

                    exist.LastModifiedByUserId = model.LastModifiedByUserId;
                    exist.LastModifiedOnDate = DateTime.Now;
                    exist.LoweredRoleName = model.RoleName.ToLower();

                    var resultModel = AutoMapperUtils.AutoMap<IdmRoles, RoleUpdateModel>(exist);

                    unitOfWork.GetRepository<IdmRoles>().Update(exist);

                    //sửa các right in role
                    var rightInRole = unitOfWork.GetRepository<IdmRightsInRole>().Get(a => a.RoleId == model.RoleId);                    
                    //xóa right
                    if(rightInRole != null || rightInRole.Count() > 0)
                    {
                        unitOfWork.GetRepository<IdmRightsInRole>().DeleteRange(rightInRole);
                    }
                    //thêm lại
                    if (model.RightsOfRole.Count() > 0)
                    {
                        foreach (var right in model.RightsOfRole)
                        {
                            var rirDTO = new IdmRightsInRole()
                            {
                                RoleId = model.RoleId,
                                RightCode = right.RightCode,
                                CreatedDate = DateTime.Now,
                                ModifiedDate = DateTime.Now
                            };
                            unitOfWork.GetRepository<IdmRightsInRole>().Add(rirDTO);
                        }
                    }

                    if (await unitOfWork.SaveAsync() >= 1)
                       return new ResponseObject<RoleUpdateModel>(resultModel, "Updated success", Code.Success);
                    else
                        return new ResponseObject<RoleUpdateModel>(null, "Update failed", Code.ServerError);
                }

                //return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<RoleUpdateModel>(null, ex.Message, Code.ServerError);
            }
        }
    }

   
}
