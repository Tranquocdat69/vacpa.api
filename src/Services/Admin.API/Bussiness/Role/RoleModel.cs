﻿using System;
using System.Collections.Generic;
using CommonUtils;
using DataUtils;

namespace Business
{
    public class RoleModel
    {
        public Guid ApplicationId { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public string RoleCode { get; set; }
        public List<UserModel> UserList { get; set; }

        //HaNH
        public List<RightModel> RightsOfRole { get; set; }
    }

    public class RoleQueryModel : PaginationRequest
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class RoleUpdateModel
    {
        public Guid ApplicationId { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleCode { get; set; }
        public bool EnableDelete { get; set; }
        public string Description { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        //HaNH
        public DateTime LastModifiedOnDate { get; set; }
        public List<RightModel> RightsOfRole { get; set; }
    }
    public class RoleCreateModel
    {
        public Guid ApplicationId { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleCode { get; set; }
        public bool EnableDelete { get; set; }
        public string Description { get; set; }
        public Guid CreatedByUserId { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        //HaNH
        public List<RightModel> RightsOfRole { get; set; }
    }

    //HaNH
    //Quản lý quyền của role trong hệ thống
    public class RightInRoleModel
    {
        public long Id { get; set; }
        public Guid RoleId { get; set; }
        public string RightCode { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public DateTime ModifiedOnDate { get; set; }
    }
}
