﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public interface IRightInRoleHandler
    {
        #region CRUD
        Task<Response> CreateAsync(string rightCode, Guid roleId);
        Task<Response> GetByRoleIdAsync(Guid roleId);
        Task<Response> UpdateAsync(RightInRoleModel model);
        Task<Response> DeleteById(long id);
        Task<Response> AddRightsToRole(List<string> rightCodes, Guid roleId);
        #endregion
    }
}
