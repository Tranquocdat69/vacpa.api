﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonUtils;
using DataUtils;
using Infrastructure;

namespace Business
{
    public interface IRoleHandler
    {
        Task<Response> GetByFilterAsync(RoleQueryModel filter);
        Task<Response> GetAllAsync();
        Task<Response> CreateAsync(RoleCreateModel model);
        Task<Response> UpdateAsync(RoleUpdateModel model);
        Task<Response> GetByIdAsync(Guid roleId);
        Task<Response> DeleteAsync(Guid roleId);
        Task<Response> DeleteMultiAsync(List<Guid> listId);

        //Insert rights to role
    }

    
}