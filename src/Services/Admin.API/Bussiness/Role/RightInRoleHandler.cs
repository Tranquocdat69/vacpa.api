﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataUtils;
using Microsoft.EntityFrameworkCore;
using CommonUtils;
using Infrastructure;
using Serilog;

namespace Business
{
    //HaNH
    public class RightInRoleHandler : IRightInRoleHandler
    {
        public async Task<Response> CreateAsync(string rightCode, Guid roleId)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    //Get new Id for the object
                    var model = new IdmRightsInRole()
                    {
                        RightCode = rightCode,
                        RoleId = roleId,
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now
                    };
                    unit.GetRepository<IdmRightsInRole>().Add(model);
                    if (await unit.SaveAsync() >= 1)
                    {
                        var res = AutoMapperUtils.AutoMap<IdmRightsInRole, RightInRoleModel>(model);
                        return new ResponseObject<RightInRoleModel>(res, "Create successfully", Code.Success);
                    }
                    else
                        return new ResponseObject<RightInRoleModel>(null, "Save failed!", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<RightInRoleModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> DeleteById(long id)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var record = await unit.GetRepository<IdmRightsInRole>().Get(x => x.Id == id).FirstOrDefaultAsync();
                    if (record != null)
                    {
                        unit.GetRepository<IdmRightsInRole>().Delete(record);
                        var response = AutoMapperUtils.AutoMap<IdmRightsInRole, RightInRoleModel>(record);
                        if (await unit.SaveAsync() >= 1)
                            return new ResponseObject<RightInRoleModel>(response, "Delete successfully", Code.Success);
                        else
                            return new ResponseObject<RightInRoleModel>(null, "Delete failed", Code.ServerError);
                    }
                    else
                        return new ResponseObject<RightInRoleModel>(null, "No data found", Code.NotFound);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<RightInRoleModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> GetByRoleIdAsync(Guid roleId)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = unit.GetRepository<IdmRightsInRole>().Get(x => x.RoleId == roleId);
                    if (data != null)
                    {
                        var res = new List<RightInRoleModel>();
                        foreach (var item in data)
                        {
                            res.Add(AutoMapperUtils.AutoMap<IdmRightsInRole, RightInRoleModel>(item));
                        }
                        return new ResponseList<RightInRoleModel>(res, "Get successfully", Code.Success);
                    }
                    else
                        return new ResponseList<RightInRoleModel>(null, "No data found", Code.NotFound);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseList<RightInRoleModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> UpdateAsync(RightInRoleModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unit.GetRepository<IdmRightsInRole>().Get(x => x.Id == model.Id).FirstOrDefaultAsync();
                    if (data != null)
                    {
                        data.ModifiedDate = DateTime.Now;
                        data.RightCode = model.RightCode;
                        data.RoleId = model.RoleId;

                        unit.GetRepository<IdmRightsInRole>().Update(data);
                        if (await unit.SaveAsync() >= 1)
                            return new ResponseObject<RightInRoleModel>(AutoMapperUtils.AutoMap<IdmRightsInRole, RightInRoleModel>(data), "Update successfully", Code.Success);
                        else
                            return new ResponseObject<RightInRoleModel>(null, "Update failed!", Code.ServerError);
                    }
                    else
                        return new ResponseObject<RightInRoleModel>(model, "No data found", Code.NotFound);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseObject<RightInRoleModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> AddRightsToRole(List<string> rightCodes, Guid roleId)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    //Kiem tra null
                    if (string.IsNullOrEmpty(roleId.ToString()))
                        return new ResponseError(Code.NotFound, "No roleId input");
                    //check xem role có những quyền đó chưa
                    var listUsingRights = await unit.GetRepository<IdmRightsInRole>().GetListAsync(x => x.RoleId == roleId);
                    //quyền nào chưa có cho vào list
                    var listToAdd = new List<IdmRightsInRole>();
                    foreach (var item in rightCodes)
                    {
                        //Nếu chưa tồn tại quyền thì tạo mới để add
                        if (!listUsingRights.All(x => x.RightCode == item))
                        {
                            listToAdd.Add(new IdmRightsInRole
                            {
                                RightCode = item,
                                RoleId = roleId,
                                CreatedDate = DateTime.Now,
                            });
                        }
                    }
                    //add vào role nếu có data
                    if (listToAdd.Any())
                        unit.GetRepository<IdmRightsInRole>().AddRange(listToAdd);
                    if (await unit.SaveAsync() >= 1)
                        return new Response(Code.Success, "Add rights to role successfully!");
                    else
                    {
                        Log.Error("The statement can not be executed!");
                        return new Response(Code.ServerError, "The sql statement can not be executed");
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
    }
}
