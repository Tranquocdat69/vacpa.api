﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class OrganizationModel
    {
        public Guid ApplicationId { get; set; }
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }

        public Guid ParentOrganizationId { get; set; }
        public string ParentOrganizationName { get; set; }

        public string Code { get; set; }
        public int Status { get; set; }
        public int? Loai { get; set; }

        //organization detail
        public string Type { get; set; }
        public bool? IsPublic { get; set; }
        public int? ThuTu { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string IdPath { get; set; }
        public string Path { get; set; }
        public int? Level { get; set; }
        public bool? HasChild { get; set; }

        public string CreatedByFullName { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public string LastModifiedByFullName { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
    }


    public class OrganizationQueryModel: PaginationRequest
    {
        public int? Loai { get; set; }
        public int? Status { get; set; }
    } 
}
