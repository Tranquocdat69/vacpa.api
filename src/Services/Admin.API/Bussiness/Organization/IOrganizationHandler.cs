﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public interface IOrganizationHandler
    {
        Task<List<OrganizationModel>> GetAll();
        Task<Response> GetAllFromCache();
        Task<Response> GetOrganizationById(Guid id);
        Task<Response> GetAllOrganization(Guid? currentId);
        Task<Response> GetOrganizationByFilter(OrganizationQueryModel model);
        Task<Response> CreateOrganization(OrganizationModel model);
        Task<Response> UpdateOrganization(OrganizationModel model);
        Task<Response> DeleteOrganization(Guid id);

        Task<Response> DeleteMultiAsync(List<Guid> ids);
    }
}
