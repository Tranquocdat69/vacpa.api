﻿using CommonUtils;
using DataUtils;
using Infrastructure;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class OrganizationHandler : IOrganizationHandler
    {
        public async Task<List<OrganizationModel>> GetAll()
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var datas = unit.GetRepository<IdmOrganizations>().GetAll().Where(x => x.Status == 1);

                    if (datas != null)
                    {
                        var result = new List<OrganizationModel>();
                        foreach (var item in datas)
                        {
                            result.Add(AutoMapperUtils.AutoMap<IdmOrganizations, OrganizationModel>(item));
                        }

                        return result;
                    }
                    return new List<OrganizationModel>();
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new List<OrganizationModel>();
            }
        }

        public async Task<Response> GetAllFromCache()
        {
            try
            {
                return new ResponseList<OrganizationModel>(OrganizationCollection.Instance.Collection.Where(x => x.Status == 1).OrderBy(x => x.Code).ToList(),
                    "Get data successfully", Code.Success);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new Response(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> CreateOrganization(OrganizationModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var record = unit.GetRepository<IdmOrganizations>().Get(x => x.OrganizationId == model.OrganizationId
                    && x.ApplicationId == model.ApplicationId).FirstOrDefault();
                    if (record == null)
                    {
                        var newRecord = new IdmOrganizations()
                        {
                            OrganizationId = Guid.NewGuid(),
                            ApplicationId = model.ApplicationId,
                            OrganizationName = model.OrganizationName,
                            Code = model.Code,
                            
                            ParentOrganizationId = model.ParentOrganizationId,
                            ParentOrganizationName = model.ParentOrganizationName,
                            Status = model.Status,
                            Loai = model.Loai,
                            Type = model.Type,
                            ThuTu = model.ThuTu,
                            Description = model.Description,
                            Address = model.Address,
                            PhoneNumber = model.PhoneNumber,
                            Fax = model.Fax,
                            Email = model.Email,
                            HasChild = model.HasChild,

                            CreatedByFullName = model.CreatedByFullName,
                            CreatedByUserId = model.CreatedByUserId,
                            CreatedOnDate = DateTime.Now,

                        };
                        unit.GetRepository<IdmOrganizations>().Add(newRecord);
                        if (await unit.SaveAsync() > 0)
                            return new ResponseObject<OrganizationModel>(AutoMapperUtils.AutoMap<IdmOrganizations, OrganizationModel>(newRecord), "Create successfully!", Code.Success);
                        return new ResponseError(Code.ServerError, "The sql statement can not be executed!");
                    }
                    return new ResponseError(Code.MethodNotAllowed, "The data exists!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> DeleteOrganization(Guid id)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var record = unit.GetRepository<IdmOrganizations>().Get(x => x.OrganizationId == id).FirstOrDefault();
                    if (record == null)
                        return new ResponseError(Code.NotFound, "No data found!");
                    //delete children
                    var children1 = unit.GetRepository<IdmUsersInOrganization>().GetAll().Where(x => x.OrganizationId == id);

                    if (children1.Any())
                    {
                        return new ResponseError(Code.ServerError, "Không thể xóa vì đang có người dùng ơ trong đơn vị!");
                    }

                    unit.GetRepository<IdmOrganizations>().Delete(record);
                    if (await unit.SaveAsync() > 0)
                        return new Response(Code.Success, "Delete successfully!");
                    return new ResponseError(Code.ServerError, "The sql statement can not be executed!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> DeleteMultiAsync(List<Guid> ids)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    foreach (var item in ids)
                    {
                        await this.DeleteOrganization(item);
                    }

                    return new Response(Code.Success, "Delete successfully!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetAllOrganization(Guid? currentId)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var records =  unit.GetRepository<IdmOrganizations>().Get().Where(x => x.OrganizationId != currentId).OrderBy(x => x.CreatedOnDate);

                    if (records.Count() > 0)
                    {
                        var result = new List<OrganizationModel>();
                        foreach (var item in records)
                        {
                            result.Add(AutoMapperUtils.AutoMap<IdmOrganizations, OrganizationModel>(item));
                        }

                        return new ResponseList<OrganizationModel>(result, "Get successfully!", Code.Success);
                    }
                    return new Response(Code.NotFound, "No data found!!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetOrganizationByFilter(OrganizationQueryModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var record = unit.GetRepository<IdmOrganizations>().GetAll();

                    if (model.ApplicationId.HasValue)
                    {
                        record = record.Where(x => x.ApplicationId == model.ApplicationId);
                    }
                    //if (model.Status.HasValue)
                    //    record = record.Where(x => x.Status == model.Status.Value);
                    if (!String.IsNullOrEmpty(model.FullTextSearch))
                        record = record.Where(x => x.Code.ToLower().Contains(model.FullTextSearch.ToLower())
                    || x.OrganizationName.ToLower().Contains(model.FullTextSearch.ToLower()) && x.ApplicationId == model.ApplicationId);
                    if (model.Loai.HasValue)
                        record = record.Where(x => x.Loai == model.Loai);

                    //if (model.Status.HasValue)
                    //    record = record.Where(x => x.Status == model.Status.Value);
                    record = record.OrderBy(x => x.Level);
                    //Pagination
                    int pageNumber = model.Page.HasValue ? model.Page.Value : model.Page.GetValueOrDefault();
                    int pageSize = model.Size.HasValue ? model.Size.Value : model.Size.GetValueOrDefault();
                    var totalRecords = record.Count();
                    record = record.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                    var retData = new List<OrganizationModel>();
                    foreach (var item in record)
                    {
                        var convertItem = AutoMapperUtils.AutoMap<IdmOrganizations, OrganizationModel>(item);
                        retData.Add(convertItem);
                    }

                    foreach(var item in retData)
                    {
                        var detailData = (from s in record where s.OrganizationId == item.ParentOrganizationId select s).FirstOrDefault();
                        if(detailData != null)
                        {
                            item.ParentOrganizationName = detailData.OrganizationName;
                        }
                    }

                    var response = new Pagination<OrganizationModel>();
                    response.Content = retData;
                    response.NumberOfElements = record.Count();
                    response.Page = pageNumber;
                    response.Size = pageSize;
                    response.TotalElements = totalRecords;
                    response.TotalPages = totalRecords / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                    var r = new ResponsePagination<OrganizationModel>(response);

                    return r;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetOrganizationById(Guid id)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var record = await unit.GetRepository<IdmOrganizations>().Get(x => x.OrganizationId == id).FirstOrDefaultAsync();
                    if (record != null)
                    {
                        return new ResponseObject<OrganizationModel>(AutoMapperUtils.AutoMap<IdmOrganizations, OrganizationModel>(record), "Get successfully!", Code.Success);
                    }
                    return new Response(Code.NotFound, "No data found!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> UpdateOrganization(OrganizationModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var record = unit.GetRepository<IdmOrganizations>().Get(x => x.OrganizationId == model.OrganizationId).FirstOrDefault();
                    if (record != null)
                    {
                        record.ApplicationId = model.ApplicationId;
                        record.OrganizationName = model.OrganizationName;
                        record.Code = model.Code;
                        record.ParentOrganizationId = model.ParentOrganizationId;
                        record.ParentOrganizationName = model.ParentOrganizationName;
                        record.Status = model.Status;
                        record.Loai = model.Loai;
                        record.Type = model.Type;
                        record.ThuTu = model.ThuTu;
                        record.Description = model.Description;
                        record.Address = model.Address;
                        record.PhoneNumber = model.PhoneNumber;
                        record.Fax = model.Fax;
                        record.Email = model.Email;

                        record.LastModifiedByFullName = model.LastModifiedByFullName;
                        record.LastModifiedByUserId = model.LastModifiedByUserId;
                        record.LastModifiedOnDate = DateTime.Now;

                        unit.GetRepository<IdmOrganizations>().Update(record);
                        if (await unit.SaveAsync() > 0)
                            return new ResponseObject<OrganizationModel>(AutoMapperUtils.AutoMap<IdmOrganizations, OrganizationModel>(record), "Update successfully!", Code.Success);
                        return new ResponseError(Code.ServerError, "The sql statement can not be executed!");
                    }
                    return new Response(Code.NotFound, "No data found!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
    }
}