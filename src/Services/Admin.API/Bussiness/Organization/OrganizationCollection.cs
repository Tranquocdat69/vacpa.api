﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonUtils;
using DataUtils;
using Infrastructure;

namespace Business
{
    public class OrganizationCollection
    {
        private readonly IOrganizationHandler _handler;
        public HashSet<OrganizationModel> Collection;

        protected OrganizationCollection(IOrganizationHandler handler)
        {
            _handler = handler;
            LoadToHashSet();
        }
        protected OrganizationCollection()
        {
            _handler = new OrganizationHandler();
            LoadToHashSet();
        }

        public static OrganizationCollection Instance { get; } = new OrganizationCollection();

        public void LoadToHashSet()
        {
            Collection = new HashSet<OrganizationModel>();
            var listResponse =  _handler.GetAll().Result;
            if (listResponse != null)
            {
                // Add to hashset
                foreach (var response in listResponse)
                        Collection.Add(response);
            }
        }

        public string GetName(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.OrganizationId == id);
            return result?.OrganizationName;
        }

        public bool Check()
        {
            try
            {
                if (Collection != null && Collection.Count > 0)
                {
                    return true;
                }else{
                    LoadToHashSet();
                }
                return false;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public OrganizationModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.OrganizationId == id);
            if (result == null)
            {
                result = new OrganizationModel();
            }
            return result;
        }
    }
}