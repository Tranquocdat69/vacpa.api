﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public interface IRightHandler
    {
        Task<ResponseList<RightModel>> GetAllAsync();
        Task<ResponseObject<RightCreateModel>> CreateAsync(RightCreateModel model);
        Task<ResponseObject<RightUpdateModel>> UpdateAsync(RightUpdateModel model);
        Task<ResponseObject<RightModel>> GetByCodeAsync(string code);
        Task<ResponseList<RightModel>> GetByGroupRight();
        Task<ResponseObject<List<RightModel>>> GetByFilterAsync(RightFilterModel filterModel);
        Task<ResponseObject<RightModel>> DeleteByIdAsync(string rightCode);
        Task<Response> DeleteMany(List<String> ids);
    }
}
