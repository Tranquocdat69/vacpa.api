﻿using CommonUtils;
using DataUtils;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class RightHandler : IRightHandler
    {
        public async Task<ResponseObject<RightCreateModel>> CreateAsync(RightCreateModel model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var datas = unitOfWork.GetRepository<IdmRight>().GetAll();
                    // Filter by RightCode
                    var data = await (from r in datas
                                      where r.RightCode == model.RightCode
                                      select r).FirstOrDefaultAsync();

                    // Convert
                    if (data == null)
                    {
                        if (model.Level != null)
                        {
                            model.Level = 1;
                        }
                        var rightDTO = AutoMapperUtils.AutoMap<RightCreateModel, IdmRight>(model);
                        unitOfWork.GetRepository<IdmRight>().Add(rightDTO);
                        if (await unitOfWork.SaveAsync() >= 1)
                        {
                            return new ResponseObject<RightCreateModel>(model, "Create successfully!", Code.Success);
                        }
                        else return new ResponseObject<RightCreateModel>(null, "Fail to save", Code.ServerError);
                    }
                    else return new ResponseObject<RightCreateModel>(null, "Right code exists", Code.ServerError);
                }

                //return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<RightCreateModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<ResponseObject<RightModel>> DeleteByIdAsync(String rightCode)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unitOfWork.GetRepository<IdmRight>().Get(a => a.RightCode == rightCode).FirstOrDefaultAsync();
                    // Filter by RightCode kiểm tra quyền tồn tại chưa

                    if (data != null)
                    {
                        // Kiểm tra đã sử dụng chưa
                        var rightsOfUser = await (from rou in unitOfWork.GetRepository<IdmRightsOfUser>().GetAll()
                                                  where rou.RightCode == rightCode
                                                  select rou).FirstOrDefaultAsync();
                        var rightsInRole = await (from rou in unitOfWork.GetRepository<IdmRightsInRole>().GetAll()
                                                  where rou.RightCode == rightCode
                                                  select rou).FirstOrDefaultAsync();
                        if (rightsOfUser == null && rightsInRole == null)
                        {
                            unitOfWork.GetRepository<IdmRight>().Delete(data);

                            if (await unitOfWork.SaveAsync() >= 1)
                            {
                                var responseObject = AutoMapperUtils.AutoMap<IdmRight, RightModel>(data);
                                return new ResponseObject<RightModel>(responseObject, "Delete successfully", Code.Success);
                            }
                            else
                            {
                                return new ResponseObject<RightModel>(null, "Delete failed!", Code.ServerError);
                            }
                        }
                        else
                        {
                            return new ResponseObject<RightModel>(null, "Right(s) in used", Code.ServerError);
                        }
                    }
                    else
                    {
                        return new ResponseObject<RightModel>(null, "Right code not found", Code.NotFound);
                    }
                };
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<RightModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<ResponseList<RightModel>> GetAllAsync()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {

                    var listItem = await unitOfWork.GetRepository<IdmRight>().GetAll().ToListAsync();

                    var result = new List<RightModel>();

                    if (listItem != null)
                    {
                        foreach (var item in listItem)
                        {
                            var convertObject = AutoMapperUtils.AutoMap<IdmRight, RightModel>(item);
                            result.Add(convertObject);
                        }
                        return new ResponseList<RightModel>(result, "Get successfully", Code.Success);
                    }
                    else
                        return new ResponseList<RightModel>(null, "No data found", Code.NotFound);

                    //IntegrationEvent orderPaymentIntegrationEvent;

                    //orderPaymentIntegrationEvent = new OrderStartedIntegrationEvent(Guid.NewGuid().ToString());

                    //_eventBus.Publish(orderPaymentIntegrationEvent);

                    //return r;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseList<RightModel>(null, ex.Message, Code.ServerError);
            }

        }

        public async Task<ResponseObject<RightModel>> GetByCodeAsync(string code)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var datas = unitOfWork.GetRepository<IdmRight>().GetAll();
                    // Filter by RightCode
                    var data = await (from r in datas
                                      where r.RightCode == code
                                      select r).FirstOrDefaultAsync();

                    // Convert
                    if (data != null)
                    {
                        var resultModel = AutoMapperUtils.AutoMap<IdmRight, RightModel>(data);
                        return new ResponseObject<RightModel>(resultModel, "Get successfully", Code.Success);
                    }
                    else return new ResponseObject<RightModel>(null, "No data found", Code.NotFound);
                }

                //return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<RightModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<ResponseList<RightModel>> GetByGroupRight()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {

                    var listItem = await unitOfWork.GetRepository<IdmRight>().Get(x => x.IsGroup).ToListAsync();

                    var result = new List<RightModel>();

                    if (listItem != null)
                    {
                        foreach (var item in listItem)
                        {
                            var convertObject = AutoMapperUtils.AutoMap<IdmRight, RightModel>(item);
                            result.Add(convertObject);
                        }
                        return new ResponseList<RightModel>(result, "Get successfully", Code.Success);
                    }
                    else
                        return new ResponseList<RightModel>(null, "No data found", Code.NotFound);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseList<RightModel>(null, ex.Message, Code.ServerError);
            }

        }


        public async Task<ResponseObject<List<RightModel>>> GetByFilterAsync(RightFilterModel filterModel)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var result = new ResponseObject<List<RightModel>>();
                    var data = unit.GetRepository<IdmRight>().GetAll();
                    if (data != null)
                    {
                        if (!string.IsNullOrEmpty(filterModel.FullTextSearch))
                        {
                            data = from dt in data
                                   where dt.RightCode.ToLower().Contains(filterModel.FullTextSearch.ToLower())
                                   || dt.RightName.ToLower().Contains(filterModel.FullTextSearch.ToLower())
                                   || dt.Description.ToLower().Contains(filterModel.FullTextSearch.ToLower())
                                   select dt;
                        }


                        if (data != null)
                        {
                            if (filterModel.Size <= 0)
                            {
                                filterModel.Size = 20;
                            }
                            filterModel.Page = filterModel.Page - 1;
                            int excludedRows = ((int)filterModel.Page) * ((int)filterModel.Size);
                            if (excludedRows <= 0)
                            {
                                excludedRows = 0;
                            }
                            var totalCount = data.Count();
                            data = data.Skip(excludedRows).Take((int)filterModel.Size);

                            var fullQueryData = await data.ToListAsync();
                            // Convert Data
                            if (fullQueryData != null && fullQueryData.Count() > 0)
                            {
                                result.Message = "Success get filter with filter";
                                result.Status = 1;
                                result.DataCount = fullQueryData.Count();
                                result.TotalCount = totalCount;
                                result.Data = AutoMapperUtils.AutoMap<IdmRight, RightModel>(fullQueryData);
                            }
                            return result;
                        }
                        else
                            return new ResponseObject<List<RightModel>>(null, "No data found for filter", Code.NotFound);

                    }
                    else
                        return new ResponseObject<List<RightModel>>(null, "No data found", Code.NotFound);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<RightModel>>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<ResponseObject<RightUpdateModel>> UpdateAsync(RightUpdateModel model)
        {
            try
            {

                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unitOfWork.GetRepository<IdmRight>().Get(a => a.RightCode == model.RightCode).FirstOrDefaultAsync();
                    // Convert
                    if (data != null)
                    {
                        data.RightName = model.RightName;
                        data.IsGroup = model.IsGroup;
                        data.Description = model.Description;
                        data.Level = model.Level;
                        data.GroupCode = model.GroupCode;

                        unitOfWork.GetRepository<IdmRight>().Update(data);
                        if (await unitOfWork.SaveAsync() >= 1)
                        {
                            return new ResponseObject<RightUpdateModel>(model);
                        }
                        else return new ResponseObject<RightUpdateModel>(null, "Save failed", Code.ServerError);
                    }
                    else return new ResponseObject<RightUpdateModel>(null, "No data found", Code.NotFound);
                }

                //return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<RightUpdateModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> DeleteMany(List<String> ids)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var resultList = new List<RightModel>();
                    foreach (var id in ids)
                    {
                        var deleteResult = await DeleteByIdAsync(id);
                    }
                    var result = new Response(Code.Success, "Delete multiple success");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
    }
}
