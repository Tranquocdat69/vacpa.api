﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class RightModel
    {
        public string RightCode { get; set; }    
        public string RightName { get; set; } 
        public string Description { get; set; }
        public bool? Status { get; set; }
        public int? Order { get; set; }
        public bool? IsGroup { get; set; }
        public int? Level { get; set; }    
        public string GroupCode { get; set; }
    }
    public class RightCreateModel
    {
        public string RightCode { get; set; }
        public string RightName { get; set; }
        public string Description { get; set; }
        public bool? Status { get; set; }
        public int? Order { get; set; }
        public bool IsGroup { get; set; }
        public int? Level { get; set; }
        public string GroupCode { get; set; }
    }
    public class RightUpdateModel
    {
        public string RightCode { get; set; }
        public string RightName { get; set; }
        public string Description { get; set; }
        public bool? Status { get; set; }
        public int? Order { get; set; }
        public bool IsGroup { get; set; }
        public int Level { get; set; }
        public string GroupCode { get; set; }

    }
    public class RightFilterModel : PaginationRequest
    {       

    }

}
