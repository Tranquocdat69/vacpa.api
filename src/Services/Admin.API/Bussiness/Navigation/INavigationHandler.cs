﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public interface INavigationHandler
    {
        Task<Response> GetNavigationsByUserId(Guid userId, Guid appId);
        Task<Response> CheckNavigationsByUserID(string userId, string navigationUrl, string v);
        Task<Response> GetAllRightsInNavigations(string userId, string navigationId, string navigationUrl, string v);
        Task<ResponseObject<List<NavigationModel>>> GetNavigations(string textSearch, bool v);
        Task<Response> GetAllNavigations(string p);
        //ManhNX: Lay ra cac chuc nang cuoi
        Task<Response> GetNavSubChild();
        Task<Response> CreateRoleInNavigations(NavigationRoleUpdateModel value);
        Task<Response> UpdateRoleInNavigations(NavigationRoleUpdateModel value);
        Task<Response> Create(NavigationCreateRequestModel value);
        Task<Response> Update(NavigationUpdateRequestModel value);
        Task<Response> Delete(Guid navigationId);
        Task<Response> GetNavigationByRole(Guid roleId);
    }
}
