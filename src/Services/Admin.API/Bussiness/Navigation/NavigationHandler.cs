﻿using CommonUtils;
using DataUtils;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Business
{
    public class NavigationHandler : INavigationHandler
    {
        public async Task<Response> GetNavigationsByUserId(Guid userId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var query = (from user in unitOfWork.GetRepository<IdmUsers>().GetAll()
                                 join userrole in unitOfWork.GetRepository<IdmUsersInRoles>().GetAll()
                                 on user.UserId equals userrole.UserId
                                 join navrole in unitOfWork.GetRepository<NavigationRole>().GetAll()
                                 on userrole.RoleId equals navrole.RoleId
                                 join nav in unitOfWork.GetRepository<Navigation>().GetAll()
                                 on navrole.NavigationId equals nav.NavigationId
                                 where nav.Status == true && user.UserId == userId && userrole.ApplicationId == applicationId
                                 select nav).Distinct().OrderBy(r => r.Order);

                    var sRights = await query.ToListAsync();
                    List<NavigationModel> rRights = new List<NavigationModel>();

                    foreach (Navigation sRight in sRights)
                    {
                        if (sRight.ParentId == null)
                        {
                            rRights.Add(GetNav(sRights, sRight));
                        }

                    }
                    return new ResponseObject<List<NavigationModel>>(rRights,string.Empty, Code.Success);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "data not found");
            }
        }
        public async Task<Response> CheckNavigationsByUserID(string userID, string navigationUrl, string applicationId)
        {
            try
            {
                navigationUrl = "#" + navigationUrl;
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await (from user in unitOfWork.GetRepository<IdmUsers>().GetAll()
                                join userrole in unitOfWork.GetRepository<IdmUsersInRoles>().GetAll()
                                on user.UserId equals userrole.UserId
                                join role in unitOfWork.GetRepository<IdmRoles>().GetAll()
                                on userrole.RoleId equals role.RoleId
                                join navrole in unitOfWork.GetRepository<NavigationRole>().GetAll()
                                on role.RoleId equals navrole.RoleId
                                join nav in unitOfWork.GetRepository<Navigation>().GetAll()
                                on navrole.NavigationId equals nav.NavigationId
                                where nav.Status == true &&
                                    user.UserId == new Guid(userID) &&
                                    userrole.ApplicationId == new Guid(applicationId) &&
                                    !string.IsNullOrEmpty(nav.UrlRewrite) &&
                                    navigationUrl.ToLower().Contains(nav.UrlRewrite.ToLower())
                                select nav).FirstOrDefaultAsync();
                    if (data != null)
                        return new ResponseObject<bool>(true, string.Empty, Code.Success);
                    else                        
                    return new ResponseError(Code.ServerError, "data not found");

                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Error");
            }
        }
        public async Task<Response> GetAllRightsInNavigations(string userID, string navigationId, string navigationUrl, string applicationId)
        {
            try
            {
                navigationUrl = "#" + navigationUrl;
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var query = (from user in unitOfWork.GetRepository<IdmUsers>().GetAll()
                                 join userrole in unitOfWork.GetRepository<IdmUsersInRoles>().GetAll()
                                 on user.UserId equals userrole.UserId
                                 where user.UserId == new Guid(userID) && userrole.ApplicationId == new Guid(applicationId)
                                 join role in unitOfWork.GetRepository<IdmRoles>().GetAll()
                                 on userrole.RoleId equals role.RoleId
                                 join rightrole in unitOfWork.GetRepository<IdmRightsInRole>().GetAll()
                                 on role.RoleId equals rightrole.RoleId
                                 join right in unitOfWork.GetRepository<IdmRight>().GetAll()
                                 on rightrole.RightCode equals right.RightCode
                                 where right.Status == true
                                 join navrole in unitOfWork.GetRepository<NavigationRole>().GetAll() on role.RoleId equals navrole.RoleId
                                 join nav in unitOfWork.GetRepository<Navigation>().GetAll() on navrole.NavigationId equals nav.NavigationId
                                 where nav.Status == true && nav.UrlRewrite == navigationUrl
                                 select right).Distinct().OrderBy(r => r.Order);

                    var sRights = await query.ToListAsync();
                    List<RightModel> rRights = new List<RightModel>();

                    foreach (IdmRight sRight in sRights)
                    {
                        //if (sRight.ParentRightId == null)
                        //{
                        //    rRights.Add(GetRight(sRights, sRight));
                        //}

                    }
                    return new ResponseObject<List<RightModel>>(rRights,string.Empty, Code.Success);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        public async Task<Response> GetAllNavigations(string applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var sRights = await unitOfWork.GetRepository<Navigation>().GetAll().OrderBy(s => s.Order).ToListAsync();
                    return new ResponseObject<List<BaseNavigationModel>>(ConvertDatas(sRights), string.Empty, Code.Success);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        //ManhNX
        public async Task<Response> GetNavSubChild()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var nav = await unitOfWork.GetRepository<Navigation>().GetAll().OrderBy(s => s.Order).Where(s => s.HasChild == false).Where(t => t.Status == true).ToListAsync();
                    return new ResponseObject<List<BaseNavigationModel>>(ConvertDatas(nav), string.Empty, Code.Success);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        private BaseNavigationModel ConvertData(Navigation navigationModel)
        {
            try
            {
                //var str = "Convert field |";

                if (navigationModel != null)
                {
                    //str += navigationModel.NavigationId;
                    BaseNavigationModel fieldModel = new BaseNavigationModel()
                    {
                        NavigationId = navigationModel.NavigationId,
                        Name = navigationModel.Name,
                        Order = navigationModel.Order,
                        IdPath = navigationModel.IdPath,
                        Path = navigationModel.Path,
                        Level = navigationModel.Level,
                        Status = navigationModel.Status,

                    };
                    return fieldModel;
                }
                return null;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return null;
            }
        }
        private List<BaseNavigationModel> ConvertDatas(IEnumerable<Navigation> fields)
        {
            List<BaseNavigationModel> fieldList = new List<BaseNavigationModel>();
            foreach (Navigation field in fields)
            {
                fieldList.Add(ConvertData(field));
            }
            return fieldList;
        }

        /// <summary>
        /// Get all navigation tree with parent - subchild
        /// </summary>
        /// <param name="applicationId">allow null</param>
        /// <returns></returns>
        public async Task<ResponseObject<List<NavigationModel>>> GetNavigations(string textSearch, bool IsGetRoles)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    List<NavigationModel> rRights = new List<NavigationModel>();
                    List<Navigation> sRights = await unitOfWork.GetRepository<Navigation>().GetAll().OrderBy(s => s.Order).ToListAsync();

                    if (!String.IsNullOrEmpty(textSearch))
                    {
                        sRights = sRights.Where(x => x.Name.ToLower().Contains(textSearch)).ToList();
                    }
                    if (sRights != null && sRights.Count > 0)
                    {
                        foreach (Navigation sRight in sRights)
                        {
                            if (sRight.ParentId == null)
                            {
                                rRights.Add(GetNav(sRights, sRight, IsGetRoles));
                            }
                        }
                        return new ResponseObject<List<NavigationModel>>(rRights, string.Empty, Code.Success);
                    }
                    return new ResponseObject<List<NavigationModel>>(null, string.Empty, Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<NavigationModel>>(null, ex.Message, Code.ServerError);
            }
        }
        /// <summary>
        /// Get điều hướng
        /// </summary>
        /// <param name="Navs">list all nav</param>
        /// <param name="Nav">item cha</param>
        /// <param name="p">có lấy roles list</param>
        /// <returns></returns>
        private NavigationModel GetNav(List<Navigation> Navs, Navigation Nav, bool isGetRoles)
        {
            NavigationModel rNav = new NavigationModel();
            rNav.NavigationId = Nav.NavigationId;
            rNav.ApplicationId = Nav.ApplicationId;
            rNav.ParentId = Nav.ParentId;
            rNav.IconClass = Nav.IconClass;
            rNav.Code = Nav.Code;
            rNav.Name = Nav.Name;
            rNav.Order = Nav.Order;
            rNav.Status = Nav.Status;
            rNav.UrlRewrite = Nav.UrlRewrite;
            rNav.IdPath = Nav.IdPath;
            rNav.Path = Nav.Path;
            rNav.Level = Nav.Level;
            rNav.SubUrl = Nav.SubUrl;
            rNav.SubRight = new List<NavigationModel>();
            using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))

            {
                if (isGetRoles)
                {
                    var navR = unitOfWork.GetRepository<NavigationRole>().Get(s => s.NavigationId == Nav.NavigationId).Select(s => s.RoleId).Distinct().ToList();
                    List<string> rL = new List<string>();
                    foreach (var item in navR)
                    {
                        rL.Add(item.ToString());
                    }
                    rNav.RoleList = rL.ToArray();
                }

                foreach (Navigation tNav in Navs)
                {
                    if (tNav.ParentId == Nav.NavigationId)
                    {
                        rNav.SubRight.Add(GetNav(Navs, tNav, isGetRoles));
                    }
                }
            }

            return rNav;
        }
        public async Task<Response> Create(NavigationCreateRequestModel obj)
        {
            try
            {
                if (string.IsNullOrEmpty(obj.Code) ||string.IsNullOrEmpty(obj.Name) )
                {
                    return new ResponseObject<NavigationModel>(null, "Kiểm tra lại dữ liệu, Không thể để tên, mã trống!", Code.Forbidden);
                }
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    Guid Id = Guid.NewGuid();
                    var cNav = new Navigation()
                    {
                        Name = obj.Name,
                        Code = obj.Code,
                        Status = obj.Status != null?obj.Status.Value:true,
                        UrlRewrite = obj.UrlRewrite,
                        ApplicationId = obj.ApplicationId,
                        IconClass = obj.IconClass,
                        Order = obj.Order,
                        CreatedByUserId = obj.CreatedByUserId,
                        CreatedOnDate = obj.CreatedOnDate,
                        NavigationId = Id,
                        SubUrl = obj.SubUrl,

                        IdPath = obj.ParentModel != null ? (obj.ParentModel.IdPath == null ? (Id + "/") : (obj.ParentModel.IdPath + Id + "/")) : (Id + "/"),
                        Path = obj.ParentModel != null ? (obj.ParentModel.Path == null ? (obj.Name + "/") : (obj.ParentModel.Path + obj.Name + "/")) : (obj.Name + "/"),
                        Level = obj.ParentModel != null ? (obj.ParentModel.NavigationId == null ? 0 : (obj.ParentModel.Level + 1)) : 0,
                    };

                    if (obj.ParentModel != null)
                        if (obj.ParentModel.NavigationId != null)
                        {
                            cNav.ParentId = obj.ParentModel.NavigationId;
                        }

                    if (!string.IsNullOrEmpty(cNav.UrlRewrite) && cNav.UrlRewrite != "#/")
                    {
                        if (CheckUrlRewrite(cNav.UrlRewrite, null))
                            return new ResponseObject<NavigationModel>( new NavigationModel
                            {
                                NavigationId = cNav.NavigationId,
                                Name = cNav.Name,
                                ParentId = cNav.ParentId,
                                Code = cNav.Code,
                                UrlRewrite = cNav.UrlRewrite,
                                Order = cNav.Order,
                                Status = cNav.Status,
                                SubUrl = cNav.SubUrl,
                            }, "Trùng đường dẫn",Code.BadRequest);
                    }

                    // sonpn 23082018 comment
                    //if (!string.IsNullOrEmpty(cNav.Code))
                    //{
                    //    if (CheckCode(cNav.Code, null).Data)
                    //        return new ResponseObject<NavigationModel>(0, "Trùng Code", new NavigationModel
                    //        {
                    //            NavigationId = cNav.NavigationId,
                    //            Name = cNav.Name,
                    //            ParentId = cNav.ParentId,
                    //            Code = cNav.Code,
                    //            UrlRewrite = cNav.UrlRewrite,
                    //            Order = cNav.Order,
                    //            Status = cNav.Status,
                    //            SubUrl = cNav.SubUrl,
                    //        });
                    //}

                    unitOfWork.GetRepository<Navigation>().Add(cNav);
                    var parentItem = (from t in unitOfWork.GetRepository<Navigation>().GetAll() where t.NavigationId == cNav.ParentId select t).FirstOrDefault();
                    if (parentItem != null)
                    {
                        parentItem.HasChild = true;
                        unitOfWork.GetRepository<Navigation>().Update(parentItem);
                    }

                    if (await unitOfWork.SaveAsync() > 0)
                    {
                        return new ResponseObject<NavigationModel>(new NavigationModel
                        {
                            NavigationId = cNav.NavigationId,
                            Name = cNav.Name,
                            ParentId = cNav.ParentId,
                            Code = cNav.Code,
                            UrlRewrite = cNav.UrlRewrite,
                            Order = cNav.Order,
                            Status = cNav.Status,
                            SubUrl = cNav.SubUrl,
                        }, "Tạo điều hướng thành công!", Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<NavigationModel>(null, "Kiểm tra lại dữ liệu, có thể trùng tên điều hướng hoặc lỗi khác!", Code.NotFound);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<NavigationModel>(null, "Kiểm tra lại dữ liệu, có thể trùng tên điều hướng hoặc lỗi khác!", Code.ServerError);
            }
        }

        /// <summary>
        /// Sửa điều hướng
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<Response> Update(NavigationUpdateRequestModel obj)
        {
            try
            {
                if (string.IsNullOrEmpty(obj.Code) ||string.IsNullOrEmpty(obj.Name) )
                {
                    return new ResponseObject<NavigationModel>(null, "Kiểm tra lại dữ liệu, Không thể để tên, mã trống!", Code.Forbidden);
                }
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var uNav = unitOfWork.GetRepository<Navigation>().Find(obj.NavigationId);
                    if (uNav != null)
                    {
                        var navsRepo = unitOfWork.GetRepository<Navigation>();
                        var childList = GetListChildNav(obj.NavigationId);
                        //kiem tra nav cha khong dk la chinh no hoac con no
                        childList.Add(obj.NavigationId.ToString());
                        foreach (string navigationId in childList)
                        {
                            if (obj.ParentModel != null)
                                if (navigationId == obj.ParentModel.NavigationId.ToString())
                                    return new ResponseObject<NavigationModel>(null, "Không được chọn điều hướng cha là điều hướng con của điều hướng hiện tại!", Code.ServerError);
                        }
                        if (!string.IsNullOrEmpty(obj.UrlRewrite) && obj.UrlRewrite != "#/")
                        {
                            if (CheckUrlRewrite(obj.UrlRewrite, obj.NavigationId))
                              
                                return new ResponseObject<NavigationModel>(null, "Trùng đường dẫn!", Code.BadRequest);
                        }

                        // sonpn 23082018 comment
                        //if (!string.IsNullOrEmpty(obj.Code))
                        //{
                        //    if (CheckCode(obj.Code, obj.NavigationId).Data)
                        //        return new ResponseObject<NavigationModel>(0, "Trùng Code!", null);
                        //}

                        uNav.Name = obj.Name;
                        uNav.UrlRewrite = obj.UrlRewrite;
                        uNav.IconClass = obj.IconClass;
                        uNav.Status = obj.Status != null ? obj.Status.Value : true;
                        uNav.Order = obj.Order;
                        uNav.LastModifiedByUserId = obj.LastModifiedByUserId;
                        uNav.LastModifiedOnDate = DateTime.Now;
                        uNav.Code = obj.Code;
                        uNav.SubUrl = obj.SubUrl;
                        if (obj.ParentModel == null || obj.ParentModel.NavigationId == null)
                        {
                            uNav.ParentId = null;
                            uNav.IdPath = uNav.NavigationId + "/";
                            uNav.Path = obj.Name + "/";
                            uNav.Level = 0;
                        }
                        else
                        {
                            var ParentNavId = obj.ParentModel.NavigationId;
                            if (ParentNavId != uNav.ParentId)
                            {
                                var parentItem = (from t in unitOfWork.GetRepository<Navigation>().GetAll() where t.NavigationId == ParentNavId select t).FirstOrDefault();
                                //TODO: UPDATE nav CON CỦA nav HIỆN TẠI
                                if (parentItem != null)
                                {
                                    parentItem.HasChild = true;
                                    unitOfWork.GetRepository<Navigation>().Update(parentItem);

                                    uNav.ParentId = obj.ParentModel.NavigationId;
                                    uNav.IdPath = obj.ParentModel.IdPath + uNav.NavigationId + "/";
                                    uNav.Path = obj.ParentModel.Path + obj.Name + "/";
                                    uNav.Level = obj.ParentModel.Level + 1;
                                }
                            }
                        }
                        navsRepo.Update(uNav);
                        UpdateNavChilds(uNav);

                        //DELETE role old parent
                        var navRoles = unitOfWork.GetRepository<NavigationRole>();
                        //FromSubNavigation: từ các quyền lấy từ nav con => tìm các con
                        var updateNavRoles = (from d in unitOfWork.GetRepository<NavigationRole>().GetAll() where d.NavigationId == obj.NavigationId && d.FromSubNavigation != null select d.FromSubNavigation).Distinct().ToList();
                        //lấy các quyền của nó cho cha nó để xóa đi
                        var deleteNavRoles = (from d in unitOfWork.GetRepository<NavigationRole>().GetAll() where d.FromSubNavigation == obj.NavigationId select d).ToList();
                        if (updateNavRoles.Count() > 0)
                        {
                            var listParent = GetListParentNav(obj.NavigationId);
                            if (listParent.Count > 0)
                            {
                                foreach (var temp in updateNavRoles)
                                {
                                    foreach (var parent in listParent)
                                    {
                                        var uNavRoles2 = (from d in unitOfWork.GetRepository<NavigationRole>().GetAll() where d.FromSubNavigation == temp && d.NavigationId == new Guid(parent) select d).ToList();
                                        deleteNavRoles = deleteNavRoles.Concat(uNavRoles2).ToList();
                                    }
                                }
                            }
                        }
                        foreach (var item in deleteNavRoles)
                        {
                            navRoles.Delete(item);
                        }
                        //unitOfWork.GetRepository<Navigation>().Update(uNav);
                        if (await unitOfWork.SaveAsync() >= 1)
                        {
                            MoveRole(uNav.NavigationId);
                            return new ResponseObject<NavigationModel>(new NavigationModel
                            {
                                NavigationId = uNav.NavigationId,
                                Name = uNav.Name,
                                ParentId = uNav.ParentId,
                                Code = uNav.Code,
                                UrlRewrite = uNav.UrlRewrite
                            }, "Update success",Code.Success);
                        }
                        else
                            return new ResponseObject<NavigationModel>( new NavigationModel
                            {
                                NavigationId = uNav.NavigationId,
                                Name = uNav.Name,
                                ParentId = uNav.ParentId,
                                Code = uNav.Code,
                                UrlRewrite = uNav.UrlRewrite
                            }, "Update failed",Code.ServerError);
                    }
                    else
                        return new ResponseObject<NavigationModel>(new NavigationModel
                        {
                            NavigationId = uNav.NavigationId,
                            Name = uNav.Name,
                            ParentId = uNav.ParentId,
                            Code = uNav.Code,
                            UrlRewrite = uNav.UrlRewrite
                        }, "Update error",Code.ServerError);
                }
                //return new ResponseObject<NavigationModel>(0, "", null);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<NavigationModel>(null, "Trùng tên điều hướng hoặc lỗi khác!", Code.ServerError);
            }
        }

        /// <summary>
        /// update parent navigation roles
        /// </summary>
        /// <param name="navigationId"></param>
        public void MoveRole(Guid navigationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var uNavRoles = new NavigationRoleUpdateModel();
                    uNavRoles.NavigationId = navigationId;
                    var cNavRoleRepo = unitOfWork.GetRepository<NavigationRole>();
                    var listParentsNavigationId = GetListParentNav(navigationId);
                    var listRole = unitOfWork.GetRepository<NavigationRole>().Get(u => u.NavigationId == navigationId);
                    foreach (var navRole in listRole)
                    {
                        foreach (var item in listParentsNavigationId)
                        {
                            var navR = new NavigationRole()
                            {
                                NavigationRoleId = Guid.NewGuid(),
                                RoleId = navRole.RoleId,
                                NavigationId = new Guid(item),
                            };
                            if (navRole.FromSubNavigation == null)
                                navR.FromSubNavigation = navRole.NavigationId;
                            else
                                navR.FromSubNavigation = navRole.FromSubNavigation;
                            cNavRoleRepo.Add(navR);
                        }
                    }
                    if (unitOfWork.Save() >= 1)
                    {
                        //logger.Info("Move role success!");
                    }
                    else
                    {

                        //logger.Info("Move role Error!");
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
            }
        }

        //rename navigation
        private void UpdateNavChildsPath(Guid nodeId, string oldName, string newName, IRepository<Navigation> navRepo)
        {
            // 1. Lay ve danh sach tat ca cac nav con
            var childNavs = (from navs in navRepo.GetAll() where navs.ParentId == nodeId select navs);
            if (childNavs != null)
            {
                // 2. Duyet qua tung nav con
                foreach (var childNav in childNavs)
                {
                    childNav.Path = childNav.Path.Replace(oldName + "/", newName + "/");
                    navRepo.Update(childNav);

                    // 2.1 Cap nhat duong dan tat ca nav con
                    UpdateNavChildsPath(childNav.NavigationId, oldName, newName, navRepo);
                }
            }
        }
        private async  Task<bool> UpdateNavChilds(Navigation ParentNav)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var parentNavId = ParentNav.NavigationId;
                    // 1. Lay ve danh sach tat ca cac nav con
                    var childNavs = (from navs in await unitOfWork.GetRepository<Navigation>().GetAll().ToListAsync()
                                     where navs.ParentId == parentNavId select navs);
                    if (childNavs != null)
                    {
                        // 2. Duyet qua tung nav con
                        foreach (var childNav in childNavs)
                        {
                            childNav.ParentId = ParentNav.NavigationId;
                            childNav.Level = ParentNav.Level + 1;
                            childNav.Path = ParentNav.Path + childNav.Name + "/";
                            childNav.IdPath = ParentNav.IdPath + childNav.NavigationId + "/";
                            childNav.LastModifiedOnDate = DateTime.Now;
                           
                            unitOfWork.GetRepository<Navigation>().Update(childNav);
                            // 2.1 Cap nhat duong dan tat ca nav con
                            await UpdateNavChilds(childNav);
                            
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return false;
            }
        }

        /// <summary>
        /// Kiểm tra tồn tại đường dẫn
        /// </summary>
        /// <param name="url"></param>
        /// <param name="navigationId"></param>
        /// <returns></returns>
        public bool CheckUrlRewrite(string url, Guid? navigationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = from nav in unitOfWork.GetRepository<Navigation>().GetAll()
                               where
                               nav.UrlRewrite.ToLower() == url.ToLower()
                               select nav;
                    if (navigationId != null)
                        data = from dt in data where dt.NavigationId != navigationId select dt;
                    if (data.Count() > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return false;
            }
        }

        /// <summary>
        /// Kiểm tra trùng code
        /// </summary>
        /// <param name="code"></param>
        /// <param name="navigationId"></param>
        /// <returns></returns>
        public async Task<Response> CheckCode(string code, Guid? navigationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = from nav in unitOfWork.GetRepository<Navigation>().GetAll()
                               where
                               nav.Code.ToLower() == code.ToLower()
                               select nav;
                    if (navigationId != null)
                        data = from dt in data where dt.NavigationId != navigationId select dt;
                    if (await data.CountAsync() > 0)
                        return new ResponseObject<bool>(true, string.Empty, Code.Success);
                    else
                        return new ResponseObject<bool>(false, string.Empty, Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<bool>(false, string.Empty, Code.ServerError);
            }
        }

        /// <summary>
        /// Get List Parent Navigation
        /// </summary>
        /// <param name="navId"></param>
        /// <returns></returns>
        private List<string> GetListParentNav(Guid? navId)
        {
            using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
            {
                List<string> navIds = new List<string>();
                // Lay ve nhom nguoi dung
                var sNav = unitOfWork.GetRepository<Navigation>().Get(sp => sp.NavigationId == navId).FirstOrDefault();

                if (sNav != null && sNav.ParentId != null)
                {
                    navIds.Add(sNav.ParentId.ToString());
                    var navParentId = GetListParentNav(sNav.ParentId);
                    if (navParentId.Count > 0)
                    {
                        foreach (var item in navParentId)
                        {
                            navIds.Add(item);
                        }
                    }
                }
                return navIds;
            }
        }

        private List<string> GetListChildNav(Guid? navId)
        {
            using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
            {
                List<string> navIds = new List<string>();
                // Lay ve tat ca dieu huong con cua dh hien tai
                var sNav = unitOfWork.GetRepository<Navigation>().Get(sp => sp.ParentId == navId);

                if (sNav != null || sNav.Count() > 0)
                {
                    foreach (var item in sNav)
                    {
                        navIds.Add(item.NavigationId.ToString());
                        var navChildId = GetListChildNav(item.NavigationId);
                        if (navChildId.Count > 0)
                        {
                            foreach (var obj in navChildId)
                            {
                                navIds.Add(obj);
                            }
                        }
                    }
                }
                return navIds;
            }
        }

        /// <summary>
        /// Lấy về một Điều hướng
        /// </summary>
        /// <param name="nav"></param>
        /// <returns></returns>
        private NavigationModel GetOneNav(Navigation nav)
        {
            using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
            {
                var role = new NavigationModel
                {
                    NavigationId = nav.NavigationId,
                    Name = nav.Name,
                    Code = nav.Code,
                    Order = nav.Order,
                    Status = nav.Status,
                    RoleList = nav.NavigationRole.Select(s => s.RoleId.ToString()).ToArray(),
                    IconClass = nav.IconClass,
                    ParentId = nav.ParentId,
                    UrlRewrite = nav.UrlRewrite
                };
                return role;
            }
        }

        /// <summary>
        /// Tạo quyền cho điều hướng con và điều hướng cha tương ứng
        /// </summary>
        /// <param name="nav"></param>
        /// <returns></returns>
        public async Task<Response> CreateRoleInNavigations(NavigationRoleUpdateModel nav)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    // Lay ve nhom nguoi dung
                    var sNav = unitOfWork.GetRepository<Navigation>().Get(sp => sp.NavigationId == nav.NavigationId).FirstOrDefault();

                    if (sNav != null)
                    {

                        // Cap nhat nhom nguoi dung
                        var cNavRoleRepo = unitOfWork.GetRepository<NavigationRole>();

                        //// Xoa tat ca chuc nang duoc gan voi nhom nguoi dung
                        //cNavRoleRepo.Delete(rr => rr.NavigationId == (nav.NavigationId));

                        var listNavigationId = GetListParentNav(nav.NavigationId);

                        // Them moi lan luot cac chuc nang moi duoc gan voi nhom nguoi dung
                        foreach (string RoleId in nav.RoleList)
                        {
                            foreach (var item in listNavigationId)
                            {
                                var navR = new NavigationRole()
                                {
                                    NavigationRoleId = Guid.NewGuid(),
                                    RoleId = new Guid(RoleId),
                                    NavigationId = new Guid(item),
                                    FromSubNavigation = nav.NavigationId,
                                };
                                cNavRoleRepo.Add(navR);
                            }
                            var navRole = new NavigationRole()
                            {
                                NavigationRoleId = Guid.NewGuid(),
                                RoleId = new Guid(RoleId),
                                NavigationId = nav.NavigationId,
                            };
                            cNavRoleRepo.Add(navRole);
                        }
                        // Commit tat ca lenh
                        if (await unitOfWork.SaveAsync() >= 1)
                        {
                            return new ResponseObject<NavigationModel>(GetOneNav(sNav), string.Empty, Code.Success);
                        };
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return null;
            }
        }

        /// <summary>
        /// Sửa phân quyền cho Điều hướng
        /// </summary>
        /// <param name="nav"></param>
        /// <returns></returns>
        public async Task<Response> UpdateRoleInNavigations(NavigationRoleUpdateModel nav)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    // kiểm tra điều hướng tồn tại
                    var sNav = unitOfWork.GetRepository<Navigation>().Get(sp => sp.NavigationId == nav.NavigationId).FirstOrDefault();

                    if (sNav != null)
                    {
                        var cNavRoleRepo = unitOfWork.GetRepository<NavigationRole>();
                        //get all old roles of nav, tách những role bị trùng
                        var oldRoles = unitOfWork.GetRepository<NavigationRole>().Get(o => o.NavigationId == nav.NavigationId).Select(s => s.RoleId).Distinct().ToList();
                        //new roles list
                        var newRoles = nav.RoleList.ToList();
                        //list all roles not changed
                        var temporaryRoles = new List<Guid>();
                        foreach (var oItem in oldRoles)
                        {
                            foreach (var nRoleId in newRoles)
                            {
                                if (oItem == new Guid(nRoleId))
                                {
                                    temporaryRoles.Add(oItem);
                                    break;
                                }
                            }
                        }
                        if (temporaryRoles.Count() == newRoles.Count() && newRoles.Count() == oldRoles.Count())
                            return new ResponseObject<NavigationModel>(GetOneNav(sNav), "Roles không thay đổi", Code.BadRequest);
                        //Remove roles unchanged
                        foreach (var temp in temporaryRoles)
                        {
                            oldRoles.Remove(temp);
                            newRoles.Remove(temp.ToString());
                        }

                        //delete old roles and add new roles
                        foreach (var oItem in oldRoles)
                        {
                            //vì ở trên select distinct nên phải get lại
                            var listNavRole =  await unitOfWork.GetRepository<NavigationRole>().Get(g => g.NavigationId == nav.NavigationId && g.RoleId == oItem).ToListAsync();
                            foreach (var item in listNavRole)
                            {
                                // xoa tat ca row an theo Fromsub = navId => các quyền cha kế thừa từ con
                                // xoa row co nav = fromsub => 
                                // xoa row fromsub = fromsub, xoa row
                                if(item.FromSubNavigation != null)
                                {
                                    if (!string.IsNullOrEmpty(item.FromSubNavigation.ToString()))
                                    {

                                        unitOfWork.GetRepository<NavigationRole>().Delete(rr => (rr.FromSubNavigation == item.NavigationId && rr.RoleId == item.RoleId) ||
                                          (rr.NavigationId == item.FromSubNavigation && rr.RoleId == item.RoleId) || (rr.FromSubNavigation == item.FromSubNavigation && rr.RoleId == item.RoleId) ||
                                          (rr.NavigationRoleId == item.NavigationRoleId));
                                    }
                                }
                                
                                else
                                {
                                    unitOfWork.GetRepository<NavigationRole>().Delete(rr => (rr.FromSubNavigation == item.NavigationId && rr.RoleId == item.RoleId) ||
                                      (rr.NavigationRoleId == item.NavigationRoleId));
                                }
                            }
                        }
                        if (newRoles.Count > 0)
                        {
                            var listNavigationId = GetListParentNav(nav.NavigationId);
                            foreach (var RoleId in newRoles)
                            {
                                foreach (var item in listNavigationId)
                                {
                                    var navR = new NavigationRole()
                                    {
                                        NavigationRoleId = Guid.NewGuid(),
                                        RoleId = new Guid(RoleId),
                                        NavigationId = new Guid(item),
                                        FromSubNavigation = nav.NavigationId,
                                    };
                                    cNavRoleRepo.Add(navR);
                                }
                                var navRole = new NavigationRole()
                                {
                                    NavigationRoleId = Guid.NewGuid(),
                                    RoleId = new Guid(RoleId),
                                    NavigationId = nav.NavigationId,
                                };
                                cNavRoleRepo.Add(navRole);
                            }
                        }

                        // Commit tat ca lenh
                        if (await unitOfWork.SaveAsync() >= 1)
                        {
                            return new ResponseObject<NavigationModel>(GetOneNav(sNav), string.Empty, Code.Success);
                        }
                        else
                            //TODO:
                            return new ResponseObject<NavigationModel>(null, "Erorr: Sửa role thất bại", Code.BadRequest);
                    }
                    else
                        return new ResponseObject<NavigationModel>(null, "", Code.BadRequest);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<NavigationModel>(null, "Exception: Không sửa được role ", Code.BadRequest);
            }
        }

        /// <summary>
        /// Xóa điều hướng - xóa cha thì xóa luôn con
        /// </summary>
        /// <param name="navigationId">mã điều hướng</param>
        /// <returns></returns>
        public bool DeleteNavHasChild(Guid navigationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var datas = unitOfWork.GetRepository<Navigation>();
                    var dNav = unitOfWork.GetRepository<Navigation>().Find(navigationId);
                    if (dNav != null)
                    {
                        var navRoles = unitOfWork.GetRepository<NavigationRole>();
                        var dNavRoles = (from d in unitOfWork.GetRepository<NavigationRole>().GetAll() where d.NavigationId == dNav.NavigationId || d.FromSubNavigation == dNav.NavigationId select d);
                        foreach (var obj in dNavRoles)
                        {
                            navRoles.Delete(obj);
                        }
                        //xóa các điều hướng con
                        var listNavigationId = GetListChildNav(dNav.NavigationId);
                        // Them moi lan luot cac chuc nang moi duoc gan voi nhom nguoi dung
                        foreach (var item in listNavigationId)
                        {
                            //var dchilds = (from d in unitOfWork.GetRepository<Navigation>().GetAll()
                            //      where d.ParentId == dNav.NavigationId
                            //      select d);
                            var dchild = unitOfWork.GetRepository<Navigation>().Find(new Guid(item));
                            navRoles.Delete(rr => rr.NavigationId == new Guid(item) || rr.FromSubNavigation == new Guid(item));
                            datas.Delete(dchild);
                        }
                        datas.Delete(dNav);
                    }
                    if (unitOfWork.Save() >= 1)
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return false;
            }
        }

        //undone: có thể chỉnh sửa lại respone một chút
        /// <summary>
        /// Xóa những nav ko có nav con
        /// </summary>
        /// <param name="navigationId"></param>
        /// <returns></returns>
        public async Task<Response> Delete(Guid navigationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var datas = unitOfWork.GetRepository<Navigation>();
                    var dNav = unitOfWork.GetRepository<Navigation>().Find(navigationId);
                    if (dNav != null)
                    {
                        //delete Role_Navigation
                        var navRoles = unitOfWork.GetRepository<NavigationRole>();
                        var dNavRoles = (from d in unitOfWork.GetRepository<NavigationRole>().GetAll() where d.NavigationId == dNav.NavigationId || d.FromSubNavigation == dNav.NavigationId select d);
                        foreach (var obj in dNavRoles)
                        {
                            navRoles.Delete(obj);
                        }
                        datas.Delete(dNav);
                    }
                    if (await unitOfWork.SaveAsync() >= 1)
                        return new ResponseObject<NavigationDeleteResponseModel>(null, "Xóa thành công!", Code.Success);
                    else
                        return new ResponseObject<NavigationDeleteResponseModel>(null, "Không xóa được ", Code.BadRequest);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<NavigationDeleteResponseModel>(null, "Exception: Không xóa được ", Code.BadRequest);
            }
        }

        public NavigationModel GetNav(IList<Navigation> Navs, Navigation Nav)
        {
            NavigationModel rNav = new NavigationModel();
            rNav.NavigationId = Nav.NavigationId;
            rNav.ParentId = Nav.ParentId;
            rNav.Code = Nav.Code;
            rNav.Name = Nav.Name;
            rNav.UrlRewrite = Nav.UrlRewrite;
            rNav.IconClass = Nav.IconClass;
            rNav.SubRight = new List<NavigationModel>();
            //rNav.RoleList = Nav.Navigation_Role.Select(s => s.RoleID.ToString()).ToArray();
            if (Nav.HasChild)
            {
                foreach (Navigation tNav in Navs)
                {
                    if (tNav.ParentId == Nav.NavigationId)
                    {
                        rNav.SubRight.Add(GetNav(Navs, tNav));
                    }
                }
            }
            return rNav;
        }

        //Get danh sách các navigation theo từng Role
        /* 
        select distinct(a.NavigationId),c.RoleName, b.Name from Navigation_role a
        join Navigation b on a.NavigationId = b.NavigationId
        join aspnet_Roles c on a.RoleId = c.RoleId
        where a.RoleId ='AE04D738-502A-4D7A-A199-ECD0C82C053C'
         */
        /// <summary>
        /// Get all Navigation by Role id
        /// </summary>
        /// <param name="roleId">roleId</param>
        /// <returns></returns>
        public async Task<Response> GetNavigationByRole(Guid roleId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    List<NavigationModel> finalNavigationRole = new List<NavigationModel>();
                    var datas = (from nav in unitOfWork.GetRepository<Navigation>().GetAll() 
                                 join navR in unitOfWork.GetRepository<NavigationRole>().GetAll() 
                                 on nav.NavigationId equals navR.NavigationId 
                                 join role in unitOfWork.GetRepository<IdmRoles>().GetAll() 
                                 on navR.RoleId equals role.RoleId 
                                 where navR.RoleId == roleId select nav).Distinct();

                    var re = await datas.ToListAsync();
                    //
                    IList<NavigationModel> allNavigationTree = GetNavigations(null, false).Result.Data;

                    foreach (var item in re)
                    {
                        foreach (var item2 in allNavigationTree)
                        {
                            if (item.NavigationId == item2.NavigationId)
                            {
                                item2.IsRoleChecked = true;
                            }
                            else
                                //item2.IsRoleChecked = false;
                                CheckSubNavigationRole(item.NavigationId, item2);
                        }
                    }
                    return new ResponseObject<IList<NavigationModel>>(allNavigationTree, string.Empty, Code.Success);

                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<IList<NavigationModel>>(null, ex.Message, Code.ServerError);
            }
        }
        public void CheckSubNavigationRole(Guid navigationId, NavigationModel item)
        {
            foreach (var item2 in item.SubRight)
            {
                if (navigationId == item2.NavigationId)
                {
                    item2.IsRoleChecked = true;
                }
                else
                    //item2.IsRoleChecked = false;
                    CheckSubNavigationRole(navigationId, item2);
            }
        }
    }
}
