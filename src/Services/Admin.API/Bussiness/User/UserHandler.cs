﻿using ApiUtils;
using CommonUtils;
using DataUtils;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class UserHandler : IUserHandler
    {
        //public IEventBus _eventBus;
        private MembershipPasswordFormat passwordFormat;
        public PasswordRender _password;
        private readonly string _ipfmUri;
        private readonly IWorkflowHandler _workflowHandler;
        private readonly IBus _busControl;
        private readonly string EmailService = Utils.GetConfig("emailservice:apiuri");
        private readonly string CatalogService = Utils.GetConfig("catalogservice:apiuri");

        public UserHandler(IWorkflowHandler workflowHandler, IBus handler)
        {
            _workflowHandler = workflowHandler;
            _busControl = handler;
        }
        public UserHandler()
        {
            _ipfmUri = Utils.GetConfig("Authentication:IPFM:Uri");
        }

        //public UserHandler(IEventBus eventBus)
        //{
        //    _eventBus = eventBus;
        //    _ipfmUri = Utils.GetConfig("Authentication:IPFM:Uri");
        //}

        public async Task<Response> GetFilter(UserFilterModel filter)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var response = new Pagination<SystemUserModel>();
                    var data = from dt in unitOfWork.GetRepository<IdmUsers>().Get(null, o =>
                        o.Include(r => r.IdmUsersInRoles).ThenInclude(uir => uir.Role)
                        .Include(r => r.IdmRightsOfUser).ThenInclude(uir => uir.RightCodeNavigation)
                        .Include(r => r.IdmUsersInOrganization).ThenInclude(uir => uir.Organization)

                        )
                               select dt;
                    if (filter.ApplicationId.HasValue)
                    {
                        data = data.Where(dt => dt.ApplicationId == filter.ApplicationId);
                    }
                    if (!string.IsNullOrEmpty(filter.FullTextSearch))
                    {
                        var text = filter.FullTextSearch.ToLower();
                        data = data.Where(dt =>
                                        dt.UserName.ToLower().Contains(text) || dt.FullName.ToLower().Contains(text) ||
                                        dt.NickName.ToLower().Contains(text) || dt.Email.ToLower().Contains(text) ||
                                        dt.MobilePin.ToLower().Contains(text)
                                        );
                    }
                    if (filter.Status.HasValue)
                    {
                        data = data.Where(dt => dt.IsLockedOut == filter.Status.Value);
                    }
                    if (filter.Month.HasValue)
                    {
                        data = data.Where(dt => dt.Birthday.Month == filter.Month);
                    }
                    if (filter.Type.HasValue)
                    {
                        data = data.Where(dt => dt.Type == filter.Type);
                    }
                    if (!filter.Size.HasValue)
                    {
                        filter.Size = 20;
                    }
                    filter.Page = filter.Page - 1;
                    int excludedRows = ((int)filter.Page) * ((int)filter.Size);
                    if (excludedRows <= 0)
                    {
                        excludedRows = 0;
                    }

                    data = data.OrderByDescending(dt => dt.LastModifiedOnDate);
                    var totalCount = data.Count();


                    data = data.Skip(excludedRows).Take((int)filter.Size);


                    var dataResult = await data.ToListAsync();
                    var fullQueryData1 = AutoMapperUtils.AutoMap<IdmUsers, SystemUserModel>(dataResult, "Roles", "Rights");
                    response.Content = new List<SystemUserModel>();
                    foreach (var item in dataResult)
                    {
                        response.Content.Add(Convert(item));
                    }

                    //response.Content = fullQueryData;
                    response.NumberOfElements = totalCount;
                    response.Page = filter.Page.HasValue ? filter.Page.Value : 0;
                    response.Size = filter.Size.HasValue ? filter.Size.Value : 0;
                    response.TotalElements = totalCount;
                    response.TotalPages = totalCount / (filter.Size.HasValue ? (filter.Size.Value + 1) : 1);

                    var r = new ResponsePagination<SystemUserModel>(response);

                    return r;
                }
            }
            catch (System.Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<UserModel>>(null, ex.Message, Code.ServerError);
            }
        }


        #region convert_model
        public SystemUserModel Convert(IdmUsers user)
        {
            var model = new SystemUserModel();
            model.UserId = user.UserId;
            model.UserName = user.UserName;
            model.FinanceCode = user.FinanceCode;
            model.Email = user.Email;
            model.OtherEmail = user.OtherEmail;
            model.FullName = user.FullName;
            model.Active = !user.IsLockedOut;
            model.IsLockedOut = user.IsLockedOut;
            model.Mobile = user.MobilePin;
            var CV = UserCollection.Instance.GetUserById(user.UserId);
            model.LastModifiedByFullName = CV.LastModifiedByFullName;
            model.LastModifiedByUserId = CV.LastModifiedByUserId;
            model.LastModifiedOnDate = CV.LastModifiedOnDate;
            model.ChucVu = CV != null ? CV.ChucVu : "";

            model.Positions = new List<PositionModel>();
            if (CV.Positions != null)
            {
                foreach (var pu in CV.Positions)
                {
                    model.Positions.Add(new PositionModel()
                    {
                        Code = pu.Code,
                        Id = pu.Id,
                        Name = pu.Code,
                        Note = pu.Code,
                        Order = pu.Order,
                        Status = pu.Status
                    });
                }
            }

            // Extent
            //model.NickName = !string.IsNullOrEmpty(membership.ShortName) ? membership.ShortName : membership.NickName;

            // QuyenNH: Convert NickName value from ShortName to NickName
            model.NickName = !string.IsNullOrEmpty(user.NickName) ? user.NickName : user.ShortName;

            model.ApplicationId = user.ApplicationId.ToString();



            //model.UpdateBy = UserCollection.Instance.GetUserById(user.LastModifiedByUserId.Value).FullName;
            //model.UpdateDate = user.LastModifiedOnDate.Value;

            try
            {
                var getOrg = user.IdmUsersInOrganization.FirstOrDefault();
                if (getOrg != null)
                {
                    model.OrganizationId = getOrg.OrganizationId.ToString();
                    model.OrganizationName = getOrg.OrganizationName;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");

            }


            try
            {
                model.Rights = new List<SystemRightModel>();
                var getRights = user.IdmRightsOfUser.ToList();
                foreach (var gt in getRights)
                {
                    if ((gt.ApplicationId.ToString() == model.ApplicationId) || (string.IsNullOrEmpty(model.ApplicationId)))
                        model.Rights.Add(ConvertRight(gt.RightCodeNavigation));
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");

            }

            try
            {
                model.Roles = new List<SystemRoleModel>();
                var getRoles = user.IdmUsersInRoles.ToList();
                foreach (var gt in getRoles)
                {
                    if ((gt.ApplicationId.ToString() == model.ApplicationId) || (string.IsNullOrEmpty(model.ApplicationId)))
                        model.Roles.Add(ConvertRole(gt.Role));
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
            }

            //try
            //{
            //    model.Profiles = new List<UserProfileBaseModel>();
            //    var profiles = user.AspnetProfile.ToList();
            //    foreach (var gt in profiles)
            //    {
            //        model.Profiles.Add(new UserProfileBaseModel()
            //        {
            //            Id = gt.Id,
            //            UserId = gt.UserId,
            //            PropertyNames = gt.PropertyNames,
            //            PropertyValuesString = gt.PropertyValuesString
            //        });
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Serilog.Log.Error(ex, "");
            //}



            return model;
        }


        public SystemRightModel ConvertRight(IdmRight data)
        {
            var model = new SystemRightModel();
            model.Enabled = data.Status.HasValue ? data.Status.Value : false;
            model.Order = data.Order.HasValue ? data.Order.Value : 0;
            model.RightCode = data.RightCode;
            model.RightName = data.RightName;
            model.Description = data.Description;
            model.Level = data.Level;
            model.GroupCode = data.GroupCode;
            model.IsGroup = data.IsGroup;
            return model;
        }
        public SystemRoleModel ConvertRole(IdmRoles data)
        {
            var model = new SystemRoleModel();
            // trinm add đẻ đồng bộ dữ liệu sang MSA
            model.ApplicationId = data.ApplicationId;
            model.RoleId = data.RoleId.ToString();
            model.RoleCode = data.RoleCode;
            model.RoleName = data.RoleName;
            model.CreatedDate = data.CreatedOnDate;
            model.UpdateDate = data.LastModifiedOnDate;
            //model.LastModifiedByUserId = data.LastModifiedByUserId;
            //model.LastModifiedByUsername = UserCollection.Instance.GetUserById(data.LastModifiedByUserId).FullName;
            return model;
        }

        #endregion


        public async Task<Response> Create(SystemUserModel model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var checkEmail = unitOfWork.GetRepository<IdmUsers>().Get(a => a.Email == model.Email).FirstOrDefault();
                    if (checkEmail != null)
                    {
                        return new ResponseError(Code.ServerError, "Email đã tồn tại");
                    }
                    var checkUsername = unitOfWork.GetRepository<IdmUsers>().Get(a => a.UserName == model.UserName).FirstOrDefault();
                    if (checkUsername != null)
                    {
                        return new ResponseError(Code.ServerError, "Username đãn tồn tại");
                    }


                    if (!string.IsNullOrEmpty(model.FinanceCode))
                    {
                        var checkExistedFinanceCoe = unitOfWork.GetRepository<IdmUsers>().Get(a => a.FinanceCode == model.FinanceCode).FirstOrDefault();
                        if (checkExistedFinanceCoe != null)
                        {
                            return new ResponseError(Code.ServerError, "Mã tài chính đã tồn tại");
                        }
                    }

                    DateTime createDate = DateTime.Now;
                    string salt = Utils.CreateSalt512();
                    Guid appId = new Guid(model.ApplicationId);
                    var user = new IdmUsers()
                    {
                        UserId = Guid.NewGuid(),
                        UserName = model.UserName,
                        LoweredUserName = model.UserName.ToLowerInvariant(),
                        FullName = model.FullName,
                        ShortName = model.ShortName,
                        NickName = model.NickName,
                        ChucVu = model.ChucVu,
                        MobilePin = model.Mobile,
                        Email = model.Email,
                        OtherEmail = model.OtherEmail,
                        FinanceCode = model.FinanceCode,

                        IsLockedOut = !model.IsLockedOut,
                        ApplicationId = appId,
                        IsAnonymous = false,
                        LastActivityDate = createDate,
                        Type = model.Type,
                        Password = Utils.GenerateHMAC(model.Password, salt),
                        PasswordFormat = (int)passwordFormat,
                        PasswordSalt = salt,

                        CreatedByUserId = model.CreatedByUserId,
                        CreatedOnDate = DateTime.Now,
                    };

                    unitOfWork.GetRepository<IdmUsers>().Add(user);
                    var responseData = AutoMapperUtils.AutoMap<IdmUsers, UserCreateModel>(user);

                    //thêm role cho user
                    if (model.Roles != null && model.Roles.Count() > 0)
                    {
                        foreach (var role in model.Roles)
                        {
                            var roleOfUserDTO = new IdmUsersInRoles
                            {
                                Id = Guid.NewGuid(),
                                RoleId = new Guid(role.RoleId),
                                UserId = user.UserId,
                                CreateDate = DateTime.Now,
                                ApplicationId = new Guid(model.ApplicationId)
                            };
                            unitOfWork.GetRepository<IdmUsersInRoles>().Add(roleOfUserDTO);
                        }
                    }


                    #region extent fmip
                    #endregion extent fmip

                    // trinm Thêm chức vụ
                    if (!String.IsNullOrEmpty(model.ChucVu))
                    {
                        var posData = JsonConvert.DeserializeObject<List<Guid>>(model.ChucVu);
                        List<CreatePositionModel> posList = new List<CreatePositionModel>();
                        foreach (var pos in posData)
                        {
                            var dataPos = new CreatePositionModel()
                            {
                                Id = Guid.NewGuid(),
                                UserId = user.UserId,
                                PositionId = pos,
                            };
                            posList.Add(dataPos);
                        };


                        using var client1 = new HttpClient();

                        client1.BaseAddress = new System.Uri(CatalogService);

                        client1.DefaultRequestHeaders.Accept.Clear();
                        client1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client1.DefaultRequestHeaders.Add("apikey", Utils.GetConfig("catalogservice:apikey"));

                        var user1 = Utils.GetConfig("Authentication:AdminUser");
                        var password = Utils.GetConfig("Authentication:AdminPassWord");
                        var base64String = System.Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user1}:{password}"));
                        client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                        var uriPosition = "api/position/create-positionofuser";

                        string json = JsonConvert.SerializeObject(posList, Formatting.Indented);
                        var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                        var responsePosition = await client1.PostAsync(client1.BaseAddress + uriPosition, httpContent);
                        Console.WriteLine("Log Url CreateUser: " + client1.BaseAddress + uriPosition);

                        if (!responsePosition.IsSuccessStatusCode)
                        {
                            Console.WriteLine("Log CreateUser: " + responsePosition.RequestMessage);
                            return new ResponseError(Code.ServerError, "Lỗi thêm mới", null);
                        }
                    }

                    //trinm them don vi cho user
                    if (model.UserOrganization != null)
                    {
                        IdmUsersInOrganization uinoDTO = new IdmUsersInOrganization
                        {
                            Id = Guid.NewGuid(),
                            UserId = user.UserId,
                            OrganizationId = model.UserOrganization.OrganizationId,
                            OrganizationName = model.UserOrganization.OrganizationName,
                            CreatedByUserId = model.CreatedByUserId,
                            CreatedOnDate = DateTime.Now,
                            //Role = model.UserOrganization.Role
                        };

                        unitOfWork.GetRepository<IdmUsersInOrganization>().Add(uinoDTO);
                    }

                    //----------------------------------------------------------------------------
                    //đồng bộ IPFM & LDAP
                    var IsLdapSync = true;
                    //đồng bộ LDAP
                    //if (IsIpfmSync)
                    //{
                    //    var LDAP_config = Utils.GetConfig("ldap:sync");
                    //    if (LDAP_config == "1")
                    //    {
                    //        var ldapHandler = new LDAPHandler();
                    //        UserIdentity userIdentity = new UserIdentity
                    //        {
                    //            DisplayName = model.FullName,
                    //            Email = !string.IsNullOrEmpty(model.Email) ? model.Email : "",
                    //            Mobile = !string.IsNullOrEmpty(model.Mobile) ? model.Mobile : "0",
                    //            UserName = model.UserName,
                    //            UserPassword = model.Password,
                    //            Uid = user.UserId.ToString(),
                    //            EmployeeNumber = Utils.GenerateNewRandom(),
                    //            // HomeAddress = !string.IsNullOrEmpty(model.Address) ? model.Address : "",
                    //            // FirstName = !string.IsNullOrEmpty(model.FirstName) ? model.FirstName : model.FullName,
                    //            // LastName = !string.IsNullOrEmpty(model.LastName) ? model.LastName : model.FullName,
                    //            EmployeeType = model.Type == 1 ? "1" : "0"
                    //        };

                    //        var resultLdap = ldapHandler.AddUser(userIdentity);
                    //        if (resultLdap.Result == -1)
                    //        {
                    //            IsLdapSync = false;
                    //        }
                    //        else
                    //        {
                    //            IsLdapSync = true;
                    //        }
                    //    }
                    //}

                    

                    if (IsLdapSync)
                    {
                        if (await unitOfWork.SaveAsync() >= 1)
                        {

                            // đồng bộ dữ liệu
                            model.UserId = user.UserId;
                            await _busControl.SendAsync(SyncDataMSAToSOA.CREATE_USER, model);

                            return new ResponseObject<UserCreateModel>(responseData, "Add successfully", Code.Success);
                        }
                        else
                        {
                            return new ResponseObject<UserCreateModel>(null, "Add failed", Code.ServerError);
                        }
                    }
                    else
                    {
                        return new ResponseError(Code.ServerError, "Không đồng bộ được LDAP và IPFM", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        private string CheckLGSP(UserCreateModel model)
        {
            if (model.UserInArea != null)
            {
                if (model.UserInArea.AddressCode != null)
                {
                    return model.UserInArea.AddressCode;
                }
                if (model.UserInArea.WardCode != null)
                {
                    return model.UserInArea.WardCode;
                }
                if (model.UserInArea.DistrictCode != null)
                {
                    return model.UserInArea.DistrictCode;
                }
                if (model.UserInArea.ProvinceCode != null)
                {
                    return model.UserInArea.ProvinceCode;
                }
                return string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }

        public async Task<Response> GetAll()
        {
            try
            {
                Console.WriteLine("_handler.GetAll() 1 ");
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    Console.WriteLine("_handler.GetAll() 2");
                    var listItem = await (from s in unitOfWork.GetRepository<IdmUsers>().Get(null, o =>
                      o.Include(r => r.IdmUsersInRoles).ThenInclude(uir => uir.Role)
                      .Include(r => r.IdmRightsOfUser).ThenInclude(uir => uir.RightCodeNavigation))
                                          join uia in unitOfWork.GetRepository<IdmUserInArea>().GetAll()
                                          on s.UserId equals uia.UserId into ab1
                                          from uiar in ab1.DefaultIfEmpty()
                                          join uio in unitOfWork.GetRepository<IdmUsersInOrganization>().GetAll()
                                          on s.UserId equals uio.UserId into ab
                                          from uior in ab.DefaultIfEmpty()
                                          where s.UserName == "administrator" || s.UserName == "sonpn"
                                          select new { User = s, UserInArea = uiar, UserOrganization = uior }
                    ).ToListAsync();
                    Console.WriteLine("_handler.GetAll() 3");
                    // var listItem = await (from s in unitOfWork.GetRepository<IdmUsers>().GetAll()
                    //                       join uia in unitOfWork.GetRepository<IdmUserInArea>().GetAll()
                    //                       on s.UserId equals uia.UserId into ab1
                    //                       from ok1 in ab1.DefaultIfEmpty()
                    //                       join uio in unitOfWork.GetRepository<IdmUsersInOrganization>().GetAll() 
                    //                       on s.UserId equals uio.UserId into ab
                    //                       from ok in ab.DefaultIfEmpty()
                    //                       select new { User = s, UserInArea = ok1, UserOrganization = ok }
                    // ).ToListAsync();

                    //var r = new ResponseObject<List<IdmUsers>>(listItem);
                    var result = new List<UserModel>();

                    foreach (var item in listItem)
                    {
                        var i = new UserModel()
                        {
                            UserName = item.User.UserName,
                            Id = item.User.UserId,
                            //Active = item.Application
                            ApplicationId = item.User.ApplicationId,
                            //Avatar = item.Avartar
                            Email = item.User.Email,
                            MobilePin = item.User.MobilePin,
                            NickName = item.User.NickName,
                            FullName = item.User.FullName,
                            IsAnonymous = item.User.IsAnonymous,
                            IsLockedOut = item.User.IsLockedOut,
                            LastActivityDate = item.User.LastActivityDate,
                            LoweredUserName = item.User.LoweredUserName,
                            MobileAlias = item.User.MobileAlias,
                            Name = item.User.UserName,
                            Type = item.User.Type,
                            UserId = item.User.UserId,
                            Avatar = item.User.Avatar,
                            Address = item.User.Address,
                        };
                        if (item.UserInArea != null)
                        {
                            i.UserInArea = new UserInArea()
                            {
                                Id = item.UserInArea.Id,
                                AddressCode = item.UserInArea.AddressCode,
                                AddressId = item.UserInArea.AddressId,
                                AddressName = item.UserInArea.AddressName,
                                DistrictCode = item.UserInArea.DistrictCode,
                                DistrictId = item.UserInArea.DistrictId,
                                DistrictName = item.UserInArea.DistrictName,
                                FullAddress = item.UserInArea.FullAddress,
                                Level = item.UserInArea.Level,
                                ProvinceCode = item.UserInArea.ProvinceCode,
                                ProvinceId = item.UserInArea.ProvinceId,
                                ProvinceName = item.UserInArea.ProvinceName,
                                UserId = item.UserInArea.UserId,
                                WardCode = item.UserInArea.WardCode,
                                WardId = item.UserInArea.WardId,
                                WardName = item.UserInArea.WardName
                            };
                        }

                        if (item.UserOrganization != null)
                        {
                            i.UserOrganization = new UsersInOrganizationModel()
                            {
                                Id = item.UserOrganization.Id,
                                OrganizationId = item.UserOrganization.OrganizationId,
                                OrganizationName = item.UserOrganization.OrganizationName,
                                CreatedByUserId = item.UserOrganization.CreatedByUserId,
                                CreatedOnDate = item.UserOrganization.CreatedOnDate,
                                LastModifiedByUserId = item.UserOrganization.LastModifiedByUserId,
                                LastModifiedOnDate = item.UserOrganization.LastModifiedOnDate,
                                UserId = item.UserOrganization.UserId,
                                Role = item.UserOrganization.Role
                            };
                        }



                        result.Add(i);
                    }

                    //IntegrationEvent orderPaymentIntegrationEvent;

                    //orderPaymentIntegrationEvent = new OrderStartedIntegrationEvent(Guid.NewGuid().ToString());

                    //_eventBus.Publish(orderPaymentIntegrationEvent);

                    return new ResponseObject<List<UserModel>>(result);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("_handler.GetAll()", ex);
                return new ResponseObject<List<UserModel>>(new List<UserModel>());
            }

        }

        public async Task<Response> GetById(Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var user = await unitOfWork.GetRepository<IdmUsers>().Get(a => a.UserId == userId).FirstAsync();
                    UserModel userModel = AutoMapperUtils.AutoMap<IdmUsers, UserModel>(user);

                    var userInArea = unitOfWork.GetRepository<IdmUserInArea>().Get(a => a.UserId == userId).FirstOrDefault();
                    if (userInArea != null)
                    {
                        userModel.UserInArea = AutoMapperUtils.AutoMap<IdmUserInArea, UserInArea>(userInArea);
                    }

                    //HaNH
                    var userOrganization = unitOfWork.GetRepository<IdmUsersInOrganization>().Get(a => a.UserId == userId).FirstOrDefault();
                    if (userOrganization != null)
                    {
                        userModel.UserOrganization = AutoMapperUtils.AutoMap<IdmUsersInOrganization, UsersInOrganizationModel>(userOrganization);
                    }
                    var r = new ResponseObject<UserModel>(userModel);
                    return r;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message, null);
            }
        }

        public async Task<Response> GetByUserName(string userName)
        {
            try
            {
                var user = UserCollectionOld.Instance.GetUserByUserName(userName);
                // var r = await GetById(user.Id);
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var dtoUser = await unitOfWork.GetRepository<IdmUsers>().Get(a => a.UserId == user.Id).FirstAsync();
                    UserProfileModel userModel = AutoMapperUtils.AutoMap<IdmUsers, UserProfileModel>(dtoUser);

                    var userInArea = unitOfWork.GetRepository<IdmUserInArea>().Get(a => a.UserId == user.Id).FirstOrDefault();
                    if (userInArea != null)
                    {
                        userModel.UserInArea = AutoMapperUtils.AutoMap<IdmUserInArea, UserInArea>(userInArea);
                    }
                    var r = new ResponseObject<UserProfileModel>(userModel);
                    return r;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message, null);
            }
        }

        public async Task<Response> Update(SystemUserModel model)
        {
            using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
            {
                try
                {
                    var cusrRepo = unitOfWork.GetRepository<IdmUsers>();
                    var userToEdit = cusrRepo.Get(u => u.UserId == model.UserId).FirstOrDefault();
                    if (userToEdit != null)
                    {
                        if (model.Email != userToEdit.Email)
                        {
                            var checkEmail = unitOfWork.GetRepository<IdmUsers>().Get(a => a.UserId != model.UserId && a.Email == model.Email).FirstOrDefault();
                            if (checkEmail != null)
                            {
                                return new ResponseError(Code.ServerError, "Email used");
                            }
                        }

                        var checkUsername = unitOfWork.GetRepository<IdmUsers>().Get(a => a.UserId != model.UserId && a.UserName == model.UserName).FirstOrDefault();
                        if (checkUsername != null)
                        {
                            return new ResponseError(Code.ServerError, "Username used");
                        }

                        userToEdit.FullName = model.FullName;
                        userToEdit.MobilePin = model.Mobile;
                        userToEdit.NickName = model.NickName;
                        userToEdit.Email = model.Email;
                        userToEdit.Avatar = model.Avatar;
                        userToEdit.LoweredUserName = model.UserName.ToLowerInvariant();
                        userToEdit.ShortName = model.ShortName;
                        userToEdit.ChucVu = model.ChucVu;
                        userToEdit.OtherEmail = model.OtherEmail;
                        userToEdit.FinanceCode = model.FinanceCode;
                        userToEdit.IsLockedOut = !model.IsLockedOut;
                        userToEdit.ApplicationId = new Guid(model.ApplicationId);
                        userToEdit.IsAnonymous = false;
                        userToEdit.Type = model.Type;
                        //userToEdit.Address = model.Address;

                        //if (!model.IsApproved)
                        //{
                        //    userToEdit.IsLockedOut = !model.IsLockedOut;
                        //}
                        if (model.Password != null && model.Password != "")
                        {
                            userToEdit.Password = Utils.GenerateHMAC(model.Password, userToEdit.PasswordSalt);
                        }
                        if (!string.IsNullOrEmpty(model.ApplicationId))
                        {
                            userToEdit.ApplicationId = new Guid(model.ApplicationId);
                        }
                        //if (model.Type != null)
                        //{
                        //    userToEdit.Type = model.Type;
                        //}
                        cusrRepo.Update(userToEdit);

                        //update role của user
                        if (model.Roles != null)
                        {
                            //xóa all role of user
                            var oldRoles = unitOfWork.GetRepository<IdmUsersInRoles>().Get(a => a.UserId == model.UserId);
                            unitOfWork.GetRepository<IdmUsersInRoles>().DeleteRange(oldRoles);

                            //thêm lại
                            var uirDTOList = new List<IdmUsersInRoles>();
                            foreach (var role in model.Roles)
                            {
                                var uirDTO = new IdmUsersInRoles()
                                {
                                    Id = Guid.NewGuid(),
                                    RoleId = new Guid(role.RoleId),
                                    UserId = model.UserId,
                                    CreateDate = DateTime.Now,
                                    ApplicationId = new Guid(model.ApplicationId)
                                };
                                uirDTOList.Add(uirDTO);
                            }
                            if (uirDTOList != null)
                            {
                                unitOfWork.GetRepository<IdmUsersInRoles>().AddRange(uirDTOList);
                            }
                        }

                        //HaNH
                        //Update don vi cho user
                        if (model.UserOrganization != null)
                        {
                            var organization = await unitOfWork.GetRepository<IdmUsersInOrganization>().Get(x => x.UserId == model.UserId).FirstOrDefaultAsync();
                            if (organization != null)
                            {
                                //Neu ton tai du lieu thi update
                                organization.OrganizationId = model.UserOrganization.OrganizationId;
                                organization.OrganizationName = model.UserOrganization.OrganizationName;
                                //organization.Role = model.UserOrganization.Role;
                                organization.LastModifiedByUserId = model.CreatedByUserId;
                                organization.LastModifiedOnDate = DateTime.Now;

                                unitOfWork.GetRepository<IdmUsersInOrganization>().Update(organization);
                            }
                            else
                            {
                                //Them moi don vi
                                var uioDTo = new IdmUsersInOrganization
                                {
                                    Id = Guid.NewGuid(),
                                    UserId = model.UserId,
                                    CreatedByUserId = model.UserOrganization.CreatedByUserId,
                                    CreatedOnDate = DateTime.Now,
                                    OrganizationId = model.UserOrganization.OrganizationId,
                                    OrganizationName = model.UserOrganization.OrganizationName,
                                    Role = model.UserOrganization.Role,
                                };

                                unitOfWork.GetRepository<IdmUsersInOrganization>().Add(uioDTo);
                            }

                        }

                        // trinm Thêm chức vụ
                        if (!String.IsNullOrEmpty(model.ChucVu))
                        {
                            var posData = JsonConvert.DeserializeObject<List<Guid>>(model.ChucVu);
                            List<CreatePositionModel> posList = new List<CreatePositionModel>();
                            foreach (var pos in posData)
                            {
                                var dataPos = new CreatePositionModel()
                                {
                                    Id = Guid.NewGuid(),
                                    UserId = userToEdit.UserId,
                                    PositionId = pos,
                                };
                                posList.Add(dataPos);
                            };


                            using var client1 = new HttpClient();

                            client1.BaseAddress = new System.Uri(CatalogService);

                            client1.DefaultRequestHeaders.Accept.Clear();
                            client1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            client1.DefaultRequestHeaders.Add("apikey", Utils.GetConfig("catalogservice:apikey"));

                            var user1 = Utils.GetConfig("Authentication:AdminUser");
                            var password = Utils.GetConfig("Authentication:AdminPassWord");
                            var base64String = System.Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user1}:{password}"));
                            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                            var uriPosition = "api/position/create-positionofuser";

                            string json = JsonConvert.SerializeObject(posList, Formatting.Indented);
                            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                            var responsePosition = await client1.PostAsync(client1.BaseAddress + uriPosition, httpContent);


                            if (!responsePosition.IsSuccessStatusCode)
                            {
                                return new ResponseError(Code.ServerError, "Lỗi thêm mới", null);
                            }
                        }

                        // await unitOfWork.SaveAsync();

                        #region đồng bộ LDAP

                        var IsLdapSync = true;
                        //var LDAP_config = Utils.GetConfig("ldap:sync");
                        //if (LDAP_config == "1")
                        //{
                        //    var ldapHandler = new LDAPHandler();
                        //    UserIdentity userIdentity = new UserIdentity
                        //    {
                        //        DisplayName = model.FullName,
                        //        Email = !string.IsNullOrEmpty(model.Email) ? model.Email : "",
                        //        Mobile = !string.IsNullOrEmpty(model.Mobile) ? model.Mobile : "0",
                        //        UserName = model.Username,
                        //        UserPassword = model.Password,
                        //        Uid = model.UserId.ToString(),
                        //        // HomeAddress = !string.IsNullOrEmpty(model.Address) ? model.Address : "",
                        //        FirstName = model.FullName,
                        //        LastName = model.FullName,
                        //        EmployeeType = model.Type == 1 ? "1" : "0"
                        //    };

                        //    var resultLdap = ldapHandler.EditUser(userIdentity);
                        //    if (resultLdap != "Cập nhật tài khoản thành công!")
                        //    {
                        //        if (resultLdap == "TK không tồn tại")
                        //        {
                        //            //nếu ldap chưa có thì tạo mới
                        //            var resultLdapCreate = ldapHandler.AddUser(userIdentity);
                        //            if (resultLdapCreate.Result == -1)
                        //            {
                        //                IsLdapSync = false;
                        //            }
                        //            else
                        //            {
                        //                IsLdapSync = true;
                        //            }
                        //        }
                        //    }
                        //    else
                        //    {
                        //        IsLdapSync = true;
                        //    }
                        //}
                        #endregion đồng bộ LDAP

                        if (IsLdapSync)
                        {
                            await unitOfWork.SaveAsync();

                            // đồng bộ dữ liệu
                            await _busControl.SendAsync(SyncDataMSAToSOA.UPDATE_USER, model);
                            var result = new ResponseObject<SystemUserModel>(AutoMapperUtils.AutoMap<IdmUsers, SystemUserModel>(userToEdit));
                            return result;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex, "");
                    return null;
                }
            }
        }

        public async Task<Response> LockUser(UserLockingModel model)
        {
            using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
            {
                try
                {
                    var cusrRepo = unitOfWork.GetRepository<IdmUsers>();
                    var userToEdit = cusrRepo.Get(u => u.UserId == model.UserId).FirstOrDefault();
                    if (userToEdit != null)
                    {
                        userToEdit.IsLockedOut = !model.IsApproved;
                        cusrRepo.Update(userToEdit);
                        await unitOfWork.SaveAsync();
                        UserCollectionOld.Instance.LoadToHashSet();
                        var result = new ResponseObject<IdmUsers>(userToEdit);
                        return result;
                    }
                    else
                    {
                        return new ResponseError(Code.Forbidden, "user not found");
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex, "");
                    return null;
                }
            }
        }

        public async Task<Response> UpdateUserPassword(UpdateUserPasswordModel model)
        {
            using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
            {
                try
                {
                    var userToEdit = unitOfWork.GetRepository<IdmUsers>().Get(u => u.UserId == model.UserId).FirstOrDefault();
                    if (userToEdit != null)
                    {
                        //check if old password is right or wrong
                        var hashedPass = Utils.GenerateHMAC(model.OldPassword, userToEdit.PasswordSalt);
                        if (hashedPass == userToEdit.Password || model.IsAdmin == true)
                        {
                            //đồng bộ IPFM & LDAP
                            var IsLdapSync = false;
                            var IsIpfmSync = false;

                            //đồng bộ IPFM

                            //lấy thông tin User trong IPFM
                            var ipfmUserList = new List<ipfmUserModel>();
                            var resApiHandler1 = new ResApiHandler<ipfmGetAllUserModel, ipfmUserModel>(CommonUtils.ResApiTarget.Ipfm);
                            var ipfmUsersResponse = await resApiHandler1.Get(_ipfmUri + "account/get_all_accounts");
                            if (ipfmUsersResponse != null)
                            {
                                ipfmUserList = ipfmUsersResponse.Users;
                            }
                            var ipfmUser = new ipfmUserModel();
                            if (ipfmUserList != null && ipfmUserList.Count() > 0)
                            {
                                ipfmUser = ipfmUserList.Where(a => a.Username.ToLower() == userToEdit.UserName.ToLower()).FirstOrDefault();
                            }

                            if (ipfmUser != null)
                            {
                                var ipfmPasswordUpdate = AutoMapperUtils.AutoMap<ipfmUserModel, ipfmUserModel>(ipfmUser);
                                ipfmPasswordUpdate.Password = Utils.sha256_hash(model.NewPassword);

                                var resApiHandler = new ResApiHandler<UserCreateResponse, ipfmUserModel>(CommonUtils.ResApiTarget.Ipfm);
                                var userUpdate = await resApiHandler.Post(_ipfmUri + "account/update", ipfmPasswordUpdate);
                                if (userUpdate != null)
                                {
                                    if (userUpdate.Code == 0)
                                    {
                                        IsIpfmSync = true;
                                    }
                                }
                            }
                            else
                            {
                                return new ResponseError(Code.BadRequest, "User not found in IPFM");
                            }

                            //đồng bộ LDAP
                            if (IsIpfmSync)
                            {
                                var LDAP_config = Utils.GetConfig("ldap:sync");
                                if (LDAP_config == "1")
                                {
                                    var ldapHandler = new LDAPHandler();
                                    UserIdentity userIdentity = new UserIdentity
                                    {
                                        DisplayName = userToEdit.FullName,
                                        Email = !string.IsNullOrEmpty(userToEdit.Email) ? userToEdit.Email : "",
                                        Mobile = !string.IsNullOrEmpty(userToEdit.MobilePin) ? userToEdit.MobilePin : "0",
                                        UserName = model.Username,
                                        UserPassword = model.NewPassword,
                                        Uid = model.UserId.ToString(),
                                        // HomeAddress = !string.IsNullOrEmpty(model.Address) ? model.Address : "",
                                        FirstName = userToEdit.FullName,
                                        LastName = userToEdit.FullName,
                                        EmployeeType = userToEdit.Type == 1 ? "1" : "0"
                                    };

                                    var resultLdap = ldapHandler.EditUser(userIdentity);
                                    if (resultLdap == "Cập nhật tài khoản thành công!")
                                    {
                                        IsLdapSync = true;
                                    }
                                    else
                                    {
                                        IsLdapSync = false;
                                    }
                                }
                            }
                            if (IsIpfmSync && IsLdapSync)
                            {
                                userToEdit.Password = Utils.GenerateHMAC(model.NewPassword, userToEdit.PasswordSalt);
                                unitOfWork.GetRepository<IdmUsers>().Update(userToEdit);
                                if (await unitOfWork.SaveAsync() >= 1)
                                {
                                    return new ResponseObject<UserCreateModel>(AutoMapperUtils.AutoMap<IdmUsers, UserCreateModel>(userToEdit), "Update password successfully", Code.Success);
                                }
                                else
                                {
                                    return new ResponseObject<UserCreateModel>(null, "Update failed", Code.ServerError);
                                }
                            }
                            else
                            {
                                return new ResponseError(Code.BadRequest, "LDAP or IPFM not synced");
                            }
                        }
                        else
                        {
                            return new ResponseError(Code.BadRequest, "Old password is wrong");
                        }
                    }
                    else
                    {
                        return new ResponseError(Code.Forbidden, "user not found");
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex, "");
                    return null;
                }
            }
        }

        public async Task<Response> Authentication(string userName, string password)
        {
            userName = userName.Trim();
            password = password.Trim();
            var userCollection = UserCollectionOld.Instance.Collection;
            foreach (var user in userCollection)
                if (user.UserName == userName || user.Email == userName)
                {
                    var passhash = Utils.GenerateHMAC(password, user.PasswordSalt);
                    if (passhash == user.Password)
                        return new ResponseObject<UserModel>(user);
                    return new ResponseError(Code.BadRequest, "Sai mật khẩu");
                }

            return new ResponseError(Code.BadRequest, "Không tìm thấy tài khoản");
        }
        //ManhNX
        public async Task<Response> Register(UserRegisterModel model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var checkEmail = unitOfWork.GetRepository<IdmUsers>().Get(a => a.Email == model.Email).FirstOrDefault();
                    if (checkEmail != null)
                    {
                        return new ResponseError(Code.ServerError, "Email used");
                    }
                    var checkUsername = unitOfWork.GetRepository<IdmUsers>().Get(a => a.UserName == model.Username).FirstOrDefault();
                    if (checkUsername != null)
                    {
                        return new ResponseError(Code.ServerError, "Username used");
                    }

                    DateTime createDate = DateTime.Now;
                    string salt = Utils.CreateSalt512();
                    Guid appId = unitOfWork.GetRepository<IdmApplications>().GetAll().Select(x => x.ApplicationId).FirstOrDefault();
                    var user = new IdmUsers()
                    {
                        UserId = Guid.NewGuid(),
                        UserName = model.Username,
                        LoweredUserName = model.Username.ToLowerInvariant(),
                        ApplicationId = appId,
                        IsAnonymous = false,
                        LastActivityDate = createDate,
                        Type = model.Type.Value,
                        NickName = model.NickName,
                        MobilePin = model.Mobile,
                        Email = model.Email,
                        FullName = model.FullName,
                        Password = Utils.GenerateHMAC(model.Password, salt),
                        PasswordFormat = (int)passwordFormat,
                        PasswordSalt = salt,
                        IsLockedOut = false, //!model.IsApproved,
                        Birthday = model.Birthday,
                        SchemeName = model.SchemeName,
                        WorkFlowProcessTime = model.CreatedOnDate,
                        WorkflowProcessUserId = model.CreatedByUserId
                    };
                    //Gán WF
                    // var workflowRequestResult = await _workflowHandler.CreateDocument(new Document()
                    // {
                    //     Id = user.UserId,
                    //     Name = "PHE_DUYET_TAI_KHOAN_" + DateTime.Now.ToString("HH-mm-ss"),
                    //     Number = 1,
                    //     Comment = "Khởi tạo",
                    //     AuthorId = model.WorkflowProcessUserId.HasValue ? model.WorkflowProcessUserId.Value : model.CreatedByUserId.Value,
                    //     ManagerId = Guid.Empty,
                    //     Sum = 0,
                    //     State = "",
                    //     StateName = "",
                    //     SchemeName = model.SchemeName
                    // }) as ResponseObject<Document>;

                    // if (workflowRequestResult.Code != Code.Success)
                    // {
                    //     return new ResponseError(Code.Forbidden, "Workflow not work");
                    // }
                    // user.WorkFlowCurrentState = workflowRequestResult.Data.StateName;

                    unitOfWork.GetRepository<IdmUsers>().Add(user);
                    var responseData = AutoMapperUtils.AutoMap<IdmUsers, UserRegisterModel>(user);
                    #region testLDAP
                    //----------------------------------------------------------------------------
                    //đồng bộ IPFM & LDAP
                    //var IsLdapSync = false;
                    //var IsIpfmSync = false;

                    //đồng bộ LDAP
                    //if (IsIpfmSync)
                    //{
                    //    var LDAP_config = Utils.GetConfig("ldap:sync");
                    //    if (LDAP_config == "1")
                    //    {
                    //        var ldapHandler = new LDAPHandler();
                    //        UserIdentity userIdentity = new UserIdentity
                    //        {
                    //            DisplayName = model.FullName,
                    //            Email = !string.IsNullOrEmpty(model.Email) ? model.Email : "",
                    //            Mobile = !string.IsNullOrEmpty(model.Mobile) ? model.Mobile : "0",
                    //            UserName = model.Username,
                    //            UserPassword = model.Password,
                    //            Uid = user.UserId.ToString(),
                    //            EmployeeNumber = Utils.GenerateNewRandom(),
                    //            // HomeAddress = !string.IsNullOrEmpty(model.Address) ? model.Address : "",
                    //            // FirstName = !string.IsNullOrEmpty(model.FirstName) ? model.FirstName : model.FullName,
                    //            // LastName = !string.IsNullOrEmpty(model.LastName) ? model.LastName : model.FullName,
                    //            EmployeeType = model.Type == 1 ? "1" : "0"
                    //        };

                    //        var resultLdap = ldapHandler.AddUser(userIdentity);
                    //        if (resultLdap.Result == -1)
                    //        {
                    //            IsLdapSync = false;
                    //        }
                    //        else
                    //        {
                    //            IsLdapSync = true;
                    //        }
                    //    }
                    //}
                    #endregion
                    //if (IsLdapSync && IsIpfmSync)
                    //{
                    if (unitOfWork.Save() >= 1)
                    {

                        using var client = new HttpClient();
                        client.BaseAddress = new System.Uri(EmailService);
                        var uri = "Email/SendEmail";
                        client.DefaultRequestHeaders.Add("Accept", "text/html");
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));
                        client.Timeout = TimeSpan.FromMilliseconds(20000);

                        var keyValue = new RegisterModel()
                        {
                            USER_NAME = model.Username,
                        };

                        var Subject = "Tài khoản" + " " + '"' + model.Username + '"' + " " + "đã được gửi, chờ xử lý";

                        IList<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>> {
                                { new KeyValuePair<string, string>("ToEmails", (!String.IsNullOrEmpty(model.Email)) ? model.Email:"minhtrihd97@gmail.com") },
                                { new KeyValuePair<string, string>("CcEmails", "hoan@gmail.com") },
                                { new KeyValuePair<string, string>("BccEmails", "trinm@gmail.com") },
                                { new KeyValuePair<string, string>("Subject", Subject) },
                                { new KeyValuePair<string, string>("BodyValues", JsonConvert.SerializeObject(keyValue)) },
                                { new KeyValuePair<string, string>("Type", "2") },
                                { new KeyValuePair<string, string>("Files", "") },
                            };

                        var response = await client.PostAsync(client.BaseAddress + uri, new FormUrlEncodedContent(nameValueCollection));

                        if (response.IsSuccessStatusCode)
                        {
                            var result1 = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                        }

                        return new ResponseObject<UserRegisterModel>(responseData, "Add successfully", Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<UserRegisterModel>(null, "Add failed", Code.ServerError);
                    }
                    //}
                    //else
                    //{
                    //    return new ResponseError(Code.ServerError, "Không đồng bộ được LDAP và IPFM", null);
                    //}
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        public async Task<Response> CreateUserByQTHT(UserCreateByQTHTModel model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var checkEmail = unitOfWork.GetRepository<IdmUsers>().Get(a => a.Email == model.Email).FirstOrDefault();
                    if (checkEmail != null)
                    {
                        return new ResponseError(Code.ServerError, "Email used");
                    }
                    var checkUsername = unitOfWork.GetRepository<IdmUsers>().Get(a => a.UserName == model.Username).FirstOrDefault();
                    if (checkUsername != null)
                    {
                        return new ResponseError(Code.ServerError, "Username used");
                    }

                    if (!string.IsNullOrEmpty(model.FinanceCode))
                    {
                        var checkExistedFinanceCoe = unitOfWork.GetRepository<IdmUsers>().Get(a => a.FinanceCode == model.FinanceCode).FirstOrDefault();
                        if (checkExistedFinanceCoe != null)
                        {
                            return new ResponseError(Code.ServerError, "Mã tài chính đã tồn tại");
                        }
                    }

                    DateTime createDate = DateTime.Now;
                    string salt = Utils.CreateSalt512();
                    Guid appId = new Guid(model.ApplicationId);
                    var user = new IdmUsers()
                    {
                        UserId = Guid.NewGuid(),
                        UserName = model.Username,
                        LoweredUserName = model.Username.ToLowerInvariant(),
                        ApplicationId = appId,
                        IsAnonymous = false,
                        LastActivityDate = createDate,
                        Type = model.Type.Value,
                        NickName = model.NickName,
                        MobilePin = model.Mobile,
                        Email = model.Email,
                        FullName = model.FullName,
                        Password = Utils.GenerateHMAC(model.Password, salt),
                        PasswordFormat = (int)passwordFormat,
                        PasswordSalt = salt,
                        IsLockedOut = true, //!model.IsApproved,
                        Birthday = model.Birthday,
                        SchemeName = model.SchemeName,
                        WorkFlowProcessTime = model.CreatedOnDate,
                        WorkflowProcessUserId = model.CreatedByUserId
                    };

                    unitOfWork.GetRepository<IdmUsers>().Add(user);
                    var responseData = AutoMapperUtils.AutoMap<IdmUsers, UserCreateByQTHTModel>(user);

                    //thêm role cho user
                    if (model.RoleList != null && model.RoleList.Count() > 0)
                    {
                        foreach (var role in model.RoleList)
                        {
                            var roleOfUserDTO = new IdmUsersInRoles
                            {
                                Id = Guid.NewGuid(),
                                RoleId = role,
                                UserId = user.UserId,
                                CreateDate = DateTime.Now,
                                ApplicationId = new Guid(model.ApplicationId)
                            };
                            unitOfWork.GetRepository<IdmUsersInRoles>().Add(roleOfUserDTO);
                        }
                    }


                    //if (model.UserOrganization != null)
                    //{
                    //    IdmUsersInOrganization uinoDTO = new IdmUsersInOrganization
                    //    {
                    //        Id = Guid.NewGuid(),
                    //        UserId = user.UserId,
                    //        OrganizationId = model.UserOrganization.OrganizationId,
                    //        OrganizationName = model.UserOrganization.OrganizationName,
                    //        CreatedByUserId = model.CreatedByUserId,
                    //        CreatedOnDate = DateTime.Now,
                    //        Role = model.UserOrganization.Role
                    //    };

                    //    unitOfWork.GetRepository<IdmUsersInOrganization>().Add(uinoDTO);
                    //}


                    if (await unitOfWork.SaveAsync() >= 1)
                    {
                        return new ResponseObject<UserCreateByQTHTModel>(responseData, "Add successfully", Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<UserCreateByQTHTModel>(null, "Add failed", Code.ServerError);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        public async Task<Response> GetUserRegisterFilter(UserRegisterFilterModel filter)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var result = new ResponseObject<List<UserModel>>();
                    var userInRole = unitOfWork.GetRepository<IdmUsersInRoles>().GetAll();
                    var role = unitOfWork.GetRepository<IdmRoles>().GetAll();
                    var data = from dt in unitOfWork.GetRepository<IdmUsers>().Get()
                               select dt;
                    if (filter.ApplicationId.HasValue)
                    {
                        data = data.Where(dt => dt.ApplicationId == filter.ApplicationId);
                    }
                    if (!string.IsNullOrEmpty(filter.FullTextSearch))
                    {
                        var text = filter.FullTextSearch.ToLower();
                        data = data.Where(dt => dt.UserName.ToLower().Contains(text) ||
                            dt.FullName.ToLower().Contains(text) || dt.Email.ToLower().Contains(text));
                    }
                    if (!filter.Size.HasValue)
                    {
                        filter.Size = 20;
                    }
                    filter.Page = filter.Page - 1;
                    int excludedRows = ((int)filter.Page) * ((int)filter.Size);
                    if (excludedRows <= 0)
                    {
                        excludedRows = 0;
                    }
                    if (filter.IsLockedOut.HasValue)
                    {
                        data = data.Where(dt => dt.IsLockedOut == filter.IsLockedOut.Value);
                    }
                    if (filter.Type.HasValue)
                    {
                        data = data.Where(dt => dt.Type == filter.Type);
                    }
                    if (filter.WFStatus == "2")
                    {
                        data = data.Where(dt => dt.WorkFlowCurrentState == "Chờ duyệt");
                    }
                    if (filter.WFStatus == "3")
                    {
                        data = data.Where(dt => dt.WorkFlowCurrentState == "Đã duyệt");
                    }
                    var totalCount = data.Count();
                    data = data.Skip(excludedRows).Take((int)filter.Size);

                    var dataResult = await data.ToListAsync();

                    var fullQueryData = AutoMapperUtils.AutoMap<IdmUsers, UserModel>(dataResult);
                    // Get List Role 
                    foreach (var item in fullQueryData)
                    {
                        var roleName = "";
                        var listRoleId = userInRole.Where(x => x.UserId == item.UserId).ToList();
                        if (listRoleId.Count() > 0)
                        {
                            foreach (var item1 in listRoleId)
                            {
                                var roleId = role.Where(x => x.RoleId == item1.RoleId).FirstOrDefault();
                                roleName += roleId.RoleName + ",";
                            }
                            roleName = roleName.Substring(0, roleName.LastIndexOf(','));
                        }
                        item.ListRoleName = roleName;
                    }
                    var response = new Pagination<UserModel>();
                    response.Content = fullQueryData;
                    response.NumberOfElements = totalCount;
                    response.Page = filter.Page.HasValue ? filter.Page.Value : 0;
                    response.Size = filter.Size.HasValue ? filter.Size.Value : 0;
                    response.TotalElements = totalCount;
                    response.TotalPages = totalCount / (filter.Size.HasValue ? (filter.Size.Value + 1) : 1);

                    var r = new ResponsePagination<UserModel>(response);

                    return r;
                }
            }
            catch (System.Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<UserModel>>(null, ex.Message, Code.ServerError);
            }
        }
        public async Task<Response> UpdateUserByQTHT(UserUpdateByQTHTModel model)
        {
            using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
            {
                try
                {
                    var cusrRepo = unitOfWork.GetRepository<IdmUsers>();
                    var userToEdit = cusrRepo.Get(u => u.UserId == model.UserId).FirstOrDefault();
                    if (userToEdit != null)
                    {
                        var checkEmail = unitOfWork.GetRepository<IdmUsers>().Get(a => a.UserId != model.UserId && a.Email == model.Email).FirstOrDefault();
                        if (checkEmail != null)
                        {
                            return new ResponseError(Code.ServerError, "Email used");
                        }

                        var checkUsername = unitOfWork.GetRepository<IdmUsers>().Get(a => a.UserId != model.UserId && a.UserName == model.Username).FirstOrDefault();
                        if (checkUsername != null)
                        {
                            return new ResponseError(Code.ServerError, "Username used");
                        }
                        userToEdit.UserName = model.Username;
                        userToEdit.FullName = model.FullName;
                        userToEdit.MobilePin = model.Mobile;
                        userToEdit.NickName = model.NickName;
                        userToEdit.Email = model.Email;
                        userToEdit.Birthday = model.Birthday;

                        if (!model.IsApproved)
                        {
                            userToEdit.IsLockedOut = !model.IsApproved;
                        }
                        if (model.Password != null && model.Password != "")
                        {
                            userToEdit.Password = Utils.GenerateHMAC(model.Password, userToEdit.PasswordSalt);
                        }
                        if (!string.IsNullOrEmpty(model.ApplicationId))
                        {
                            userToEdit.ApplicationId = new Guid(model.ApplicationId);
                        }
                        if (model.Type != null)
                        {
                            userToEdit.Type = model.Type.Value;
                        }
                        cusrRepo.Update(userToEdit);

                        //update role của user
                        if (model.RoleList != null)
                        {
                            //xóa all role of user
                            var oldRoles = unitOfWork.GetRepository<IdmUsersInRoles>().Get(a => a.UserId == model.UserId);
                            unitOfWork.GetRepository<IdmUsersInRoles>().DeleteRange(oldRoles);

                            //thêm lại
                            var uirDTOList = new List<IdmUsersInRoles>();
                            foreach (var roleId in model.RoleList)
                            {
                                var uirDTO = new IdmUsersInRoles()
                                {
                                    Id = Guid.NewGuid(),
                                    RoleId = roleId,
                                    UserId = model.UserId,
                                    CreateDate = DateTime.Now,
                                    ApplicationId = new Guid(model.ApplicationId)
                                };
                                uirDTOList.Add(uirDTO);
                            }
                            if (uirDTOList != null)
                            {
                                unitOfWork.GetRepository<IdmUsersInRoles>().AddRange(uirDTOList);
                            }
                        }
                        #region đồng bộ LDAP

                        //var IsLdapSync = false;

                        //    var LDAP_config = Utils.GetConfig("ldap:sync");
                        //    if (LDAP_config == "1")
                        //    {
                        //        var ldapHandler = new LDAPHandler();
                        //        UserIdentity userIdentity = new UserIdentity
                        //        {
                        //            DisplayName = model.FullName,
                        //            Email = !string.IsNullOrEmpty(model.Email) ? model.Email : "",
                        //            Mobile = !string.IsNullOrEmpty(model.Mobile) ? model.Mobile : "0",
                        //            UserName = model.Username,
                        //            UserPassword = model.Password,
                        //            Uid = model.UserId.ToString(),
                        //            FirstName = model.FullName,
                        //            LastName = model.FullName,
                        //            EmployeeType = model.Type == 1 ? "1" : "0"
                        //        };

                        //        var resultLdap = ldapHandler.EditUser(userIdentity);
                        //        if (resultLdap != "Cập nhật tài khoản thành công!")
                        //        {
                        //            if (resultLdap == "TK không tồn tại")
                        //            {
                        //                //nếu ldap chưa có thì tạo mới
                        //                var resultLdapCreate = ldapHandler.AddUser(userIdentity);
                        //                if (resultLdapCreate.Result == -1)
                        //                {
                        //                    IsLdapSync = false;
                        //                }
                        //                else
                        //                {
                        //                    IsLdapSync = true;
                        //                }
                        //            }
                        //        }
                        //        else
                        //        {
                        //            IsLdapSync = true;
                        //        }
                        //    }


                        #endregion đồng bộ LDAP

                        //if (IsLdapSync)
                        //{
                        await unitOfWork.SaveAsync();
                        var result = new ResponseObject<UserModel>(AutoMapperUtils.AutoMap<IdmUsers, UserModel>(userToEdit));
                        return result;
                        //}
                        //else
                        //{
                        //    return null;
                        //}
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex, "");
                    return null;
                }
            }
        }
        public Response CheckMailAlready(CheckMailModel model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var cusrRepo = unitOfWork.GetRepository<IdmUsers>();
                    var userToEdit = cusrRepo.Get(u => u.UserId == model.UserId).FirstOrDefault();
                    if (userToEdit != null)
                    {
                        var checkEmail = unitOfWork.GetRepository<IdmUsers>().Get(a => a.UserId != model.UserId && a.Email == model.Email.Trim()).FirstOrDefault();
                        if (checkEmail != null)
                        {
                            return new ResponseObject<bool>(true);
                        }
                        else
                        {
                            return new ResponseObject<bool>(false);
                        }
                    }
                    else
                    {
                        return new ResponseObject<UserCreateModel>(null, "UserId not exist", Code.ServerError);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        public async Task<Response> ForgotPassword(string username, string email)
        {
            using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
            {
                try
                {
                    var cusrRepo = unitOfWork.GetRepository<IdmUsers>();
                    var userToEdit = cusrRepo.Get(u => u.UserName == username && u.Email == email).FirstOrDefault();
                    if (userToEdit != null)
                    {
                        //Random mat khau cho tai khoan
                        var newPassword = CreateRandomPassword(6);
                        userToEdit.Password = Utils.GenerateHMAC(newPassword, userToEdit.PasswordSalt);

                        cusrRepo.Update(userToEdit);

                        #region đồng bộ LDAP
                        //var IsLdapSync = false;
                        //var LDAP_config = Utils.GetConfig("ldap:sync");
                        //if (LDAP_config == "1")
                        //{
                        //    var ldapHandler = new LDAPHandler();
                        //    UserIdentity userIdentity = new UserIdentity
                        //    {
                        //        //DisplayName = model.FullName,
                        //        //Email = !string.IsNullOrEmpty(model.Email) ? model.Email : "",
                        //        //Mobile = !string.IsNullOrEmpty(model.Mobile) ? model.Mobile : "0",
                        //        //UserName = model.Username,
                        //        UserPassword = newPassword
                        //        //Uid = userId.ToString(),
                        //        // HomeAddress = !string.IsNullOrEmpty(model.Address) ? model.Address : "",
                        //        //FirstName = model.FullName,
                        //        //LastName = model.FullName,
                        //        //EmployeeType = model.Type == 1 ? "1" : "0"
                        //    };

                        //    var resultLdap = ldapHandler.EditUser(userIdentity);
                        //    if (resultLdap != "Cập nhật tài khoản thành công!")
                        //    {
                        //        if (resultLdap == "TK không tồn tại")
                        //        {
                        //            //nếu ldap chưa có thì tạo mới
                        //            var resultLdapCreate = ldapHandler.AddUser(userIdentity);
                        //            if (resultLdapCreate.Result == -1)
                        //            {
                        //                IsLdapSync = false;
                        //            }
                        //            else
                        //            {
                        //                IsLdapSync = true;
                        //            }
                        //        }
                        //    }
                        //    else
                        //    {
                        //        IsLdapSync = true;
                        //    }
                        //}
                        #endregion đồng bộ LDAP

                        //if (IsLdapSync)
                        //{

                        await unitOfWork.SaveAsync();
                        var result = new ResponseObject<UserModel>(AutoMapperUtils.AutoMap<IdmUsers, UserModel>(userToEdit));
                        // Gui emai toi tk khach

                        using var client = new HttpClient();
                        client.BaseAddress = new System.Uri(EmailService);
                        var uri = "Email/SendEmail";
                        client.DefaultRequestHeaders.Add("Accept", "text/html");
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));
                        client.Timeout = TimeSpan.FromMilliseconds(20000);

                        var keyValue = new EmailBodyModel()
                        {
                            LAST_MODIFIED_ON_DATE = DateTime.Now.ToString(),
                            NEW_PASSWORD = newPassword,

                        };

                        var Subject = "Tài khoản" + " " + '"' + username + '"' + " " + "đã được đổ mật khẩu";

                        IList<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>> {
                                { new KeyValuePair<string, string>("ToEmails", (!String.IsNullOrEmpty(email)) ? email:"minhtrihd97@gmail.com") },
                                { new KeyValuePair<string, string>("CcEmails", "hoan@gmail.com") },
                                { new KeyValuePair<string, string>("BccEmails", "trinm@gmail.com") },
                                { new KeyValuePair<string, string>("Subject", Subject) },
                                { new KeyValuePair<string, string>("BodyValues", JsonConvert.SerializeObject(keyValue)) },
                                { new KeyValuePair<string, string>("Type", "1") },
                                { new KeyValuePair<string, string>("Files", "") },
                            };

                        var response = await client.PostAsync(client.BaseAddress + uri, new FormUrlEncodedContent(nameValueCollection));

                        if (response.IsSuccessStatusCode)
                        {
                            var result1 = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                        }


                        Console.WriteLine(result.Message);
                        return new ResponseError(Code.Success, "Send mail success!");
                        //}
                        //else
                        //{
                        //    return new ResponseError(Code.ServerError, "LDAP sync failed!");
                        //}
                    }
                    else
                    {
                        var checkEmail = unitOfWork.GetRepository<IdmUsers>().Get(a => a.Email == email).FirstOrDefault();
                        if (checkEmail == null)
                        {
                            return new ResponseError(Code.ServerError, "Wrong Email");
                        }

                        var checkUsername = unitOfWork.GetRepository<IdmUsers>().Get(a => a.UserName == username).FirstOrDefault();
                        if (checkUsername == null)
                        {
                            return new ResponseError(Code.ServerError, "Wrong Username");
                        }
                        return new ResponseError(Code.ServerError, "Username or password incorrect");
                    }
                }
                catch (Exception ex)
                {
                    Serilog.Log.Error(ex, "");
                    return null;
                }
            }
        }
        private static string CreateRandomPassword(int length = 6)
        {
            // Create a string of characters, numbers, special characters that allowed in the password  
            string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();

            // Select one random character at a time from the string  
            // and create an array of chars  
            char[] chars = new char[length];
            for (int i = 0; i < length; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }
            return new string(chars);
        }
        //workflow
        public async Task<Response> GetCommands(InputCommand model)
        {
            try
            {
                var result = await _workflowHandler.GetCommands(model);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        public async Task<Response> ExecuteCommand(InputCommand model)
        {
            try
            {
                var result = await _workflowHandler.ExcuteCommand(model) as ResponseObject<DocumentStateModel>;
                if (result.Code == Code.Success)
                {
                    // update status for entity
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {
                        var data = await unit.GetRepository<IdmUsers>().Get(x => x.UserId == model.DocumentId).FirstOrDefaultAsync();
                        if (data != null)
                        {
                            data.WorkFlowCurrentState = result.Data.StateName;
                            data.WorkFlowProcessTime = DateTime.Now;
                            data.WorkflowProcessUserId = new Guid(model.UserId);
                            data.WorkflowProcessUserName = model.UserFullName;
                        }
                        if (await unit.SaveAsync() > 0)
                        {
                            // là trạng thái cuối => Mo khoa va thong bao mail ve cho nguoi dang ky
                            if (result.Data.IsFinished.HasValue && result.Data.IsFinished.Value)
                            {
                                // Mo khoa
                                var inputLockUser = new UserLockingModel();
                                inputLockUser.UserId = Guid.Parse(model.UserId);
                                inputLockUser.IsApproved = false;
                                await LockUser(inputLockUser);
                                // Gui mail cho nguoi dang ky
                                //dynamic ProgramId = model.DocumentId;
                                //var resApiHandler = new ResApiHandler<IPFMResponseModel, dynamic>(CommonUtils.ResApiTarget.Ipfm);
                                //IPFMResponseModel setAcceptResponse = await resApiHandler.Post(mdcService + "program/accept", ProgramId);

                                //if (setAcceptResponse.Code == 0)
                                //{
                                //    return new Response(Code.Success, "Duyệt thành công");
                                //}
                                //return new ResponseError(Code.ServerError, setAcceptResponse.Description);

                                return null;
                            }
                            return result;
                        }
                        else
                        {
                            result.Code = Code.Forbidden;
                            result.Message = "Workflow đã hoạt động chỉ chưa lưu được thông tin vào thực thể";
                            return result;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> ExecuteRegisterCommand(InputCommand model)
        {
            try
            {
                var result = await _workflowHandler.ExcuteCommand(model) as ResponseObject<DocumentStateModel>;
                if (result.Code == Code.Success)
                {
                    // update status for entity
                    using (var unit = new UnitOfWork(new DatabaseFactory()))
                    {
                        var data = await unit.GetRepository<IdmUsers>().Get(x => x.UserId == model.DocumentId).FirstOrDefaultAsync();
                        if (data != null)
                        {
                            data.WorkFlowCurrentState = result.Data.StateName;
                            data.WorkFlowProcessTime = DateTime.Now;
                            data.WorkflowProcessUserId = new Guid(model.UserId);
                            data.WorkflowProcessUserName = model.UserFullName;
                            if (data.ApplicationId == Guid.Empty)
                            {
                                var executedUser = await unit.GetRepository<IdmUsers>().Get(x => x.UserId == model.DocumentId).FirstOrDefaultAsync();
                                if (executedUser != null)
                                    data.ApplicationId = executedUser.ApplicationId;
                            }
                        }
                        if (await unit.SaveAsync() > 0)
                        {
                            //// là trạng thái cuối => Mo khoa va thong bao mail ve cho nguoi dang ky
                            //if (result.Data.IsFinished.HasValue && result.Data.IsFinished.Value)
                            //{
                            //    //return null;
                            //}
                            using var client = new HttpClient();
                            client.BaseAddress = new System.Uri(EmailService);
                            var uri = "Email/SendEmail";
                            client.DefaultRequestHeaders.Add("Accept", "text/html");
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));
                            client.Timeout = TimeSpan.FromMilliseconds(20000);

                            var keyValue = new RegisterModel()
                            {
                                USER_NAME = data.UserName,
                            };

                            var Subject = "Tài khoản" + " " + '"' + data.UserName + '"' + " " + "đã được gửi, chờ xử lý";

                            IList<KeyValuePair<string, string>> nameValueCollection = new List<KeyValuePair<string, string>> {
                                { new KeyValuePair<string, string>("ToEmails", (!String.IsNullOrEmpty(data.Email)) ? data.Email:"minhtrihd97@gmail.com") },
                                { new KeyValuePair<string, string>("CcEmails", "hoan@gmail.com") },
                                { new KeyValuePair<string, string>("BccEmails", "trinm@gmail.com") },
                                { new KeyValuePair<string, string>("Subject", Subject) },
                                { new KeyValuePair<string, string>("BodyValues", JsonConvert.SerializeObject(keyValue)) },
                                { new KeyValuePair<string, string>("Type", "3") },
                                { new KeyValuePair<string, string>("Files", "") },
                                };

                            var response = await client.PostAsync(client.BaseAddress + uri, new FormUrlEncodedContent(nameValueCollection));

                            if (response.IsSuccessStatusCode)
                            {
                                var result1 = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                            }

                            return result;
                        }
                        else
                        {
                            result.Code = Code.Forbidden;
                            result.Message = "Workflow đã hoạt động chỉ chưa lưu được thông tin vào thực thể";
                            return result;
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        #region user with rights and roles

        public async Task<Response> GetRolesAndRightsOfuser(Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var result = new RoleAndRightOfUser();
                    result.Roles = new List<RoleOfUser>();
                    result.FreeRights = new List<RightsOfUser>();

                    var roleIdList = unitOfWork.GetRepository<IdmUsersInRoles>().Get(a => a.UserId == userId).Select(a => a.RoleId);
                    var roleList = await unitOfWork.GetRepository<IdmRoles>().Get(a => roleIdList.Contains(a.RoleId)).ToListAsync();
                    result.Roles = AutoMapperUtils.AutoMap<IdmRoles, RoleOfUser>(roleList);

                    // get right of each role
                    foreach (var role in result.Roles)
                    {
                        role.RightsInRole = new List<RightsOfUser>();
                        var rightCodeList = await unitOfWork.GetRepository<IdmRightsInRole>().Get(a => a.RoleId == role.RoleId).Select(a => a.RightCode).ToListAsync();
                        var rightList = await unitOfWork.GetRepository<IdmRight>().Get(a => rightCodeList.Contains(a.RightCode)).ToListAsync(); //all rights of this role
                        role.RightsInRole = AutoMapperUtils.AutoMap<IdmRight, RightsOfUser>(rightList);
                    }

                    //get free rights of user
                    var freeRightList = await unitOfWork.GetRepository<IdmRightsOfUser>().Get(a => a.UserId == userId).Select(a => new { a.RightCode, a.Enable }).ToListAsync();
                    var freeRightCodes = freeRightList.Select(a => a.RightCode).ToList();
                    var freeRights = await unitOfWork.GetRepository<IdmRight>().Get(a => freeRightCodes.Contains(a.RightCode)).ToListAsync(); //all free rights of this user
                    result.FreeRights = AutoMapperUtils.AutoMap<IdmRight, RightsOfUser>(freeRights);
                    foreach (var right in result.FreeRights)
                    {
                        right.Enable = freeRightList.Where(a => a.RightCode == right.RightCode).Select(a => a.Enable).FirstOrDefault();
                    }

                    return new ResponseObject<RoleAndRightOfUser>(result, "success", Code.Success);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> AddRoleIntoUser(RoleOfUser model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    //check trùng
                    var duplicate = await unitOfWork.GetRepository<IdmUsersInRoles>().Get(a => a.UserId == model.UserId && a.RoleId == model.RoleId).FirstOrDefaultAsync();
                    if (duplicate != null)
                    {
                        return new ResponseError(Code.BadRequest, "Role used");
                    }

                    var userInRoleDTO = new IdmUsersInRoles()
                    {
                        Id = Guid.NewGuid(),
                        UserId = model.UserId,
                        RoleId = model.RoleId,
                        CreateDate = DateTime.Now,
                        ApplicationId = model.ApplicationId
                    };
                    unitOfWork.GetRepository<IdmUsersInRoles>().Add(userInRoleDTO);
                    if (unitOfWork.Save() >= 1)
                    {
                        var resultModel = new RoleOfUser
                        {
                            UserId = model.UserId,
                            RoleId = model.RoleId,
                            RoleName = model.RoleName,
                            ApplicationId = model.ApplicationId
                        };
                        return new ResponseObject<RoleOfUser>(resultModel, "success", Code.Success);
                    }
                    else
                    {
                        return new ResponseError(Code.ServerError, "failed to save");
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> DeleteRoleOfUser(Guid userId, Guid roleId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var ROU = unitOfWork.GetRepository<IdmUsersInRoles>().Get(a => a.UserId == userId && a.RoleId == roleId).FirstOrDefault();
                    if (ROU != null)
                    {
                        unitOfWork.GetRepository<IdmUsersInRoles>().Delete(ROU);
                        if (unitOfWork.Save() >= 1)
                        {
                            return new ResponseObject<RoleOfUser>(null, "success", Code.Success);
                        }
                        else
                        {
                            return new ResponseError(Code.ServerError, "failed to delete");
                        }
                    }
                    return new ResponseError(Code.ServerError, "data not found");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        //HaNH
        /// <summary>
        /// Thêm mới một quyền cho user
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <param name="rightCode"></param>
        /// <param name="appId">ApplicationId</param>
        /// <returns></returns>
        public async Task<Response> AddUserRight(Guid userId, string rightCode, Guid appId)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    #region Check user

                    var userModel = unit.GetRepository<IdmUsers>().Get(x => x.UserId == userId).First().UserId;
                    if (userModel == null)
                        return new ResponseObject<UserModel>(null, "No user found", Code.NotFound);

                    #endregion Check user

                    //Lấy quyền hiện tịa của người dùng nếu tồn tại
                    var currentRight = await unit.GetRepository<IdmRightsOfUser>().Get(x => x.UserId == userId &&
                    x.RightCode == rightCode && x.ApplicationId == appId).FirstOrDefaultAsync();

                    if (currentRight == null)
                    {
                        var addRight = new IdmRightsOfUser
                        {
                            UserId = userId,
                            RightCode = rightCode,
                            InheritedFromRoles = String.Empty,
                            Enable = true,
                            Inherited = false,
                            ApplicationId = appId,
                            CreatedDate = DateTime.Now,
                        };

                        unit.GetRepository<IdmRightsOfUser>().Add(addRight);

                        if (await unit.SaveAsync() >= 1)
                            return new Response(Code.Success, "Add rights to user successfully!");
                        else
                        {
                            Serilog.Log.Error("The sql statement can not be executed");
                            return new ResponseError(Code.BadRequest, "The sql statement can not be executed!");
                        }
                    }
                    return new Response(Code.NotFound, "No suitable right found");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        /// <summary>
        /// Thêm mới nhiều quyền cho user
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <param name="listRightCodes">List quyền</param>
        /// <param name="appId">ApplicationId</param>
        /// <param name="actorId"></param>
        /// <returns></returns>
        public async Task<Response> AddUserRights(Guid userId, List<string> listRightCodes, Guid appId, Guid? actorId)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    #region Check user

                    var userModel = unit.GetRepository<IdmUsers>().Get(x => x.UserId == userId).First().UserId;
                    if (userModel == null)
                        return new ResponseObject<UserModel>(null, "No user found", Code.NotFound);

                    #endregion Check user

                    //Lấy danh sách quyền hiện tịa của người dùng nếu tồn tại
                    var listCurrentRights = await unit.GetRepository<IdmRightsOfUser>().GetListAsync(x => x.UserId == userId &&
                    listRightCodes.Contains(x.RightCode) && x.ApplicationId == appId);

                    //Tạo role kế thừa, quyền riêng
                    //var role = MakeIndependentPermission(); MDM đã sử dụng
                    //Tạo danh sách các quyền cần add thêm vào cho user
                    var listRightsToAdd = new List<IdmRightsOfUser>();
                    //foreach (var currentRigths in listCurrentRights)
                    //{
                    //    currentRigths.InheritedFromRoles = /*AddRolesInherited(currentRigths.InheritedFromRoles, role)*/ String.Empty;
                    //    currentRigths.Enable = true;
                    //    unit.GetRepository<IdmRightsOfUser>().Update(currentRigths);
                    //}

                    //Kiểm tra xem các quyền hiện tại có chứa quyền nào trùng với quyèn sắp add không
                    foreach (var rightCode in listRightCodes)
                    {
                        if (listCurrentRights.All(x => x.RightCode != rightCode))
                        {
                            //Nếu không trùng thì add vào danh sách chuẩn bị add
                            listRightsToAdd.Add(new IdmRightsOfUser
                            {
                                UserId = userId,
                                RightCode = rightCode,
                                InheritedFromRoles = String.Empty,
                                Enable = true,
                                Inherited = false,
                                ApplicationId = appId,
                                CreatedDate = DateTime.Now,
                            });
                        }
                    }
                    //Nếu mà danh sách pending có chứa quyền chưa trùng thì add
                    if (listRightsToAdd.Any()) unit.GetRepository<IdmRightsOfUser>().AddRange(listRightsToAdd);
                    if (await unit.SaveAsync() >= 1)
                        return new Response(Code.Success, "Add rights to user successfully!");
                    else
                    {
                        Serilog.Log.Error("The sql statement can not be executed");
                        return new ResponseError(Code.BadRequest, "The sql statement can not be executed!");
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        /// <summary>
        /// Xoá nhièu quyền của user
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <param name="rightIds">danh sách quyền</param>
        /// <param name="appId">ApplicationId</param>
        /// <returns></returns>
        public async Task<Response> DeleteUserRights(Guid userId, List<string> rightIds, Guid appId)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    #region Check user availibility

                    var user = unit.GetRepository<IdmUsers>().Get(x => x.UserId == userId
                    && x.ApplicationId == appId);
                    if (user == null)
                        return new ResponseError(Code.BadRequest, "No user found");

                    #endregion Check user availibility

                    #region Check rights of user

                    var listCurrentRights = await unit.GetRepository<IdmRightsOfUser>().Get(x => x.ApplicationId == appId
                    && x.UserId == userId).ToListAsync();

                    if (!listCurrentRights.Any())
                        return new ResponseError(Code.NotFound, "No rights found");

                    #endregion Check rights of user

                    var listToRemove = new List<IdmRightsOfUser>();
                    foreach (var item in listCurrentRights)
                    {
                        //Nếu là quyền thuộc một role thì tắt quyền đó đi
                        if (!String.IsNullOrEmpty(item.InheritedFromRoles))
                        {
                            item.Enable = false;
                            unit.GetRepository<IdmRightsOfUser>().Update(item);
                        }
                        else
                            listToRemove.Add(item);
                    }

                    if (listToRemove.Count() >= 0)
                        unit.GetRepository<IdmRightsOfUser>().DeleteRange(listToRemove);
                    if (await unit.SaveAsync() >= 0)
                        return new Response(Code.Success, "Delete rights successfully");
                    else
                        return new ResponseError(Code.BadRequest, "Sql statement can not be executed");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> UpdateRightOfUser(Guid userId, string rightCode, Guid appId)
        {
            try
            {
                using (var unitofWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var exist = unitofWork.GetRepository<IdmRightsOfUser>().Get(a => a.UserId == userId && a.RightCode == rightCode).AsNoTracking().FirstOrDefault();
                    if (exist != null)
                    {
                        exist.Enable = !exist.Enable;
                        unitofWork.GetRepository<IdmRightsOfUser>().Update(exist);
                        if (await unitofWork.SaveAsync() >= 0)
                        {
                            return new Response(Code.Success, "Update success");
                        }
                        else
                        {
                            return new ResponseError(Code.BadRequest, "Update failed");
                        }
                    }
                    return new ResponseError(Code.BadRequest, "data not found");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> AddAllOrganization()
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var listUser = await unit.GetRepository<IdmUsers>().GetAll().ToListAsync();
                    foreach (var item in listUser)
                    {
                        IdmUsersInOrganization uinoDTO = new IdmUsersInOrganization
                        {
                            Id = Guid.NewGuid(),
                            UserId = item.UserId,
                            OrganizationId = new Guid("d0021c71-be88-4267-ade5-53deb3866d87"),
                            OrganizationName = "Cục an toàn thông tin",
                            CreatedByUserId = new Guid("cc2a09fd-3b06-412f-92b8-d81be5168c35"),
                            CreatedOnDate = DateTime.Now,
                            Role = ""
                        };

                        unit.GetRepository<IdmUsersInOrganization>().Add(uinoDTO);
                    }
                    if (unit.Save() > 0)
                        return new Response(Code.Success, "Get success");
                    return new ResponseError(Code.BadRequest, "data not found");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        #endregion user with rights and roles


        //TriNM
        public async Task<Response> GetBirthday()
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var listUser = from dt in unit.GetRepository<IdmUsers>().Get()
                                   select dt;
                    var today = DateTime.Now;
                    var monthCurrent = today.Month;
                    if (listUser != null)
                    {
                        listUser = listUser.Where(dt => dt.Birthday.Month == monthCurrent);
                        var res = new List<UserModel>();
                        foreach (var item in listUser)
                        {
                            res.Add(AutoMapperUtils.AutoMap<IdmUsers, UserModel>(item));
                        }
                        return new ResponseList<UserModel>(res, "Get successfully", Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<UserModel>(null, "Not found", Code.NotFound);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
    }

    #region IdmHelper-MDM

    public static class IdmHelper
    {
        /// <summary>
        ///     Tạo Id quyền riêng
        /// </summary>
        /// <returns></returns>
        public static Guid MakeIndependentPermission()
        {
            return new Guid("00000000-0000-0000-0000-000000000000");
        }

        /// <summary>
        ///     Lấy về danh sách RoleId đã kế thừa từ string
        /// </summary>
        /// <param name="currInheritedFromRoles"></param>
        /// <returns></returns>
        public static List<Guid> LoadRolesInherited(string currInheritedFromRoles)
        {
            try
            {
                if (string.IsNullOrEmpty(currInheritedFromRoles))
                {
                    var result = new List<Guid>();
                    return result;
                }
                var jsonObject = JsonConvert.DeserializeObject<List<Guid>>(currInheritedFromRoles);

                var newlist = new List<Guid>();

                foreach (var item in jsonObject)
                    if (!newlist.Contains(item))
                        newlist.Add(item);
                jsonObject = jsonObject.GroupBy(role => role)
                    .Select(g => g.First())
                    .ToList();

                return jsonObject;
            }
            catch (Exception)
            {
                var result = new List<Guid>();
                return result;
            }
        }

        /// <summary>
        ///     Sinh ra chuỗi Json kế thừa từ 1 danh sách RoleId lưu vào DB
        /// </summary>
        /// <param name="listRoleId"></param>
        /// <returns></returns>
        public static string GenRolesInherited(List<Guid> listRoleId)
        {
            try
            {
                var jsonStr = JsonConvert.SerializeObject(listRoleId);
                return jsonStr;
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        ///     Sinh ra chuỗi Json kế thừa từ 1 RoleId lưu vào DB
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public static string GenRolesInherited(Guid roleId)
        {
            var roles = new List<Guid>
            {
                roleId
            };
            return GenRolesInherited(roles);
        }

        /// <summary>
        ///     Bỏ 1 RoleId kế thừa
        /// </summary>
        /// <param name="currInheritedFromRoles"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public static string RemoveRolesInherited(string currInheritedFromRoles, Guid roleId)
        {
            var roles = LoadRolesInherited(currInheritedFromRoles) ?? new List<Guid>();
            if (roles.Contains(roleId)) roles.Remove(roleId);
            var result = GenRolesInherited(roles);
            return result;
        }

        /// <summary>
        ///     Bỏ 1 danh sách RoleId kế thừa
        /// </summary>
        /// <param name="currInheritedFromRoles"></param>
        /// <param name="listRoleId"></param>
        /// <returns></returns>
        public static string RemoveRolesInherited(string currInheritedFromRoles, List<Guid> listRoleId)
        {
            var result = currInheritedFromRoles;
            if (listRoleId == null)
                listRoleId = new List<Guid>();
            foreach (var role in listRoleId) result = RemoveRolesInherited(result, role);
            return result;
        }

        /// <summary>
        ///     Thêm 1 RoleId kế thừa
        /// </summary>
        /// <param name="currInheritedFromRoles"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public static string AddRolesInherited(string currInheritedFromRoles, Guid roleId)
        {
            //check RolesInherited truoc
            var roles = LoadRolesInherited(currInheritedFromRoles) ?? new List<Guid>();
            if (!roles.Contains(roleId))
                roles.Add(roleId);
            var result = GenRolesInherited(roles);
            return result;
        }

        /// <summary>
        ///     Thêm 1 danh sách RoleId kế thừa
        /// </summary>
        /// <param name="currInheritedFromRoles"></param>
        /// <param name="listRoleId"></param>
        /// <returns></returns>
        public static string AddRolesInherited(string currInheritedFromRoles, List<Guid> listRoleId)
        {
            var result = currInheritedFromRoles;
            if (listRoleId == null)
                listRoleId = new List<Guid>();
            foreach (var role in listRoleId) result = AddRolesInherited(result, role);
            return result;
        }



    }
    #endregion IdmHelper-MDM
}