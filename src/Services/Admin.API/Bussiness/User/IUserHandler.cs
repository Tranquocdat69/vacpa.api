﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiUtils;
using CommonUtils;
using DataUtils;

namespace Business
{
    public interface IUserHandler
    {
        Task<Response> GetAll();
        Task<Response> Create(SystemUserModel model);
        Task<Response> Update(SystemUserModel model);
        Task<Response> GetById(Guid userId);
        Task<Response> LockUser(UserLockingModel model);
        Task<Response> Authentication(string username, string password);
        Task<Response> GetFilter(UserFilterModel filterModel); 

        //user with right and role
        Task<Response> GetRolesAndRightsOfuser(Guid userId);
        Task<Response> AddRoleIntoUser(RoleOfUser model);
        Task<Response> DeleteRoleOfUser(Guid userId, Guid roleId);

        //HaNH
        //Thêm mới một quyền cho user
        Task<Response> AddUserRight(Guid userId, string rightCode, Guid appId);
        //Thêm mới nhiều quyền cho user
        Task<Response> AddUserRights(Guid userId,List<string> rightCodes, Guid appId, Guid? actorId);
        //Xoá nhiều quyền của user
        Task<Response> DeleteUserRights(Guid userId, List<string> rightIds, Guid appId);
        Task<Response> UpdateRightOfUser(Guid id, string rightCode, Guid appId);
        Task<Response> GetByUserName(string userName);
        Task<Response> UpdateUserPassword(UpdateUserPasswordModel model);

        Task<Response> AddAllOrganization();
        // ManhNX
        Task<Response> Register(UserRegisterModel model);
        Task<Response> CreateUserByQTHT(UserCreateByQTHTModel model);
        Task<Response> GetUserRegisterFilter(UserRegisterFilterModel model);
        Task<Response> UpdateUserByQTHT(UserUpdateByQTHTModel model);
        Response CheckMailAlready(CheckMailModel model);
        Task<Response> ForgotPassword(string username, string email);

        //TriNM
        Task<Response> GetBirthday();

        // Workflow
        Task<Response> GetCommands(InputCommand model);
        Task<Response> ExecuteCommand(InputCommand model);
        Task<Response> ExecuteRegisterCommand(InputCommand model);
    }
}