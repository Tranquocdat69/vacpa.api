﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using ApiUtils;
using CommonUtils;
using Newtonsoft.Json;

namespace Business
{
    public class UserCollectionOld
    {
        private readonly IUserHandler _handler;
        public HashSet<UserModel> Collection;

        protected UserCollectionOld(IUserHandler handler)
        {
            _handler = handler;
            // LoadToHashSet();
        }
        protected UserCollectionOld()
        {
            _handler = new UserHandler();
            // LoadToHashSet();
        }

        public static UserCollectionOld Instance { get; } = new UserCollectionOld();

        public void LoadToHashSet()
        {

            Collection = new HashSet<UserModel>();
            var enable = Utils.GetConfig("Redis:Disable");
            var type = Utils.GetConfig("Redis:Type");
            if (type == "Pub" )
            {
                Console.WriteLine("_handler.GetAll()");
                var listResponse = _handler.GetAll().Result;
                Console.WriteLine("_handler.GetAll()");
                if (listResponse.Code == Code.Success)
                {
                    // Add to hashset
                    if (listResponse is ResponseObject<List<UserModel>> listResponseObj)
                        foreach (var response in listResponseObj.Data)
                            Collection.Add(response);

                    var cache = RedisConnectorHelper.Connection.GetDatabase();
                    cache.StringSet("USERS", JsonConvert.SerializeObject(Collection.ToList()));
                }
            }
            else
            {
                try
                {
                    Thread.Sleep(1000);
                    var cache = RedisConnectorHelper.Connection.GetDatabase();
                    var u = cache.StringGet("USERS");
                    var users = JsonConvert.DeserializeObject<List<UserModel>>(u);
                    Console.WriteLine("cache.StringSet(USERS, stringCache)");
                    foreach (var response in users)
                        Collection.Add(response);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("DB cache.StringGet(USERS, stringCache): " + ex.Message);

                    var listResponse = _handler.GetAll().Result;
                    if (listResponse.Code == Code.Success)
                    {
                        // Add to hashset
                        if (listResponse is ResponseObject<List<UserModel>> listResponseObj)
                            foreach (var response in listResponseObj.Data)
                                Collection.Add(response);
                    }
                }
            }
        }

        public string GetName(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result?.Name;
        }

        public UserModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            if (result == null)
            {
                result = new UserModel();
            }
            return result;
        }

        public bool Check()
        {
            try
            {
                if (Collection != null && Collection.Count > 0)
                {
                    return true;
                }else{
                    LoadToHashSet();
                }
                return false;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public UserModel GetUserByUserName(string sub)
        {
            var result = Collection.FirstOrDefault(u => u.UserName == sub);
            if (result == null)
            {
                result = new UserModel();
            }
            return result;
        }

        public bool CheckUserActive(string sub)
        {
            var result = Collection.FirstOrDefault(u => u.UserName == sub);
            if (result != null && !result.IsLockedOut)
            {
                return true;
            }else{
                return false;
            }
        }
    }
}