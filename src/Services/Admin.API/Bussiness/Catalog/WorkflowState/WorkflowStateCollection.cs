﻿//using Admin.API.Business.Workflow.WorkflowState;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;

namespace Business
{
    public class WorkflowStateCollection
    {
        private readonly IWorkflowStateHandler _handler;
        public HashSet<WorkflowStateModel> Collection;

        protected WorkflowStateCollection(IWorkflowStateHandler handler)
        {
            _handler = handler;
            LoadToHashSet();
        }
        protected WorkflowStateCollection()
        {
            _handler = new WorkflowStateHandler();
            LoadToHashSet();
        }

        public static WorkflowStateCollection Instance { get; } = new WorkflowStateCollection();

        public void LoadToHashSet()
        {
            Collection = new HashSet<WorkflowStateModel>();
            var listResponse = _handler.GetAllAsync().Result;
            if (listResponse != null)
            {
                // Add to hashset
                foreach (var response in listResponse)
                    Collection.Add(response);
            }
        }

        public string GetStateName(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.StateId == id);
            return result?.StateName;
        }

        public WorkflowStateModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.StateId == id);
            if (result == null)
            {
                result = new WorkflowStateModel();
            }
            return result;
        }
    }
}
