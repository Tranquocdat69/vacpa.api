﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class WorkflowStateModel
    {
        public Guid StateId { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }

    }

    public class WorkflowStateUpdateModel
    {
        public Guid StateId { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }

    }

    public class WorkflowStateCreateModel
    {
        public Guid StateId { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }

    }

    public class WorkflowStateQueryModel : PaginationRequest
    {

    }
}
