﻿using CommonUtils;
using DataUtils;
using Infrastructure;
using Serilog;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class WorkflowStateHandler : IWorkflowStateHandler
    {
        public async Task<Response> CreateAsync(WorkflowStateCreateModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unit.GetRepository<catalog_WorkflowState>().Get(x => x.StateCode == model.StateCode).FirstOrDefaultAsync();
                    if (data != null)
                        return new Response(Code.MethodNotAllowed, "Code exists");
                    var record = AutoMapperUtils.AutoMap<WorkflowStateCreateModel, catalog_WorkflowState>(model);
                    record.StateId = Guid.NewGuid();
                 
                    //if (model.UserId.HasValue)
                    //    record.CreatedByUserId = model.UserId.Value;
                    unit.GetRepository<catalog_WorkflowState>().Add(record);
                    if (await unit.SaveAsync() > 0)
                        return new ResponseObject<WorkflowStateModel>(AutoMapperUtils.AutoMap<catalog_WorkflowState, WorkflowStateModel>(record), "Create successfully", Code.Success);
                    return new ResponseError(Code.ServerError, "The sql statement can not be executed!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> DeleteAsync(Guid id)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var record = await unit.GetRepository<catalog_WorkflowState>().Get(x => x.StateId == id).FirstOrDefaultAsync();
                    if (record != null)
                    {
                        unit.GetRepository<catalog_WorkflowState>().Delete(record);
                        if (await unit.SaveAsync() > 0)
                            return new Response(Code.Success, "Delete successfully!");
                        return new ResponseError(Code.ServerError, "The sql statement can not be executed!");
                    }
                    return new ResponseError(Code.NotFound, "No data found!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> DeleteMany(List<Guid> ids)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var resultList = new List<WorkflowStateModel>();
                    foreach (var id in ids)
                    {
                        var deleteResult = await DeleteAsync(id) as ResponseObject<WorkflowStateModel>;
                    }

                    var result = new Response(Code.Success, "Delete multiple success");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<List<WorkflowStateModel>> GetAllAsync()
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = unit.GetRepository<catalog_WorkflowState>().GetAll();
                    if (data.Any())
                    {
                        var res = new List<WorkflowStateModel>();
                        foreach (var item in data)
                        {
                            res.Add(AutoMapperUtils.AutoMap<catalog_WorkflowState, WorkflowStateModel>(item));
                        }
                        return res;
                    }
                    else
                        return new List<WorkflowStateModel>();
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new List<WorkflowStateModel>();
            }
        }

        public async Task<Response> GetByFilterASync(WorkflowStateQueryModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = from dt in unit.GetRepository<catalog_WorkflowState>().GetAll().OrderBy(x => x.StateCode) select dt;
                    if (data.Any())
                    {
                        if (!String.IsNullOrEmpty(model.FullTextSearch))
                        {
                            var txt = model.FullTextSearch.ToLower();
                            data = data.Where(x => x.StateCode.ToLower().Contains(txt) || x.StateName.ToLower().Contains(txt));
                        }

                        if (!model.Size.HasValue)
                        {
                            model.Size = 20;
                        }
                        model.Page = model.Page - 1;
                        int excludedRows = ((int)model.Page) * ((int)model.Size);
                        if (excludedRows <= 0)
                        {
                            excludedRows = 0;
                        }

                        var totalCount = data.Count();
                        data = data.Skip(excludedRows).Take((int)model.Size);

                        var dataResult = await data.ToListAsync();

                        var fullQueryData = AutoMapperUtils.AutoMap<catalog_WorkflowState, WorkflowStateModel>(dataResult);

                        var response = new Pagination<WorkflowStateModel>();
                        response.Content = fullQueryData;
                        response.NumberOfElements = totalCount;
                        response.Page = model.Page.HasValue ? model.Page.Value : 0;
                        response.Size = model.Size.HasValue ? model.Size.Value : 0;
                        response.TotalElements = totalCount;
                        response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);


                        return new ResponsePagination<WorkflowStateModel>(response);
                    }
                    else
                        return new Response(Code.NotFound, "No data found");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetByIdAsync(Guid id)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unit.GetRepository<catalog_WorkflowState>().Get(x => x.StateId == id).FirstOrDefaultAsync();
                    if (data != null)
                        return new ResponseObject<WorkflowStateModel>(AutoMapperUtils.AutoMap<catalog_WorkflowState, WorkflowStateModel>(data), "Get successfully", Code.Success);
                    return new ResponseError(Code.NotFound, "No data found");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> UpdateAsync(WorkflowStateUpdateModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unit.GetRepository<catalog_WorkflowState>().Get(x => x.StateId == model.StateId).FirstOrDefaultAsync();
                    if (data == null)
                        return new ResponseError(Code.NotFound, "No data found!");
                    else
                    {
                        var record = await unit.GetRepository<catalog_WorkflowState>().Get(x => x.StateId == model.StateId).FirstOrDefaultAsync();
                        if (record != null)
                        {
                            record.StateCode = model.StateCode;
                            record.StateName = model.StateName;

                            unit.GetRepository<catalog_WorkflowState>().Update(record);

                            if (unit.Save() > 0)
                                return new Response(Code.Success, "Update succesfully!");
                            return new ResponseError(Code.ServerError, "The sql statement can not be executed!");
                        }
                        return new ResponseError(Code.NotFound, "No data found");
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetAllFromCache()
        {
            try
            {
                return new ResponseList<WorkflowStateModel>(WorkflowStateCollection.Instance.Collection.ToList(), "get data successfully", Code.Success);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseList<WorkflowStateModel>(null, ex.Message, Code.ServerError);
            }
        }
    }
}
