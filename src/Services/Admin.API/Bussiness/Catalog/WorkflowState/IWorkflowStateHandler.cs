﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public interface IWorkflowStateHandler
    {
        Task<Response> CreateAsync(WorkflowStateCreateModel model);
        Task<List<WorkflowStateModel>> GetAllAsync();
        Task<Response> GetAllFromCache();
        Task<Response> GetByFilterASync(WorkflowStateQueryModel model);
        Task<Response> GetByIdAsync(Guid id);
        Task<Response> UpdateAsync(WorkflowStateUpdateModel model);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteMany(List<Guid> ids);
    }
}
