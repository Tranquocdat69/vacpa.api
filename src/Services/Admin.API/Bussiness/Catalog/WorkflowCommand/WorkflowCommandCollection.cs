﻿//using Admin.API.Business.Catalog.WorkflowCommand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class WorkflowCommandCollection
    {
        private readonly IWorkflowCommandHandler _handler;
        public HashSet<WorkflowCommandModel> Collection;

        protected WorkflowCommandCollection(IWorkflowCommandHandler handler)
        {
            _handler = handler;
            LoadToHashSet();
        }
        protected WorkflowCommandCollection()
        {
            _handler = new WorkflowCommandHandler();
            LoadToHashSet();
        }

        public static WorkflowCommandCollection Instance { get; } = new WorkflowCommandCollection();

        public void LoadToHashSet()
        {
            Collection = new HashSet<WorkflowCommandModel>();
            var listResponse = _handler.GetAllAsync().Result;
            if (listResponse != null)
            {
                // Add to hashset
                foreach (var response in listResponse)
                    Collection.Add(response);
            }
        }

        public string GetCommandName(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.CommandId == id);
            return result?.CommandName;
        }

        public WorkflowCommandModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.CommandId == id);
            if (result == null)
            {
                result = new WorkflowCommandModel();
            }
            return result;
        }
    }
}
