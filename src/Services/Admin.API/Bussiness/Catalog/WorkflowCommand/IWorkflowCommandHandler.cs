﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public interface IWorkflowCommandHandler
    {
        Task<Response> CreateAsync(WorkflowCommandCreateModel model);
        Task<List<WorkflowCommandModel>> GetAllAsync();
        Task<Response> GetAllFromCache();
        Task<Response> GetByFilterASync(WorkflowCommandQueryModel model);
        Task<Response> GetByIdAsync(Guid id);
        Task<Response> UpdateAsync(WorkflowCommandUpdateModel model);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteMany(List<Guid> ids);
    }
}
