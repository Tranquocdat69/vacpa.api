﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class WorkflowCommandModel
    {
        public Guid CommandId { get; set; }
        public string CommandCode { get; set; }
        public string CommandName { get; set; }
        public int Order { get; set; }

    }

    public class WorkflowCommandUpdateModel
    {
        public Guid CommandId { get; set; }
        public string CommandCode { get; set; }
        public string CommandName { get; set; }
        public int Order { get; set; }
     
    }

    public class WorkflowCommandCreateModel
    {
        public Guid CommandId { get; set; }
        public string CommandCode { get; set; }
        public string CommandName { get; set; }
        public int Order { get; set; }
   
    }

    public class WorkflowCommandQueryModel : PaginationRequest
    {
        
    }
}
