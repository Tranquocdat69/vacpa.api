﻿using CommonUtils;
using DataUtils;
using Infrastructure;
using Serilog;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class WorkflowCommandHandler : IWorkflowCommandHandler
    {
        public async Task<Response> CreateAsync(WorkflowCommandCreateModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unit.GetRepository<catalog_WorkflowCommand>().Get(x => x.CommandCode == model.CommandCode).FirstOrDefaultAsync();
                    if (data != null)
                        return new Response(Code.MethodNotAllowed, "Code exists");
                    var record = AutoMapperUtils.AutoMap<WorkflowCommandCreateModel, catalog_WorkflowCommand>(model);
                    record.CommandId = Guid.NewGuid();
                    
                    //if (model.UserId.HasValue)
                    //    record.CreatedByUserId = model.UserId.Value;
                    unit.GetRepository<catalog_WorkflowCommand>().Add(record);
                    if (await unit.SaveAsync() > 0)
                        return new ResponseObject<WorkflowCommandModel>(AutoMapperUtils.AutoMap<catalog_WorkflowCommand, WorkflowCommandModel>(record), "Create successfully", Code.Success);
                    return new ResponseError(Code.ServerError, "The sql statement can not be executed!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> DeleteAsync(Guid id)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var record = await unit.GetRepository<catalog_WorkflowCommand>().Get(x => x.CommandId == id).FirstOrDefaultAsync();
                    if (record != null)
                    {
                        unit.GetRepository<catalog_WorkflowCommand>().Delete(record);
                        if (await unit.SaveAsync() > 0)
                            return new Response(Code.Success, "Delete successfully!");
                        return new ResponseError(Code.ServerError, "The sql statement can not be executed!");
                    }
                    return new ResponseError(Code.NotFound, "No data found!");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> DeleteMany(List<Guid> ids)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                {
                    var resultList = new List<WorkflowCommandModel>();
                    foreach (var id in ids)
                    {
                        var deleteResult = await DeleteAsync(id) as ResponseObject<WorkflowCommandModel>;
                    }

                    var result = new Response(Code.Success, "Delete multiple success");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<List<WorkflowCommandModel>> GetAllAsync()
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = unit.GetRepository<catalog_WorkflowCommand>().GetAll();
                    if (data.Any())
                    {
                        var res = new List<WorkflowCommandModel>();
                        foreach (var item in data)
                        {
                            res.Add(AutoMapperUtils.AutoMap<catalog_WorkflowCommand, WorkflowCommandModel>(item));
                        }
                        return res;
                    }
                    else
                        return new List<WorkflowCommandModel>();
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new List<WorkflowCommandModel>();
            }
        }

        public async Task<Response> GetByFilterASync(WorkflowCommandQueryModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = from dt in unit.GetRepository<catalog_WorkflowCommand>().GetAll().OrderBy(x => x.Order) select dt;
                    if (data.Any())
                    {
                        if (!String.IsNullOrEmpty(model.FullTextSearch))
                        {
                            var txt = model.FullTextSearch.ToLower();
                            data = data.Where(x => x.CommandCode.ToLower().Contains(txt) ||  x.CommandName.ToLower().Contains(txt));
                        }

                        if (!model.Size.HasValue)
                        {
                            model.Size = 20;
                        }
                        model.Page = model.Page - 1;
                        int excludedRows = ((int)model.Page) * ((int)model.Size);
                        if (excludedRows <= 0)
                        {
                            excludedRows = 0;
                        }

                        var totalCount = data.Count();
                        data = data.Skip(excludedRows).Take((int)model.Size);

                        var dataResult = await data.ToListAsync();

                        var fullQueryData = AutoMapperUtils.AutoMap<catalog_WorkflowCommand, WorkflowCommandModel>(dataResult);

                        var response = new Pagination<WorkflowCommandModel>();
                        response.Content = fullQueryData;
                        response.NumberOfElements = totalCount;
                        response.Page = model.Page.HasValue ? model.Page.Value : 0;
                        response.Size = model.Size.HasValue ? model.Size.Value : 0;
                        response.TotalElements = totalCount;
                        response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);


                        return new ResponsePagination<WorkflowCommandModel>(response);
                    }
                    else
                        return new Response(Code.NotFound, "No data found");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetByIdAsync(Guid id)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unit.GetRepository<catalog_WorkflowCommand>().Get(x => x.CommandId == id).FirstOrDefaultAsync();
                    if (data != null)
                        return new ResponseObject<WorkflowCommandModel>(AutoMapperUtils.AutoMap<catalog_WorkflowCommand, WorkflowCommandModel>(data), "Get successfully", Code.Success);
                    return new ResponseError(Code.NotFound, "No data found");
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> UpdateAsync(WorkflowCommandUpdateModel model)
        {
            try
            {
                using (var unit = new UnitOfWork(new DatabaseFactory()))
                {
                    var data = await unit.GetRepository<catalog_WorkflowCommand>().Get(x => x.CommandId == model.CommandId).FirstOrDefaultAsync();
                    if (data == null)
                        return new ResponseError(Code.NotFound, "No data found!");
                    else
                    {
                        var record = await unit.GetRepository<catalog_WorkflowCommand>().Get(x => x.CommandId == model.CommandId).FirstOrDefaultAsync();
                        if (record != null)
                        {
                            record.CommandCode = model.CommandCode;
                            record.CommandName = model.CommandName;
                            record.Order = model.Order;
               
                            unit.GetRepository<catalog_WorkflowCommand>().Update(record);

                            if (unit.Save() > 0)
                                return new Response(Code.Success, "Update succesfully!");
                            return new ResponseError(Code.ServerError, "The sql statement can not be executed!");
                        }
                        return new ResponseError(Code.NotFound, "No data found");
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetAllFromCache()
        {
            try
            {
                return new ResponseList<WorkflowCommandModel>(WorkflowCommandCollection.Instance.Collection.ToList(), "get data successfully", Code.Success);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseList<WorkflowCommandModel>(null, ex.Message, Code.ServerError);
            }
        }
    }
}
