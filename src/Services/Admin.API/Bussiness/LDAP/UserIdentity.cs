﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public class UserIdentity
    {
        /// <summary>
        /// cn type string
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// userPassword type string
        /// </summary>
        public string UserPassword { get; set; }
        public string Email { get; set; }
        /// <summary>
        /// uid type string
        /// </summary>
        public string Uid { get; set; }
        /// <summary>
        /// mail type string
        /// </summary>

        /// <summary>
        /// ou type string
        /// </summary>
        public string OrganizationUnit { get; set; }
        /// <summary>
        /// sn type string
        /// </summary>
        ///
        public string FirstName { get; set; }
        /// <summary>
        /// initials
        /// </summary>
        public string InitialName { get; set; }
        /// <summary>
        /// givenName
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// displayName
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// mobile
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// employeeNumber type string
        /// </summary>
        public string EmployeeNumber { get; set; }
        /// <summary>
        /// employeeType type int
        /// </summary>
        public string EmployeeType { get; set; }
        /// <summary>
        /// st type string
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// street
        /// </summary>
        public string Street { get; set; }

        public string HomeAddress { get; set; }
        public string AccountStatus { get; set; }
    }
    public class MessageResponseModel
    {
        public int? Result { get; set; }
        public string Message { get; set; }
    }
}
