﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.Net;
using System.Security.Cryptography;
using Novell.Directory.Ldap;

using LdapConnection = System.DirectoryServices.Protocols.LdapConnection;
using CommonUtils;

namespace Business
{
    public class LDAPHandler : ILDAPHandler
    {
        //private readonly ILogService _logger = BusinessServiceLocator.Instance.GetService<ILogService>();
        readonly string _ldapHost = Utils.GetConfig("ldap:ldap-host");
        readonly string _ldapPort = Utils.GetConfig("ldap:ldap-port");
        readonly string _ldapRootPath = Utils.GetConfig("ldap:ldap-rootpath");
        /// <summary>
        /// ldapPath: Đường dẫn nơi truy xuất dữ liệu LDAP, ex:"LDAP://localhost:389/ou=Users,dc=beta,dc=com,dc=vn"
        /// </summary>
        readonly string _ldapPath = Utils.GetConfig("ldap:ldap-path");
        /// <summary>
        /// Nơi lưu trữ tài khoản
        /// </summary>
        readonly string _ldapContainer = Utils.GetConfig("ldap:ldap-container");
        /// <summary>
        /// userManager: Tài khoản quản trị domai, ex:"cn=manager,dc=cgcn,dc=vn"
        /// </summary>
        readonly string _userManager = Utils.GetConfig("ldap:ldap-usermanager");
        /// <summary>
        /// passWordUserManager: Mật khẩu tài khoản quản trị domai
        /// </summary>
        readonly string _passWordUserManager = Utils.GetConfig("ldap:ldap-password-usermanager");
        /// <summary>
        /// Thêm mới tài khoản
        /// </summary>
        /// <param name="objectUser">Thông tin chi tiết</param>
        /// <returns></returns>
        /// 

        public MessageResponseModel AddUser(UserIdentity objectUser)
        {
            try
            {
                // Creating an LdapConnection instance 
                Novell.Directory.Ldap.LdapConnection ldapConn = new Novell.Directory.Ldap.LdapConnection();

                //Connect function will create a socket connection to the server
                ldapConn.Connect(_ldapHost, Convert.ToInt32(_ldapPort));

                //Bind function will Bind the user object Credentials to the Server
                ldapConn.Bind(_userManager, _passWordUserManager);

                //Creates the List attributes of the entry and add them to attribute set 
                LdapAttributeSet attributeSet = new LdapAttributeSet();
                attributeSet.Add(new LdapAttribute("objectclass", new string[] { "inetOrgPerson", "top", "person" }));
                attributeSet.Add(new LdapAttribute("cn", objectUser.UserName.Trim()));
                attributeSet.Add(new LdapAttribute("uid", objectUser.UserName.Trim()));
                //attributeSet.Add(new LdapAttribute("homeDirectory", objectUser.UserName.Trim()));
                //attributeSet.Add(new LdapAttribute("gidNumber", "0"));
                //attributeSet.Add(new LdapAttribute("uidNumber", objectUser.EmployeeNumber));
                attributeSet.Add(new LdapAttribute("displayName", objectUser.DisplayName));
                attributeSet.Add(new LdapAttribute("mobile", objectUser.Mobile));
                // attributeSet.Add(new LdapAttribute("givenname", !string.IsNullOrEmpty(objectUser.FirstName) ? objectUser.FirstName: "" ));
                attributeSet.Add(new LdapAttribute("sn", objectUser.UserName.Trim()));
                attributeSet.Add(new LdapAttribute("mail", objectUser.Email));
                // attributeSet.Add(new LdapAttribute("homepostaladdress", !string.IsNullOrEmpty(objectUser.HomeAddress) ?  objectUser.HomeAddress: ""));
                attributeSet.Add(new LdapAttribute("userPassword", objectUser.UserPassword));
                attributeSet.Add(new LdapAttribute("employeeType", objectUser.EmployeeType));

                // DN of the entry to be added
                string dn = "cn=" + objectUser.UserName.Trim() + "," + _ldapContainer;
                LdapEntry newEntry = new LdapEntry(dn, attributeSet);

                //Add the entry to the directory
                ldapConn.Add(newEntry);

                var result = new MessageResponseModel
                {
                    Result = 1,
                    Message = "Tạo tài khoản thành công!"
                };
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                var result = new MessageResponseModel
                {
                    Result = -1,
                    Message = "Tạo tài khoản thất bại: " + ex.Message
                };
                return result;
            }
        }
        /// <summary>
        /// Cập nhật tài khoản
        /// </summary>
        /// <param name="objectUser">Thông tin chi tiết tài khoản</param>
        /// <returns></returns>
        public string EditUser(UserIdentity objectUser)
        {
            try
            {
                string dn = "cn=" + objectUser.UserName + "," + _ldapContainer;

                // Creating an LdapConnection instance 
                Novell.Directory.Ldap.LdapConnection ldapConn = new Novell.Directory.Ldap.LdapConnection();

                //Connect function will create a socket connection to the server
                ldapConn.Connect(_ldapHost, Convert.ToInt32(_ldapPort));

                //Bind function will Bind the user object Credentials to the Server
                ldapConn.Bind(_userManager, _passWordUserManager);

                ArrayList modList = new ArrayList();
                //String desc = "This object belongs to test user";
                // Add a new value to the description attribute
                //LdapAttribute attribute = new LdapAttribute("description", desc);
                //modList.Add(new LdapModification(LdapModification.ADD, attribute));


                //Replace the existing email  with the new email value
                LdapAttribute attribute = new LdapAttribute("mail", objectUser.Email);
                modList.Add(new LdapModification(LdapModification.Replace, attribute));

                attribute = new LdapAttribute("displayName", objectUser.DisplayName);
                modList.Add(new LdapModification(LdapModification.Replace, attribute));

                attribute = new LdapAttribute("mobile", objectUser.Mobile);
                modList.Add(new LdapModification(LdapModification.Replace, attribute));

                attribute = new LdapAttribute("givenname", objectUser.FirstName);
                modList.Add(new LdapModification(LdapModification.Replace, attribute));

                attribute = new LdapAttribute("sn", objectUser.LastName);
                modList.Add(new LdapModification(LdapModification.Replace, attribute));

                attribute = new LdapAttribute("employeeType", objectUser.EmployeeType);
                modList.Add(new LdapModification(LdapModification.Replace, attribute));

                if (!string.IsNullOrEmpty(objectUser.UserPassword))
                {
                    attribute = new LdapAttribute("userPassword", objectUser.UserPassword);
                    modList.Add(new LdapModification(LdapModification.Replace, attribute));
                }

                LdapModification[] mods = new LdapModification[modList.Count];
                Type mtype = Type.GetType("Novell.Directory.LdapModification");
                mods = (LdapModification[])modList.ToArray(typeof(LdapModification));

                //Modify the entry in the directory
                ldapConn.Modify(dn, mods);

                return "Cập nhật tài khoản thành công!";
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                if (ex.Message == "No Such Object")
                {
                    return "TK không tồn tại";
                }           
                return "Cập nhật tài khoản thất bại!";
            }
        }
        /// <summary>
        /// Xoá tài khoản người dùng
        /// </summary>
        /// <param name="userName">Tên tài khoản</param>
        /// <returns></returns>
        public bool DeleteUser(string userName)
        {
            try
            {
                string dn = "cn=" + userName + "," + _ldapContainer;

                // Creating an LdapConnection instance 
                Novell.Directory.Ldap.LdapConnection ldapConn = new Novell.Directory.Ldap.LdapConnection();

                //Connect function will create a socket connection to the server
                ldapConn.Connect(_ldapHost, Convert.ToInt32(_ldapPort));

                //Bind function will Bind the user object Credentials to the Server
                ldapConn.Bind(_userManager, _passWordUserManager);

                //Deletes the entry from the directory
                ldapConn.Delete(dn);

                return true;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return false;
            }
        }
        /// <summary>
        /// Xoá tài khoản khách hàng
        /// </summary>
        /// <param name="userName">Tên tài khoản</param>
        /// <returns></returns>
        public bool DeleteAccount(string userName)
        {
            try
            {
                string _ldapContainerAcc = Utils.GetConfig("ldap:ldap-container:account");
                string dn = "cn=" + userName + "," + _ldapContainerAcc;

                // Creating an LdapConnection instance 
                Novell.Directory.Ldap.LdapConnection ldapConn = new Novell.Directory.Ldap.LdapConnection();

                //Connect function will create a socket connection to the server
                ldapConn.Connect(_ldapHost, Convert.ToInt32(_ldapPort));

                //Bind function will Bind the user object Credentials to the Server
                ldapConn.Bind(_userManager, _passWordUserManager);

                //Deletes the entry from the directory
                ldapConn.Delete(dn);

                return true;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return false;
            }
        }
        /// <summary>
        /// Mở khóa/khóa tài khoản người dùng
        /// </summary>
        /// <returns></returns>
        public string ChangeLockUser(string userName, string status)
        {
            try
            {
                string dn = "cn=" + userName + "," + _ldapContainer;

                // Creating an LdapConnection instance 
                Novell.Directory.Ldap.LdapConnection ldapConn = new Novell.Directory.Ldap.LdapConnection();

                //Connect function will create a socket connection to the server
                ldapConn.Connect(_ldapHost, Convert.ToInt32(_ldapPort));

                //Bind function will Bind the user object Credentials to the Server
                ldapConn.Bind(_userManager, _passWordUserManager);

                ArrayList modList = new ArrayList();
                //String desc = "This object belongs to test user";
                // Add a new value to the description attribute
                //LdapAttribute attribute = new LdapAttribute("description", desc);
                //modList.Add(new LdapModification(LdapModification.ADD, attribute));

                //Replace the existing email  with the new email value
                LdapAttribute attribute = new LdapAttribute("employeeType", status);
                modList.Add(new LdapModification(LdapModification.Replace, attribute));

                Type mtype = Type.GetType("Novell.Directory.LdapModification");
                var mods = (LdapModification[])modList.ToArray(typeof(LdapModification));

                //Modify the entry in the directory
                ldapConn.Modify(dn, mods);

                string strStatus = "";
                strStatus = status == "0" ? "Khóa tài khoản thành công!" : "Mở khóa tài khoản thành công!";

                return strStatus;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return "Cập nhật tài khoản thất bại!";
            }
        }
        /// <summary>
        /// Lấy về thông tin chi tiết của một tài khoản
        /// </summary>
        /// <param name="userName">Tên tài khoản</param>
        /// <returns></returns>
        public ResponseObject<UserIdentity> GetUserInfo(string userName)
        {
            try
            {
                DirectoryEntry rootEntry =
                    new DirectoryEntry(_ldapPath, _userManager, _passWordUserManager, AuthenticationTypes.FastBind)
                    {
                        AuthenticationType = AuthenticationTypes.None
                    };
                //Or whatever it need be
                DirectorySearcher searcher = new DirectorySearcher(rootEntry);
                var queryFormat = "(&(objectClass=person)(cn=*{0}*))";
                searcher.Filter = string.Format(queryFormat, userName);
                SearchResult result = searcher.FindOne();
                UserIdentity objUser = new UserIdentity();
                if (result != null)
                {
                    try
                    {
                        objUser.DisplayName = result.Properties["displayName"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.Email = result.Properties["mail"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.EmployeeNumber = result.Properties["employeeNumber"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.FirstName = result.Properties["givenName"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.InitialName = result.Properties["initials"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.LastName = result.Properties["sn"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.Mobile = result.Properties["mobile"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.OrganizationUnit = result.Properties["ou"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.State = result.Properties["st"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.Uid = result.Properties["uid"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.Street = result.Properties["street"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.HomeAddress = result.Properties["homepostaladdress"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.UserName = result.Properties["cn"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    rootEntry.Dispose();
                    searcher.Dispose();
                    return new ResponseObject<UserIdentity>(objUser, "Success", Code.Success);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return null;
            }
        }
        /// <summary>
        /// Lấy về danh sách tài khoản (Có thể tìm kiếm theo các tiêu chí)
        /// </summary>
        /// <param name="keyword">Giá trị để tìm kiếm</param>
        /// <param name="pagesize">Số lượng bản ghi hiển thị trên một trang</param>
        /// <returns></returns>
        public ResponseObject<List<UserIdentity>> SearchUser(string keyword, int pagesize)
        {
            List<UserIdentity> lstUserIdentity = new List<UserIdentity>();
            try
            {
                DirectoryEntry rootEntry = new DirectoryEntry(_ldapPath, _userManager, _passWordUserManager,
                    AuthenticationTypes.FastBind)
                { AuthenticationType = AuthenticationTypes.None };
                DirectorySearcher searcher = new DirectorySearcher(rootEntry);
                var queryFormat =
                    "(&(objectClass=person)(|(cn=*{0}*)(displayName=*{0}*)(mail=*{0}*)(mobile=*{0}*)(st=*{0}*)(street=*{0}*)(employeeNumber=*{0}*)))";
                searcher.Filter = string.Format(queryFormat, keyword);
                searcher.SearchScope = System.DirectoryServices.SearchScope.Subtree;
                if (pagesize == 0)
                {
                    searcher.PageSize = 100;
                }
                else
                {
                    searcher.PageSize = pagesize;
                }

                foreach (SearchResult result in searcher.FindAll())
                {
                    UserIdentity objUser = new UserIdentity();
                    try
                    {
                        objUser.DisplayName = result.Properties["displayName"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.Email = result.Properties["mail"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.EmployeeNumber = result.Properties["employeeNumber"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.FirstName = result.Properties["givenName"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.InitialName = result.Properties["initials"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.LastName = result.Properties["sn"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.Mobile = result.Properties["mobile"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.OrganizationUnit = result.Properties["ou"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.State = result.Properties["st"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.Street = result.Properties["street"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.HomeAddress = result.Properties["homepostaladdress"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.UserName = result.Properties["cn"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    try
                    {
                        objUser.Uid = result.Properties["uid"][0].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        Serilog.Log.Error(ex, "");
                    }

                    if (objUser.UserName != null && !objUser.UserName.Trim().Equals("") &&
                        !objUser.Uid.Trim().Equals(""))
                    {
                        lstUserIdentity.Add(objUser);
                    }
                }

                rootEntry.Dispose();
                searcher.Dispose();
                return new ResponseObject<List<UserIdentity>>(lstUserIdentity,"Success", Code.Success);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return null;
            }
        }
        /// <summary>
        /// Thay đổi mật khẩu danh cho người dùng
        /// </summary>
        /// <param name="userName">Tên tài khoản</param>
        /// <param name="ouName">OU của tài khoản</param>
        /// <param name="newPass">Mật khẩu mới</param>
        /// <param name="oldPass">Mật khẩu cũ</param>
        /// <returns></returns>
        public string ChangePass(string userName, string ouName, string newPass, string oldPass, bool? isAdmin)
        {
            try
            {
                //check đăng nhập Ldap, hoặc nếu là admin thì cho phép thay password khi không cần biết user password
                if (Authenticate(userName, ouName, oldPass) || isAdmin == true)
                {
                    string dn = "cn=" + userName + "," + _ldapContainer;

                // Creating an LdapConnection instance 
                    Novell.Directory.Ldap.LdapConnection ldapConn = new Novell.Directory.Ldap.LdapConnection();

                    //Connect function will create a socket connection to the server
                    ldapConn.Connect(_ldapHost, Convert.ToInt32(_ldapPort));

                    //Bind function will Bind the user object Credentials to the Server
                    ldapConn.Bind(_userManager, _passWordUserManager);

                    ArrayList modList = new ArrayList();
                    //String desc = "This object belongs to test user";
                    // Add a new value to the description attribute
                    //LdapAttribute attribute = new LdapAttribute("description", desc);
                    //modList.Add(new LdapModification(LdapModification.ADD, attribute));

                    //Replace the existing email  with the new email value
                    LdapAttribute attribute = new LdapAttribute("userPassword", GenerateSaltedSHA1(newPass));
                    modList.Add(new LdapModification(LdapModification.Replace, attribute));

                    LdapModification[] mods = new LdapModification[modList.Count];
                    Type mtype = Type.GetType("Novell.Directory.LdapModification");
                    mods = (LdapModification[])modList.ToArray(typeof(LdapModification));

                    //Modify the entry in the directory
                    ldapConn.Modify(dn, mods);

                    return "Thay đổi mật khẩu thành công!";

                    // DirectoryEntry rootEntry =
                    //     new DirectoryEntry(_ldapPath, _userManager, _passWordUserManager,
                    //         AuthenticationTypes.FastBind)
                    //     { AuthenticationType = AuthenticationTypes.None };
                    // DirectorySearcher searcher = new DirectorySearcher(rootEntry);
                    // var queryFormat = "(&(objectClass=person)(cn=*{0}*))";
                    // searcher.Filter = string.Format(queryFormat, userName);
                    // SearchResult result = searcher.FindOne();
                    // if (result != null)
                    // {
                    //     // create new object from search result    
                    //     DirectoryEntry entryToUpdate = result.GetDirectoryEntry();
                    //     entryToUpdate.Properties["userPassword"].Value = GenerateSaltedSHA1(newPass);
                    //     entryToUpdate.CommitChanges();
                    //     rootEntry.Dispose();
                    //     entryToUpdate.Dispose();
                    //     searcher.Dispose();
                    //     return "Thay đổi mật khẩu thành công!";
                    // }
                    // else
                    // {
                    //     //_logger.Info("Tài khoản " + userName + " không tìm thấy trong hệ thống!");
                    //     return "Tài khoản " + userName + " không tìm thấy trong hệ thống!";
                    // }
                }
                else
                {
                    //_logger.Info("Mật khẩu cũ không chính xác!");
                    return "Mật khẩu cũ không chính xác!";
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return ex.Message;
            }
        }
        /// <summary>
        /// Thay đổi mật khẩu của tài khoản dành cho người quản trị
        /// </summary>
        /// <param name="userName">Tên tài khoản cần đổi mật khẩu</param>
        /// <param name="newPass">Mật khẩu mới</param>
        /// <returns></returns>
        public MessageResponseModel ChangePassRoleAdmin(string userName, string newPass)
        {
            try
            {
                DirectoryEntry rootEntry =
                    new DirectoryEntry(_ldapPath, _userManager, _passWordUserManager,
                            AuthenticationTypes.FastBind)
                    { AuthenticationType = AuthenticationTypes.None };
                DirectorySearcher searcher = new DirectorySearcher(rootEntry);
                var queryFormat = "(&(objectClass=person)(cn=*{0}*))";
                searcher.Filter = string.Format(queryFormat, userName);
                SearchResult result = searcher.FindOne();
                if (result != null)
                {
                    // create new object from search result    
                    DirectoryEntry entryToUpdate = result.GetDirectoryEntry();
                    entryToUpdate.Properties["userPassword"].Value = GenerateSaltedSHA1(newPass);
                    entryToUpdate.CommitChanges();
                    rootEntry.Dispose();
                    entryToUpdate.Dispose();
                    searcher.Dispose();

                    var message = new MessageResponseModel
                    {
                        Result = 1,
                        Message = "Thay đổi mật khẩu tài khoản " + userName + " thành công!"
                    };

                    return message;
                }
                else
                {
                    var message = new MessageResponseModel
                    {
                        Result = -1,
                        Message = "Tài khoản " + userName + " có vấn đề nên không thể thay đổi mật khẩu!"
                    };

                    return message;
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                var message = new MessageResponseModel
                {
                    Result = -1,
                    Message = ex.Message
                };

                return message;
            }
        }
        /// <summary>
        /// Thay đổi mật khẩu của tài khoản về mật khẩu mặc định
        /// </summary>
        /// <param name="userName">Tên tài khoản</param>
        /// <param name="ouName">OU của tài khoản</param>
        /// <param name="length">Độ dài của mật khẩu</param>
        /// <returns></returns>
        public string ResetPass(string userName, string ouName, int length)
        {
            try
            {
                DirectoryEntry rootEntry =
                    new DirectoryEntry(_ldapPath, _userManager, _passWordUserManager, AuthenticationTypes.FastBind)
                    {
                        AuthenticationType = AuthenticationTypes.None
                    };
                DirectorySearcher searcher = new DirectorySearcher(rootEntry);
                var queryFormat = "(&(objectClass=person)(cn=*{0}*))";
                searcher.Filter = string.Format(queryFormat, userName);
                searcher.SearchScope = System.DirectoryServices.SearchScope.Subtree;
                searcher.CacheResults = false;
                SearchResult result = searcher.FindOne();
                if (result != null)
                {
                    // create new object from search result    
                    DirectoryEntry entryToUpdate = result.GetDirectoryEntry();
                    var temp = CreatePassword(length);
                    entryToUpdate.Properties["userPassword"].Value = GenerateSaltedSHA1(temp);
                    entryToUpdate.CommitChanges();
                    rootEntry.Dispose();
                    entryToUpdate.Dispose();
                    searcher.Dispose();
                    return temp;
                }
                else
                {
                    return "Tài khoản " + userName + " không tìm thấy trong hệ thống!";
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return "";
            }
        }
        
        /// <summary>
        /// Kiểm tra xem tài khoản muốn đổi mật khẩu có hợp lệ hay không
        /// </summary>
        /// <param name="userName">Tên tài khoản</param>
        /// <param name="ouName">Thuộc OU nào</param>
        /// <param name="pass">Mật khẩu hiện tại</param>
        /// <returns></returns>
        public bool Authenticate(string userName, string ouName, string pass)
        {
            try
            {
                // Create the new LDAP connection
                LdapDirectoryIdentifier ldi = new LdapDirectoryIdentifier(_ldapHost, int.Parse(_ldapPort));
                LdapConnection ldapConnection =
                    new LdapConnection(ldi) { AuthType = AuthType.Basic };
                ldapConnection.SessionOptions.ProtocolVersion = 3;
                NetworkCredential nc = new NetworkCredential("cn=" + userName + ",ou=" + ouName + "," + _ldapRootPath, pass); //password
                ldapConnection.Bind(nc);
                Console.WriteLine("LdapConnection authentication success");
                ldapConnection.Dispose();
                return true;
            }
            catch (System.DirectoryServices.Protocols.LdapException ex)
            {
                // _logger.Error("Unable to login:" + ex.Message);
                Serilog.Log.Error(ex, "");
                return false;
            }
            catch (Exception ex)
            {
                // _logger.Error("Unexpected exception occured:" + ex.GetType() + ":" + ex.Message);
                Serilog.Log.Error(ex, "");
                return false;
            }
            // return true;
        }
        /// <summary>
        /// Tạo mật khẩu mã hoá với SHA1
        /// </summary>
        /// <param name="plainTextString"></param>
        /// <returns></returns>
        public string GenerateSaltedSHA1(string plainTextString)
        {
            HashAlgorithm algorithm = new SHA1Managed();
            var saltBytes = GenerateSalt(4);
            var plainTextBytes = Encoding.ASCII.GetBytes(plainTextString);

            var plainTextWithSaltBytes = AppendByteArray(plainTextBytes, saltBytes);
            var saltedSHA1Bytes = algorithm.ComputeHash(plainTextWithSaltBytes);
            var saltedSHA1WithAppendedSaltBytes = AppendByteArray(saltedSHA1Bytes, saltBytes);

            return "{SSHA}" + Convert.ToBase64String(saltedSHA1WithAppendedSaltBytes);
        }
        private byte[] AppendByteArray(byte[] byteArray1, byte[] byteArray2)
        {
            var byteArrayResult =
                new byte[byteArray1.Length + byteArray2.Length];

            for (var i = 0; i < byteArray1.Length; i++)
                byteArrayResult[i] = byteArray1[i];
            for (var i = 0; i < byteArray2.Length; i++)
                byteArrayResult[byteArray1.Length + i] = byteArray2[i];

            return byteArrayResult;
        }
        private byte[] GenerateSalt(int saltSize)
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[saltSize];
            rng.GetBytes(buff);
            return buff;
        }
        public string CreatePassword(int length)
        {
            const string valid = "12345aA@";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
    }
}
