﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
    public interface ILDAPHandler
    {
        MessageResponseModel AddUser(UserIdentity objectUser);
        string EditUser(UserIdentity objectUser);
        bool DeleteUser(string userName);
        ResponseObject<UserIdentity> GetUserInfo(string userName);
        ResponseObject<List<UserIdentity>> SearchUser(string keyword, int pagesize);
        string ChangePass(string userName, string ouName, string newPass, string oldPass, bool? isAdmin);
        string ResetPass(string userName, string ouName, int length);
    }
}
