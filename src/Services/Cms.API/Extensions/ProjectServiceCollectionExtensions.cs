namespace Bussiness
{
    using ApiUtils;
    using Microsoft.Extensions.DependencyInjection;

    /// <summary>
    /// <see cref="IServiceCollection"/> extension methods add project services.
    /// </summary>
    /// <remarks>
    /// AddSingleton - Only one instance is ever created and returned.
    /// AddScoped - A new instance is created and returned for each request/response cycle.
    /// AddTransient - A new instance is created and returned each time.
    /// </remarks>
    public static class ProjectServiceCollectionExtensions
    {
        public static IServiceCollection AddProjectServices(this IServiceCollection services) =>
            services
        #region SYS

                .AddSingleton<IApplicationHandler, ApplicationHandler>()
                .AddSingleton<IWorkflowHandler, DbWorkflowHandler>()
        #endregion

        #region Ecm

        #endregion
            ;
    }
}