﻿using System.Linq;
using Bussiness;
using CommonUtils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Debug;

namespace Infrastructure
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            return new DataContext("", databaseType, connectionString, isTesting);
        }
    }
    public class DataContext : DbContext
    {
        private string prefix;


        public static readonly ILoggerFactory loggerFactory = new LoggerFactory(new[] {
              new DebugLoggerProvider()
        });

        public DataContext()
        {
            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            DatabaseType = databaseType;
            ConnectionString = connectionString;
            IsTesting = isTesting;
        }

        public DataContext(string prefix, string databaseType, string connectionString, bool isTesting = false)
        {
            Prefix = prefix;
            DatabaseType = databaseType;
            ConnectionString = connectionString;
            IsTesting = isTesting;
        }

        #region sys
        public virtual DbSet<IdmApplications> IdmApplications { get; set; }
        
        #endregion

        #region ecm

        #endregion

        public string Prefix { get; set; }
        public string DatabaseType { get; set; }
        public string ConnectionString { get; set; }
        public bool IsTesting { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            if (!optionsBuilder.IsConfigured)
            {
                #region Config database
                if (IsTesting)
                {
                    optionsBuilder.UseInMemoryDatabase("testDatabase");
                }
                else
                {
                    switch (DatabaseType)
                    {
                        //case "MySqlPomeloDatabase":
                        //    optionsBuilder.UseMySql(ConnectionString);
                        //    break;
                        //case "MSSQLDatabase":
                        //    optionsBuilder.UseSqlServer(ConnectionString);
                        //    break;
                        //case "OracleDatabase":
                        //    optionsBuilder.UseOracle(ConnectionString);
                        //    break;
                        case "PostgreSQLDatabase":
                            optionsBuilder.UseNpgsql(ConnectionString);
                            break;
                        //case "Sqlite":
                        //    optionsBuilder.UseSqlite(ConnectionString);
                        //    break;
                        default:
                            optionsBuilder.UseSqlServer(ConnectionString);
                            break;
                    }
                }
                #endregion

                optionsBuilder.ReplaceService<IModelCacheKeyFactory, DynamicModelCacheKeyFactory>();
                optionsBuilder.UseLoggerFactory(loggerFactory).EnableSensitiveDataLogging();
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }

    }
    public class DynamicModelCacheKeyFactory : IModelCacheKeyFactory
    {
        public object Create(DbContext context)
        {

            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            if (context is DataContext dynamicContext)
            {
                return (context.GetType(), dynamicContext.Prefix, databaseType, connectionString, isTesting);
            }

            return context.GetType();
        }
    }
}