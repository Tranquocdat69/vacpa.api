﻿using System;
using CommonUtils;
using DataUtils;

namespace Bussiness
{
    public class BaseApplicationModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class ApplicationModel : BaseApplicationModel
    {
        public string Description { get; set; }
        public bool IsDefault { get; set; }
    }

    public class ApplicationQueryModel : PaginationRequest
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class ApplicationCreateModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Guid? TemplateApp { get; set; }
    }

    public class ApplicationUpdateModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}