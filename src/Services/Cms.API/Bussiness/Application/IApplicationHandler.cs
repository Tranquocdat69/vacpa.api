﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonUtils;
using DataUtils;

namespace Bussiness
{
    public interface IApplicationHandler
    {
        Task<Response> GetAll();

    }
}