using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using ApiUtils;
using Bussiness;
using CommonUtils;
using DataUtils;
using Microsoft.EntityFrameworkCore;

namespace Business
{
   public class DbBootstrapHandler : IBootstrapHandler
   {
      public DbBootstrapHandler()
      {

      }

    //   public async Task<bool> InitFireBased()
    //   {
    //      FirebaseCloudMessage.InitFirebaseApp();
    //      return true;
    //   }

      public bool RefreshCache()
      {
         try
         {
            // var tasks = new Task[]{
            //     await Task.Run(async () => UserCollection.Instance.LoadToHashSet()),
            //     await Task.Run(async () => OrganizationCollection.Instance.LoadToHashSet())
            // };

            var t1 = Task.Factory.StartNew(() => UserCollection.Instance.LoadToHashSet());
            // var t2 = Task.Factory.StartNew(() => OrganizationCollection.Instance.LoadToHashSet());
            // var t3 = Task.Factory.StartNew(() => TermsCollection.Instance.LoadToHashSet());
            // var t4 = Task.Factory.StartNew(() => PtsxCollection.Instance.LoadToHashSet());

            Task.WaitAll(t1);
            // set timer 30m
            var intervalTime = !string.IsNullOrEmpty(Utils.GetConfig("AppSettings:IntervalTime")) ? Utils.GetConfig("AppSettings:IntervalTime") : "1800000";

            Interval.Set(int.Parse(intervalTime));
            // await OrganizationCollection.Instance.LoadToHashSet();
            return true;
         }
         catch (System.Exception ex)
         {
            Serilog.Log.Error(ex, "");
            return false;
         }

      }

        // public async Task<bool> RefreshCacheUser()
        // {
        //     try
        //     {
        //        var t1 = Task.Factory.StartNew(() => UserCollection.Instance.LoadToHashSet());
        //        Task.WaitAll(t1);
        //        // await OrganizationCollection.Instance.LoadToHashSet();
        //        return true;
        //     }
        //     catch (System.Exception ex)
        //     {
        //        Serilog.Log.Error(ex, "");
        //        return false;
        //     }
            
        // }
        
    }

   public static class Interval
   {
      public static System.Timers.Timer Set(int interval)
      {
         var timer = new System.Timers.Timer(interval);

         // Hook up the Elapsed event for the timer. 
         timer.Elapsed += OnTimedEvent;

         // Have the timer fire repeated events (true is the default)
         timer.AutoReset = true;

         // Start the timer
         timer.Enabled = true;

         // timer.Elapsed += (s, e) => {
         //       timer.Enabled = false;
         //        // Hook up the Elapsed event for the timer. 
         //       timer.Elapsed += OnTimedEvent;
         //       timer.AutoReset = true;
         //       // action.Wait();
         //       timer.Enabled = true;
         // };
         // timer.Enabled = true;
         return timer;
      }

      private static void OnTimedEvent(Object source, ElapsedEventArgs e)
      {
         var t1 = Task.Factory.StartNew(() => UserCollection.Instance.LoadToHashSet());
         Task.WaitAll(t1);
         Console.WriteLine("The Elapsed reload user cache event was raised at {0:HH:mm:ss.fff}", e.SignalTime);
         Serilog.Log.Information("The Elapsed reload user cache event was raised at {0:HH:mm:ss.fff}", e.SignalTime);
      }

      public static void Stop(System.Timers.Timer timer)
      {
         timer.Stop();
         timer.Dispose();
      }
   }
}
