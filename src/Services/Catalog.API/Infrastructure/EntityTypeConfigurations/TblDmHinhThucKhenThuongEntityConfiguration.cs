﻿using Catalog.API.Infrastructure.Datatables.QLHV;
using Microsoft.EntityFrameworkCore;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmHinhThucKhenThuongEntityConfiguration : IEntityTypeConfiguration<TblDmHinhThucKhenThuong>
    {
        public void Configure(EntityTypeBuilder<TblDmHinhThucKhenThuong> builder)
        {
            builder.HasKey(e => e.HinhThucKhenThuongGuid);

            builder.ToTable("tblDMHinhThucKhenThuong");

            builder.Property(e => e.HinhThucKhenThuongGuid).ValueGeneratedNever();

            builder.Property(e => e.TenHinhThucKhenThuong)
                    .IsRequired()
                    .HasMaxLength(500);

            builder.Property(e => e.HinhThucKhenThuongId)
                    .HasColumnName("HinhThucKhenThuongID");

            builder.Property(e => e.NgaySuaCuoi).HasColumnType("date");
        }
    }
}
