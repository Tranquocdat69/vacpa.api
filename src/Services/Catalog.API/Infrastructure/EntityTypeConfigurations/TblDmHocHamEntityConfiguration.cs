﻿using Catalog.API.Infrastructure.Datatables.QLHV;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmHocHamEntityConfiguration : IEntityTypeConfiguration<TblDMHocHam>
    {
        public void Configure(EntityTypeBuilder<TblDMHocHam> builder)
        {
            builder.HasKey(e => e.HocHamGuid);

            builder.ToTable("tblDMHocHam");

            builder.Property(e => e.HocHamGuid).ValueGeneratedNever();

            builder.Property(e => e.TenHocHam)
                    .IsRequired()
                    .HasMaxLength(500);

            builder.Property(e => e.HocHamId)
                    .HasColumnName("HocHamID");

            builder.Property(e => e.NgaySuaCuoi).HasColumnType("date");
        }
    }
}
