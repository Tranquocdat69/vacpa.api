﻿using Catalog.API.Infrastructure.Datatables.HoiVienCaNhan;
using Catalog.API.Infrastructure.Datatables.QLHV;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblHoiVienCaNhanEntityConfiguration : IEntityTypeConfiguration<TblHoiVienCaNhan>
    {
        public void Configure(EntityTypeBuilder<TblHoiVienCaNhan> builder)
        {
            builder.HasKey(e => e.HoiVienCaNhanId);

            builder.ToTable("tblHoiVienCaNhan");

            builder.Property(e => e.HoiVienCaNhanId)
                .HasColumnName("HoiVienCaNhanID")
                .ValueGeneratedNever();

            builder.Property(e => e.ChucVuId).HasColumnName("ChucVuID");

            builder.Property(e => e.ChuyenNganhDaoTaoId).HasColumnName("ChuyenNganhDaoTaoID");

            builder.Property(e => e.ChuyenNganhNam)
                .HasMaxLength(10)
                .IsFixedLength();

            builder.Property(e => e.CmndNgayCap)
                .HasColumnName("CMND_NgayCap")
                .HasColumnType("date");

            builder.Property(e => e.CmndTinhId).HasColumnName("CMND_TinhID");

            builder.Property(e => e.DiaChi).HasMaxLength(500);

            builder.Property(e => e.DiaChiHuyenId)
                .HasColumnName("DiaChi_HuyenID")
                .HasMaxLength(10)
                .IsFixedLength();

            builder.Property(e => e.DiaChiTinhId)
                .HasColumnName("DiaChi_TinhID")
                .HasMaxLength(10)
                .IsFixedLength();

            builder.Property(e => e.DiaChiXaId)
                .HasColumnName("DiaChi_XaID")
                .HasMaxLength(10)
                .IsFixedLength();

            builder.Property(e => e.DienThoai).HasMaxLength(100);

            builder.Property(e => e.DonViCongTac).HasMaxLength(500);

            builder.Property(e => e.Email).HasMaxLength(100);

            builder.Property(e => e.HanCapDen).HasColumnType("date");

            builder.Property(e => e.HanCapTu).HasColumnType("date");

            builder.Property(e => e.HoDem).HasMaxLength(100);

            builder.Property(e => e.HocHamId).HasColumnName("HocHamID");

            builder.Property(e => e.HocHamNam)
                .HasMaxLength(10)
                .IsFixedLength();

            builder.Property(e => e.HocViId).HasColumnName("HocViID");

            builder.Property(e => e.HocViNam)
                .HasMaxLength(10)
                .IsFixedLength();

            builder.Property(e => e.HoiVienTapTheId).HasColumnName("HoiVienTapTheID");

            builder.Property(e => e.LoaiHoiVienCaNhan).HasComment("0: người quan tâm; 1: hội viên; 2: Kiểm toán viên");

            builder.Property(e => e.LoaiHoiVienCaNhanChiTiet).HasDefaultValueSql("0");

            builder.Property(e => e.MaHoiVienCaNhan).HasMaxLength(100);

            builder.Property(e => e.Mobile).HasMaxLength(50);

            builder.Property(e => e.NganHangId).HasColumnName("NganHangID");

            builder.Property(e => e.NgayCapChungChiKtv)
                .HasColumnName("NgayCapChungChiKTV")
                .HasColumnType("date");

            builder.Property(e => e.NgayCapGiayChungNhanDkhn)
                .HasColumnName("NgayCapGiayChungNhanDKHN")
                .HasColumnType("date");

            builder.Property(e => e.NgayCapNhat).HasColumnType("date");

            builder.Property(e => e.NgayDuyet).HasColumnType("date");

            builder.Property(e => e.NgayGiaNhap).HasColumnType("date");

            builder.Property(e => e.NgayNhap).HasColumnType("date");

            builder.Property(e => e.NgayQuyetDinhKetNap).HasColumnType("date");

            builder.Property(e => e.NgaySinh).HasColumnType("date");

            builder.Property(e => e.NguoiCapNhat).HasMaxLength(50);

            builder.Property(e => e.NguoiDuyet).HasMaxLength(50);

            builder.Property(e => e.NguoiNhap).HasMaxLength(50);

            builder.Property(e => e.QueQuanHuyenId)
                .HasColumnName("QueQuan_HuyenID")
                .HasMaxLength(10)
                .IsFixedLength();

            builder.Property(e => e.QueQuanTinhId)
                .HasColumnName("QueQuan_TinhID")
                .HasMaxLength(10)
                .IsFixedLength();

            builder.Property(e => e.QueQuanXaId)
                .HasColumnName("QueQuan_XaID")
                .HasMaxLength(10)
                .IsFixedLength();

            builder.Property(e => e.QuocTichId).HasColumnName("QuocTichID");

            builder.Property(e => e.SoChungChiKtv)
                .HasColumnName("SoChungChiKTV")
                .HasMaxLength(50);

            builder.Property(e => e.SoCmnd)
                .HasColumnName("SoCMND")
                .HasMaxLength(50);

            builder.Property(e => e.SoGiayChungNhanDkhn)
                .HasColumnName("SoGiayChungNhanDKHN")
                .HasMaxLength(50);

            builder.Property(e => e.SoQuyetDinhKetNap).HasMaxLength(250);

            builder.Property(e => e.SoTaiKhoanNganHang).HasMaxLength(50);

            builder.Property(e => e.SoThichId).HasColumnName("SoThichID");

            builder.Property(e => e.Ten).HasMaxLength(50);

            builder.Property(e => e.TinhTrangHoiVienId)
                .HasColumnName("TinhTrangHoiVienID")
                .HasComment("0: không hoạt động; 1: hoạt động");

            builder.Property(e => e.TroLyKtv)
                .HasColumnName("TroLyKTV")
                .HasComment("1: Là trợ lý kiểm toán viên; 0: Không là trợ lý kiểm toán viên");

            builder.Property(e => e.TruongDaiHocId).HasColumnName("TruongDaiHocID");
        }
    }
}
