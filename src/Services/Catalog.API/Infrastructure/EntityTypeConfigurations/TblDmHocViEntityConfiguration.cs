﻿using Catalog.API.Infrastructure.Datatables.QLHV;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmHocViEntityConfiguration: IEntityTypeConfiguration<TblDmHocVi>
    {
        public void Configure(EntityTypeBuilder<TblDmHocVi> builder)
        {
            builder.HasKey(e => e.HocViGuid);

            builder.ToTable("tblDMHocVi");

            builder.Property(e => e.HocViGuid).ValueGeneratedNever();

            builder.Property(e => e.TenHocVi)
                    .IsRequired()
                    .HasMaxLength(500);

            builder.Property(e => e.HocViId)
                    .HasColumnName("HocViID");

            builder.Property(e => e.NgaySuaCuoi).HasColumnType("date");
        }
    }
}
