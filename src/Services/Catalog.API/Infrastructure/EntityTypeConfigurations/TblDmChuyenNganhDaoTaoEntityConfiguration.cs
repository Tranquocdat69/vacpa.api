﻿using Catalog.API.Infrastructure.Datatables.QLHV;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmChuyenNganhDaoTaoEntityConfiguration : IEntityTypeConfiguration<TblDmChuyenNganhDaoTao>
    {
        public void Configure(EntityTypeBuilder<TblDmChuyenNganhDaoTao> buidler)
        {
            buidler.HasKey(e => e.ChuyenNganhDaoTaoGuid);

            buidler.ToTable("tblDMChuyenNganhDaoTao");

            buidler.Property(e => e.ChuyenNganhDaoTaoGuid)
                .HasComment("")
                .ValueGeneratedNever();

            buidler.Property(e => e.ChuyenNganhDaoTaoId).HasColumnName("ChuyenNganhDaoTaoID");

            buidler.Property(e => e.NgaySuaCuoi).HasColumnType("date");

            buidler.Property(e => e.NguoiSuaCuoi).HasComment("");

            buidler.Property(e => e.TenChuyenNganhDaoTao)
                .IsRequired()
                .HasMaxLength(500);
        }
    }
}
