﻿using Catalog.API.Infrastructure.Datatables.QLHV;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmTrinhDoChuyenMonEntityConfiguration : IEntityTypeConfiguration<TblDmTrinhDoChuyenMon>
    {
        public void Configure(EntityTypeBuilder<TblDmTrinhDoChuyenMon> builder)
        {
            builder.HasKey(e => e.TrinhDoChuyenMonGuid);

            builder.ToTable("tblDMTrinhDoChuyenMon");

            builder.Property(e => e.TrinhDoChuyenMonGuid).HasDefaultValueSql("uuid_generate_v1()");

            builder.Property(e => e.ApplicationId).HasColumnName("ApplicationID");

            builder.Property(e => e.TrinhDoChuyenMonId).HasColumnName("TrinhDoChuyenMonID");

            builder.Property(e => e.TenTrinhDoChuyenMon).HasMaxLength(500);

            builder.Property(e => e.NgaySuaCuoi).HasColumnType("date");
        }
    }
}
