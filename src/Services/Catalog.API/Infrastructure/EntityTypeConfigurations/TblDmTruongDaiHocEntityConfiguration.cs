﻿using Catalog.API.Infrastructure.Datatables.QLHV;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmTruongDaiHocEntityConfiguration : IEntityTypeConfiguration<TblDmTruongDaiHoc>
    {
        public void Configure(EntityTypeBuilder<TblDmTruongDaiHoc> builder)
        {
            builder.HasKey(e => e.TruongDaiHocGuid);

            builder.ToTable("tblDMTruongDaiHoc");

            builder.Property(e => e.TruongDaiHocGuid).ValueGeneratedNever();

            builder.Property(e => e.TenTruongDaiHoc)
                    .IsRequired()
                    .HasMaxLength(500);

            builder.Property(e => e.TruongDaiHocId)
                    .HasColumnName("TruongDaiHocID");

            builder.Property(e => e.NgaySuaCuoi).HasColumnType("date");
        }
    }
}
