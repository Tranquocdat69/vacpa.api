﻿using Catalog.API.Infrastructure.Datatables.QLHV;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmSoThichEntityConfiguration : IEntityTypeConfiguration<TblDmSoThich>
    {
        public void Configure(EntityTypeBuilder<TblDmSoThich> builder)
        {
            builder.HasKey(e => e.SoThichGuid);

            builder.ToTable("tblDMSoThich");

            builder.Property(e => e.SoThichGuid).ValueGeneratedNever();

            builder.Property(e => e.TenSoThich)
                    .IsRequired()
                    .HasMaxLength(500);

            builder.Property(e => e.SoThichId)
                    .HasColumnName("SoThichID");

            builder.Property(e => e.NgaySuaCuoi).HasColumnType("date");
        }
    }
}
