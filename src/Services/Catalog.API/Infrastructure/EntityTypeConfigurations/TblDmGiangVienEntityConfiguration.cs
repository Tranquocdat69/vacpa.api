﻿using Catalog.API.Infrastructure.Datatables.CNKT;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmGiangVienEntityConfiguration : IEntityTypeConfiguration<TblDmGiangVien>
    {
        public void Configure(EntityTypeBuilder<TblDmGiangVien> builder)
        {
            builder.HasKey(e => e.GiangVienGuid);

            builder.ToTable("tblDMGiangVien");

            builder.HasIndex(e => e.MaChucVu)
                .HasName("IX_tblDMGiangVien_ChucVuID");

            builder.Property(e => e.GiangVienGuid).HasDefaultValueSql("uuid_generate_v1()");

            builder.Property(e => e.ApplicationId).HasColumnName("ApplicationID");

            builder.Property(e => e.ChungChi).HasMaxLength(500);

            builder.Property(e => e.DonViCongTac)
                .IsRequired()
                .HasMaxLength(500);

            builder.Property(e => e.GiangVienId).HasColumnName("GiangVienID");

            builder.Property(e => e.GioiTinh)
                .IsRequired()
                .HasMaxLength(1)
                .IsFixedLength();

            builder.Property(e => e.NgaySinh).HasColumnType("date");

            builder.Property(e => e.NgaySuaCuoi).HasColumnType("date");

            builder.Property(e => e.TenGiangVien)
                .IsRequired()
                .HasMaxLength(250);

            builder.HasOne(d => d.ChucVuNavigation)
                .WithMany(p => p.TblDmGiangVien)
                .HasForeignKey(d => d.MaChucVu)
                .HasConstraintName("FK__tblDMGian__MaChu__22751F6C");

            builder.HasOne(d => d.TrinhDoChuyenMonNavigation)
                .WithMany(p => p.TblDmgiangVien)
                .HasForeignKey(d => d.MaTrinhDoChuyenMon)
                .HasConstraintName("FK__TblDmGiangVien__MaTrinhDoChuyenMon");
        }
    }
}
