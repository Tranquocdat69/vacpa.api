﻿using Catalog.API.Infrastructure.Datatables.QLHV;
using Microsoft.EntityFrameworkCore;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmHinhThucKyLuatEntityConfiguration : IEntityTypeConfiguration<TblDmHinhThucKyLuat>
    {
        public void Configure(EntityTypeBuilder<TblDmHinhThucKyLuat> builder)
        {
            builder.HasKey(e => e.HinhThucKyLuatGuid);

            builder.ToTable("tblDMHinhThucKyLuat");

            builder.Property(e => e.HinhThucKyLuatGuid).ValueGeneratedNever();

            builder.Property(e => e.TenHinhThucKyLuat)
                    .IsRequired()
                    .HasMaxLength(500);

            builder.Property(e => e.HinhThucKyLuatId)
                    .HasColumnName("HinhThucKyLuatID");

            builder.Property(e => e.NgaySuaCuoi).HasColumnType("date");
        }
    }
}
