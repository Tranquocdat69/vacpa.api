﻿using Catalog.API.Infrastructure.Datatables.QLHV;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmCauHoiBiMatEntityConfiguration : IEntityTypeConfiguration<TblDmCauHoiBiMat>
    {
        public void Configure(EntityTypeBuilder<TblDmCauHoiBiMat> builder)
        {
            builder.HasKey(e => e.CauHoiBiMatGuid);

            builder.ToTable("tblDMCauHoiBiMat");

            builder.Property(e => e.CauHoiBiMatGuid).ValueGeneratedNever();

            builder.Property(e => e.CauHoi)
                .IsRequired()
                .HasMaxLength(500);

            builder.Property(e => e.CauHoiBiMatId)
                .HasColumnName("CauHoiBiMatID");

            builder.Property(e => e.NgaySuaCuoi).HasColumnType("date");
        }
    }
}
