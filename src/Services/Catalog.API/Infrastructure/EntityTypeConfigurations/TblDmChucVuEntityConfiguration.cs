﻿using Catalog.API.Infrastructure.Datatables.CNKT;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmChucVuEntityConfiguration : IEntityTypeConfiguration<TblDmChucVu>
    {
        public void Configure(EntityTypeBuilder<TblDmChucVu> builder)
        {
            builder.HasKey(e => e.ChucVuGuid);

            builder.ToTable("tblDMChucVu");

            builder.Property(e => e.ChucVuGuid).HasDefaultValueSql("uuid_generate_v1()");

            builder.Property(e => e.ApplicationId).HasColumnName("ApplicationID");

            builder.Property(e => e.ChucVuId).HasColumnName("ChucVuID");

            builder.Property(e => e.TenChucVu)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(e => e.NgaySuaCuoi).HasColumnType("date");
        }
    }
}
