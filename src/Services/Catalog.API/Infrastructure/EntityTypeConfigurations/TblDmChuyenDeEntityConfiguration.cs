﻿using Catalog.API.Infrastructure.Datatables.CNKT;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmChuyenDeEntityTypeConfiguration : IEntityTypeConfiguration<TblDmChuyenDe>
    {
        public void Configure(EntityTypeBuilder<TblDmChuyenDe> buidler)
        {
            buidler.HasKey(e => e.ChuyenDeGuid)
                    .HasName("PK_tblCNKTChuyenDe");

            buidler.ToTable("tblDMChuyenDe");

            buidler.Property(e => e.ChuyenDeGuid).HasDefaultValueSql("uuid_generate_v1()");

            buidler.Property(e => e.ChuyenDeId);

            buidler.Property(e => e.TenChuyenDe)
                .IsRequired()
                .HasMaxLength(250);
        }
    }
}