﻿using Catalog.API.Infrastructure.Datatables.DMChung;
using Microsoft.EntityFrameworkCore;

namespace Catalog.API.Infrastructure.EntityTypeConfigurations
{
    public class TblDmNganHangEntityConfiguration : IEntityTypeConfiguration<TblDmNganHang>
    {
        public void Configure(EntityTypeBuilder<TblDmNganHang> builder)
        {
            builder.HasKey(e => e.NganHangGuid);

            builder.ToTable("tblDMNganHang");

            builder.Property(e => e.NganHangGuid).ValueGeneratedNever();

            builder.Property(e => e.TenNganHang)
                    .IsRequired()
                    .HasMaxLength(500);

            builder.Property(e => e.NganHangId)
                    .HasColumnName("NganHangID");

            builder.Property(e => e.NgaySuaCuoi).HasColumnType("date");
        }
    }
}
