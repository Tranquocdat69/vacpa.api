﻿namespace Catalog.API.Infrastructure.Datatables.QLHV
{
    public class TblDmTruongDaiHoc
    {
        public Guid TruongDaiHocGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? TruongDaiHocId { get; set; }
        public string TenTruongDaiHoc { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
}
