﻿namespace Catalog.API.Infrastructure.Datatables.QLHV
{
    public class TblDmSoThich
    {
        public Guid SoThichGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? SoThichId { get; set; }
        public string TenSoThich { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
}
