﻿using Catalog.API.Infrastructure.Datatables.CNKT;
using System;
using System.Collections.Generic;

namespace Catalog.API.Infrastructure.Datatables.QLHV
{
    public partial class TblDmTrinhDoChuyenMon
    {
        public TblDmTrinhDoChuyenMon()
        {
            TblDmgiangVien = new HashSet<TblDmGiangVien>();
        }

        public Guid TrinhDoChuyenMonGuid { get; set; }
        public Guid? ApplicationId { get; set; }
        public int? TrinhDoChuyenMonId { get; set; }
        public string TenTrinhDoChuyenMon { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
        public virtual ICollection<TblDmGiangVien> TblDmgiangVien { get; set; }
    }
}
