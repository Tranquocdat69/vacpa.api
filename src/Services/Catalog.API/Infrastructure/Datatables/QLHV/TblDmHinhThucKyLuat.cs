﻿namespace Catalog.API.Infrastructure.Datatables.QLHV
{
    public class TblDmHinhThucKyLuat
    {
        public Guid HinhThucKyLuatGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? HinhThucKyLuatId { get; set; }
        public string TenHinhThucKyLuat { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
}
