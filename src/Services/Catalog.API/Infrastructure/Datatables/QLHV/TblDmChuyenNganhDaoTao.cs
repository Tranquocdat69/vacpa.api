﻿using System;
using System.Collections.Generic;

namespace Catalog.API.Infrastructure.Datatables.QLHV
{
    public partial class TblDmChuyenNganhDaoTao
    {
        public Guid ChuyenNganhDaoTaoGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? ChuyenNganhDaoTaoId { get; set; }
        public string TenChuyenNganhDaoTao { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
}
