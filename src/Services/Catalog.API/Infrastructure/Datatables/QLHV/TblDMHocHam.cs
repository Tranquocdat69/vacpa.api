﻿namespace Catalog.API.Infrastructure.Datatables.QLHV
{
    public class TblDMHocHam
    {
        public Guid HocHamGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? HocHamId { get; set; }
        public string TenHocHam { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
}
