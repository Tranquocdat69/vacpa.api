﻿namespace Catalog.API.Infrastructure.Datatables.QLHV
{
    public class TblDmHocVi
    {
        public Guid HocViGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? HocViId { get; set; }
        public string TenHocVi { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
}
