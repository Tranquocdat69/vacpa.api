﻿using System;
using System.Collections.Generic;

namespace Catalog.API.Infrastructure.Datatables.QLHV
{
    public partial class TblDmCauHoiBiMat
    {
        public Guid CauHoiBiMatGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? CauHoiBiMatId { get; set; }
        public string CauHoi { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
}
