﻿namespace Catalog.API.Infrastructure.Datatables.QLHV
{
    public class TblDmHinhThucKhenThuong
    {
        public Guid HinhThucKhenThuongGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? HinhThucKhenThuongId { get; set; }
        public string TenHinhThucKhenThuong { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
}
