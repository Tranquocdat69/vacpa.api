﻿using System;
using System.Collections.Generic;

namespace Catalog.API.Infrastructure.Datatables.CNKT
{
    public partial class TblDmChuyenDe
    {
        public Guid ChuyenDeGuid { get; set; }
        public int? ChuyenDeId { get; set; }
        public string TenChuyenDe { get; set; }
        public byte TrangThai { get; set; }
    }
}
