﻿using Catalog.API.Infrastructure.Datatables.QLHV;
using System;
using System.Collections.Generic;

namespace Catalog.API.Infrastructure.Datatables.CNKT
{
    public partial class TblDmGiangVien
    {
        public Guid GiangVienGuid { get; set; }
        public string MaGiangVien { get; set; }
        public int? GiangVienId { get; set; }
        public Guid? ApplicationId { get; set; }
        public string TenGiangVien { get; set; }
        public Guid? MaChucVu { get; set; }
        public Guid? MaTrinhDoChuyenMon { get; set; }
        public string DonViCongTac { get; set; }
        public string ChungChi { get; set; }
        public string GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public Guid? MaHoiVienCaNhan { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }

        public virtual TblDmChucVu ChucVuNavigation { get; set; }
        public virtual TblDmTrinhDoChuyenMon TrinhDoChuyenMonNavigation { get; set; }
    }
}
