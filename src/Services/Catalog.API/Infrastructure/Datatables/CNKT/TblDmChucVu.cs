﻿using System;
using System.Collections.Generic;

namespace Catalog.API.Infrastructure.Datatables.CNKT
{
    public partial class TblDmChucVu
    {
        public TblDmChucVu()
        {
            TblDmGiangVien = new HashSet<TblDmGiangVien>();
        }

        public Guid ChucVuGuid { get; set; }
        public Guid? ApplicationId { get; set; }
        public int? ChucVuId { get; set; }
        public string TenChucVu { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }

        public virtual ICollection<TblDmGiangVien> TblDmGiangVien { get; set; }
    }
}
