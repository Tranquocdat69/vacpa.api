﻿namespace Catalog.API.Infrastructure.Datatables.DMChung
{
    public class TblDmNganHang
    {
        public Guid NganHangGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? NganHangId { get; set; }
        public string TenNganHang { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
}
