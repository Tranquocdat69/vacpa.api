﻿using System.Linq;
using Catalog.API.Infrastructure.Datatables.CNKT;
using Catalog.API.Infrastructure.Datatables.HoiVienCaNhan;
using Catalog.API.Infrastructure.Datatables.DMChung;
using Catalog.API.Infrastructure.Datatables.QLHV;
using Catalog.API.Infrastructure.EntityTypeConfigurations;
using CommonUtils;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Debug;

namespace Infrastructure
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            return new DataContext("", databaseType, connectionString, isTesting);
        }
    }
    public class DataContext : DbContext
    {
        //private string prefix;

        public static readonly ILoggerFactory loggerFactory = new LoggerFactory(new[] {
              new DebugLoggerProvider()
        });

        public DataContext()
        {
            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            DatabaseType = databaseType;
            ConnectionString = connectionString;
            IsTesting = isTesting;
        }

        public DataContext(string prefix, string databaseType, string connectionString, bool isTesting = false)
        {
            Prefix = prefix;
            DatabaseType = databaseType;
            ConnectionString = connectionString;
            IsTesting = isTesting;
        }

        #region sys
        public virtual DbSet<IdmApplications> IdmApplications { get; set; }
        #endregion

        #region catalog
        public virtual DbSet<TblDmChuyenDe> TblDmChuyenDe { get; set; }
        public virtual DbSet<TblDmGiangVien> TblDmGiangVien { get; set; }
        public virtual DbSet<TblDmChucVu> TblDmChucVu { get; set; }
        public virtual DbSet<TblDmTrinhDoChuyenMon> TblDmtrinhDoChuyenMon { get; set; }
        public virtual DbSet<TblDmCauHoiBiMat> TblDmCauHoiBiMat { get; set; }
        public virtual DbSet<TblDmChuyenNganhDaoTao> TblDmChuyenNganhDaoTao { get; set; }
        //phần TblHoiVienCaNhan khai báo để query dữ liệu về sau xóa vì member ở service khác 
        public virtual DbSet<TblHoiVienCaNhan> TblHoiVienCaNhan { get; set; }
        public virtual DbSet<TblDMHocHam> TblDMHocHam { get; set; }
        public virtual DbSet<TblDmHocVi> TblDMHocVi { get; set; }
        public virtual DbSet<TblDmTruongDaiHoc> TblDmTruongDaiHoc { get; set; }
        public virtual DbSet<TblDmHinhThucKhenThuong> TblDmHinhThucKhenThuong { get; set; }
        public virtual DbSet<TblDmHinhThucKyLuat> TblDmHinhThucKyLuat { get; set; }
        public virtual DbSet<TblDmSoThich> TblDmSoThich { get; set; }
        public virtual DbSet<TblDmNganHang> TblDmNganHang { get; set; }

        #endregion


        public string Prefix { get; set; }
        public string DatabaseType { get; set; }
        public string ConnectionString { get; set; }
        public bool IsTesting { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                #region Config database
                if (IsTesting)
                {
                    optionsBuilder.UseInMemoryDatabase("testDatabase");
                }
                else
                {
                    switch (DatabaseType)
                    {
                        case "PostgreSQLDatabase":
                            optionsBuilder.UseNpgsql(ConnectionString);
                            break;
                        default:
                            optionsBuilder.UseSqlServer(ConnectionString);
                            break;
                    }
                }
                #endregion

                optionsBuilder.ReplaceService<IModelCacheKeyFactory, DynamicModelCacheKeyFactory>();
                optionsBuilder.UseLoggerFactory(loggerFactory).EnableSensitiveDataLogging();
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("uuid-ossp");

            modelBuilder.ApplyConfiguration(new TblDmChuyenDeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmGiangVienEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmChucVuEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmTrinhDoChuyenMonEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmCauHoiBiMatEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmChuyenNganhDaoTaoEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblHoiVienCaNhanEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmHocHamEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmHocViEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmTruongDaiHocEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmHinhThucKhenThuongEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmHinhThucKyLuatEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmSoThichEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmChuyenNganhDaoTaoEntityConfiguration());         
            modelBuilder.ApplyConfiguration(new TblDmSoThichEntityConfiguration());
            modelBuilder.ApplyConfiguration(new TblDmNganHangEntityConfiguration());


            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
    public class DynamicModelCacheKeyFactory : IModelCacheKeyFactory
    {
        public object Create(DbContext context)
        {

            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            if (context is DataContext dynamicContext)
            {
                return (context.GetType(), dynamicContext.Prefix, databaseType, connectionString, isTesting);
            }

            return context.GetType();
        }
    }
}
