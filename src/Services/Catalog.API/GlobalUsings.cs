﻿global using Catalog.API.Infrastructure.Datatables;
global using CommonUtils;
global using DataUtils;
global using Microsoft.AspNetCore.Authorization;
global using Microsoft.AspNetCore.Mvc;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.EntityFrameworkCore.Metadata.Builders;
global using Newtonsoft.Json;
global using System;
global using System.Collections.Generic;
global using System.Threading.Tasks;

