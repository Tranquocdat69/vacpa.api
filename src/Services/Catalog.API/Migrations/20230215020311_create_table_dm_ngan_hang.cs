﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Catalog.API.Migrations
{
    public partial class create_table_dm_ngan_hang : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblDMNganHang",
                columns: table => new
                {
                    NganHangGuid = table.Column<Guid>(nullable: false),
                    ApplicationID = table.Column<Guid>(nullable: true),
                    NganHangID = table.Column<int>(nullable: true),
                    TenNganHang = table.Column<string>(maxLength: 500, nullable: false),
                    TrangThai = table.Column<byte>(nullable: true),
                    NguoiSuaCuoi = table.Column<Guid>(nullable: true),
                    NgaySuaCuoi = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblDMNganHang", x => x.NganHangGuid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblDMNganHang");
        }
    }
}
