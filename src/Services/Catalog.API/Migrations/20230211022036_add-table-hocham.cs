﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Catalog.API.Migrations
{
    public partial class addtablehocham : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblDMHocHam",
                columns: table => new
                {
                    HocHamGuid = table.Column<Guid>(nullable: false),
                    ApplicationID = table.Column<Guid>(nullable: true),
                    HocHamID = table.Column<int>(nullable: true),
                    TenHocHam = table.Column<string>(maxLength: 500, nullable: false),
                    TrangThai = table.Column<byte>(nullable: true),
                    NguoiSuaCuoi = table.Column<Guid>(nullable: true),
                    NgaySuaCuoi = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblDMHocHam", x => x.HocHamGuid);
                });

            migrationBuilder.CreateTable(
                name: "tblHoiVienCaNhan",
                columns: table => new
                {
                    HoiVienCaNhanID = table.Column<int>(nullable: false),
                    MaHoiVienCaNhan = table.Column<string>(maxLength: 100, nullable: true),
                    LoaiHoiVienCaNhan = table.Column<char>(nullable: true, comment: "0: người quan tâm; 1: hội viên; 2: Kiểm toán viên"),
                    TroLyKTV = table.Column<char>(nullable: true, comment: "1: Là trợ lý kiểm toán viên; 0: Không là trợ lý kiểm toán viên"),
                    HoDem = table.Column<string>(maxLength: 100, nullable: true),
                    Ten = table.Column<string>(maxLength: 50, nullable: true),
                    GioiTinh = table.Column<char>(nullable: true),
                    NgaySinh = table.Column<DateTime>(type: "date", nullable: true),
                    QuocTichID = table.Column<int>(nullable: true),
                    QueQuan_TinhID = table.Column<string>(fixedLength: true, maxLength: 10, nullable: true),
                    QueQuan_HuyenID = table.Column<string>(fixedLength: true, maxLength: 10, nullable: true),
                    QueQuan_XaID = table.Column<string>(fixedLength: true, maxLength: 10, nullable: true),
                    DiaChi = table.Column<string>(maxLength: 500, nullable: true),
                    DiaChi_TinhID = table.Column<string>(fixedLength: true, maxLength: 10, nullable: true),
                    DiaChi_HuyenID = table.Column<string>(fixedLength: true, maxLength: 10, nullable: true),
                    DiaChi_XaID = table.Column<string>(fixedLength: true, maxLength: 10, nullable: true),
                    TruongDaiHocID = table.Column<int>(nullable: true),
                    ChuyenNganhDaoTaoID = table.Column<int>(nullable: true),
                    ChuyenNganhNam = table.Column<string>(fixedLength: true, maxLength: 10, nullable: true),
                    HocViID = table.Column<int>(nullable: true),
                    HocViNam = table.Column<string>(fixedLength: true, maxLength: 10, nullable: true),
                    HocHamID = table.Column<int>(nullable: true),
                    HocHamNam = table.Column<string>(fixedLength: true, maxLength: 10, nullable: true),
                    SoCMND = table.Column<string>(maxLength: 50, nullable: true),
                    CMND_NgayCap = table.Column<DateTime>(type: "date", nullable: true),
                    CMND_TinhID = table.Column<int>(nullable: true),
                    SoTaiKhoanNganHang = table.Column<string>(maxLength: 50, nullable: true),
                    NganHangID = table.Column<int>(nullable: true),
                    SoChungChiKTV = table.Column<string>(maxLength: 50, nullable: true),
                    NgayCapChungChiKTV = table.Column<DateTime>(type: "date", nullable: true),
                    SoGiayChungNhanDKHN = table.Column<string>(maxLength: 50, nullable: true),
                    ChucVuID = table.Column<int>(nullable: true),
                    HoiVienTapTheID = table.Column<int>(nullable: true),
                    DonViCongTac = table.Column<string>(maxLength: 500, nullable: true),
                    NgayCapGiayChungNhanDKHN = table.Column<DateTime>(type: "date", nullable: true),
                    HanCapTu = table.Column<DateTime>(type: "date", nullable: true),
                    HanCapDen = table.Column<DateTime>(type: "date", nullable: true),
                    Email = table.Column<string>(maxLength: 100, nullable: true),
                    Mobile = table.Column<string>(maxLength: 50, nullable: true),
                    DienThoai = table.Column<string>(maxLength: 100, nullable: true),
                    SoThichID = table.Column<int>(nullable: true),
                    NgayGiaNhap = table.Column<DateTime>(type: "date", nullable: true),
                    NgayNhap = table.Column<DateTime>(type: "date", nullable: true),
                    NguoiNhap = table.Column<string>(maxLength: 50, nullable: true),
                    NgayCapNhat = table.Column<DateTime>(type: "date", nullable: true),
                    NguoiCapNhat = table.Column<string>(maxLength: 50, nullable: true),
                    NgayDuyet = table.Column<DateTime>(type: "date", nullable: true),
                    NguoiDuyet = table.Column<string>(maxLength: 50, nullable: true),
                    TinhTrangHoiVienID = table.Column<int>(nullable: true, comment: "0: không hoạt động; 1: hoạt động"),
                    SoQuyetDinhKetNap = table.Column<string>(maxLength: 250, nullable: true),
                    NgayQuyetDinhKetNap = table.Column<DateTime>(type: "date", nullable: true),
                    NamGuiEmailChucMung = table.Column<int>(nullable: true),
                    LoaiHoiVienCaNhanChiTiet = table.Column<int>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblHoiVienCaNhan", x => x.HoiVienCaNhanID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblDMHocHam");

            migrationBuilder.DropTable(
                name: "tblHoiVienCaNhan");
        }
    }
}
