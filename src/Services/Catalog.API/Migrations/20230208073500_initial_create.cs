﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Catalog.API.Migrations
{
    public partial class initial_create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "idm_Applications",
                columns: table => new
                {
                    ApplicationId = table.Column<Guid>(nullable: false),
                    ApplicationName = table.Column<string>(maxLength: 256, nullable: false),
                    LoweredApplicationName = table.Column<string>(maxLength: 256, nullable: false),
                    Description = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_Applications", x => x.ApplicationId);
                });

            migrationBuilder.CreateTable(
                name: "tblDMCauHoiBiMat",
                columns: table => new
                {
                    CauHoiBiMatGuid = table.Column<Guid>(nullable: false),
                    ApplicationID = table.Column<Guid>(nullable: true),
                    CauHoiBiMatID = table.Column<int>(nullable: true),
                    CauHoi = table.Column<string>(maxLength: 500, nullable: false),
                    TrangThai = table.Column<byte>(nullable: true),
                    NguoiSuaCuoi = table.Column<Guid>(nullable: true),
                    NgaySuaCuoi = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblDMCauHoiBiMat", x => x.CauHoiBiMatGuid);
                });

            migrationBuilder.CreateTable(
                name: "tblDMChucVu",
                columns: table => new
                {
                    ChucVuGuid = table.Column<Guid>(nullable: false, defaultValueSql: "uuid_generate_v1()"),
                    ApplicationID = table.Column<Guid>(nullable: true),
                    ChucVuID = table.Column<int>(nullable: true),
                    TenChucVu = table.Column<string>(maxLength: 250, nullable: false),
                    TrangThai = table.Column<byte>(nullable: true),
                    NguoiSuaCuoi = table.Column<Guid>(nullable: true),
                    NgaySuaCuoi = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblDMChucVu", x => x.ChucVuGuid);
                });

            migrationBuilder.CreateTable(
                name: "tblDMChuyenDe",
                columns: table => new
                {
                    ChuyenDeGuid = table.Column<Guid>(nullable: false, defaultValueSql: "uuid_generate_v1()"),
                    ChuyenDeId = table.Column<int>(nullable: true),
                    TenChuyenDe = table.Column<string>(maxLength: 250, nullable: false),
                    TrangThai = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblCNKTChuyenDe", x => x.ChuyenDeGuid);
                });

            migrationBuilder.CreateTable(
                name: "tblDMChuyenNganhDaoTao",
                columns: table => new
                {
                    ChuyenNganhDaoTaoGuid = table.Column<Guid>(nullable: false, comment: ""),
                    ApplicationID = table.Column<Guid>(nullable: true),
                    ChuyenNganhDaoTaoID = table.Column<int>(nullable: true),
                    TenChuyenNganhDaoTao = table.Column<string>(maxLength: 500, nullable: false),
                    TrangThai = table.Column<byte>(nullable: true),
                    NguoiSuaCuoi = table.Column<Guid>(nullable: true, comment: ""),
                    NgaySuaCuoi = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblDMChuyenNganhDaoTao", x => x.ChuyenNganhDaoTaoGuid);
                });

            migrationBuilder.CreateTable(
                name: "tblDMTrinhDoChuyenMon",
                columns: table => new
                {
                    TrinhDoChuyenMonGuid = table.Column<Guid>(nullable: false, defaultValueSql: "uuid_generate_v1()"),
                    ApplicationID = table.Column<Guid>(nullable: true),
                    TrinhDoChuyenMonID = table.Column<int>(nullable: true),
                    TenTrinhDoChuyenMon = table.Column<string>(maxLength: 500, nullable: true),
                    TrangThai = table.Column<byte>(nullable: true),
                    NguoiSuaCuoi = table.Column<Guid>(nullable: true),
                    NgaySuaCuoi = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblDMTrinhDoChuyenMon", x => x.TrinhDoChuyenMonGuid);
                });

            migrationBuilder.CreateTable(
                name: "tblDMGiangVien",
                columns: table => new
                {
                    GiangVienGuid = table.Column<Guid>(nullable: false, defaultValueSql: "uuid_generate_v1()"),
                    MaGiangVien = table.Column<string>(nullable: true),
                    GiangVienID = table.Column<int>(nullable: true),
                    ApplicationID = table.Column<Guid>(nullable: true),
                    TenGiangVien = table.Column<string>(maxLength: 250, nullable: false),
                    MaChucVu = table.Column<Guid>(nullable: true),
                    MaTrinhDoChuyenMon = table.Column<Guid>(nullable: true),
                    DonViCongTac = table.Column<string>(maxLength: 500, nullable: false),
                    ChungChi = table.Column<string>(maxLength: 500, nullable: true),
                    GioiTinh = table.Column<string>(fixedLength: true, maxLength: 1, nullable: false),
                    NgaySinh = table.Column<DateTime>(type: "date", nullable: true),
                    MaHoiVienCaNhan = table.Column<Guid>(nullable: true),
                    TrangThai = table.Column<byte>(nullable: true),
                    NguoiSuaCuoi = table.Column<Guid>(nullable: true),
                    NgaySuaCuoi = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblDMGiangVien", x => x.GiangVienGuid);
                    table.ForeignKey(
                        name: "FK__tblDMGian__MaChu__22751F6C",
                        column: x => x.MaChucVu,
                        principalTable: "tblDMChucVu",
                        principalColumn: "ChucVuGuid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__TblDmGiangVien__MaTrinhDoChuyenMon",
                        column: x => x.MaTrinhDoChuyenMon,
                        principalTable: "tblDMTrinhDoChuyenMon",
                        principalColumn: "TrinhDoChuyenMonGuid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tblDMGiangVien_ChucVuID",
                table: "tblDMGiangVien",
                column: "MaChucVu");

            migrationBuilder.CreateIndex(
                name: "IX_tblDMGiangVien_MaTrinhDoChuyenMon",
                table: "tblDMGiangVien",
                column: "MaTrinhDoChuyenMon");

            //migrationBuilder.CreateTable(
            //   name: "tblDMHocHam",
            //   columns: table => new
            //   {
            //       HocHamGuid = table.Column<Guid>(nullable: false),
            //       ApplicationID = table.Column<Guid>(nullable: true),
            //       HocHamID = table.Column<int>(nullable: true),
            //       TenHocHam = table.Column<string>(maxLength: 500, nullable: false),
            //       TrangThai = table.Column<byte>(nullable: true),
            //       NguoiSuaCuoi = table.Column<Guid>(nullable: true),
            //       NgaySuaCuoi = table.Column<DateTime>(type: "date", nullable: true)
            //   },
            //   constraints: table =>
            //   {
            //       table.PrimaryKey("PK_tblDMCauHoiBiMat", x => x.CauHoiBiMatGuid);
            //   });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "idm_Applications");

            migrationBuilder.DropTable(
                name: "tblDMCauHoiBiMat");

            migrationBuilder.DropTable(
                name: "tblDMChuyenDe");

            migrationBuilder.DropTable(
                name: "tblDMChuyenNganhDaoTao");

            migrationBuilder.DropTable(
                name: "tblDMGiangVien");

            migrationBuilder.DropTable(
                name: "tblDMChucVu");

            migrationBuilder.DropTable(
                name: "tblDMTrinhDoChuyenMon");

            //migrationBuilder.DropTable(
            //   name: "tblDMHocHam");
        }
    }
}
