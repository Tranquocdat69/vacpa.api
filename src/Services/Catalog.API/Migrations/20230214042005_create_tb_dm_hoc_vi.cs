﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Catalog.API.Migrations
{
    public partial class create_tb_dm_hoc_vi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblDMHocVi",
                columns: table => new
                {
                    HocViGuid = table.Column<Guid>(nullable: false),
                    ApplicationID = table.Column<Guid>(nullable: true),
                    HocViID = table.Column<int>(nullable: true),
                    TenHocVi = table.Column<string>(maxLength: 500, nullable: false),
                    TrangThai = table.Column<byte>(nullable: true),
                    NguoiSuaCuoi = table.Column<Guid>(nullable: true),
                    NgaySuaCuoi = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblDMHocVi", x => x.HocViGuid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblDMHocVi");
        }
    }
}
