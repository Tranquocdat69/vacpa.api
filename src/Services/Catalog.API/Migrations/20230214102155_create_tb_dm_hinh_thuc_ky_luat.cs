﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Catalog.API.Migrations
{
    public partial class create_tb_dm_hinh_thuc_ky_luat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblDMHinhThucKyLuat",
                columns: table => new
                {
                    HinhThucKyLuatGuid = table.Column<Guid>(nullable: false),
                    ApplicationID = table.Column<Guid>(nullable: true),
                    HinhThucKyLuatID = table.Column<int>(nullable: true),
                    TenHinhThucKyLuat = table.Column<string>(maxLength: 500, nullable: false),
                    TrangThai = table.Column<byte>(nullable: true),
                    NguoiSuaCuoi = table.Column<Guid>(nullable: true),
                    NgaySuaCuoi = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblDMHinhThucKyLuat", x => x.HinhThucKyLuatGuid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblDMHinhThucKyLuat");
        }
    }
}
