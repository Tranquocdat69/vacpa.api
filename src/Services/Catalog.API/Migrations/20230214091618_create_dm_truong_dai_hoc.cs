﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Catalog.API.Migrations
{
    public partial class create_dm_truong_dai_hoc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblDMTruongDaiHoc",
                columns: table => new
                {
                    TruongDaiHocGuid = table.Column<Guid>(nullable: false),
                    ApplicationID = table.Column<Guid>(nullable: true),
                    TruongDaiHocID = table.Column<int>(nullable: true),
                    TenTruongDaiHoc = table.Column<string>(maxLength: 500, nullable: false),
                    TrangThai = table.Column<byte>(nullable: true),
                    NguoiSuaCuoi = table.Column<Guid>(nullable: true),
                    NgaySuaCuoi = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblDMTruongDaiHoc", x => x.TruongDaiHocGuid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblDMTruongDaiHoc");
        }
    }
}
