using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business
{
    public interface IBootstrapHandler
    {
        // Task<bool> Bootstrap(bool isTesting);
        bool RefreshCache();
        // Task<bool> RefreshCacheUser();

        // Task<bool> InitFireBased();
    }
}
