﻿using Catalog.API.Infrastructure.Datatables.QLHV;
using System.Linq;

namespace Catalog.API.Bussiness.TrinhDoChuyenMon
{
    public class TrinhDoChuyenMonHandler : ITrinhDoChuyenMonHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public TrinhDoChuyenMonHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> GetAllAsync()
        {
            List<TrinhDoChuyenMonModel> response = new List<TrinhDoChuyenMonModel>();
            List<TblDmTrinhDoChuyenMon> listTblDmTrinhDoChuyenMon = await _unitOfWork.GetRepository<TblDmTrinhDoChuyenMon>().GetAll().ToListAsync();
            foreach (var item in listTblDmTrinhDoChuyenMon)
            {
                TrinhDoChuyenMonModel trinhDoChuyenMonModel = AutoMapperUtils.AutoMap<TblDmTrinhDoChuyenMon, TrinhDoChuyenMonModel>(item);
                response.Add(trinhDoChuyenMonModel);
            }
            return new ResponseList<TrinhDoChuyenMonModel>(response);
        }

        public async Task<Response> FilterAsync(TrinhDoChuyenMonModelFilter filter)
        {
            try
            {
                var response = new Pagination<TrinhDoChuyenMonModel>();
                IQueryable<TblDmTrinhDoChuyenMon> data = from dt in _unitOfWork
                                                         .GetRepository<TblDmTrinhDoChuyenMon>()
                                                         .GetAll()
                                                         select dt;
                if (filter.ApplicationId.HasValue)
                {
                    data = data.Where(d => d.ApplicationId == filter.ApplicationId.Value);
                }
                if (filter.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == filter.TrangThai.Value);
                }
                if (!filter.Size.HasValue)
                {
                    filter.Size = 20;
                }
                filter.Page = filter.Page - 1;
                int excludedRows = ((int)filter.Page) * ((int)filter.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }
                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);
                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int)filter.Size);

                var dataResult = await data.ToListAsync();
                response.Content = new List<TrinhDoChuyenMonModel>();
                foreach (var item in dataResult)
                {
                    TrinhDoChuyenMonModel trinhDoChuyenMonModel = AutoMapperUtils.AutoMap<TblDmTrinhDoChuyenMon, TrinhDoChuyenMonModel>(item);
                    response.Content.Add(trinhDoChuyenMonModel);
                }

                response.NumberOfElements = totalCount;
                response.Page = filter.Page.HasValue ? filter.Page.Value : 0;
                response.Size = filter.Size.HasValue ? filter.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (filter.Size.HasValue ? (filter.Size.Value + 1) : 1);

                var result = new ResponsePagination<TrinhDoChuyenMonModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<TrinhDoChuyenMonModel>>(null, ex.Message, Code.ServerError);
            }
        }

    }
}
