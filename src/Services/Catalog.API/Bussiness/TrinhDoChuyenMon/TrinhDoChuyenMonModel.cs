﻿namespace Catalog.API.Bussiness.TrinhDoChuyenMon
{
    public class TrinhDoChuyenMonModel
    {
        public Guid TrinhDoChuyenMonGuid { get; set; }
        public Guid? ApplicationId { get; set; }
        public int? TrinhDoChuyenMonId { get; set; }
        public string TenTrinhDoChuyenMon { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }

    }
    public class TrinhDoChuyenMonModelFilter : PaginationRequest
    {
        public byte? TrangThai { get; set; }
        public string TenTrinhDoChuyenMon { get; set; }
    }
}
