﻿namespace Catalog.API.Bussiness.TrinhDoChuyenMon
{
    public interface ITrinhDoChuyenMonHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> FilterAsync(TrinhDoChuyenMonModelFilter filter);
    }
}
