﻿namespace Catalog.API.Bussiness.ChucVu
{
    public class ChucVuModel
    {
        public Guid ChucVuGuid { get; set; }
        public Guid? ApplicationId { get; set; }
        public int? ChucVuId { get; set; }
        public string TenChucVu { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }

    public class ChucVuModelFilter : PaginationRequest
    {
        public byte? TrangThai { get; set; }
    }
}
