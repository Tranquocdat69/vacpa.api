﻿namespace Catalog.API.Bussiness.ChucVu
{
    public interface IChucVuHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> FilterAsync(ChucVuModelFilter filter);
    }
}
