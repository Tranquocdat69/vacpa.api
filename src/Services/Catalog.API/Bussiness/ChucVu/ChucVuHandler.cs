﻿using Catalog.API.Infrastructure.Datatables.CNKT;
using System.Linq;

namespace Catalog.API.Bussiness.ChucVu
{
    public class ChucVuHandler : IChucVuHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public ChucVuHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> GetAllAsync()
        {
            List<TblDmChucVu> listDmChucVu = await _unitOfWork.GetRepository<TblDmChucVu>().GetAll().ToListAsync();
            List<ChucVuModel> response = new List<ChucVuModel>();
            if (listDmChucVu is not null && listDmChucVu.Count > 0)
            {
                foreach (var item in listDmChucVu)
                {
                    response.Add(AutoMapperUtils.AutoMap<TblDmChucVu, ChucVuModel>(item));
                }
            }
            return new ResponseList<ChucVuModel>(response);
        }

        public async Task<Response> FilterAsync(ChucVuModelFilter filter)
        {
            try
            {
                var response = new Pagination<ChucVuModel>();
                IQueryable<TblDmChucVu> data = from dt in _unitOfWork
                                                         .GetRepository<TblDmChucVu>()
                                                         .GetAll()
                                                         select dt;
                if (filter.ApplicationId.HasValue)
                {
                    data = data.Where(d => d.ApplicationId == filter.ApplicationId.Value);
                }
                if (filter.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == filter.TrangThai.Value);
                }
                if (!filter.Size.HasValue)
                {
                    filter.Size = 20;
                }
                filter.Page = filter.Page - 1;
                int excludedRows = ((int)filter.Page) * ((int)filter.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }
                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);
                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int)filter.Size);

                var dataResult = await data.ToListAsync();
                response.Content = new List<ChucVuModel>();
                foreach (var item in dataResult)
                {
                    ChucVuModel trinhDoChuyenMonModel = AutoMapperUtils.AutoMap<TblDmChucVu, ChucVuModel>(item);
                    response.Content.Add(trinhDoChuyenMonModel);
                }

                response.NumberOfElements = totalCount;
                response.Page = filter.Page.HasValue ? filter.Page.Value : 0;
                response.Size = filter.Size.HasValue ? filter.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (filter.Size.HasValue ? (filter.Size.Value + 1) : 1);

                var result = new ResponsePagination<ChucVuModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<ChucVuModel>>(null, ex.Message, Code.ServerError);
            }
        }
    }
}
