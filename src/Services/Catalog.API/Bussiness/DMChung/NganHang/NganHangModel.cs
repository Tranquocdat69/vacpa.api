﻿namespace Catalog.API.Bussiness.DMChung.NganHang
{
    public class NganHangModel
    {
        public Guid NganHangGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? NganHangId { get; set; }
        public string TenNganHang { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class NganHangDeleteModel
    {
        public Guid NganHangGuid { get; set; }
        public string Name { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
    }
    public class NganHangModelFilter : PaginationRequest
    {
        public int? TrangThai { get; set; }
        public string TenNganHang { get; set; }
    }
}
