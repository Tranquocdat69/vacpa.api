﻿using Catalog.API.Infrastructure.Datatables.DMChung;
using System.Linq;

namespace Catalog.API.Bussiness.DMChung.NganHang
{
    public class NganHangHandler : INganHangHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public NganHangHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        private bool IsCreateNganHangModelNotValid(NganHangModel model)
        {
            if (model.TenNganHang is null)
            {
                return true;
            }
            return false;
        }
        public async Task<Response> CreateAsync(NganHangModel model)
        {
            try
            {
                if (IsCreateNganHangModelNotValid(model))
                {
                    return new ResponseObject<NganHangModel>(null, "Dữ liệu cung cấp chưa đầy đủ", Code.ServerError);
                }

                IRepository<TblDmNganHang> repository = _unitOfWork.GetRepository<TblDmNganHang>();
                string strName = model.TenNganHang.ToLower().Trim().Replace(" ", "");
                TblDmNganHang check = await repository.Get(x => x.TenNganHang.ToLower().Trim().Replace(" ", "").Equals(strName)).FirstOrDefaultAsync();

                if (check is not null)
                {
                    return new ResponseObject<NganHangModel>(null, $"Tên học vị: {model.TenNganHang} đã tồn tại", Code.ServerError);
                }

                TblDmNganHang tblNganHang = AutoMapperUtils.AutoMap<NganHangModel, TblDmNganHang>(model);
                tblNganHang.NganHangGuid = Guid.NewGuid();
                tblNganHang.NgaySuaCuoi = DateTime.Now;

                NganHangModel result = AutoMapperUtils.AutoMap<TblDmNganHang, NganHangModel>(tblNganHang);

                repository.Add(tblNganHang);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<NganHangModel>(result, "Thêm mới thành công", Code.Success);
                }
                else
                {
                    return new ResponseObject<NganHangModel>(null, "Thêm mới thất bại", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<NganHangModel>(null, "Exception: Thêm mới học vị thất bại", Code.ServerError);
            }
        }

        public async Task<Response> DeleteByNganHangGuidAsync(string NganHangGuid)
        {
            try
            {
                var datas = _unitOfWork.GetRepository<TblDmNganHang>();
                var dNav = _unitOfWork.GetRepository<TblDmNganHang>().Find(NganHangGuid);
                if (dNav != null)
                {
                    datas.Delete(dNav);
                }
                if (await _unitOfWork.SaveAsync() >= 1)
                    return new ResponseObject<NganHangDeleteModel>(null, "Xóa thành công!", Code.Success);
                else
                    return new ResponseObject<NganHangDeleteModel>(null, "Không xóa được ", Code.BadRequest);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<NganHangDeleteModel>(null, "Exception: Không xóa được ", Code.BadRequest);
            }
        }

        public async Task<Response> FilterNganHangAsync(NganHangModelFilter model)
        {
            try
            {
                var response = new Pagination<NganHangModel>();
                IQueryable<TblDmNganHang> data = from dt in _unitOfWork.GetRepository<TblDmNganHang>().GetAll()
                                                select dt;
                if (model.ApplicationId.HasValue)
                {
                    data = data.Where(ch => ch.ApplicationID == model.ApplicationId.Value);
                }
                if (!string.IsNullOrEmpty(model.FullTextSearch))
                {
                    var text = model.FullTextSearch.Trim().ToLower();
                    data = data.Where(ch => ch.TenNganHang.ToLower().Contains(text));
                }
                if (model.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == model.TrangThai.Value);
                }
                if (!model.Size.HasValue)
                {
                    model.Size = 20;
                }
                model.Page = model.Page - 1;
                int excludedRows = ((int)model.Page) * ((int)model.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }

                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);

                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int)model.Size);

                var dataResult = await data.ToListAsync();

                response.Content = new List<NganHangModel>();
                foreach (var item in dataResult)
                {
                    response.Content.Add(AutoMapperUtils.AutoMap<TblDmNganHang, NganHangModel>(item));
                }

                response.NumberOfElements = totalCount;
                response.Page = model.Page.HasValue ? model.Page.Value : 0;
                response.Size = model.Size.HasValue ? model.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                var result = new ResponsePagination<NganHangModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<NganHangModel>>(null, "Exception: không tìm được", Code.ServerError);
            }
        }

        public async Task<Response> GetAllAsync()
        {
            try
            {
                List<TblDmNganHang> listDmNganHang = await _unitOfWork.GetRepository<TblDmNganHang>().GetAll().ToListAsync();
                List<NganHangModel> response = new List<NganHangModel>();
                if (listDmNganHang is not null && listDmNganHang.Count > 0)
                {
                    foreach (TblDmNganHang item in listDmNganHang)
                    {
                        response.Add(AutoMapperUtils.AutoMap<TblDmNganHang, NganHangModel>(item));
                    }
                }
                return new ResponseList<NganHangModel>(response);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<NganHangModel>>(null, "Exception: không tìm được", Code.ServerError);
            }
        }

        public async Task<Response> UpdateAsync(NganHangModel model)
        {
            try
            {
                var uNav = _unitOfWork.GetRepository<TblDmNganHang>().Find(model.NganHangGuid);
                if (uNav != null)
                {
                    var navsRepo = _unitOfWork.GetRepository<TblDmNganHang>();
                    uNav.TenNganHang = model.TenNganHang;
                    uNav.TrangThai = model.TrangThai;
                    uNav.NgaySuaCuoi = DateTime.Now;
                    uNav.NgaySuaCuoi = model.NgaySuaCuoi;
                    navsRepo.Update(uNav);

                    if (await _unitOfWork.SaveAsync() >= 1)
                    {
                        return new ResponseObject<NganHangModel>(AutoMapperUtils.AutoMap<TblDmNganHang, NganHangModel>(uNav), "Update successfully!", Code.Success);
                    }
                    else
                        return new ResponseError(Code.ServerError, "Update failed!");
                }
                return new Response(Code.NotFound, "No data found!");

            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<NganHangModel>(null, "Lỗi khi cập nhật!", Code.ServerError);
            }
        }
    }
}
