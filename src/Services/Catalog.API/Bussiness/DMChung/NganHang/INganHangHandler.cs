﻿
namespace Catalog.API.Bussiness.DMChung.NganHang
{
    public interface INganHangHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> CreateAsync(NganHangModel model);
        Task<Response> UpdateAsync(NganHangModel model);
        Task<Response> DeleteByNganHangGuidAsync(string NganHangGuid);
        Task<Response> FilterNganHangAsync(NganHangModelFilter model);
    }
}
