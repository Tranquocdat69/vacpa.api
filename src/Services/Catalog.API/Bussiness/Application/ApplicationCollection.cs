﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonUtils;
using DataUtils;
using Infrastructure;

namespace Business
{
    public class ApplicationCollection
    {
        private readonly IApplicationHandler _handler;
        public HashSet<ApplicationModel> Collection;

        protected ApplicationCollection()
        {
            _handler = new ApplicationHandler();
            LoadToHashSet();
        }

        public static ApplicationCollection Instance { get; } = new ApplicationCollection();

        public void LoadToHashSet()
        {
            Collection = new HashSet<ApplicationModel>();
            var listResponse = _handler.GetAll();
            if (listResponse.Result.Code == Code.Success)
            {
                // Add to hashset
                //if (listResponse is ResponseObject<List<IdmApplications>> listResponseObj)
                //    foreach (var response in listResponseObj.Data)
                //        Collection.Add(response);
            }
        }

        public string GetName(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result?.Name;
        }

        public BaseApplicationModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result;
        }
    }
}