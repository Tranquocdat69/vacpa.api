﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataUtils;
using Microsoft.EntityFrameworkCore;
using Serilog;
using CommonUtils;
using Infrastructure;

namespace Business
{
    public class ApplicationHandler : IApplicationHandler
    {
        public async Task<Response> GetAll()
        {
            using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
            {
                var listCatalogItem = await unitOfWork.GetRepository<IdmApplications>().GetAll().ToListAsync();

                var r = new ResponseObject<List<IdmApplications>>(listCatalogItem);

                return r;
            }
        }
    }
}