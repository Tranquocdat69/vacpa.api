﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonUtils;
using DataUtils;

namespace Business
{
    public interface IApplicationHandler
    {
        Task<Response> GetAll();

    }
}