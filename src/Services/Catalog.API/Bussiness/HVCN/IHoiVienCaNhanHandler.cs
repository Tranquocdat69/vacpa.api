﻿using Catalog.API.Bussiness.GiangVien;

namespace Catalog.API.Bussiness.HVCN
{
    public interface IHoiVienCaNhanHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> GetByMaHoiVienAsync(string maHoiVienCaNhan);
        Task<Response> FilterHoiVienAsync(HoiVienCaNhanModelFilter hoiVienCaNhanFilter);
        Task<Response> CreateAsync(HoiVienCaNhanModel hoiVienCaNhanModel);
        Task<Response> UpdateAsync(HoiVienCaNhanModel hoiVienCaNhanModel);
        Task<Response> DeleteByMaGiangVienAsync(string maHoiVienCaNhan);
    }
}
