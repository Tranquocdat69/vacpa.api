﻿namespace Catalog.API.Bussiness.HVCN
{
    public class HoiVienCaNhanModel
    {
        public int HoiVienCaNhanId { get; set; }
        public string MaHoiVienCaNhan { get; set; }
        public char? LoaiHoiVienCaNhan { get; set; }
        public char? TroLyKtv { get; set; }
        public string HoDem { get; set; }
        public string Ten { get; set; }
        public char? GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public int? QuocTichId { get; set; }
        public string QueQuanTinhId { get; set; }
        public string QueQuanHuyenId { get; set; }
        public string QueQuanXaId { get; set; }
        public string DiaChi { get; set; }
        public string DiaChiTinhId { get; set; }
        public string DiaChiHuyenId { get; set; }
        public string DiaChiXaId { get; set; }
        public int? TruongDaiHocId { get; set; }
        public int? ChuyenNganhDaoTaoId { get; set; }
        public string ChuyenNganhNam { get; set; }
        public int? HocViId { get; set; }
        public string HocViNam { get; set; }
        public int? HocHamId { get; set; }
        public string HocHamNam { get; set; }
        public string SoCmnd { get; set; }
        public DateTime? CmndNgayCap { get; set; }
        public int? CmndTinhId { get; set; }
        public string SoTaiKhoanNganHang { get; set; }
        public int? NganHangId { get; set; }
        public string SoChungChiKtv { get; set; }
        public DateTime? NgayCapChungChiKtv { get; set; }
        public string SoGiayChungNhanDkhn { get; set; }
        public int? ChucVuId { get; set; }
        public int? HoiVienTapTheId { get; set; }
        public string DonViCongTac { get; set; }
        public DateTime? NgayCapGiayChungNhanDkhn { get; set; }
        public DateTime? HanCapTu { get; set; }
        public DateTime? HanCapDen { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string DienThoai { get; set; }
        public int? SoThichId { get; set; }
        public DateTime? NgayGiaNhap { get; set; }
        public DateTime? NgayNhap { get; set; }
        public string NguoiNhap { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public string NguoiCapNhat { get; set; }
        public DateTime? NgayDuyet { get; set; }
        public string NguoiDuyet { get; set; }
        public int? TinhTrangHoiVienId { get; set; }
        public string SoQuyetDinhKetNap { get; set; }
        public DateTime? NgayQuyetDinhKetNap { get; set; }
        public int? NamGuiEmailChucMung { get; set; }
        public int? LoaiHoiVienCaNhanChiTiet { get; set; }
    }
    public class HoiVienCaNhanCreateModel
    {
        public int HoiVienCaNhanId { get; set; }
        public string MaHoiVienCaNhan { get; set; }
        public char? LoaiHoiVienCaNhan { get; set; }
        public char? TroLyKtv { get; set; }
        public string HoDem { get; set; }
        public string Ten { get; set; }
        public char? GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public int? QuocTichId { get; set; }
        public string QueQuanTinhId { get; set; }
        public string QueQuanHuyenId { get; set; }
        public string QueQuanXaId { get; set; }
        public string DiaChi { get; set; }
        public string DiaChiTinhId { get; set; }
        public string DiaChiHuyenId { get; set; }
        public string DiaChiXaId { get; set; }
        public int? TruongDaiHocId { get; set; }
        public int? ChuyenNganhDaoTaoId { get; set; }
        public string ChuyenNganhNam { get; set; }
        public int? HocViId { get; set; }
        public string HocViNam { get; set; }
        public int? HocHamId { get; set; }
        public string HocHamNam { get; set; }
        public string SoCmnd { get; set; }
        public DateTime? CmndNgayCap { get; set; }
        public int? CmndTinhId { get; set; }
        public string SoTaiKhoanNganHang { get; set; }
        public int? NganHangId { get; set; }
        public string SoChungChiKtv { get; set; }
        public DateTime? NgayCapChungChiKtv { get; set; }
        public string SoGiayChungNhanDkhn { get; set; }
        public int? ChucVuId { get; set; }
        public int? HoiVienTapTheId { get; set; }
        public string DonViCongTac { get; set; }
        public DateTime? NgayCapGiayChungNhanDkhn { get; set; }
        public DateTime? HanCapTu { get; set; }
        public DateTime? HanCapDen { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string DienThoai { get; set; }
        public int? SoThichId { get; set; }
        public DateTime? NgayGiaNhap { get; set; }
        public DateTime? NgayNhap { get; set; }
        public string NguoiNhap { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public string NguoiCapNhat { get; set; }
        public DateTime? NgayDuyet { get; set; }
        public string NguoiDuyet { get; set; }
        public int? TinhTrangHoiVienId { get; set; }
        public string SoQuyetDinhKetNap { get; set; }
        public DateTime? NgayQuyetDinhKetNap { get; set; }
        public int? NamGuiEmailChucMung { get; set; }
        public int? LoaiHoiVienCaNhanChiTiet { get; set; }
    }
    public class HoiVienCaNhanModelFilter : PaginationRequest
    {
        public string MaHoiVienCaNhan { get; set; }
        public string HoVaTen { get; set; }
        public string DonViCongTac { get; set; }
        public char? LoaiHoiVienCaNhan { get; set; }
    }
    public class HoiVienCaNhanResposeModel
    {
        public string MaHoiVienCaNhan { get; set; }
        public string HoDem { get; set; }
        public string Ten { get; set; }
        public char? LoaiHoiVienCaNhan { get; set; }
        public string SoChungChiKtv { get; set; }
        public DateTime? NgayCapChungChiKtv { get; set; }
        public char? GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
    }
}
