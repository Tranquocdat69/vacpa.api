﻿using Catalog.API.Bussiness.ChucVu;
using Catalog.API.Bussiness.GiangVien;
using Catalog.API.Bussiness.TrinhDoChuyenMon;
using Catalog.API.Infrastructure.Datatables.CNKT;
using Catalog.API.Infrastructure.Datatables.HoiVienCaNhan;
using Catalog.API.Infrastructure.Datatables.QLHV;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System.Linq;
using System.Reactive;

namespace Catalog.API.Bussiness.HVCN
{
    public class HoiVienCaNhanHandler : IHoiVienCaNhanHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public HoiVienCaNhanHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<Response> CreateAsync(HoiVienCaNhanModel hoiVienCaNhanModel)
        {
            try
            {
                IRepository<TblHoiVienCaNhan> repository = _unitOfWork.GetRepository<TblHoiVienCaNhan>();

                /*TblDmGiangVien checktblDmGiangVien = await repository.Get(x => x.MaGiangVien == giangVienModel.MaGiangVien).FirstOrDefaultAsync();
                if (checktblDmGiangVien is not null)
                {
                    return new ResponseObject<GiangVienModel>(null, "Giang vien da ton tai", Code.ServerError);
                }*/

                TblHoiVienCaNhan tblHoiVienCaNhan = AutoMapperUtils.AutoMap<HoiVienCaNhanModel, TblHoiVienCaNhan>(hoiVienCaNhanModel);


                HoiVienCaNhanModel result = AutoMapperUtils.AutoMap<TblHoiVienCaNhan, HoiVienCaNhanModel>(tblHoiVienCaNhan);

                repository.Add(tblHoiVienCaNhan);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<HoiVienCaNhanModel>(result, "Them moi giang vien thanh cong", Code.Success);
                }
                else
                {
                    return new ResponseObject<HoiVienCaNhanModel>(null, "Co loi khi them moi giang vien", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HoiVienCaNhanModel>(null, ex.Message, Code.ServerError);
            }
        }

        public Task<Response> DeleteByMaGiangVienAsync(string maHoiVienCaNhan)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> FilterHoiVienAsync(HoiVienCaNhanModelFilter hoiVienCaNhanFilter)
        {
            try
            {
                var response = new Pagination<HoiVienCaNhanResposeModel>();
                IQueryable<TblHoiVienCaNhan> data = from dt in _unitOfWork.GetRepository<TblHoiVienCaNhan>().GetAll()
                                                  select dt;
                if (hoiVienCaNhanFilter.MaHoiVienCaNhan != null)
                {
                    var text = hoiVienCaNhanFilter.MaHoiVienCaNhan.ToLower();
                    data = data.Where(dt =>
                                    dt.MaHoiVienCaNhan.ToLower().Contains(text)
                                    );
                }
                if (!string.IsNullOrEmpty(hoiVienCaNhanFilter.HoVaTen))
                {
                    var text = hoiVienCaNhanFilter.HoVaTen.ToLower();
                    data = data.Where(dt =>
                                    dt.HoDem.ToLower().Contains(text) || dt.Ten.ToLower().Contains(text)
                                    );
                }               
                if (hoiVienCaNhanFilter.LoaiHoiVienCaNhan != null)
                {
                    data = data.Where(dt => dt.LoaiHoiVienCaNhan == hoiVienCaNhanFilter.LoaiHoiVienCaNhan);
                }
                if (hoiVienCaNhanFilter.DonViCongTac != null)
                {
                    data = data.Where(dt => dt.DonViCongTac == hoiVienCaNhanFilter.DonViCongTac);
                }
                var dataResult = await data.ToListAsync();
                response.Content = new List<HoiVienCaNhanResposeModel>();
              
                foreach (var item in dataResult)
                {
                    response.Content.Add(Convert(item));
                }
                var result = new ResponsePagination<HoiVienCaNhanResposeModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<HoiVienCaNhanResposeModel>>(null, ex.Message, Code.ServerError);
            }
        }
        private HoiVienCaNhanResposeModel Convert(TblHoiVienCaNhan tblHoiVienCaNhan)
        {
            var hoiVienCaNhanResposeModel = new HoiVienCaNhanResposeModel();
            hoiVienCaNhanResposeModel.MaHoiVienCaNhan = tblHoiVienCaNhan.MaHoiVienCaNhan;
            hoiVienCaNhanResposeModel.LoaiHoiVienCaNhan = tblHoiVienCaNhan.LoaiHoiVienCaNhan;
            hoiVienCaNhanResposeModel.HoDem=tblHoiVienCaNhan.HoDem;
            hoiVienCaNhanResposeModel.Ten=tblHoiVienCaNhan.Ten;
            hoiVienCaNhanResposeModel.GioiTinh = tblHoiVienCaNhan.GioiTinh;
            hoiVienCaNhanResposeModel.SoChungChiKtv=tblHoiVienCaNhan.SoChungChiKtv;
            hoiVienCaNhanResposeModel.NgayCapChungChiKtv = tblHoiVienCaNhan.NgayCapChungChiKtv;
            hoiVienCaNhanResposeModel.NgaySinh=tblHoiVienCaNhan.NgaySinh;
            return hoiVienCaNhanResposeModel;
        }      
        public async Task<Response> GetAllAsync()
        {
            List<TblHoiVienCaNhan> listDmHoiVienCaNhan = await _unitOfWork.GetRepository<TblHoiVienCaNhan>().GetAll().ToListAsync();
            List<HoiVienCaNhanModel> response = new List<HoiVienCaNhanModel>();
            if (listDmHoiVienCaNhan is not null && listDmHoiVienCaNhan.Count > 0)
            {
                foreach (TblHoiVienCaNhan item in listDmHoiVienCaNhan)
                {
                    response.Add(AutoMapperUtils.AutoMap<TblHoiVienCaNhan, HoiVienCaNhanModel>(item));
                }
            }
            return new ResponseList<HoiVienCaNhanModel>(response);
        }

        public async Task<Response> GetByMaHoiVienAsync(string maHoiVienCaNhan)
        {
            try
            {
                var response = new Pagination<HoiVienCaNhanResposeModel>();
                IQueryable<TblHoiVienCaNhan> data = from dt in _unitOfWork.GetRepository<TblHoiVienCaNhan>().GetAll()
                                                    select dt;
                if (maHoiVienCaNhan != null)
                {
                    var text = maHoiVienCaNhan.ToLower();
                    data = data.Where(dt =>dt.MaHoiVienCaNhan.ToLower().Contains(text));
                }           
                var dataResult = await data.ToListAsync();
                response.Content = new List<HoiVienCaNhanResposeModel>();

                foreach (var item in dataResult)
                {
                    response.Content.Add(Convert(item));
                }
                var result = new ResponsePagination<HoiVienCaNhanResposeModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<GiangVienModel>>(null, ex.Message, Code.ServerError);
            }           
        }

        public Task<Response> UpdateAsync(HoiVienCaNhanModel hoiVienCaNhanModel)
        {
            throw new NotImplementedException();
        }
    }
}
