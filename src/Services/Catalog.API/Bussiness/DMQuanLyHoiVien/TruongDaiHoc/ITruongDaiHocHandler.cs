﻿using Catalog.API.Bussiness.DMQuanLyHoiVien.HocVi;

namespace Catalog.API.Bussiness.DMQuanLyHoiVien.TruongDaiHoc
{
    public interface ITruongDaiHocHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> CreateAsync(TruongDaiHocModel model);
        Task<Response> UpdateAsync(TruongDaiHocModel model);
        Task<Response> DeleteByTruongDaiHocGuidAsync(string truongDaiHocGuid);
        Task<Response> FilterTruongDaiHocAsync(TruongDaiHocModelFilter model);
    }
}
