﻿namespace Catalog.API.Bussiness.DMQuanLyHoiVien.TruongDaiHoc
{
    public class TruongDaiHocModel
    {
        public Guid TruongDaiHocGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? TruongDaiHocId { get; set; }
        public string TenTruongDaiHoc { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class TruongDaiHocDeleteModel
    {
        public Guid TruongDaiHocGuid { get; set; }
        public string Name { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
    }
    public class TruongDaiHocModelFilter : PaginationRequest
    {
        public int? TrangThai { get; set; }
        public string TenTruongDaiHoc { get; set; }
    }
}
