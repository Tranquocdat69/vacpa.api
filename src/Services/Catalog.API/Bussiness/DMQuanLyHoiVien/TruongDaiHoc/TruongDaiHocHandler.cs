﻿
using Catalog.API.Bussiness.DMQuanLyHoiVien.TruongDaiHoc;
using Catalog.API.Bussiness.DMQuanLyHoiVien.TruongDaiHoc;
using Catalog.API.Bussiness.DMQuanLyHoiVien.TruongDaiHoc;
using Catalog.API.Infrastructure.Datatables.QLHV;
using System.Linq;

namespace Catalog.API.Bussiness.DMQuanLyHoiVien.TruongDaiHoc
{
    public class TruongDaiHocHandler : ITruongDaiHocHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public TruongDaiHocHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        private bool IsCreateTruongDaiHocModelNotValid(TruongDaiHocModel model)
        {
            if (model.TenTruongDaiHoc is null)
            {
                return true;
            }
            return false;
        }
        public async Task<Response> CreateAsync(TruongDaiHocModel model)
        {
            try
            {
                if (IsCreateTruongDaiHocModelNotValid(model))
                {
                    return new ResponseObject<TruongDaiHocModel>(null, "Dữ liệu cung cấp chưa đầy đủ", Code.ServerError);
                }

                IRepository<TblDmTruongDaiHoc> repository = _unitOfWork.GetRepository<TblDmTruongDaiHoc>();
                string strName = model.TenTruongDaiHoc.ToLower().Trim().Replace(" ", "");
                TblDmTruongDaiHoc check = await repository.Get(x => x.TenTruongDaiHoc.ToLower().Trim().Replace(" ", "").Equals(strName)).FirstOrDefaultAsync();

                if (check is not null)
                {
                    return new ResponseObject<TruongDaiHocModel>(null, $"Tên học vị: {model.TenTruongDaiHoc} đã tồn tại", Code.ServerError);
                }

                TblDmTruongDaiHoc tblTruongDaiHoc = AutoMapperUtils.AutoMap<TruongDaiHocModel, TblDmTruongDaiHoc>(model);
                tblTruongDaiHoc.TruongDaiHocGuid = Guid.NewGuid();
                tblTruongDaiHoc.NgaySuaCuoi = DateTime.Now;

                TruongDaiHocModel result = AutoMapperUtils.AutoMap<TblDmTruongDaiHoc, TruongDaiHocModel>(tblTruongDaiHoc);

                repository.Add(tblTruongDaiHoc);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<TruongDaiHocModel>(result, "Thêm mới thành công", Code.Success);
                }
                else
                {
                    return new ResponseObject<TruongDaiHocModel>(null, "Thêm mới thất bại", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<TruongDaiHocModel>(null, "Exception: Thêm mới học vị thất bại", Code.ServerError);
            }
        }

        public async Task<Response> DeleteByTruongDaiHocGuidAsync(string truongDaiHocGuid)
        {
            try
            {
                var datas = _unitOfWork.GetRepository<TblDmTruongDaiHoc>();
                var dNav = _unitOfWork.GetRepository<TblDmTruongDaiHoc>().Find(truongDaiHocGuid);
                if (dNav != null)
                {
                    datas.Delete(dNav);
                }
                if (await _unitOfWork.SaveAsync() >= 1)
                    return new ResponseObject<TruongDaiHocDeleteModel>(null, "Xóa thành công!", Code.Success);
                else
                    return new ResponseObject<TruongDaiHocDeleteModel>(null, "Xóa thất bại", Code.BadRequest);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<TruongDaiHocDeleteModel>(null, "Exception: Không xóa được ", Code.BadRequest);
            }
        }

        public async Task<Response> FilterTruongDaiHocAsync(TruongDaiHocModelFilter model)
        {
            try
            {
                var response = new Pagination<TruongDaiHocModel>();
                IQueryable<TblDmTruongDaiHoc> data = from dt in _unitOfWork.GetRepository<TblDmTruongDaiHoc>().GetAll()
                                              select dt;
                if (model.ApplicationId.HasValue)
                {
                    data = data.Where(ch => ch.ApplicationID == model.ApplicationId.Value);
                }
                if (!string.IsNullOrEmpty(model.FullTextSearch))
                {
                    var text = model.FullTextSearch.Trim().ToLower();
                    data = data.Where(ch => ch.TenTruongDaiHoc.ToLower().Contains(text));
                }
                if (model.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == model.TrangThai.Value);
                }
                if (!model.Size.HasValue)
                {
                    model.Size = 20;
                }
                model.Page = model.Page - 1;
                int excludedRows = ((int)model.Page) * ((int)model.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }

                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);

                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int)model.Size);

                var dataResult = await data.ToListAsync();

                response.Content = new List<TruongDaiHocModel>();
                foreach (var item in dataResult)
                {
                    response.Content.Add(AutoMapperUtils.AutoMap<TblDmTruongDaiHoc, TruongDaiHocModel>(item));
                }

                response.NumberOfElements = totalCount;
                response.Page = model.Page.HasValue ? model.Page.Value : 0;
                response.Size = model.Size.HasValue ? model.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                var result = new ResponsePagination<TruongDaiHocModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<TruongDaiHocModel>>(null, "Exception: không tìm được", Code.ServerError);
            }
        }

        public async Task<Response> GetAllAsync()
        {
            try
            {
                List<TblDmTruongDaiHoc> listDmTruongDaiHoc = await _unitOfWork.GetRepository<TblDmTruongDaiHoc>().GetAll().ToListAsync();
                List<TruongDaiHocModel> response = new List<TruongDaiHocModel>();
                if (listDmTruongDaiHoc is not null && listDmTruongDaiHoc.Count > 0)
                {
                    foreach (TblDmTruongDaiHoc item in listDmTruongDaiHoc)
                    {
                        response.Add(AutoMapperUtils.AutoMap<TblDmTruongDaiHoc, TruongDaiHocModel>(item));
                    }
                }
                return new ResponseList<TruongDaiHocModel>(response);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<TruongDaiHocModel>>(null, "Exception: không tìm được", Code.ServerError);
            }
        }

        public async Task<Response> UpdateAsync(TruongDaiHocModel model)
        {
            try
            {
                var uNav = _unitOfWork.GetRepository<TblDmTruongDaiHoc>().Find(model.TruongDaiHocGuid);
                if (uNav != null)
                {
                    var navsRepo = _unitOfWork.GetRepository<TblDmTruongDaiHoc>();
                    uNav.TenTruongDaiHoc = model.TenTruongDaiHoc;
                    uNav.TrangThai = model.TrangThai;
                    uNav.NgaySuaCuoi = DateTime.Now;
                    uNav.NgaySuaCuoi = model.NgaySuaCuoi;
                    navsRepo.Update(uNav);

                    if (await _unitOfWork.SaveAsync() >= 1)
                    {
                        return new ResponseObject<TruongDaiHocModel>(AutoMapperUtils.AutoMap<TblDmTruongDaiHoc, TruongDaiHocModel>(uNav), "Update successfully!", Code.Success);
                    }
                    else
                        return new ResponseError(Code.ServerError, "Update failed!");
                }
                return new Response(Code.NotFound, "No data found!");

            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<TruongDaiHocModel>(null, "Exception: Lỗi khi cập nhật!", Code.ServerError);
            }
        }
    }
}
