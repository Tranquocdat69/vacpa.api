﻿using Catalog.API.Bussiness.DMQuanLyHoiVien.HinhThucKyLuat;
using Catalog.API.Infrastructure.Datatables.QLHV;
using System.Linq;

namespace Catalog.API.Bussiness.DMQuanLyHoiVien.HinhThucKyLuat
{
    public class HinhThucKyLuatHandler : IHinhThucKyLuatHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public HinhThucKyLuatHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        private bool IsCreateHinhThucKyLuatModelNotValid(HinhThucKyLuatModel model)
        {
            if (model.TenHinhThucKyLuat is null)
            {
                return true;
            }
            return false;
        }
        public async Task<Response> CreateAsync(HinhThucKyLuatModel model)
        {
            try
            {
                if (IsCreateHinhThucKyLuatModelNotValid(model))
                {
                    return new ResponseObject<HinhThucKyLuatModel>(null, "Dữ liệu cung cấp chưa đầy đủ", Code.ServerError);
                }

                IRepository<TblDmHinhThucKyLuat> repository = _unitOfWork.GetRepository<TblDmHinhThucKyLuat>();
                string strName = model.TenHinhThucKyLuat.ToLower().Trim().Replace(" ", "");
                TblDmHinhThucKyLuat check = await repository.Get(x => x.TenHinhThucKyLuat.ToLower().Trim().Replace(" ", "").Equals(strName)).FirstOrDefaultAsync();

                if (check is not null)
                {
                    return new ResponseObject<HinhThucKyLuatModel>(null, $"Tên học vị: {model.TenHinhThucKyLuat} đã tồn tại", Code.ServerError);
                }

                TblDmHinhThucKyLuat tblHinhThucKyLuat = AutoMapperUtils.AutoMap<HinhThucKyLuatModel, TblDmHinhThucKyLuat>(model);
                tblHinhThucKyLuat.HinhThucKyLuatGuid = Guid.NewGuid();
                tblHinhThucKyLuat.NgaySuaCuoi = DateTime.Now;

                HinhThucKyLuatModel result = AutoMapperUtils.AutoMap<TblDmHinhThucKyLuat, HinhThucKyLuatModel>(tblHinhThucKyLuat);

                repository.Add(tblHinhThucKyLuat);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<HinhThucKyLuatModel>(result, "Thêm mới thành công", Code.Success);
                }
                else
                {
                    return new ResponseObject<HinhThucKyLuatModel>(null, "Thêm mới thất bại", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HinhThucKyLuatModel>(null, "Exception: Thêm mới học vị thất bại", Code.ServerError);
            }
        }

        public async Task<Response> DeleteByHinhThucKyLuatGuidAsync(string HinhThucKyLuatGuid)
        {
            try
            {
                var datas = _unitOfWork.GetRepository<TblDmHinhThucKyLuat>();
                var dNav = _unitOfWork.GetRepository<TblDmHinhThucKyLuat>().Find(HinhThucKyLuatGuid);
                if (dNav != null)
                {
                    datas.Delete(dNav);
                }
                if (await _unitOfWork.SaveAsync() >= 1)
                    return new ResponseObject<HinhThucKyLuatDeleteModel>(null, "Xóa thành công!", Code.Success);
                else
                    return new ResponseObject<HinhThucKyLuatDeleteModel>(null, "Không xóa được ", Code.BadRequest);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HinhThucKyLuatDeleteModel>(null, "Exception: Không xóa được ", Code.BadRequest);
            }
        }

        public async Task<Response> FilterHinhThucKyLuatAsync(HinhThucKyLuatModelFilter model)
        {
            try
            {
                var response = new Pagination<HinhThucKyLuatModel>();
                IQueryable<TblDmHinhThucKyLuat> data = from dt in _unitOfWork.GetRepository<TblDmHinhThucKyLuat>().GetAll()
                                                           select dt;
                if (model.ApplicationId.HasValue)
                {
                    data = data.Where(ch => ch.ApplicationID == model.ApplicationId.Value);
                }
                if (!string.IsNullOrEmpty(model.FullTextSearch))
                {
                    var text = model.FullTextSearch.Trim().ToLower();
                    data = data.Where(ch => ch.TenHinhThucKyLuat.ToLower().Contains(text));
                }
                if (model.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == model.TrangThai.Value);
                }
                if (!model.Size.HasValue)
                {
                    model.Size = 20;
                }
                model.Page = model.Page - 1;
                int excludedRows = ((int)model.Page) * ((int)model.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }

                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);

                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int)model.Size);

                var dataResult = await data.ToListAsync();

                response.Content = new List<HinhThucKyLuatModel>();
                foreach (var item in dataResult)
                {
                    response.Content.Add(AutoMapperUtils.AutoMap<TblDmHinhThucKyLuat, HinhThucKyLuatModel>(item));
                }

                response.NumberOfElements = totalCount;
                response.Page = model.Page.HasValue ? model.Page.Value : 0;
                response.Size = model.Size.HasValue ? model.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                var result = new ResponsePagination<HinhThucKyLuatModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<HinhThucKyLuatModel>>(null, "Exception: không tìm được", Code.ServerError);
            }
        }

        public async Task<Response> GetAllAsync()
        {
            try
            {
                List<TblDmHinhThucKyLuat> listDmHinhThucKyLuat = await _unitOfWork.GetRepository<TblDmHinhThucKyLuat>().GetAll().ToListAsync();
                List<HinhThucKyLuatModel> response = new List<HinhThucKyLuatModel>();
                if (listDmHinhThucKyLuat is not null && listDmHinhThucKyLuat.Count > 0)
                {
                    foreach (TblDmHinhThucKyLuat item in listDmHinhThucKyLuat)
                    {
                        response.Add(AutoMapperUtils.AutoMap<TblDmHinhThucKyLuat, HinhThucKyLuatModel>(item));
                    }
                }
                return new ResponseList<HinhThucKyLuatModel>(response);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<HinhThucKyLuatModel>>(null, "Exception: không tìm được", Code.ServerError);
            }
        }

        public async Task<Response> UpdateAsync(HinhThucKyLuatModel model)
        {
            try
            {
                var uNav = _unitOfWork.GetRepository<TblDmHinhThucKyLuat>().Find(model.HinhThucKyLuatGuid);
                if (uNav != null)
                {
                    var navsRepo = _unitOfWork.GetRepository<TblDmHinhThucKyLuat>();
                    uNav.TenHinhThucKyLuat = model.TenHinhThucKyLuat;
                    uNav.TrangThai = model.TrangThai;
                    uNav.NgaySuaCuoi = DateTime.Now;
                    uNav.NgaySuaCuoi = model.NgaySuaCuoi;
                    navsRepo.Update(uNav);

                    if (await _unitOfWork.SaveAsync() >= 1)
                    {
                        return new ResponseObject<HinhThucKyLuatModel>(AutoMapperUtils.AutoMap<TblDmHinhThucKyLuat, HinhThucKyLuatModel>(uNav), "Update successfully!", Code.Success);
                    }
                    else
                        return new ResponseError(Code.ServerError, "Update failed!");
                }
                return new Response(Code.NotFound, "No data found!");

            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HinhThucKyLuatModel>(null, "Lỗi khi cập nhật!", Code.ServerError);
            }
        }
    }
}
