﻿namespace Catalog.API.Bussiness.DMQuanLyHoiVien.HinhThucKyLuat
{
    public class HinhThucKyLuatModel
    {
        public Guid HinhThucKyLuatGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? HinhThucKyLuatId { get; set; }
        public string TenHinhThucKyLuat { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class HinhThucKyLuatDeleteModel
    {
        public Guid HinhThucKyLuatGuid { get; set; }
        public string Name { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
    }
    public class HinhThucKyLuatModelFilter : PaginationRequest
    {
        public int? TrangThai { get; set; }
        public string TenHinhThucKyLuat { get; set; }
    }
}
