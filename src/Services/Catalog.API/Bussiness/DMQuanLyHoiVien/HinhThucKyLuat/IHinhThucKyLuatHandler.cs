﻿
namespace Catalog.API.Bussiness.DMQuanLyHoiVien.HinhThucKyLuat
{
    public interface IHinhThucKyLuatHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> CreateAsync(HinhThucKyLuatModel model);
        Task<Response> UpdateAsync(HinhThucKyLuatModel model);
        Task<Response> DeleteByHinhThucKyLuatGuidAsync(string HinhThucKyLuatGuid);
        Task<Response> FilterHinhThucKyLuatAsync(HinhThucKyLuatModelFilter model);
    }
}
