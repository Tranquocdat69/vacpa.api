﻿

using Catalog.API.Bussiness.DMQuanLyHoiVien.ChuyenNganhDaoTaoTao;

namespace Catalog.API.Bussiness.DMQuanLyHoiVien.ChuyenNganhDaoTao
{
    public interface IChuyenNganhDaoTaoHandler
    {
        Task<Response> GetAll();
        Task<Response> CreateAsync(ChuyenNganhDaoTaoModel model);
        Task<Response> UpdateAsync(ChuyenNganhDaoTaoModel model);
        Task<Response> DeleteAsync(Guid chuyenNganhDaoTaoGuid);
        Task<Response> FindAsync(ChuyenNganhDaoTaoModelFilter model);
    }
}
