﻿namespace Catalog.API.Bussiness.DMQuanLyHoiVien.ChuyenNganhDaoTaoTao
{
    public class ChuyenNganhDaoTaoModel
    {
        public Guid ChuyenNganhDaoTaoGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? ChuyenNganhDaoTaoId { get; set; }
        public string TenChuyenNganhDaoTao { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class ChuyenNganhDaoTaoCreateModel
    {
        public Guid ChuyenNganhDaoTaoGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? ChuyenNganhDaoTaoId { get; set; }
        public string TenChuyenNganhDaoTao { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class ChuyenNganhDaoTaoUpdateModel
    {
        public Guid ChuyenNganhDaoTaoGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? ChuyenNganhDaoTaoId { get; set; }
        public string TenChuyenNganhDaoTao { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class ChuyenNganhDaoTaoDeleteModel
    {
        public Guid ChuyenNganhDaoTaoGuid { get; set; }
        public string Name { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
    }
    public class ChuyenNganhDaoTaoModelFilter : PaginationRequest
    {
        public int? TrangThai { get; set; }
        public string TenChuyenNganhDaoTao { get; set; }
    }
}
