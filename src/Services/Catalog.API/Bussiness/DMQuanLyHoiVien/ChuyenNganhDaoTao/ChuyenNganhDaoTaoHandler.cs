﻿
using Catalog.API.Bussiness.CauHoiBiMat;
using Catalog.API.Bussiness.DMQuanLyHoiVien.ChuyenNganhDaoTaoTao;
using Catalog.API.Infrastructure.Datatables.QLHV;
using System.Linq;

namespace Catalog.API.Bussiness.DMQuanLyHoiVien.ChuyenNganhDaoTao
{
    public class ChuyenNganhDaoTaoHandler : IChuyenNganhDaoTaoHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public ChuyenNganhDaoTaoHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        private bool IsCreateChuyenNganhDaoTaoModelNotValid(ChuyenNganhDaoTaoModel model)
        {
            if (model.TenChuyenNganhDaoTao is null)
            {
                return true;
            }
            return false;
        }
        public async Task<Response> CreateAsync(ChuyenNganhDaoTaoModel model)
        {
            try
            {
                if (IsCreateChuyenNganhDaoTaoModelNotValid(model))
                {
                    return new ResponseObject<ChuyenNganhDaoTaoModel>(null, "Dữ liệu cung cấp chưa đầy đủ", Code.ServerError);
                }

                IRepository<TblDmChuyenNganhDaoTao> repository = _unitOfWork.GetRepository<TblDmChuyenNganhDaoTao>();
                TblDmChuyenNganhDaoTao checkCauHoi = await repository.Get(x => x.TenChuyenNganhDaoTao.ToLower() == model.TenChuyenNganhDaoTao.ToLower()).FirstOrDefaultAsync();
                if (checkCauHoi is not null)
                {
                    return new ResponseObject<ChuyenNganhDaoTaoModel>(null, $"Tên chuyên ngành: {model.TenChuyenNganhDaoTao} đã tồn tại", Code.ServerError);
                }

                TblDmChuyenNganhDaoTao TblDmChuyenNganhDaoTao = AutoMapperUtils.AutoMap<ChuyenNganhDaoTaoModel, TblDmChuyenNganhDaoTao>(model);
                TblDmChuyenNganhDaoTao.ChuyenNganhDaoTaoGuid = Guid.NewGuid();
                TblDmChuyenNganhDaoTao.NgaySuaCuoi = DateTime.Now;

                ChuyenNganhDaoTaoModel result = AutoMapperUtils.AutoMap<TblDmChuyenNganhDaoTao, ChuyenNganhDaoTaoModel>(TblDmChuyenNganhDaoTao);


                repository.Add(TblDmChuyenNganhDaoTao);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<ChuyenNganhDaoTaoModel>(result, "Thêm mới chuyên ngành đào tạo thành công", Code.Success);
                }
                else
                {
                    return new ResponseObject<ChuyenNganhDaoTaoModel>(null, "Có lỗi khi thêm mới chuyên ngành đào tạo", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<ChuyenNganhDaoTaoModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> DeleteAsync(Guid chuyenNganhDaoTaoGuid)
        {
            try
            {
                var datas = _unitOfWork.GetRepository<TblDmChuyenNganhDaoTao>();
                var dNav = _unitOfWork.GetRepository<TblDmChuyenNganhDaoTao>().Find(chuyenNganhDaoTaoGuid);
                if (dNav != null)
                {
                    datas.Delete(dNav);
                }
                if (await _unitOfWork.SaveAsync() >= 1)
                    return new ResponseObject<ChuyenNganhDaoTaoDeleteModel>(null, "Xóa thành công!", Code.Success);
                else
                    return new ResponseObject<ChuyenNganhDaoTaoDeleteModel>(null, "Không xóa được ", Code.BadRequest);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<ChuyenNganhDaoTaoDeleteModel>(null, "Exception: Không xóa được ", Code.BadRequest);
            }
        }

        public async Task<Response> FindAsync(ChuyenNganhDaoTaoModelFilter model)
        {
            try
            {
                var response = new Pagination<ChuyenNganhDaoTaoModel>();
                IQueryable<TblDmChuyenNganhDaoTao> data = from dt in _unitOfWork.GetRepository<TblDmChuyenNganhDaoTao>().GetAll()
                                                    select dt;
                if (model.ApplicationId.HasValue)
                {
                    data = data.Where(ch => ch.ApplicationID == model.ApplicationId.Value);
                }
                if (!string.IsNullOrEmpty(model.FullTextSearch))
                {
                    var text = model.FullTextSearch.Trim().ToLower();
                    data = data.Where(ch => ch.TenChuyenNganhDaoTao.ToLower().Contains(text));
                }
                if (model.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == model.TrangThai.Value);
                }
                if (!model.Size.HasValue)
                {
                    model.Size = 20;
                }
                model.Page = model.Page - 1;
                int excludedRows = ((int)model.Page) * ((int)model.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }

                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);

                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int)model.Size);

                var dataResult = await data.ToListAsync();

                response.Content = new List<ChuyenNganhDaoTaoModel>();
                foreach (var item in dataResult)
                {
                    response.Content.Add(AutoMapperUtils.AutoMap<TblDmChuyenNganhDaoTao, ChuyenNganhDaoTaoModel>(item));
                }

                response.NumberOfElements = totalCount;
                response.Page = model.Page.HasValue ? model.Page.Value : 0;
                response.Size = model.Size.HasValue ? model.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                var result = new ResponsePagination<ChuyenNganhDaoTaoModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<ChuyenNganhDaoTaoModel>>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> GetAll()
        {
            try
            {
                List<TblDmChuyenNganhDaoTao> datas = await _unitOfWork.GetRepository<TblDmChuyenNganhDaoTao>().GetAll().ToListAsync();
                List<ChuyenNganhDaoTaoModel> list = new();
                if (datas != null)
                {
                    foreach (var item in datas)
                    {
                        ChuyenNganhDaoTaoModel ChuyenNganhDaoTaoModel = AutoMapperUtils.AutoMap<TblDmChuyenNganhDaoTao, ChuyenNganhDaoTaoModel>(item);
                        list.Add(ChuyenNganhDaoTaoModel);
                    }
                }
                return new ResponseList<ChuyenNganhDaoTaoModel>(list);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseList<ChuyenNganhDaoTaoModel>(null);
            }
        }

        public async Task<Response> UpdateAsync(ChuyenNganhDaoTaoModel model)
        {
            try
            {
                var uNav = _unitOfWork.GetRepository<TblDmChuyenNganhDaoTao>().Find(model.ChuyenNganhDaoTaoGuid);
                if (uNav != null)
                {
                    var navsRepo = _unitOfWork.GetRepository<TblDmChuyenNganhDaoTao>();
                    uNav.TenChuyenNganhDaoTao = model.TenChuyenNganhDaoTao;
                    uNav.TrangThai = model.TrangThai;
                    uNav.NgaySuaCuoi = DateTime.Now;
                    uNav.NgaySuaCuoi = model.NgaySuaCuoi;
                    navsRepo.Update(uNav);

                    if (await _unitOfWork.SaveAsync() >= 1)
                    {
                        return new ResponseObject<ChuyenNganhDaoTaoModel>(AutoMapperUtils.AutoMap<TblDmChuyenNganhDaoTao, ChuyenNganhDaoTaoModel>(uNav), "Update successfully!", Code.Success);
                    }
                    else
                        return new ResponseError(Code.ServerError, "Update failed!");
                }
                return new Response(Code.NotFound, "No data found!");

            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<ChuyenNganhDaoTaoModel>(null, "Lỗi khi cập nhật!", Code.ServerError);
            }
        }
    }
}
