﻿namespace Catalog.API.Bussiness.HocHam
{
    public class HocHamModel
    {
        public Guid HocHamGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? HocHamId { get; set; }
        public string TenHocHam { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class HocHamDeleteModel
    {
        public Guid HocHamGuid { get; set; }
        public string Name { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
    }
    public class HocHamModelFilter : PaginationRequest
    {
        public int? TrangThai { get; set; }
        public string TenHocHam { get; set; }
    }
}
