﻿using Catalog.API.Bussiness.HVCN;

namespace Catalog.API.Bussiness.HocHam
{
    public interface IHocHamHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> CreateAsync(HocHamModel model);
        Task<Response> UpdateAsync(HocHamModel model);
        Task<Response> DeleteByHocHamGuidAsync(string hocHamGuid);
        Task<Response> FilterHocHamAsync(HocHamModelFilter model);
    }
}
