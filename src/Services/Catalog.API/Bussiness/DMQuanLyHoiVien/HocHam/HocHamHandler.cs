﻿using Catalog.API.Bussiness.CauHoiBiMat;
using Catalog.API.Bussiness.HVCN;
using Catalog.API.Infrastructure.Datatables.HoiVienCaNhan;
using Catalog.API.Infrastructure.Datatables.QLHV;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Catalog.API.Bussiness.HocHam
{
    public class HocHamHandler : IHocHamHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public HocHamHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        private bool IsCreateCauHocHamModelNotValid(HocHamModel model)
        {
            if (model.TenHocHam is null)
            {
                return true;
            }
            return false;
        }
        public async Task<Response> CreateAsync(HocHamModel model)
        {
            try
            {
                if (IsCreateCauHocHamModelNotValid(model))
                {
                    return new ResponseObject<HocHamModel>(null, "Dữ liệu cung cấp chưa đầy đủ", Code.ServerError);
                }

                IRepository<TblDMHocHam> repository = _unitOfWork.GetRepository<TblDMHocHam>();
                string strName = model.TenHocHam.ToLower().Trim().Replace(" ", "");
                TblDMHocHam check = await repository.Get(x => x.TenHocHam.ToLower().Trim().Replace(" ","").Equals(strName)).FirstOrDefaultAsync();
                if (check is not null)
                {
                    return new ResponseObject<HocHamModel>(null, $"Tên học hàm: {model.TenHocHam} đã tồn tại", Code.ServerError);
                }

                TblDMHocHam tblHocHam = AutoMapperUtils.AutoMap<HocHamModel, TblDMHocHam>(model);
                tblHocHam.HocHamGuid = Guid.NewGuid();
                tblHocHam.NgaySuaCuoi = DateTime.Now;

                HocHamModel result = AutoMapperUtils.AutoMap<TblDMHocHam, HocHamModel>(tblHocHam);

                repository.Add(tblHocHam);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<HocHamModel>(result, "Them moi hoc ham thanh cong", Code.Success);
                }
                else
                {
                    return new ResponseObject<HocHamModel>(null, "Co loi khi them moi hoc ham", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HocHamModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> DeleteByHocHamGuidAsync(string hocHamGuid)
        {
            try
            {
                var datas = _unitOfWork.GetRepository<TblDMHocHam>();
                var dNav = _unitOfWork.GetRepository<TblDMHocHam>().Find(hocHamGuid);
                if (dNav != null)
                {
                    datas.Delete(dNav);
                }
                if (await _unitOfWork.SaveAsync() >= 1)
                    return new ResponseObject<HocHamDeleteModel>(null, "Xóa thành công!", Code.Success);
                else
                    return new ResponseObject<HocHamDeleteModel>(null, "Không xóa được ", Code.BadRequest);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HocHamDeleteModel>(null, "Exception: Không xóa được ", Code.BadRequest);
            }
        }

        public async Task<Response> FilterHocHamAsync(HocHamModelFilter model)
        {
            try
            {
                var response = new Pagination<HocHamModel>();
                IQueryable<TblDMHocHam> data = from dt in _unitOfWork.GetRepository<TblDMHocHam>().GetAll()
                                                    select dt;
                if (model.ApplicationId.HasValue)
                {
                    data = data.Where(ch => ch.ApplicationID == model.ApplicationId.Value);
                }
                if (!string.IsNullOrEmpty(model.FullTextSearch))
                {
                    var text = model.FullTextSearch.Trim().ToLower();
                    data = data.Where(ch => ch.TenHocHam.ToLower().Contains(text));
                }
                if (model.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == model.TrangThai.Value);
                }
                if (!model.Size.HasValue)
                {
                    model.Size = 20;
                }
                model.Page = model.Page - 1;
                int excludedRows = ((int)model.Page) * ((int)model.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }

                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);

                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int)model.Size);

                var dataResult = await data.ToListAsync();

                response.Content = new List<HocHamModel>();
                foreach (var item in dataResult)
                {
                    response.Content.Add(AutoMapperUtils.AutoMap<TblDMHocHam, HocHamModel>(item));
                }

                response.NumberOfElements = totalCount;
                response.Page = model.Page.HasValue ? model.Page.Value : 0;
                response.Size = model.Size.HasValue ? model.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                var result = new ResponsePagination<HocHamModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<HocHamModel>>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> GetAllAsync()
        {
            List<TblDMHocHam> listDmHocHam = await _unitOfWork.GetRepository<TblDMHocHam>().GetAll().ToListAsync();
            List<HocHamModel> response = new List<HocHamModel>();
            if (listDmHocHam is not null && listDmHocHam.Count > 0)
            {
                foreach (TblDMHocHam item in listDmHocHam)
                {
                    response.Add(AutoMapperUtils.AutoMap<TblDMHocHam, HocHamModel>(item));
                }
            }
            return new ResponseList<HocHamModel>(response);
        }

        public async Task<Response> UpdateAsync(HocHamModel model)
        {
            try
            {
                var uNav = _unitOfWork.GetRepository<TblDMHocHam>().Find(model.HocHamGuid);
                if (uNav != null)
                {
                    var navsRepo = _unitOfWork.GetRepository<TblDMHocHam>();
                    uNav.TenHocHam = model.TenHocHam;
                    uNav.TrangThai = model.TrangThai;
                    uNav.NgaySuaCuoi = DateTime.Now;
                    uNav.NgaySuaCuoi = model.NgaySuaCuoi;
                    navsRepo.Update(uNav);

                    if (await _unitOfWork.SaveAsync() >= 1)
                    {
                        return new ResponseObject<HocHamModel>(AutoMapperUtils.AutoMap<TblDMHocHam, HocHamModel>(uNav), "Update successfully!", Code.Success);
                    }
                    else
                        return new ResponseError(Code.ServerError, "Update failed!");
                }
                return new Response(Code.NotFound, "No data found!");

            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HocHamModel>(null, "Lỗi khi cập nhật!", Code.ServerError);
            }
        }
    }
}
