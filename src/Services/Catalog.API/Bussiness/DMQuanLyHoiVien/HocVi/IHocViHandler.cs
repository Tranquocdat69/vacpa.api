﻿
namespace Catalog.API.Bussiness.DMQuanLyHoiVien.HocVi
{
    public interface IHocViHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> CreateAsync(HocViModel model);
        Task<Response> UpdateAsync(HocViModel model);
        Task<Response> DeleteByHocViGuidAsync(string hocViGuid);
        Task<Response> FilterHocViAsync(HocViModelFilter model);
    }
}
