﻿
using Catalog.API.Infrastructure.Datatables.QLHV;
using System.Linq;

namespace Catalog.API.Bussiness.DMQuanLyHoiVien.HocVi
{
    public class HocViHandler : IHocViHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public HocViHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        private bool IsCreateHocViModelNotValid(HocViModel model)
        {
            if (model.TenHocVi is null)
            {
                return true;
            }
            return false;
        }
        public async Task<Response> CreateAsync(HocViModel model)
        {
            try
            {
                if (IsCreateHocViModelNotValid(model))
                {
                    return new ResponseObject<HocViModel>(null, "Dữ liệu cung cấp chưa đầy đủ", Code.ServerError);
                }

                IRepository<TblDmHocVi> repository = _unitOfWork.GetRepository<TblDmHocVi>();
                string strName = model.TenHocVi.ToLower().Trim().Replace(" ", "");
                TblDmHocVi check = await repository.Get(x => x.TenHocVi.ToLower().Trim().Replace(" ", "").Equals(strName)).FirstOrDefaultAsync();

                if (check is not null)
                {
                    return new ResponseObject<HocViModel>(null, $"Tên học vị: {model.TenHocVi} đã tồn tại", Code.ServerError);
                }

                TblDmHocVi tblHocVi = AutoMapperUtils.AutoMap<HocViModel, TblDmHocVi>(model);


                HocViModel result = AutoMapperUtils.AutoMap<TblDmHocVi, HocViModel>(tblHocVi);

                repository.Add(tblHocVi);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<HocViModel>(result, "Thêm mới học vị thành công", Code.Success);
                }
                else
                {
                    return new ResponseObject<HocViModel>(null, "Thêm mới học vị thất bại", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HocViModel>(null, "Lỗi ngoại lệ: Thêm mới học vị thất bại", Code.ServerError);
            }
        }

        public async Task<Response> DeleteByHocViGuidAsync(string HocViGuid)
        {
            try
            {
                var datas = _unitOfWork.GetRepository<TblDmHocVi>();
                var dNav = _unitOfWork.GetRepository<TblDmHocVi>().Find(HocViGuid);
                if (dNav != null)
                {
                    datas.Delete(dNav);
                }
                if (await _unitOfWork.SaveAsync() >= 1)
                    return new ResponseObject<HocViDeleteModel>(null, "Xóa thành công!", Code.Success);
                else
                    return new ResponseObject<HocViDeleteModel>(null, "Không xóa được ", Code.BadRequest);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HocViDeleteModel>(null, "Exception: Không xóa được ", Code.BadRequest);
            }
        }

        public async Task<Response> FilterHocViAsync(HocViModelFilter model)
        {
            try
            {
                var response = new Pagination<HocViModel>();
                IQueryable<TblDmHocVi> data = from dt in _unitOfWork.GetRepository<TblDmHocVi>().GetAll()
                                               select dt;
                if (model.ApplicationId.HasValue)
                {
                    data = data.Where(ch => ch.ApplicationID == model.ApplicationId.Value);
                }
                if (!string.IsNullOrEmpty(model.FullTextSearch))
                {
                    var text = model.FullTextSearch.Trim().ToLower();
                    data = data.Where(ch => ch.TenHocVi.ToLower().Contains(text));
                }
                if (model.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == model.TrangThai.Value);
                }
                if (!model.Size.HasValue)
                {
                    model.Size = 20;
                }
                model.Page = model.Page - 1;
                int excludedRows = ((int)model.Page) * ((int)model.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }

                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);

                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int)model.Size);

                var dataResult = await data.ToListAsync();

                response.Content = new List<HocViModel>();
                foreach (var item in dataResult)
                {
                    response.Content.Add(AutoMapperUtils.AutoMap<TblDmHocVi, HocViModel>(item));
                }

                response.NumberOfElements = totalCount;
                response.Page = model.Page.HasValue ? model.Page.Value : 0;
                response.Size = model.Size.HasValue ? model.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                var result = new ResponsePagination<HocViModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<HocViModel>>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> GetAllAsync()
        {
            List<TblDmHocVi> listDmHocVi = await _unitOfWork.GetRepository<TblDmHocVi>().GetAll().ToListAsync();
            List<HocViModel> response = new List<HocViModel>();
            if (listDmHocVi is not null && listDmHocVi.Count > 0)
            {
                foreach (TblDmHocVi item in listDmHocVi)
                {
                    response.Add(AutoMapperUtils.AutoMap<TblDmHocVi, HocViModel>(item));
                }
            }
            return new ResponseList<HocViModel>(response);
        }

        public async Task<Response> UpdateAsync(HocViModel model)
        {
            try
            {
                var uNav = _unitOfWork.GetRepository<TblDmHocVi>().Find(model.HocViGuid);
                if (uNav != null)
                {
                    var navsRepo = _unitOfWork.GetRepository<TblDmHocVi>();
                    uNav.TenHocVi = model.TenHocVi;
                    uNav.TrangThai = model.TrangThai;
                    uNav.NgaySuaCuoi = DateTime.Now;
                    uNav.NgaySuaCuoi = model.NgaySuaCuoi;
                    navsRepo.Update(uNav);

                    if (await _unitOfWork.SaveAsync() >= 1)
                    {
                        return new ResponseObject<HocViModel>(AutoMapperUtils.AutoMap<TblDmHocVi, HocViModel>(uNav), "Update successfully!", Code.Success);
                    }
                    else
                        return new ResponseError(Code.ServerError, "Update failed!");
                }
                return new Response(Code.NotFound, "No data found!");

            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HocViModel>(null, "Lỗi khi cập nhật!", Code.ServerError);
            }
        }

    }
}
