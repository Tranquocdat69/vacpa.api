﻿namespace Catalog.API.Bussiness.DMQuanLyHoiVien.HocVi
{
    public class HocViModel
    {
        public Guid HocViGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? HocViId { get; set; }
        public string TenHocVi { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class HocViDeleteModel
    {
        public Guid HocViGuid { get; set; }
        public string Name { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
    }
    public class HocViModelFilter : PaginationRequest
    {
        public int? TrangThai { get; set; }
        public string TenHocVi { get; set; }
    }
}
