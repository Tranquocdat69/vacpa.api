﻿using Catalog.API.Bussiness.ChucVu;
using Catalog.API.Bussiness.GiangVien;
using Catalog.API.Bussiness.HVCN;
using Catalog.API.Infrastructure.Datatables.CNKT;
using Catalog.API.Infrastructure.Datatables.HoiVienCaNhan;
using Catalog.API.Infrastructure.Datatables.QLHV;
using CommonUtils;
using DataUtils;
using Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Catalog.API.Bussiness.CauHoiBiMat
{
    public class CauHoiBiMatHandler : ICauHoiBiMatHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public CauHoiBiMatHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        private bool IsCreateCauHoiBiMatModelNotValid(CauHoiBiMatModel model)
        {
            if (model.CauHoi is null)
            {
                return true;
            }
            return false;
        }

        public async Task<Response> CreateAsync(CauHoiBiMatModel model)
        {
            try
            {
                if (IsCreateCauHoiBiMatModelNotValid(model))
                {
                    return new ResponseObject<CauHoiBiMatModel>(null, "Dữ liệu cung cấp chưa đầy đủ", Code.ServerError);
                }

                IRepository<TblDmCauHoiBiMat> repository = _unitOfWork.GetRepository<TblDmCauHoiBiMat>();
                TblDmCauHoiBiMat checkCauHoi = await repository.Get(x => x.CauHoi.ToLower() == model.CauHoi.ToLower()).FirstOrDefaultAsync();
                if (checkCauHoi is not null)
                {
                    return new ResponseObject<CauHoiBiMatModel>(null, $"Mã giảng viên: {model.CauHoi} đã tồn tại", Code.ServerError);
                }

                TblDmCauHoiBiMat tblDmCauHoiBiMat = AutoMapperUtils.AutoMap<CauHoiBiMatModel, TblDmCauHoiBiMat>(model);
                tblDmCauHoiBiMat.CauHoiBiMatGuid = Guid.NewGuid();
                tblDmCauHoiBiMat.NgaySuaCuoi = DateTime.Now;

                CauHoiBiMatModel result = AutoMapperUtils.AutoMap<TblDmCauHoiBiMat, CauHoiBiMatModel>(tblDmCauHoiBiMat);


                repository.Add(tblDmCauHoiBiMat);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<CauHoiBiMatModel>(result, "Them moi cau hoi bi mat thanh cong", Code.Success);
                }
                else
                {
                    return new ResponseObject<CauHoiBiMatModel>(null, "Co loi khi them moi cau hoi bi mat", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<CauHoiBiMatModel>(null, ex.Message, Code.ServerError);
            }
           
        }

        public async Task<Response> DeleteAsync(Guid mach)
        {
            try
            {
                var datas = _unitOfWork.GetRepository<TblDmCauHoiBiMat>();
                var dNav = _unitOfWork.GetRepository<TblDmCauHoiBiMat>().Find(mach);
                if (dNav != null)
                {
                    datas.Delete(dNav);
                }
                if (await _unitOfWork.SaveAsync() >= 1)
                    return new ResponseObject<CauHoiBiMatDeleteModel>(null, "Xóa thành công!", Code.Success);
                else
                    return new ResponseObject<CauHoiBiMatDeleteModel>(null, "Không xóa được ", Code.BadRequest);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<CauHoiBiMatDeleteModel>(null, "Exception: Không xóa được ", Code.BadRequest);
            }

        }

        public async Task<Response> FindAsync(CauHoiBiMatModelFilter model)
        {
            try
            {
                var response = new Pagination<CauHoiBiMatModel>();
                IQueryable<TblDmCauHoiBiMat> data = from dt in _unitOfWork.GetRepository<TblDmCauHoiBiMat>().GetAll()
                                                    select dt;
                if (model.ApplicationId.HasValue)
                {
                    data = data.Where(ch => ch.ApplicationID == model.ApplicationId.Value);
                }
                if (!string.IsNullOrEmpty(model.FullTextSearch))
                {
                    var text = model.FullTextSearch.Trim().ToLower();
                    data = data.Where(ch => ch.CauHoi.ToLower().Contains(text));
                }
                if (model.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == model.TrangThai.Value);
                }
                if (!model.Size.HasValue)
                {
                    model.Size = 20;
                }
                model.Page = model.Page - 1;
                int excludedRows = ((int)model.Page) * ((int)model.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }

                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);

                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int)model.Size);

                var dataResult = await data.ToListAsync();

                response.Content = new List<CauHoiBiMatModel>();
                foreach (var item in dataResult)
                {
                    response.Content.Add(AutoMapperUtils.AutoMap<TblDmCauHoiBiMat, CauHoiBiMatModel>(item));
                }

                response.NumberOfElements = totalCount;
                response.Page = model.Page.HasValue ? model.Page.Value : 0;
                response.Size = model.Size.HasValue ? model.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                var result = new ResponsePagination<CauHoiBiMatModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<CauHoiBiMatModel>>(null, ex.Message, Code.ServerError);
            }
        }

        

        public async Task<Response> GetAll()
        {
            try
            {
                List<TblDmCauHoiBiMat> datas = await _unitOfWork.GetRepository<TblDmCauHoiBiMat>().GetAll().ToListAsync();
                List<CauHoiBiMatModel> list = new();
                if (datas != null)
                {
                    foreach (var item in datas)
                    {
                        CauHoiBiMatModel cauHoiBiMatModel = AutoMapperUtils.AutoMap<TblDmCauHoiBiMat, CauHoiBiMatModel>(item);
                        list.Add(cauHoiBiMatModel);
                    }
                }
                return new ResponseList<CauHoiBiMatModel>(list);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex.Message + ex.InnerException?.Message);
                return new ResponseList<CauHoiBiMatModel>(null);
            }
        }

        public async Task<Response> UpdateAsync(CauHoiBiMatUpdateModel model)
        {            
            try
            {
                var uNav = _unitOfWork.GetRepository<TblDmCauHoiBiMat>().Find(model.CauHoiBiMatGuid);
                if (uNav != null)
                {
                    var navsRepo = _unitOfWork.GetRepository<TblDmCauHoiBiMat>();
                    uNav.CauHoi = model.CauHoi;
                    uNav.TrangThai = model.TrangThai;
                    uNav.NgaySuaCuoi = DateTime.Now;
                    uNav.NgaySuaCuoi=model.NgaySuaCuoi;
                    navsRepo.Update(uNav);

                    if (await _unitOfWork.SaveAsync() >= 1)
                    {
                        return new ResponseObject<CauHoiBiMatModel>(AutoMapperUtils.AutoMap<TblDmCauHoiBiMat, CauHoiBiMatModel>(uNav), "Update successfully!", Code.Success);
                    }
                    else
                        return new ResponseError(Code.ServerError, "Update failed!");
                }
                return new Response(Code.NotFound, "No data found!");

            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<CauHoiBiMatModel>(null, "Lỗi khi cập nhật!", Code.ServerError);
            }
        }     
    }
}
