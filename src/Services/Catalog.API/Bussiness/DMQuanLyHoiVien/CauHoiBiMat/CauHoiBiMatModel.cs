﻿using System;

namespace Catalog.API.Bussiness.CauHoiBiMat
{
    public class CauHoiBiMatModel
    {
        public Guid CauHoiBiMatGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? CauHoiBiMatId { get; set; }
        public string CauHoi { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class CauHoiBiMatCreateModel
    {
        public Guid CauHoiBiMatGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? CauHoiBiMatId { get; set; }
        public string CauHoi { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class CauHoiBiMatUpdateModel
    {
        public Guid CauHoiBiMatGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? CauHoiBiMatId { get; set; }
        public string CauHoi { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class CauHoiBiMatDeleteModel
    {
        public Guid CauHoiBiMatGuid { get; set; }
        public string Name { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
    }
    public class CauHoiBiMatModelFilter : PaginationRequest
    {
        public int? TrangThai { get; set; }
        public string CauHoi { get; set; }
    }
}
