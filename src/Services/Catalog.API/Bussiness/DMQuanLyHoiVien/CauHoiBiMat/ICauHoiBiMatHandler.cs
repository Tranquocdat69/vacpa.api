﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.API.Bussiness.CauHoiBiMat
{
    public interface ICauHoiBiMatHandler
    {
       Task<Response> GetAll();
       Task<Response> CreateAsync(CauHoiBiMatModel model);
       Task<Response> UpdateAsync(CauHoiBiMatUpdateModel model);
       Task<Response> DeleteAsync(Guid mach);
       Task<Response> FindAsync(CauHoiBiMatModelFilter model);
    }
}
