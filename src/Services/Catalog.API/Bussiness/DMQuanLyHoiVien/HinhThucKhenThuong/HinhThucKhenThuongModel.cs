﻿namespace Catalog.API.Bussiness.DMQuanLyHoiVien.HinhThucKhenThuong
{
    public class HinhThucKhenThuongModel
    {
        public Guid HinhThucKhenThuongGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? HinhThucKhenThuongId { get; set; }
        public string TenHinhThucKhenThuong { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class HinhThucKhenThuongDeleteModel
    {
        public Guid HinhThucKhenThuongGuid { get; set; }
        public string Name { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
    }
    public class HinhThucKhenThuongModelFilter : PaginationRequest
    {
        public int? TrangThai { get; set; }
        public string TenHinhThucKhenThuong { get; set; }
    }
}
