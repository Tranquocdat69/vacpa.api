﻿using Catalog.API.Bussiness.DMQuanLyHoiVien.HocVi;

namespace Catalog.API.Bussiness.DMQuanLyHoiVien.HinhThucKhenThuong
{
    public interface IHinhThucKhenThuongHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> CreateAsync(HinhThucKhenThuongModel model);
        Task<Response> UpdateAsync(HinhThucKhenThuongModel model);
        Task<Response> DeleteByHinhThucKhenThuongGuidAsync(string HinhThucKhenThuongGuid);
        Task<Response> FilterHinhThucKhenThuongAsync(HinhThucKhenThuongModelFilter model);
    }
}
