﻿
using Catalog.API.Infrastructure.Datatables.QLHV;
using System.Linq;

namespace Catalog.API.Bussiness.DMQuanLyHoiVien.HinhThucKhenThuong
{
    public class HinhThucKhenThuongHandler : IHinhThucKhenThuongHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public HinhThucKhenThuongHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        private bool IsCreateHinhThucKhenThuongModelNotValid(HinhThucKhenThuongModel model)
        {
            if (model.TenHinhThucKhenThuong is null)
            {
                return true;
            }
            return false;
        }
        public async Task<Response> CreateAsync(HinhThucKhenThuongModel model)
        {
            try
            {
                if (IsCreateHinhThucKhenThuongModelNotValid(model))
                {
                    return new ResponseObject<HinhThucKhenThuongModel>(null, "Dữ liệu cung cấp chưa đầy đủ", Code.ServerError);
                }

                IRepository<TblDmHinhThucKhenThuong> repository = _unitOfWork.GetRepository<TblDmHinhThucKhenThuong>();
                string strName = model.TenHinhThucKhenThuong.ToLower().Trim().Replace(" ", "");
                TblDmHinhThucKhenThuong check = await repository.Get(x => x.TenHinhThucKhenThuong.ToLower().Trim().Replace(" ", "").Equals(strName)).FirstOrDefaultAsync();

                if (check is not null)
                {
                    return new ResponseObject<HinhThucKhenThuongModel>(null, $"Tên học vị: {model.TenHinhThucKhenThuong} đã tồn tại", Code.ServerError);
                }

                TblDmHinhThucKhenThuong tblHinhThucKhenThuong = AutoMapperUtils.AutoMap<HinhThucKhenThuongModel, TblDmHinhThucKhenThuong>(model);
                tblHinhThucKhenThuong.HinhThucKhenThuongGuid = Guid.NewGuid();
                tblHinhThucKhenThuong.NgaySuaCuoi = DateTime.Now;

                HinhThucKhenThuongModel result = AutoMapperUtils.AutoMap<TblDmHinhThucKhenThuong, HinhThucKhenThuongModel>(tblHinhThucKhenThuong);

                repository.Add(tblHinhThucKhenThuong);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<HinhThucKhenThuongModel>(result, "Thêm mới thành công", Code.Success);
                }
                else
                {
                    return new ResponseObject<HinhThucKhenThuongModel>(null, "Thêm mới thất bại", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HinhThucKhenThuongModel>(null, "Exception: Thêm mới học vị thất bại", Code.ServerError);
            }
        }

        public async Task<Response> DeleteByHinhThucKhenThuongGuidAsync(string HinhThucKhenThuongGuid)
        {
            try
            {
                var datas = _unitOfWork.GetRepository<TblDmHinhThucKhenThuong>();
                var dNav = _unitOfWork.GetRepository<TblDmHinhThucKhenThuong>().Find(HinhThucKhenThuongGuid);
                if (dNav != null)
                {
                    datas.Delete(dNav);
                }
                if (await _unitOfWork.SaveAsync() >= 1)
                    return new ResponseObject<HinhThucKhenThuongDeleteModel>(null, "Xóa thành công!", Code.Success);
                else
                    return new ResponseObject<HinhThucKhenThuongDeleteModel>(null, "Không xóa được ", Code.BadRequest);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HinhThucKhenThuongDeleteModel>(null, "Exception: Không xóa được ", Code.BadRequest);
            }
        }

        public async Task<Response> FilterHinhThucKhenThuongAsync(HinhThucKhenThuongModelFilter model)
        {
            try
            {
                var response = new Pagination<HinhThucKhenThuongModel>();
                IQueryable<TblDmHinhThucKhenThuong> data = from dt in _unitOfWork.GetRepository<TblDmHinhThucKhenThuong>().GetAll()
                                              select dt;
                if (model.ApplicationId.HasValue)
                {
                    data = data.Where(ch => ch.ApplicationID == model.ApplicationId.Value);
                }
                if (!string.IsNullOrEmpty(model.FullTextSearch))
                {
                    var text = model.FullTextSearch.Trim().ToLower();
                    data = data.Where(ch => ch.TenHinhThucKhenThuong.ToLower().Contains(text));
                }
                if (model.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == model.TrangThai.Value);
                }
                if (!model.Size.HasValue)
                {
                    model.Size = 20;
                }
                model.Page = model.Page - 1;
                int excludedRows = ((int)model.Page) * ((int)model.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }

                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);

                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int)model.Size);

                var dataResult = await data.ToListAsync();

                response.Content = new List<HinhThucKhenThuongModel>();
                foreach (var item in dataResult)
                {
                    response.Content.Add(AutoMapperUtils.AutoMap<TblDmHinhThucKhenThuong, HinhThucKhenThuongModel>(item));
                }

                response.NumberOfElements = totalCount;
                response.Page = model.Page.HasValue ? model.Page.Value : 0;
                response.Size = model.Size.HasValue ? model.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                var result = new ResponsePagination<HinhThucKhenThuongModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<HinhThucKhenThuongModel>>(null, "Exception: không tìm được", Code.ServerError);
            }
        }

        public async Task<Response> GetAllAsync()
        {
            try
            {
                List<TblDmHinhThucKhenThuong> listDmHinhThucKhenThuong = await _unitOfWork.GetRepository<TblDmHinhThucKhenThuong>().GetAll().ToListAsync();
                List<HinhThucKhenThuongModel> response = new List<HinhThucKhenThuongModel>();
                if (listDmHinhThucKhenThuong is not null && listDmHinhThucKhenThuong.Count > 0)
                {
                    foreach (TblDmHinhThucKhenThuong item in listDmHinhThucKhenThuong)
                    {
                        response.Add(AutoMapperUtils.AutoMap<TblDmHinhThucKhenThuong, HinhThucKhenThuongModel>(item));
                    }
                }
                return new ResponseList<HinhThucKhenThuongModel>(response);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<HinhThucKhenThuongModel>>(null, "Exception: không tìm được", Code.ServerError);
            }
        }

        public async Task<Response> UpdateAsync(HinhThucKhenThuongModel model)
        {
            try
            {
                var uNav = _unitOfWork.GetRepository<TblDmHinhThucKhenThuong>().Find(model.HinhThucKhenThuongGuid);
                if (uNav != null)
                {
                    var navsRepo = _unitOfWork.GetRepository<TblDmHinhThucKhenThuong>();
                    uNav.TenHinhThucKhenThuong = model.TenHinhThucKhenThuong;
                    uNav.TrangThai = model.TrangThai;
                    uNav.NgaySuaCuoi = DateTime.Now;
                    uNav.NgaySuaCuoi = model.NgaySuaCuoi;
                    navsRepo.Update(uNav);

                    if (await _unitOfWork.SaveAsync() >= 1)
                    {
                        return new ResponseObject<HinhThucKhenThuongModel>(AutoMapperUtils.AutoMap<TblDmHinhThucKhenThuong, HinhThucKhenThuongModel>(uNav), "Update successfully!", Code.Success);
                    }
                    else
                        return new ResponseError(Code.ServerError, "Update failed!");
                }
                return new Response(Code.NotFound, "No data found!");

            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<HinhThucKhenThuongModel>(null, "Lỗi khi cập nhật!", Code.ServerError);
            }
        }
    }
}
