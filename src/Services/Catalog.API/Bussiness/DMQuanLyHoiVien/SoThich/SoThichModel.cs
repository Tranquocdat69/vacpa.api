﻿namespace Catalog.API.Bussiness.DMQuanLyHoiVien.SoThich
{
    public class SoThichModel
    {
        public Guid SoThichGuid { get; set; }
        public Guid? ApplicationID { get; set; }
        public int? SoThichId { get; set; }
        public string TenSoThich { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
    }
    public class SoThichDeleteModel
    {
        public Guid SoThichGuid { get; set; }
        public string Name { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
    }
    public class SoThichModelFilter : PaginationRequest
    {
        public int? TrangThai { get; set; }
        public string TenSoThich { get; set; }
    }
}
