﻿
namespace Catalog.API.Bussiness.DMQuanLyHoiVien.SoThich
{
    public interface ISoThichHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> CreateAsync(SoThichModel model);
        Task<Response> UpdateAsync(SoThichModel model);
        Task<Response> DeleteBySoThichGuidAsync(string SoThichGuid);
        Task<Response> FilterSoThichAsync(SoThichModelFilter model);
    }
}
