﻿using Catalog.API.Infrastructure.Datatables.QLHV;
using System.Linq;

namespace Catalog.API.Bussiness.DMQuanLyHoiVien.SoThich
{
    public class SoThichHandler : ISoThichHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public SoThichHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        private bool IsCreateSoThichModelNotValid(SoThichModel model)
        {
            if (model.TenSoThich is null)
            {
                return true;
            }
            return false;
        }
        public async Task<Response> CreateAsync(SoThichModel model)
        {
            try
            {
                if (IsCreateSoThichModelNotValid(model))
                {
                    return new ResponseObject<SoThichModel>(null, "Dữ liệu cung cấp chưa đầy đủ", Code.ServerError);
                }

                IRepository<TblDmSoThich> repository = _unitOfWork.GetRepository<TblDmSoThich>();
                string strName = model.TenSoThich.ToLower().Trim().Replace(" ", "");
                TblDmSoThich check = await repository.Get(x => x.TenSoThich.ToLower().Trim().Replace(" ", "").Equals(strName)).FirstOrDefaultAsync();

                if (check is not null)
                {
                    return new ResponseObject<SoThichModel>(null, $"Tên học vị: {model.TenSoThich} đã tồn tại", Code.ServerError);
                }

                TblDmSoThich tblSoThich = AutoMapperUtils.AutoMap<SoThichModel, TblDmSoThich>(model);
                tblSoThich.SoThichGuid = Guid.NewGuid();
                tblSoThich.NgaySuaCuoi = DateTime.Now;

                SoThichModel result = AutoMapperUtils.AutoMap<TblDmSoThich, SoThichModel>(tblSoThich);

                repository.Add(tblSoThich);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<SoThichModel>(result, "Thêm mới thành công", Code.Success);
                }
                else
                {
                    return new ResponseObject<SoThichModel>(null, "Thêm mới thất bại", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<SoThichModel>(null, "Exception: Thêm mới học vị thất bại", Code.ServerError);
            }
        }

        public async Task<Response> DeleteBySoThichGuidAsync(string SoThichGuid)
        {
            try
            {
                var datas = _unitOfWork.GetRepository<TblDmSoThich>();
                var dNav = _unitOfWork.GetRepository<TblDmSoThich>().Find(SoThichGuid);
                if (dNav != null)
                {
                    datas.Delete(dNav);
                }
                if (await _unitOfWork.SaveAsync() >= 1)
                    return new ResponseObject<SoThichDeleteModel>(null, "Xóa thành công!", Code.Success);
                else
                    return new ResponseObject<SoThichDeleteModel>(null, "Không xóa được ", Code.BadRequest);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<SoThichDeleteModel>(null, "Exception: Không xóa được ", Code.BadRequest);
            }
        }

        public async Task<Response> FilterSoThichAsync(SoThichModelFilter model)
        {
            try
            {
                var response = new Pagination<SoThichModel>();
                IQueryable<TblDmSoThich> data = from dt in _unitOfWork.GetRepository<TblDmSoThich>().GetAll()
                                                       select dt;
                if (model.ApplicationId.HasValue)
                {
                    data = data.Where(ch => ch.ApplicationID == model.ApplicationId.Value);
                }
                if (!string.IsNullOrEmpty(model.FullTextSearch))
                {
                    var text = model.FullTextSearch.Trim().ToLower();
                    data = data.Where(ch => ch.TenSoThich.ToLower().Contains(text));
                }
                if (model.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == model.TrangThai.Value);
                }
                if (!model.Size.HasValue)
                {
                    model.Size = 20;
                }
                model.Page = model.Page - 1;
                int excludedRows = ((int)model.Page) * ((int)model.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }

                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);

                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int)model.Size);

                var dataResult = await data.ToListAsync();

                response.Content = new List<SoThichModel>();
                foreach (var item in dataResult)
                {
                    response.Content.Add(AutoMapperUtils.AutoMap<TblDmSoThich, SoThichModel>(item));
                }

                response.NumberOfElements = totalCount;
                response.Page = model.Page.HasValue ? model.Page.Value : 0;
                response.Size = model.Size.HasValue ? model.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                var result = new ResponsePagination<SoThichModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<SoThichModel>>(null, "Exception: không tìm được", Code.ServerError);
            }
        }

        public async Task<Response> GetAllAsync()
        {
            try
            {
                List<TblDmSoThich> listDmSoThich = await _unitOfWork.GetRepository<TblDmSoThich>().GetAll().ToListAsync();
                List<SoThichModel> response = new List<SoThichModel>();
                if (listDmSoThich is not null && listDmSoThich.Count > 0)
                {
                    foreach (TblDmSoThich item in listDmSoThich)
                    {
                        response.Add(AutoMapperUtils.AutoMap<TblDmSoThich, SoThichModel>(item));
                    }
                }
                return new ResponseList<SoThichModel>(response);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<SoThichModel>>(null, "Exception: không tìm được", Code.ServerError);
            }
        }

        public async Task<Response> UpdateAsync(SoThichModel model)
        {
            try
            {
                var uNav = _unitOfWork.GetRepository<TblDmSoThich>().Find(model.SoThichGuid);
                if (uNav != null)
                {
                    var navsRepo = _unitOfWork.GetRepository<TblDmSoThich>();
                    uNav.TenSoThich = model.TenSoThich;
                    uNav.TrangThai = model.TrangThai;
                    uNav.NgaySuaCuoi = DateTime.Now;
                    uNav.NgaySuaCuoi = model.NgaySuaCuoi;
                    navsRepo.Update(uNav);

                    if (await _unitOfWork.SaveAsync() >= 1)
                    {
                        return new ResponseObject<SoThichModel>(AutoMapperUtils.AutoMap<TblDmSoThich, SoThichModel>(uNav), "Update successfully!", Code.Success);
                    }
                    else
                        return new ResponseError(Code.ServerError, "Update failed!");
                }
                return new Response(Code.NotFound, "No data found!");

            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<SoThichModel>(null, "Lỗi khi cập nhật!", Code.ServerError);
            }
        }
    }
}
