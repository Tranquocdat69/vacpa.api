﻿namespace Catalog.API.Bussiness.GiangVien
{
    public interface IGiangVienHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> GetByMaGiangVienAsync(string maGiangVien);
        Task<Response> FilterAsync(FilterGiangVienModel model);
        Task<Response> CreateAsync(UpsertGiangVienModel model);
        Task<Response> UpdateAsync(UpsertGiangVienModel model);
        Task<Response> DeleteAsync(string maGiangVien);
    }
}
