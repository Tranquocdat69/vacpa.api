﻿using Catalog.API.Bussiness.ChucVu;
using Catalog.API.Bussiness.TrinhDoChuyenMon;
using Catalog.API.Infrastructure.Datatables.CNKT;
using Catalog.API.Infrastructure.Datatables.QLHV;
using System.Linq;

namespace Catalog.API.Bussiness.GiangVien
{
    public class GiangVienHandler : IGiangVienHandler
    {
        private readonly IUnitOfWork _unitOfWork;

        public GiangVienHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response> GetAllAsync()
        {
            List<TblDmGiangVien> listDmGiangVien = await _unitOfWork.GetRepository<TblDmGiangVien>().GetAll().ToListAsync();
            List<GiangVienModel> response = new List<GiangVienModel>();
            if (listDmGiangVien is not null && listDmGiangVien.Count > 0)
            {
                foreach (TblDmGiangVien item in listDmGiangVien)
                {
                    response.Add(AutoMapperUtils.AutoMap<TblDmGiangVien, GiangVienModel>(item));
                }
            }
            return new ResponseList<GiangVienModel>(response);
        }

        public async Task<Response> GetByMaGiangVienAsync(string maGiangVien)
        {
            TblDmGiangVien dmGiangVien = await _unitOfWork.GetRepository<TblDmGiangVien>().Get(x => x.MaGiangVien == maGiangVien).FirstOrDefaultAsync();
            GiangVienModel giangVienModel = AutoMapperUtils.AutoMap<TblDmGiangVien, GiangVienModel>(dmGiangVien);
            return new ResponseObject<GiangVienModel>(giangVienModel);
        }

        public async Task<Response> FilterAsync(FilterGiangVienModel model)
        {
            try
            {
                var response = new Pagination<GiangVienModel>();
                IQueryable<TblDmGiangVien> data = from dt in _unitOfWork
                                                  .GetRepository<TblDmGiangVien>()
                                                  .Get(
                                                  predicate: null,
                                                  include:
                                                  o => o.Include(gv => gv.ChucVuNavigation).Include(gv => gv.TrinhDoChuyenMonNavigation)
                                                  )
                                                  select dt;

                if (model.ApplicationId.HasValue)
                {
                    data = data.Where(gv => gv.ApplicationId == model.ApplicationId.Value);
                }
                if (!string.IsNullOrEmpty(model.FullTextSearch)) {
                    var text = model.FullTextSearch.Trim().ToLower();
                    data = data.Where(gv => gv.TenGiangVien.ToLower().Contains(text) || gv.DonViCongTac.ToLower().Contains(text));
                }
                if (model.TrangThai.HasValue)
                {
                    data = data.Where(dt => dt.TrangThai == model.TrangThai.Value);
                } 
                if (model.MaChucVu.HasValue)
                {
                    data = data.Where(dt => dt.MaChucVu == model.MaChucVu.Value);
                } 
                if (model.MaTrinhDoChuyenMon.HasValue)
                {
                    data = data.Where(dt => dt.MaTrinhDoChuyenMon == model.MaTrinhDoChuyenMon.Value);
                }
                if (!model.Size.HasValue)
                {
                    model.Size = 20;
                }
                model.Page = model.Page - 1;
                int excludedRows = ((int)model.Page) * ((int)model.Size);
                if (excludedRows <= 0)
                {
                    excludedRows = 0;
                }

                data = data.OrderByDescending(dt => dt.NgaySuaCuoi);

                var totalCount = data.Count();

                data = data.Skip(excludedRows).Take((int) model.Size);

                var dataResult = await data.ToListAsync();

                response.Content = new List<GiangVienModel>();
                foreach (var item in dataResult)
                {
                    response.Content.Add(Convert(item));
                }

                response.NumberOfElements = totalCount;
                response.Page = model.Page.HasValue ? model.Page.Value : 0;
                response.Size = model.Size.HasValue ? model.Size.Value : 0;
                response.TotalElements = totalCount;
                response.TotalPages = totalCount / (model.Size.HasValue ? (model.Size.Value + 1) : 1);

                var result = new ResponsePagination<GiangVienModel>(response);
                return result;
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<List<GiangVienModel>>(null, ex.Message, Code.ServerError);
            }
        }

        private GiangVienModel Convert(TblDmGiangVien tblDmGiangVien)
        {
            GiangVienModel giangVienModel = AutoMapperUtils.AutoMap<TblDmGiangVien, GiangVienModel>(tblDmGiangVien);
            ChucVuModel chucVuModel = AutoMapperUtils.AutoMap<TblDmChucVu, ChucVuModel>(tblDmGiangVien.ChucVuNavigation);
            TrinhDoChuyenMonModel trinhDoChuyenMonModel = AutoMapperUtils.AutoMap<TblDmTrinhDoChuyenMon, TrinhDoChuyenMonModel>(tblDmGiangVien.TrinhDoChuyenMonNavigation);
            giangVienModel.ChucVuModel = chucVuModel;
            giangVienModel.TrinhDoChuyenMonModel = trinhDoChuyenMonModel;
            return giangVienModel;
        }

        public async Task<Response> CreateAsync(UpsertGiangVienModel model)
        {
            try
            {
                if (IsUpsertGiangVienModelNotValid(model))
                {
                    return new ResponseObject<UpsertGiangVienModel>(null, "Dữ liệu cung cấp chưa đầy đủ", Code.ServerError);
                }

                IRepository<TblDmGiangVien> repository = _unitOfWork.GetRepository<TblDmGiangVien>();
                TblDmGiangVien checktblDmGiangVien = await repository.Get(x => x.MaGiangVien == model.MaGiangVien).FirstOrDefaultAsync();
                if (checktblDmGiangVien is not null)
                {
                    return new ResponseObject<UpsertGiangVienModel>(null, $"Mã giảng viên: {model.MaGiangVien} đã tồn tại", Code.ServerError);
                }

                TblDmGiangVien createGiangVien = AutoMapperUtils.AutoMap<UpsertGiangVienModel, TblDmGiangVien>(model);
                createGiangVien.GiangVienGuid = Guid.NewGuid();
                createGiangVien.NgaySuaCuoi = DateTime.Now;
                UpsertGiangVienModel result = AutoMapperUtils.AutoMap<TblDmGiangVien, UpsertGiangVienModel>(createGiangVien);

                repository.Add(createGiangVien);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<UpsertGiangVienModel>(result, "Thêm mới giảng viên thành công", Code.Success);
                }
                else
                {
                    return new ResponseObject<UpsertGiangVienModel>(null, "Có lỗi khi thêm mới giảng viên", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<UpsertGiangVienModel>(null, ex.Message, Code.ServerError);
            }
        }

        private bool IsUpsertGiangVienModelNotValid(UpsertGiangVienModel model)
        {
            if (model.MaGiangVien is null || model.TenGiangVien is null || model.MaChucVu is null || model.MaTrinhDoChuyenMon is null)
            {
                return true;
            }
            return false;
        }

        public async Task<Response> UpdateAsync(UpsertGiangVienModel model)
        {
            try
            {
                if (IsUpsertGiangVienModelNotValid(model))
                {
                    return new ResponseObject<UpsertGiangVienModel>(null, "Dữ liệu cung cấp chưa đầy đủ", Code.ServerError);
                }
                IRepository<TblDmGiangVien> repository = _unitOfWork.GetRepository<TblDmGiangVien>();
                TblDmGiangVien updateGiangVien = repository.Get(x => x.GiangVienGuid == model.GiangVienGuid).FirstOrDefault();
                if (updateGiangVien is null)
                {
                    return new ResponseObject<UpsertGiangVienModel>(null, $"Mã giảng viên: {model.MaGiangVien}  không tồn tại", Code.ServerError);
                }
                TblDmGiangVien checktblDmGiangVien = await repository.Get(x => x.GiangVienGuid != model.GiangVienGuid.Value && x.MaGiangVien == model.MaGiangVien).AsNoTracking().FirstOrDefaultAsync();
                if (checktblDmGiangVien is not null)
                {
                    return new ResponseObject<UpsertGiangVienModel>(null, $"Mã giảng viên: {model.MaGiangVien} đã tồn tại", Code.ServerError);
                }
                updateGiangVien = AutoMapperUtils.AutoMap<UpsertGiangVienModel, TblDmGiangVien>(model, updateGiangVien);
                updateGiangVien.NgaySuaCuoi = DateTime.Now;
                repository.Update(updateGiangVien);

                UpsertGiangVienModel result = AutoMapperUtils.AutoMap<TblDmGiangVien, UpsertGiangVienModel>(updateGiangVien);

                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<UpsertGiangVienModel>(result, "Cập nhật giảng viên thành công", Code.Success);
                }
                else
                {
                    return new ResponseObject<UpsertGiangVienModel>(null, "Có lỗi khi thêm mới giảng viên", Code.ServerError);
                }

            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<UpsertGiangVienModel>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> DeleteAsync(string maGiangVien)
        {
            try
            {
                IRepository<TblDmGiangVien> repository = _unitOfWork.GetRepository<TblDmGiangVien>();
                TblDmGiangVien deleteGiangVien = repository.Get(x => x.MaGiangVien == maGiangVien).FirstOrDefault();
                if (deleteGiangVien is null)
                {
                    return new ResponseObject<GiangVienModel>(null, $"Mã giảng viên: {maGiangVien}  không tồn tại", Code.ServerError);
                }
                repository.Delete(deleteGiangVien);
                GiangVienModel result = AutoMapperUtils.AutoMap<TblDmGiangVien, GiangVienModel>(deleteGiangVien);
                if (await _unitOfWork.SaveAsync() > 0)
                {
                    return new ResponseObject<GiangVienModel>(result, "Xóa giảng viên thành công", Code.Success);
                }
                else
                {
                    return new ResponseObject<GiangVienModel>(null, "Có lỗi khi xóa giảng viên", Code.ServerError);
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseObject<GiangVienModel>(null, ex.Message, Code.ServerError);
            }
        }
    }
}
