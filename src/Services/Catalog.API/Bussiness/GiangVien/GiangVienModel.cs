﻿using Catalog.API.Bussiness.ChucVu;
using Catalog.API.Bussiness.TrinhDoChuyenMon;

namespace Catalog.API.Bussiness.GiangVien
{
    public class GiangVienModel
    {
        public Guid? GiangVienGuid { get; set; }
        public string MaGiangVien { get; set; }
        public Guid? ApplicationId { get; set; }
        public string TenGiangVien { get; set; }
        public Guid? MaChucVu { get; set; }
        public Guid? MaTrinhDoChuyenMon { get; set; }
        public string DonViCongTac { get; set; }
        public string ChungChi { get; set; }
        public string GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
        public DateTime? NgaySuaCuoi { get; set; }
        public ChucVuModel ChucVuModel { get; set; }
        public TrinhDoChuyenMonModel TrinhDoChuyenMonModel { get; set; }
    }

    public class UpsertGiangVienModel
    {
        public Guid? GiangVienGuid { get; set; }
        public string MaGiangVien { get; set; }
        public Guid? ApplicationId { get; set; }
        public string TenGiangVien { get; set; }
        public Guid? MaChucVu { get; set; }
        public Guid? MaTrinhDoChuyenMon { get; set; }
        public string DonViCongTac { get; set; }
        public string ChungChi { get; set; }
        public string GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public byte? TrangThai { get; set; }
        public Guid? NguoiSuaCuoi { get; set; }
    }

    public class FilterGiangVienModel : PaginationRequest
    {
        public int? TrangThai { get; set; }
        public Guid? MaChucVu { get; set; }
        public Guid? MaTrinhDoChuyenMon { get; set; }
    }
}
