﻿using ApiUtils;
using Microsoft.Extensions.Logging;
using Serilog.Context;
using SV.Core.FW.MSA.BuildingBlocks.EventBus.Abstractions;
using System;
using System.Threading.Tasks;
using Dgld.API;

namespace Business
{
    public class OrderStartedIntegrationEventHandler : IIntegrationEventHandler<OrderStartedIntegrationEvent>
    {
        private readonly IApplicationHandler _repository;
        private readonly ILogger<OrderStartedIntegrationEventHandler> _logger;

        public OrderStartedIntegrationEventHandler(
            IApplicationHandler repository,
            ILogger<OrderStartedIntegrationEventHandler> logger)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Handle(OrderStartedIntegrationEvent @event)
        {
            

            using (LogContext.PushProperty("IntegrationEventContext", $"{@event.Id}-{Program.AppName}"))
            {
                Console.WriteLine("----- Handling integration event: " + Program.AppName);

                _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, Program.AppName, @event);

                await _repository.GetAll();
            }
        }
    }
}



