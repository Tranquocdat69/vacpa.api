﻿using ApiUtils;
using CommonUtils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ActionFilter : IActionFilter
    {
        private readonly string[] _ids;
        public ActionFilter(params string[] ids)
        {
            _ids = ids;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
        // our code before action executes
            var param = context.ActionArguments.SingleOrDefault();

            var a = _ids;

            if (param.Value == null)
            {
                context.Result = new BadRequestObjectResult("Object is null");
                return;
            }

            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(context.ModelState);
            }
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            // our code after action executes
        }
    }

    public class AsyncActionFilterExample : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            // execute any code before the action executes
            var result = await next();
            // execute any code after the action executes
        }
    }

    public class AuthorizationRole : TypeFilterAttribute
    {
        public AuthorizationRole(params string[] ids) : base(typeof(RoleAttributeImpl))
        {
            Arguments = new object[] { ids };
        }

        private class RoleAttributeImpl : IActionFilter
        {
            private readonly string[] _ids;
            //private readonly ILogger _logger;

            public RoleAttributeImpl(string[] ids)
            {
                _ids = ids;
                //_logger = loggerFactory.CreateLogger<MyAttribute>();
            }

            public void OnActionExecuting(ActionExecutingContext context)
            {
                var request = context.HttpContext.Request;
                string authHeader = (request.Headers["Authorization"]);
                var userId = Guid.Empty;

                var requestField = CommonUtils.ApiUtils.GetRequestFields(request);

                authHeader = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                var permission = "";
                if (!string.IsNullOrEmpty(authHeader))
                {
                    permission = authHeader;
                }
                // GetByMaGiangVienAsync the token
                var token = permission.Trim();
                // validatetoken
                var handerJwt = new JwtSecurityTokenHandler();
                var tokenInfo = handerJwt.ReadJwtToken(token);

                userId = new Guid(tokenInfo.Claims.Where(x => x.Type == "USER_ID").FirstOrDefault().Value);
                var applicationId = tokenInfo.Claims.Where(x => x.Type == "APPLICATION_ID").FirstOrDefault().Value;

                if (userId == Guid.Empty)
                {
                    userId = requestField.UserId;
                }

                var issuer = Utils.GetConfig("Authenticate:Jwt:Issuer");
                var key = Utils.GetConfig("Authenticate:Jwt:Key");

                SecurityToken validatedToken;
                handerJwt.ValidateToken(token, new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = issuer,
                    ValidAudience = issuer,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key))
                }, out validatedToken);
                if (validatedToken != null
                && validatedToken.Issuer == issuer
                && validatedToken.ValidFrom.CompareTo(DateTime.Now) != 1
                && validatedToken.ValidTo.CompareTo(DateTime.Now) != -1)
                {
                    var claimsJwt = new List<Claim>();

                }

                var user = UserCollection.Instance.GetModel(userId);

                if (user != null)
                {
                    var flag = false;
                    // NOW YOU CAN ACCESS _ids
                    foreach (var id in _ids)
                    {
                        var r = (from s in user.Roles where s.RoleCode.Contains(id) select s).FirstOrDefault();
                        if (r != null)
                        {
                            flag = true;
                        }

                        if (flag)
                        {
                            break;
                        }
                    }

                    if (!flag)
                    {
                        context.Result = new BadRequestObjectResult("User is not permission");
                        return;
                    }
                }
                else
                {
                    context.Result = new BadRequestObjectResult("User is not exist");
                    return;
                }
                context.Result = new BadRequestObjectResult("User is not exist");
            }

            public void OnActionExecuted(ActionExecutedContext context)
            {
            }
        }
    }
}
