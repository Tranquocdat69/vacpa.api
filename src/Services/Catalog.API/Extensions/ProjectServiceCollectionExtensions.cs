namespace Business
{
    using ApiUtils;
    using ApiUtils.Cache;
    using Catalog.API.Bussiness.CauHoiBiMat;
    using Catalog.API.Bussiness.DMQuanLyHoiVien.ChuyenNganhDaoTao;
    using Catalog.API.Bussiness.DMQuanLyHoiVien.HinhThucKhenThuong;
    using Catalog.API.Bussiness.DMQuanLyHoiVien.HinhThucKyLuat;
    using Catalog.API.Bussiness.DMQuanLyHoiVien.HocVi;
    using Catalog.API.Bussiness.DMQuanLyHoiVien.SoThich;
    using Catalog.API.Bussiness.DMQuanLyHoiVien.TruongDaiHoc;
    using Catalog.API.Bussiness.ChucVu;
    using Catalog.API.Bussiness.DMChung.NganHang;
    using Catalog.API.Bussiness.DMQuanLyHoiVien.SoThich;
    using Catalog.API.Bussiness.GiangVien;
    using Catalog.API.Bussiness.HocHam;
    using Catalog.API.Bussiness.HVCN;
    using Catalog.API.Bussiness.TrinhDoChuyenMon;
    using DataUtils;
    using Infrastructure;
    using Microsoft.Extensions.DependencyInjection;

    /// <summary>
    /// <see cref="IServiceCollection"/> extension methods add project services.
    /// </summary>
    /// <remarks>
    /// AddSingleton - Only one instance is ever created and returned.
    /// AddScoped - A new instance is created and returned for each request/response cycle.
    /// AddTransient - A new instance is created and returned each time.
    /// </remarks>
    public static class ProjectServiceCollectionExtensions
    {
        public static IServiceCollection AddProjectServices(this IServiceCollection services) =>
            services

        #region SYS
                .AddTransient<IDatabaseFactory, DatabaseFactory>()
                .AddTransient<IUnitOfWork, UnitOfWork>()
                .AddSingleton<IApplicationHandler, ApplicationHandler>()
                .AddScoped<ICauHoiBiMatHandler, CauHoiBiMatHandler>()
                .AddScoped<IHoiVienCaNhanHandler,HoiVienCaNhanHandler >()
                .AddScoped<IHocHamHandler, HocHamHandler>()
                .AddScoped<IHocViHandler, HocViHandler>()
                .AddScoped<ITruongDaiHocHandler, TruongDaiHocHandler>()
                .AddScoped<IHinhThucKhenThuongHandler, HinhThucKhenThuongHandler>()
                .AddScoped<IHinhThucKyLuatHandler, HinhThucKyLuatHandler>()
                .AddScoped<ISoThichHandler, SoThichHandler>()
                .AddScoped<IChuyenNganhDaoTaoHandler, ChuyenNganhDaoTaoHandler>()
                .AddScoped<IGiangVienHandler, GiangVienHandler>()
                .AddSingleton<ITrinhDoChuyenMonHandler, TrinhDoChuyenMonHandler>()
                .AddSingleton<IChucVuHandler, ChucVuHandler>()
                .AddScoped<INganHangHandler, NganHangHandler>()
                .AddScoped<IDistributedCacheService, RedisCacheService>()
               


        #endregion
        ;
    }
}