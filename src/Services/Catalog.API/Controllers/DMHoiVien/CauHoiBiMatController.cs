﻿using Catalog.API.Bussiness.CauHoiBiMat;
using Catalog.API.Bussiness.GiangVien;
using CommonUtils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace Catalog.API.Controllers.DMHoiVien
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class CauHoiBiMatController : Controller
    {
        private readonly ILogger<CauHoiBiMatController> _logger;
        private readonly ICauHoiBiMatHandler _handler;
        public CauHoiBiMatController(ILogger<CauHoiBiMatController> logger, ICauHoiBiMatHandler handlder)
        {
            _logger = logger;
            _handler = handlder;
        }
        [AllowAnonymous, HttpPost, Route("create")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Add([FromBody] CauHoiBiMatModel value)
        {

            var requestInfo = CommonUtils.Helper.GetRequestInfo(Request, HttpContext.User);
            var appId = requestInfo.ApplicationId;
            //value.ApplicationId = appId;
            return await _handler.CreateAsync(value);
        }
        [AllowAnonymous, HttpPut, Route("update")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Update([FromForm] CauHoiBiMatUpdateModel value)
        {
      
            return await _handler.UpdateAsync(value);
        }
        [AllowAnonymous, HttpDelete, Route("delete")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Delete(Guid mach)
        {
            return await _handler.DeleteAsync(mach);
        }
        [AllowAnonymous, HttpGet, Route("find")]
        public async Task<Response> Find(string filter)
        {
            var cauHoiBiMatFilter = JsonConvert.DeserializeObject<CauHoiBiMatModelFilter>(filter);
            var result = await _handler.FindAsync(cauHoiBiMatFilter);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("getall")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> GetAll()
        {
            return await _handler.GetAll();
        }
    }
}
