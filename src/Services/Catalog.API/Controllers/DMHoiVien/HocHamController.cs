﻿using Catalog.API.Bussiness.CauHoiBiMat;
using Catalog.API.Bussiness.HocHam;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalog.API.Controllers.DMHoiVien
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class HocHamController : Controller
    {
        private readonly ILogger<CauHoiBiMatController> _logger;
        private readonly IHocHamHandler _handler;
        public HocHamController(ILogger<CauHoiBiMatController> logger, IHocHamHandler handlder)
        {
            _logger = logger;
            _handler = handlder;
        }
        [AllowAnonymous, HttpPost, Route("create")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Add([FromBody] HocHamModel value)
        {
            var result = await _handler.CreateAsync(value);
            return result;
        }
        [AllowAnonymous, HttpPut, Route("update")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Update(HocHamModel value)
        {
            var result = await _handler.UpdateAsync(value);
            return result;
        }
        [AllowAnonymous, HttpDelete, Route("delete")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Delete(string hocHamGuid)
        {
            var result = await _handler.DeleteByHocHamGuidAsync(hocHamGuid);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("find")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Find(string filter)
        {
            var hocHamFilter = JsonConvert.DeserializeObject<HocHamModelFilter>(filter);           
            var result = await _handler.FilterHocHamAsync(hocHamFilter);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("getall")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> GetAll()
        {
            var result =  await _handler.GetAllAsync();
            return result;
        }
        
    }
}
