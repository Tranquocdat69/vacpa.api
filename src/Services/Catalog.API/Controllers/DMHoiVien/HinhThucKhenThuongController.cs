﻿using Catalog.API.Bussiness.DMQuanLyHoiVien.HinhThucKhenThuong;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalog.API.Controllers.DMHoiVien
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class HinhThucKhenThuongController : Controller
    {

        private readonly ILogger<HinhThucKhenThuongController> _logger;
        private readonly IHinhThucKhenThuongHandler _handler;
        public HinhThucKhenThuongController(ILogger<HinhThucKhenThuongController> logger, IHinhThucKhenThuongHandler handlder)
        {
            _logger = logger;
            _handler = handlder;
        }
        [AllowAnonymous, HttpPost, Route("create")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Add([FromBody] HinhThucKhenThuongModel value)
        {
            var data = await _handler.CreateAsync(value);
            return data;
        }
        [AllowAnonymous, HttpPut, Route("update")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Update(HinhThucKhenThuongModel value)
        {
            var result = await _handler.UpdateAsync(value);
            return result;
        }
        [AllowAnonymous, HttpDelete, Route("delete")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Delete(string HinhThucKhenThuongGuid)
        {
            var result = await _handler.DeleteByHinhThucKhenThuongGuidAsync(HinhThucKhenThuongGuid);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("find")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Find(string filter)
        {
            var HinhThucKhenThuongFilter = JsonConvert.DeserializeObject<HinhThucKhenThuongModelFilter>(filter);
            var result = await _handler.FilterHinhThucKhenThuongAsync(HinhThucKhenThuongFilter);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("getall")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> GetAll()
        {
            var result = await _handler.GetAllAsync();
            return result;
        }
    }
}
