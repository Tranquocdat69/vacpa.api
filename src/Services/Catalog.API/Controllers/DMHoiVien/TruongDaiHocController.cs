﻿using Catalog.API.Bussiness.DMQuanLyHoiVien.TruongDaiHoc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalog.API.Controllers.DMHoiVien
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class TruongDaiHocController : Controller
    {
        private readonly ILogger<TruongDaiHocController> _logger;
        private readonly ITruongDaiHocHandler _handler;
        public TruongDaiHocController(ILogger<TruongDaiHocController> logger, ITruongDaiHocHandler handlder)
        {
            _logger = logger;
            _handler = handlder;
        }
        [AllowAnonymous, HttpPost, Route("create")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Add([FromBody] TruongDaiHocModel value)
        {
            var data = await _handler.CreateAsync(value);
            return data;
        }
        [AllowAnonymous, HttpPut, Route("update")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Update(TruongDaiHocModel value)
        {
            var result = await _handler.UpdateAsync(value);
            return result;
        }
        [AllowAnonymous, HttpDelete, Route("delete")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Delete(string TruongDaiHocGuid)
        {
            var result = await _handler.DeleteByTruongDaiHocGuidAsync(TruongDaiHocGuid);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("find")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Find(string filter)
        {
            var TruongDaiHocFilter = JsonConvert.DeserializeObject<TruongDaiHocModelFilter>(filter);
            var result = await _handler.FilterTruongDaiHocAsync(TruongDaiHocFilter);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("getall")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> GetAll()
        {
            var result = await _handler.GetAllAsync();
            return result;
        }
    }
}
