﻿using Catalog.API.Bussiness.DMQuanLyHoiVien.HinhThucKyLuat;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalog.API.Controllers.DMHoiVien
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class HinhThucKyLuatController : Controller
    {

        private readonly ILogger<HinhThucKyLuatController> _logger;
        private readonly IHinhThucKyLuatHandler _handler;
        public HinhThucKyLuatController(ILogger<HinhThucKyLuatController> logger, IHinhThucKyLuatHandler handlder)
        {
            _logger = logger;
            _handler = handlder;
        }
        [AllowAnonymous, HttpPost, Route("create")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Add([FromBody] HinhThucKyLuatModel value)
        {
            var data = await _handler.CreateAsync(value);
            return data;
        }
        [AllowAnonymous, HttpPut, Route("update")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Update(HinhThucKyLuatModel value)
        {
            var result = await _handler.UpdateAsync(value);
            return result;
        }
        [AllowAnonymous, HttpDelete, Route("delete")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Delete(string HinhThucKyLuatGuid)
        {
            var result = await _handler.DeleteByHinhThucKyLuatGuidAsync(HinhThucKyLuatGuid);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("find")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Find(string filter)
        {
            var HinhThucKyLuatFilter = JsonConvert.DeserializeObject<HinhThucKyLuatModelFilter>(filter);
            var result = await _handler.FilterHinhThucKyLuatAsync(HinhThucKyLuatFilter);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("getall")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> GetAll()
        {
            var result = await _handler.GetAllAsync();
            return result;
        }
    }
}
