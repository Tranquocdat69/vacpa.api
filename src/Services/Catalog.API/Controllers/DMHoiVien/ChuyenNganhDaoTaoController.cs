﻿
using Catalog.API.Bussiness.CauHoiBiMat;
using Catalog.API.Bussiness.DMQuanLyHoiVien.ChuyenNganhDaoTao;
using Catalog.API.Bussiness.DMQuanLyHoiVien.ChuyenNganhDaoTaoTao;
using Catalog.API.Bussiness.HocHam;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalog.API.Controllers.DMHoiVien
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class ChuyenNganhDaoTaoController : Controller
    {
        private readonly ILogger<ChuyenNganhDaoTaoController> _logger;
        private readonly IChuyenNganhDaoTaoHandler _handler;
        public ChuyenNganhDaoTaoController(ILogger<ChuyenNganhDaoTaoController> logger, IChuyenNganhDaoTaoHandler handlder)
        {
            _logger = logger;
            _handler = handlder;
        }
        [AllowAnonymous, HttpPost, Route("create")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Add([FromBody] ChuyenNganhDaoTaoModel value)
        {
            return await _handler.CreateAsync(value);
        }
        [AllowAnonymous, HttpPut, Route("update")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Update([FromForm] ChuyenNganhDaoTaoModel value)
        {
            return await _handler.UpdateAsync(value);
        }
        [AllowAnonymous, HttpDelete, Route("delete")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Delete(Guid chuyenNganhDaoTaoGuid)
        {
            return await _handler.DeleteAsync(chuyenNganhDaoTaoGuid);
        }
        [AllowAnonymous, HttpGet, Route("find")]
        public async Task<Response> Find(string filter)
        {
            var chuyenNganhDaoTaoFilter = JsonConvert.DeserializeObject<ChuyenNganhDaoTaoModelFilter>(filter);
            var result = await _handler.FindAsync(chuyenNganhDaoTaoFilter);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("getall")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> GetAll()
        {
            return await _handler.GetAll();
        }
    }
}
