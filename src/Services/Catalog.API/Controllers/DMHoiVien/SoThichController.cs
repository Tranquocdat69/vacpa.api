﻿using Catalog.API.Bussiness.DMQuanLyHoiVien.SoThich;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalog.API.Controllers.DMHoiVien
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class SoThichController : Controller
    {

        private readonly ILogger<SoThichController> _logger;
        private readonly ISoThichHandler _handler;
        public SoThichController(ILogger<SoThichController> logger, ISoThichHandler handlder)
        {
            _logger = logger;
            _handler = handlder;
        }
        [AllowAnonymous, HttpPost, Route("create")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Add([FromBody] SoThichModel value)
        {
            var data = await _handler.CreateAsync(value);
            return data;
        }
        [AllowAnonymous, HttpPut, Route("update")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Update(SoThichModel value)
        {
            var result = await _handler.UpdateAsync(value);
            return result;
        }
        [AllowAnonymous, HttpDelete, Route("delete")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Delete(string SoThichGuid)
        {
            var result = await _handler.DeleteBySoThichGuidAsync(SoThichGuid);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("find")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Find(string filter)
        {
            var SoThichFilter = JsonConvert.DeserializeObject<SoThichModelFilter>(filter);
            var result = await _handler.FilterSoThichAsync(SoThichFilter);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("getall")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> GetAll()
        {
            var result = await _handler.GetAllAsync();
            return result;
        }
    }
}
