﻿using Catalog.API.Bussiness.DMChung.NganHang;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalog.API.Controllers.DMChung
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v-1/[controller]")]
    public class NganHangController : Controller
    {

        private readonly ILogger<NganHangController> _logger;
        private readonly INganHangHandler _handler;
        public NganHangController(ILogger<NganHangController> logger, INganHangHandler handlder)
        {
            _logger = logger;
            _handler = handlder;
        }
        [AllowAnonymous, HttpPost, Route("create")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Add([FromBody] NganHangModel value)
        {
            var data = await _handler.CreateAsync(value);
            return data;
        }
        [AllowAnonymous, HttpPut, Route("update")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Update(NganHangModel value)
        {
            var result = await _handler.UpdateAsync(value);
            return result;
        }
        [AllowAnonymous, HttpDelete, Route("delete")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Delete(string NganHangGuid)
        {
            var result = await _handler.DeleteByNganHangGuidAsync(NganHangGuid);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("find")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Find(string filter)
        {
            var NganHangFilter = JsonConvert.DeserializeObject<NganHangModelFilter>(filter);
            var result = await _handler.FilterNganHangAsync(NganHangFilter);
            return result;
        }
        [AllowAnonymous, HttpGet, Route("getall")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> GetAll()
        {
            var result = await _handler.GetAllAsync();
            return result;
        }
    }
}
