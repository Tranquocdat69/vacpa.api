﻿using Catalog.API.Bussiness.TrinhDoChuyenMon;

namespace Catalog.API.Controllers.TrinhDoChuyenMon
{
    [Route("api/v-1/[controller]")]
    [ApiController]
    public class TrinhDoChuyenMonController : ControllerBase
    {
        private readonly ITrinhDoChuyenMonHandler _trinhDoChuyenMonHandler;

        public TrinhDoChuyenMonController(ITrinhDoChuyenMonHandler trinhDoChuyenMonHandler)
        {
            _trinhDoChuyenMonHandler = trinhDoChuyenMonHandler;
        }

        [Authorize, HttpGet, Route("all")]
        public async Task<Response> GetAll()
        {
            return await _trinhDoChuyenMonHandler.GetAllAsync();
        }
        
        [Authorize, HttpGet, Route("filter")]
        public async Task<Response> Filter(string filter)
        {
            var trinhDoChuyenMonFilter = JsonConvert.DeserializeObject<TrinhDoChuyenMonModelFilter>(filter);
            return await _trinhDoChuyenMonHandler.FilterAsync(trinhDoChuyenMonFilter);
        }
    }
}
