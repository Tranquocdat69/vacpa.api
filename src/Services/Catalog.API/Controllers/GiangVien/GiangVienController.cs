﻿using Catalog.API.Bussiness.GiangVien;

namespace Catalog.API.Controllers.GiangVien
{
    [Route("api/v-1/[controller]")]
    [ApiController]
    public class GiangVienController : ControllerBase
    {
        private readonly IGiangVienHandler _giangVienHandler;

        public GiangVienController(IGiangVienHandler giangVienHandler)
        {
            _giangVienHandler = giangVienHandler;
        }

        [Authorize, HttpGet, Route("all")]
        public async Task<Response> GetAll()
        {
            return await _giangVienHandler.GetAllAsync();
        }
        
        [Authorize, HttpGet, Route("getbymagiangvien")]
        public async Task<Response> GetByMaGiangVien(string maGiangVien)
        {
            return await _giangVienHandler.GetByMaGiangVienAsync(maGiangVien);
        }

        [Authorize, HttpGet, Route("filter")]
        public async Task<Response> Filter(string filter = "")
        {
            var giangVienFilter = JsonConvert.DeserializeObject<FilterGiangVienModel>(filter);
            return await _giangVienHandler.FilterAsync(giangVienFilter);
        }

        [Authorize, HttpPost, Route("create")]
        public async Task<Response> Create(UpsertGiangVienModel giangVien)
        {
            return await _giangVienHandler.CreateAsync(giangVien);
        }
        
        [Authorize, HttpPut, Route("update")]
        public async Task<Response> Update(UpsertGiangVienModel giangVien)
        {
            return await _giangVienHandler.UpdateAsync(giangVien);
        }

        [Authorize, HttpDelete, Route("delete")]
        public async Task<Response> Delete(string maGiangVien)
        {
            return await _giangVienHandler.DeleteAsync(maGiangVien);
        }
    }
}
