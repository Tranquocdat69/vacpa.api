﻿using ApiUtils.Cache;
using Business;
using Catalog.API.Bussiness.CauHoiBiMat;
using Catalog.API.Bussiness.GiangVien;
using Catalog.API.Bussiness.HVCN;
using Catalog.API.Controllers.DMHoiVien;
using CommonUtils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace Catalog.API.Controllers.HVCN
{
    [Route("api/v-1/[controller]")]
    [ApiController]
    public class HoiVienCaNhanController : Controller
    {
        private readonly ILogger<HoiVienCaNhanController> _logger;
        private readonly IHoiVienCaNhanHandler _handler;
        private readonly IDistributedCacheService _cacheService;
        public HoiVienCaNhanController(ILogger<HoiVienCaNhanController> logger, IHoiVienCaNhanHandler handlder, IDistributedCacheService cacheService)
        {
            _logger = logger;
            _handler = handlder;
            _cacheService = cacheService;
        }
        [AllowAnonymous, HttpPost, Route("create")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Add([FromForm] HoiVienCaNhanModel value)
        {
            return await _handler.CreateAsync(value);
        }
        [AllowAnonymous, HttpPut, Route("")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Update([FromForm] HoiVienCaNhanModel value)
        {
            return await _handler.UpdateAsync(value);
        }
        [AllowAnonymous, HttpDelete, Route("")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> Delete(string maHoiVienCaNhan)
        {
            return await _handler.DeleteByMaGiangVienAsync(maHoiVienCaNhan);
        }
        [AllowAnonymous, HttpGet, Route("getAll")]
        //[EnableCors(origins: "*", headers: "*", methods: "POST")]
        public async Task<Response> GetAll()
        {
            ResponseList<HoiVienCaNhanModel> result = (ResponseList<HoiVienCaNhanModel>) await _handler.GetAllAsync();
            foreach (var item in result.Data)
            {
                _cacheService.HashSet("HoiVienCaNhan", item.MaHoiVienCaNhan, item);
            }
            return result;
        }

        [AllowAnonymous, HttpGet, Route("getByMaHoiVienCaNhan")]
        public async Task<Response> GetByMaHoiVienCaNhan(string maHoiVienCaNhan)
        {
            return await _handler.GetByMaHoiVienAsync(maHoiVienCaNhan);
        }

        [AllowAnonymous, HttpGet, Route("filter")]
        public async Task<Response> FilterGiangVien(string filter="")
        {
            var hoiVienCaNhanFiter = JsonConvert.DeserializeObject<HoiVienCaNhanModelFilter>(filter);
            return await _handler.FilterHoiVienAsync(hoiVienCaNhanFiter);
        }
    }
}
