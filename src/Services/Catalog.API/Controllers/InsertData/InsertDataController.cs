﻿using Catalog.API.Infrastructure.Datatables.CNKT;
using Catalog.API.Infrastructure.Datatables.HoiVienCaNhan;
using DataUtils;
using Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.API.Controllers.InsertData
{
    [Route("api/v-1/[controller]")]
    [ApiController]
    public class InsertDataController : ControllerBase
    {
        [AllowAnonymous, HttpGet, Route("ChangeDataFromIntToGuid")]
        public async Task<IActionResult> ChangeDataFromIntToGuid()
        {
            DataContext context = new DataContext(
                prefix: "",
                databaseType: "MSSQLDatabase",
                connectionString: "Server=(localdb)\\MSSQLLocalDB;initial catalog=vacpa;Uid=sa;Pwd=12345678;",
                isTesting: false);

            var listFromVacpa = await context.TblDmGiangVien.Select(x => new
            {
                x.MaGiangVien,
                x.TenGiangVien,
                x.MaChucVu,
                x.MaTrinhDoChuyenMon,
                x.DonViCongTac,
                x.ChungChi,
                x.GioiTinh,
                x.NgaySinh
            }).ToListAsync();

            using (var vacpaCatalogUnitOfWork = new UnitOfWork(new DatabaseFactory()))
            {
                List<TblDmChucVu> listDmChucVuVacpaCatalog = await vacpaCatalogUnitOfWork.GetRepository<TblDmChucVu>().GetAll().ToListAsync();
                foreach (var item in listFromVacpa)
                {
                    var ok = new TblDmGiangVien()
                    {
                        GiangVienGuid = Guid.NewGuid(),
                        MaGiangVien = item.MaGiangVien,
                        TenGiangVien = item.TenGiangVien,
                        MaChucVu = item.MaChucVu,
                        MaTrinhDoChuyenMon = item.MaTrinhDoChuyenMon,
                        DonViCongTac = item.DonViCongTac,
                        ChungChi = item.ChungChi,
                        GioiTinh = item.GioiTinh,
                        NgaySinh = item.NgaySinh,
                    };
                    vacpaCatalogUnitOfWork.GetRepository<TblDmGiangVien>().Add(ok);
                }
                await vacpaCatalogUnitOfWork.SaveAsync();
            }

            return Ok();
        }

        [AllowAnonymous, HttpGet, Route("ChangeDataMember")]
        public async Task<IActionResult> ChangeFromIntMember()
        {
            DataContext context = new DataContext(
                prefix: "",
                databaseType: "PostgreSQLDatabase",
                connectionString: "Host=192.168.0.34;Port=5432;Database=MemberSevice_VACPA_DB;Username=postgres;Password=Savis@1235",
                isTesting: false);

            //List<TblHoiVienCaNhan> tblHoiVienCaNhans = context.TblHoiVienCaNhan.ToList();
            //var a = 1;

            var listFromVacpa = await context.TblHoiVienCaNhan.Select(x => new
            {
                x.HoiVienCaNhanId,
                x.MaHoiVienCaNhan,
                x.LoaiHoiVienCaNhan,
                x.TroLyKtv,
                x.HoDem,
                x.Ten,
                x.GioiTinh,
                x.NgaySinh,
                x.QuocTichId,
                x.QueQuanTinhId,
                x.QueQuanHuyenId,
                x.QueQuanXaId,
                x.DiaChi,
                x.DiaChiTinhId,
                x.DiaChiHuyenId,
                x.DiaChiXaId,
                x.TruongDaiHocId,
                x.ChuyenNganhDaoTaoId,
                x.ChuyenNganhNam,
                x.HocViId,
                x.HocViNam,
                x.HocHamId,
                x.HocHamNam,
                x.SoCmnd,
                x.CmndNgayCap,
                x.CmndTinhId,
                x.SoTaiKhoanNganHang,
                x.NganHangId,
                x.SoChungChiKtv,
                x.NgayCapChungChiKtv,
                x.SoGiayChungNhanDkhn,
                x.ChucVuId,
                x.HoiVienTapTheId,
                x.DonViCongTac,
                x.NgayCapGiayChungNhanDkhn,
                x.HanCapTu,
                x.HanCapDen,
                x.Email,
                x.Mobile,
                x.DienThoai,
                x.SoThichId,
                x.NgayGiaNhap,
                x.NgayNhap,
                x.NguoiNhap,
                x.NgayCapNhat,
                x.NguoiCapNhat,
                x.NgayDuyet,
                x.NguoiDuyet,
                x.TinhTrangHoiVienId,
                x.SoQuyetDinhKetNap,
                x.NgayQuyetDinhKetNap,
                x.NamGuiEmailChucMung,
                x.LoaiHoiVienCaNhanChiTiet,

            }).ToListAsync();

            using (var vacpaCatalogUnitOfWork = new UnitOfWork(new DatabaseFactory()))
            {
                //List<TblDmChucVu> listDmChucVuVacpaCatalog = await vacpaCatalogUnitOfWork.GetRepository<TblDmChucVu>().GetAll().ToListAsync();
                foreach (var item in listFromVacpa)
                {
                    var ok = new TblHoiVienCaNhan()
                    {
                        HoiVienCaNhanId = item.HoiVienCaNhanId,
                        MaHoiVienCaNhan = item.MaHoiVienCaNhan,
                        LoaiHoiVienCaNhan = item.LoaiHoiVienCaNhan,
                        TroLyKtv = item.TroLyKtv,
                        HoDem =item.HoDem,
                        Ten =item.Ten,
                        GioiTinh =item.GioiTinh,
                        NgaySinh =item.NgaySinh,
                        QuocTichId =item.QuocTichId,
                        QueQuanTinhId =item.QueQuanTinhId,
                        QueQuanHuyenId =item.QueQuanHuyenId,
                        QueQuanXaId =item.QueQuanXaId,
                        DiaChi =item.DiaChi,
                        DiaChiTinhId =item.DiaChiTinhId,
                        DiaChiHuyenId = item.DiaChiHuyenId,
                        DiaChiXaId =item.DiaChiXaId,
                        TruongDaiHocId = item.TruongDaiHocId,
                        ChuyenNganhDaoTaoId = item.ChuyenNganhDaoTaoId,
                        ChuyenNganhNam = item.ChuyenNganhNam,
                        HocViId = item.HocViId,
                        HocViNam = item.HocViNam,
                        HocHamId = item.HocHamId,
                        HocHamNam = item.HocHamNam,
                        SoCmnd = item.SoCmnd,
                        CmndNgayCap = item.CmndNgayCap,
                        CmndTinhId = item.CmndTinhId,
                        SoTaiKhoanNganHang = item.SoTaiKhoanNganHang,
                        NganHangId = item.NganHangId,
                        SoChungChiKtv = item.SoChungChiKtv,
                        NgayCapChungChiKtv = item.NgayCapChungChiKtv,
                        SoGiayChungNhanDkhn = item.SoGiayChungNhanDkhn,
                        ChucVuId = item.ChucVuId,
                        HoiVienTapTheId = item.HoiVienTapTheId,
                        DonViCongTac = item.DonViCongTac,
                        NgayCapGiayChungNhanDkhn = item.NgayCapGiayChungNhanDkhn,
                        HanCapTu = item.HanCapTu,
                        HanCapDen = item.HanCapDen,
                        Email = item.Email,
                        Mobile = item.Mobile,
                        DienThoai = item.DienThoai,
                        SoThichId = item.SoThichId,
                        NgayGiaNhap = item.NgayGiaNhap,
                        NgayNhap = item.NgayNhap,
                        NguoiNhap = item.NguoiNhap,
                        NgayCapNhat = item.NgayCapNhat,
                        NguoiCapNhat = item.NguoiCapNhat,
                        NgayDuyet = item.NgayDuyet,
                        NguoiDuyet = item.NguoiDuyet,
                        TinhTrangHoiVienId = item.TinhTrangHoiVienId,
                        SoQuyetDinhKetNap = item.SoQuyetDinhKetNap,
                        NgayQuyetDinhKetNap = item.NgayQuyetDinhKetNap,
                        NamGuiEmailChucMung = item.NamGuiEmailChucMung,
                        LoaiHoiVienCaNhanChiTiet = item.LoaiHoiVienCaNhanChiTiet,
                    };
                    vacpaCatalogUnitOfWork.GetRepository<TblHoiVienCaNhan>().Add(ok);
                }
                await vacpaCatalogUnitOfWork.SaveAsync();
            }

            return Ok();
        }

    }
}
