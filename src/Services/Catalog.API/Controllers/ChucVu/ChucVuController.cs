﻿using Catalog.API.Bussiness.ChucVu;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Catalog.API.Controllers.ChucVu
{
    [Route("api/v-1/[controller]")]
    [ApiController]
    public class ChucVuController : ControllerBase
    {
        private readonly IChucVuHandler _chucVuHandler;

        public ChucVuController(IChucVuHandler chucVuHandler)
        {
            _chucVuHandler = chucVuHandler;
        }

        [Authorize, HttpGet, Route("all")]
        public async Task<Response> GetAll()
        {
            return await _chucVuHandler.GetAllAsync();
        }
        
        [Authorize, HttpGet, Route("filter")]
        public async Task<Response> Filter(string filter)
        {
            var trinhDoChuyenMonFilter = JsonConvert.DeserializeObject<ChucVuModelFilter>(filter);
            return await _chucVuHandler.FilterAsync(trinhDoChuyenMonFilter);
        }
    }
}
