using Microsoft.EntityFrameworkCore;

namespace DataUtils
{
    public interface IDatabaseFactory
    {
        DbContext GetDbContext();
        string GetPrefix();
    }
}