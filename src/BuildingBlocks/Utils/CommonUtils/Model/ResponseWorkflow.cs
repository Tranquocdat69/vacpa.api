﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonUtils
{
    // RULES
    // If success, status code = 1, message is empty, data is not null
    // If error, status code = 0, message is error message, data is null

    public class ResponseWorkflow<T> : ResponseWorkflow
    {
        public T Data { get; private set; }

        public ResponseWorkflow(int status, string message = null, T data = default(T))
            : base(status, message)
        {
            Data = data;
        }
    }

    public class ResponseWorkflow
    {
        public int Status { get; set; }

        public string Message { get; set; }

        public ResponseWorkflow(int status, string message = null)
        {
            Status = status;
            Message = message;
        }
    }
}
