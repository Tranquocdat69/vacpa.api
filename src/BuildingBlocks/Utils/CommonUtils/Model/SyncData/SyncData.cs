﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonUtils
{
    #region admin
    public class ApplicationSOAModel
    {
        public Guid ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string Description { get; set; }
        public string LoweredApplicationName { get; set; }
    }

    public class OrganizationSOAModel
    {
        public Guid OrganizationId { get; set; }
        public Guid ApplicationId { get; set; }
        public Guid ParentId { get; set; }
        public string ParentName { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool DeleteSuccess { get; set; }
        public string Description { get; set; }
        public string ChucVu { get; set; }
        public int? STT { get; set; }
        public string Code { get; set; }

        public string DiaChi { get; set; }
        public string SoDienThoai { get; set; }
        public string SoFax { get; set; }
        public string Email { get; set; }
        public int? Loai { get; set; }
        public string IdPath { get; set; }
        public string Path { get; set; }
        public int? Level { get; set; }
        //public bool HasChild { get; set; }

        //public string CreatedByFullName { get; set; }
        public List<DonViModel> LstDonVi { get; set; }
        public Guid CreatedByUserID { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public string LastModifiedByUserName { get; set; }
        public Guid LastModifiedByUserID { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
    }

    public class DonViModel
    {
        public string Name { get; set; }
        public Guid OrganizationId { get; set; }
    }

    public class RightSOAModel
    {
        public string RightCode { get; set; }
        public string RightName { get; set; }
        public string Description { get; set; }
        public bool? Status { get; set; }
        public int? Order { get; set; }
        public bool? IsGroup { get; set; }
        public int? Level { get; set; }
        public string GroupCode { get; set; }
    }


    public class RoleSOAModel
    {
        public Guid ApplicationId { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string LoweredRoleName { get; set; }
        public string Description { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public string RoleCode { get; set; }
    }

    public class RightInRoleSOAModel
    {
        public long Id { get; set; }
        public Guid RoleId { get; set; }
        public string RightCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    public class UserSOAModel
    {
        public Guid ApplicationId { get; set; }
        public Guid UserId { get; set; }
        public string NickName { get; set; }
        public string UserName { get; set; }
        public string MobilePin { get; set; }
        public string Email { get; set; }
        public int Type { get; set; }
        public string FullName { get; set; }
        public DateTime Birthday { get; set; }
        public string LoweredUserName { get; set; }
        public string Password { get; set; }
        public int PasswordFormat { get; set; }
        public string PasswordSalt { get; set; }
        public bool IsLockedOut { get; set; }
        public string MobileAlias { get; set; }
        public bool IsAnonymous { get; set; }
        public string Avatar { get; set; }
        public string Address { get; set; }

        public DateTime LastActivityDate { get; set; }
        public Guid? CharacterManagerId { get; set; }

        public string FinanceCode { get; set; }
        //base
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        //Workflow
        public string SchemeName { get; set; }
        public string WorkFlowCurrentState { get; set; }
        public DateTime? WorkFlowProcessTime { get; set; }
        public string WorkFlowNextState { get; set; }
        public string WorkFlowPreviousState { get; set; }
        public string WorkFlowAsignUserName { get; set; }
        public Guid? WorkflowProcessUserId { get; set; }
        public string WorkflowProcessUserName { get; set; }
    }

    public class RightOfUserSOAModel
    {
        public long Id { get; set; }
        public Guid UserId { get; set; }
        public string RightCode { get; set; }
        public string InheritedFromRoles { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool Inherited { get; set; }
        public bool Enable { get; set; }
        public Guid ApplicationId { get; set; }
    }

    public class UserInOrganizationSOAModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid OrganizationId { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public string Role { get; set; }
        public string OrganizationName { get; set; }
    }

    public class UserInRoleSOAModel
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? DeleteDate { get; set; }
        public Guid ApplicationId { get; set; }
    }

    public class NavigationSOAModel
    {
        public Guid NavigationId { get; set; }
        public Guid? ParentId { get; set; }
        public Guid ApplicationId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool? Status { get; set; }
        public int? Order { get; set; }
        public bool HasChild { get; set; }
        public string UrlRewrite { get; set; }
        public string IconClass { get; set; }
        public string NavigationNameEn { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public string IdPath { get; set; }
        public string Path { get; set; }
        public int Level { get; set; }
        public string SubUrl { get; set; }
    }

    public class NavigationRoleSOAModel
    {
        public Guid NavigationRoleId { get; set; }
        public Guid RoleId { get; set; }
        public Guid NavigationId { get; set; }
        public Guid? FromSubNavigation { get; set; }
    }

    public class ProductionPlanSOAModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? EventId { get; set; }
        public string EventName { get; set; }
        public DateTime? EventBeginDate { get; set; } // thời gian diễn ra sự kiện
        public int Type { get; set; }         // 1: theo kế hoạch đài giao     2: không theo   
        public string Duration { get; set; }
        public int? PlannedQuantity { get; set; }

        public string Episodes { get; set; }
        public int? RealQuantity { get; set; }
        public string Location { get; set; }
        public Guid? AssignningUserId { get; set; } // người phân công (thương là lãnh đạo ban)
        public string AssignningUserFullName { get; set; }
        public string Description { get; set; }
        public string NoiDung { get; set; }
        public string Note { get; set; }
        public string Color { get; set; }
        public Guid? NewsScriptFormatId { get; set; }

        public string DataHistory { get; set; }
        public string ProductType { get; set; }  //type of table ProductType
        public DateTime? StartDate { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime? PlanDateToBroadcast { get; set; }  //ngày dự kiến phát sóng (cho  Trung tâm sản xuất Phim truyền hình)
        public bool? IsFromBaseEvent { get; set; }  //những kế hoạch đc auto merge từ các sự kiện
        public Guid? ParentPlanId { get; set; } //kế hoạch cha ( nếu null thì là kế hoạch cha, có Id thì là kịch bản đc tạo từ 1 kh)
        public string ParentPlanName { get; set; }

        public Guid? NodeId { get; set; }
        public string ListFile { get; set; }

        // base
        public Guid ApplicationId { get; set; }
        public Guid OrganizationId { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public string CreatedByUserName { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public string LastModifiedByUserName { get; set; }
        public DateTime LastModifiedOnDate { get; set; }

        //Workflow
        public string SchemeName { get; set; }
        public string WorkflowCode { get; set; }
        public string WorkFlowCurrentStatus { get; set; }
        public DateTime? WorkFlowProcessTime { get; set; }
        public string WorkFlowNextState { get; set; }
        public string WorkFlowPreviousState { get; set; }
        public string WorkFlowAsignUserName { get; set; }
        public Guid? WorkflowProcessUserId { get; set; }
        public string WorkflowProcessUserName { get; set; }

        // Xóa hay ko
        public bool IsDelete { get; set; } = false;
    }

    public class GiayMoiSOAModel
    {
        public Guid Id { get; set; }
        public string TenSuKien { get; set; }
        public DateTime? ThoiGianBatDau { get; set; }
        public DateTime? ThoiGianKetThuc { get; set; }
        public string Nguon { get; set; }
        public string DiaDiem { get; set; }
        public Guid? PhongThucHien { get; set; }
        public Guid? NguoiThucHien { get; set; }
        public Guid? NguoiGiaoViec { get; set; }
        public string NguoiDangKy { get; set; }
        public DateTime? ThoiGianGiaoViec { get; set; }
        public string FileInfo { get; set; }

        //base
        public DateTime? CreatedOnDate { get; set; }
        public DateTime? ModifiedOnDate { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? ModifiedByUserId { get; set; }
        public Guid? NodeId { get; set; }
        public Guid? ApplicationId { get; set; }
        // Xóa hay ko
        public bool IsDelete { get; set; } = false;
    }

    #endregion

    #region catalog
    public class PositionSOAModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public int Status { get; set; }
        public int Order { get; set; }
        //base
        public Guid ApplicationId { get; set; }
        public Guid OrganizationId { get; set; }
        public string CreatedByFullName { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public string LastModifiedByFullName { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
    }

    public class UserPositionRelSOAModel
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }
        public Guid? PositionId { get; set; }
    }

    public class ResourcesSOAModel
    {
        public Guid Id { get; set; }
        public Guid AreaId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Default_Person { get; set; }
        public string Default_Person_ShortName { get; set; }
        public Guid DefaultPersonId { get; set; }
        public string DefaultPersonPhone { get; set; }
        public string MetadataJson { get; set; }
        public string Note { get; set; }
        public Guid Taxonomy_term_id { get; set; }
        public string TaxonomyTermCode { get; set; }
        public int Status { get; set; }

        //base
        public Guid ApplicationId { get; set; }
        public Guid OrganizationId { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
    }

    public class TaxonomyVocabularySOAModel
    {
        public Guid VocabularyId { get; set; }
        public Guid VocabularyTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public bool IsSystem { get; set; }
        public Guid ApplicationId { get; set; }
        public string Code { get; set; }
    }

    public class TaxonomyTermSOAModel
    {
        public Guid TermId { get; set; }
        public Guid VocabularyId { get; set; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; }
        public Guid TermLeft { get; set; }
        public Guid TermRight { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public Guid ApplicationId { get; set; }
        public int? Order { get; set; }
        public string VocabularyCode { get; set; }
        public int? ChildCount { get; set; }
        public string IdPath { get; set; }
        public string Path { get; set; }
        public int? Level { get; set; }
        public string Code { get; set; }
    }

    public class CatalogItemSOAModel
    {
        public Guid CatalogItemId { get; set; }
        public Guid MappedTermId { get; set; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public int Order { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public string Code { get; set; }
    }

    public class DutyTypeSOAModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public Guid ApplicationId { get; set; }
        public bool IsInit { get; set; } // Loại chức danh khởi tạo ?
        public int Order { get; set; }
    }

    public class DutySOAModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Note { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid CreatedByUserId { get; set; }
        public string CreatedByUserName { get; set; }
        public DateTime ModifiedOnDate { get; set; }
        public Guid ModifiedByUserId { get; set; }
        public string ModifiedByUserName { get; set; }
        public Guid ApplicationId { get; set; }
        public string ProductionDuty { get; set; } // List lưu Object chức danh sx linh kiện
        public int IsDadDuty { get; set; }
        public Guid? DadDutyId { get; set; }
        public Guid? DutyTypeId { get; set; }

    }
    #endregion

    #region Prenews
    public class PrenewsSOAModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string TenDeTai { get; set; }
        public string NoiDung { get; set; }
        public string MaDeTai { get; set; }
        public Guid LocationId { get; set; }
        public string DiaChiChiTiet { get; set; }
        public DateTime BatDau { get; set; }
        public DateTime KetThuc { get; set; }
        public string ListFileMedia { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public DateTime ModifiedOnDate { get; set; }
        public string ThoiGianDuKien { get; set; }
        public string SoKmDuKien { get; set; }
        public string SoKmThucTe { get; set; }
        public string SoLuongNguoi { get; set; }
        public string SoLuongKhanGia { get; set; }
        public Guid NguoiThucHien { get; set; }
        public string GhiChu { get; set; }
        public Guid ApplicationId { get; set; }
        public string TrangThai { get; set; }
        public bool CoLuuTru { get; set; }
        public bool DaXem { get; set; }
        public Guid? CreatedByUser { get; set; }
        public string NguonThongTin { get; set; }
        public string DuKienPhongVan { get; set; }
        public bool DanHienTruong { get; set; }
        public DateTime NgayHoanThanhDuKien { get; set; }
        public DateTime ThoiGianDuKienPhatSong { get; set; }
        public string BacDeCuong { get; set; }
        public string WorkflowCode { get; set; }
        public string HinhAnhQuanTrong { get; set; }
        public string Episode { get; set; }
        public string TenKhachMoi { get; set; }
        public string DisplayStatus { get; set; }
    }

    public class PrenewsRelationshipHumanSOAModel
    {
        public long DetaiTermRelationshipId { get; set; }
        public Guid DetaiId { get; set; }
        public Guid TaxonomyTermId { get; set; }
    }

    public class PrenewsRelationshipProductSOAModel
    {
        public Guid Id { get; set; }
        public Guid ObjectId { get; set; }
        public Guid TermId { get; set; }
        public int SoLuong { get; set; }
        public string Note { get; set; }
    }

    public class PrenewsEkipSOAModel
    {
        public Guid Id { get; set; }
        public Guid DeTaiId { get; set; }
        public Guid DutyId { get; set; }
        public string DutyName { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
    }

    public class SchedulePrenewsSOAModel
    {
        public long PtsxDangkyId { get; set; }
        public Guid TaxonomyTermId { get; set; }
        public Guid DetaiId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int SoLuong { get; set; }
        public string GhiChu { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class SchedulePrenewsProduct
    {
        public Guid Id { get; set; }
        public long? DangKyId { get; set; }
        public Guid? PtsxId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Note { get; set; }
    }

    public class SchedulePrenewsHumans
    {
        public Guid Id { get; set; }
        public long? DangKyId { get; set; }
        public Guid? UserId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Note { get; set; }
    }
    #endregion

    #region News
    public class NewsSOAModel
    {
        public Guid NewsId { get; set; }
        public string Name { get; set; }
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string Location { get; set; }
        public DateTime? FinishedDate { get; set; } 
        public Guid ReporterId { get; set; }
        public string ReporterName { get; set; }
        public string Body { get; set; }
        public bool IsPublic { get; set; }
        public bool IsInner { get; set; }
        public int Status { get; set; }

        public int? Order { get; set; }
        public DateTime? BroadcastOnDate { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public Guid ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string NewsCode { get; set; }
        public string INewsName { get; set; }
        public Guid NewsCategoryId { get; set; }
        public string NewsCategoryName { get; set; }
        public string NewsCategoryShortName { get; set; }
        public string NewsCategoryCode { get; set; }

        public Guid? ParentNewsId { get; set; } //TrungDD: tin gốc của các tin SendTo
        public bool IsSent { get; set; }
        public bool IsAutomated { get; set; } 
        public bool? IsDelete { get; set; }// QuyenNH: Lọc trạng thái tin bài đã xóa

        public string Guest { get; set; }
        public string ThoiLuong { get; set; }
        public string BacDeCuong { get; set; }
        public string Note { get; set; }
        public bool? IsInternational { get; set; } //SonTH: Tin quốc tế

        // workflow properties
        public string WorkflowCode { get; set; }
        public string WorkFlowCurrentStatus { get; set; }
        public DateTime? WorkFlowProcessTime { get; set; }
        public string WorkFlowNextState { get; set; }
        public string WorkFlowPreviousState { get; set; }
        public string WorkFlowAsignUserName { get; set; }
        public Guid? WorkflowProcessUserId { get; set; }
        public string WorkflowProcessUserName { get; set; }
    }

    public class NewsRelationShipSOAModel
    {
        public Guid Id { get; set; }
        public Guid NewsId { get; set; }
        public Guid ObjectId { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string FinanceCode { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }
        public float? EffortRatio { get; set; }
    }
    #endregion

}
