﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonUtils
{
    public class IPFMLoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class IPFMLoginResponseModel
    {
        public IPFMUserResponseModel User {get; set;}
        public string Result { get; set; }
        public int Code { get; set; }
        public string Token { get; set; }
        public DateTime ExpireDate { get; set; }
    }
    public class IPFMUserResponseModel {
        public string Result { get; set; }
        public int Code { get; set; }
        public string Token { get; set; }
        public DateTime ExpireDate { get; set; }
    }

    public class LGSPIDModel
    {
        public string LgspId { get; set; }
    }
}
