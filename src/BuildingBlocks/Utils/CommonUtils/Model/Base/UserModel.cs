using System;
using System.Collections.Generic;

namespace CommonUtils
{
    //public class BaseUserModel
    //{
    //    public Guid Id { get; set; }
    //    public string Name { get; set; }
    //    public string Code { get; set; }
    //    public string UserName { get; set; }
    //}

    public class UserModel : BaseUserModel
    {
        public Guid ApplicationId { get; set; }
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public Guid UserId { get; set; }
        public string NickName { get; set; }
        public string MobilePin { get; set; }
        public string Email { get; set; }
        public int Type { get; set; }
        public string FullName { get; set; }
        public DateTime Birthday { get; set; }
        public string LoweredUserName { get; set; }
        public string Password { get; set; }
        public int PasswordFormat { get; set; }
        public string PasswordSalt { get; set; }
        public bool IsLockedOut { get; set; }
        public string MobileAlias { get; set; }
        public bool IsAnonymous { get; set; }
        public string Avatar { get; set; } 
        public string Address { get; set; } 

        // ManhNX
        public string ListRoleName { get; set; }
        public int Approved { get; set; }
        public DateTime LastActivityDate { get; set; }
        public UserInArea UserInArea { get; set; }

        //HaNH
        // public List<RightsOfUser> UserRights { get; set; }
        public RoleAndRightOfUser UserRights { get; set; }
        public UsersInOrganizationModel UserOrganization { get; set; }
        //HaNT
        //TTSCharacterManager
        public Guid? CharacterManagerId { get; set; }
        //base
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        //Workflow
        public string isFinished { get; set; }
        public string SchemeName { get; set; }
        public string WorkFlowCurrentState { get; set; }
        public DateTime? WorkFlowProcessTime { get; set; }
        public string WorkFlowNextState { get; set; }
        public string WorkFlowPreviousState { get; set; }
        public string WorkFlowAsignUserName { get; set; }
        public Guid? WorkflowProcessUserId { get; set; }
        public string WorkflowProcessUserName { get; set; }
    }

    public class UserProfileModel : BaseUserModel
    {
        public Guid UserId { get; set; }
        public string NickName { get; set; }
        public string MobilePin { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string LoweredUserName { get; set; }
        public bool IsLockedOut { get; set; }
        public string MobileAlias { get; set; }
        public bool IsAnonymous { get; set; }
        public string Avatar { get; set; }
        public UserInArea UserInArea { get; set; }
        public List<RightsOfUser> UserRights { get; set; }
    }
    public class UserQueryModel : PaginationRequest
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class UserUpdateModel
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsApproved { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string Mobile { get; set; }
        public int? Type { get; set; }
        public string ApplicationId { get; set; }
        public Guid? CreatedByUserID { get; set; }
        public List<Guid> RoleList { get; set; }
        public UserInArea UserInArea { get; set; }
        //HaNH
        public List<RightsOfUser> UserRights { get; set; }
        public UsersInOrganizationModel UserOrganization { get; set; }
        public DateTime Birthday { get; set; }
        public string Avatar { get; set; }
        public string Address { get; set; }
    }
    public class UserCreateModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string OtherEmail { get; set; }
        public bool IsApproved { get; set; }
        public object ProviderUserKey { get; set; }
        public string CreatedByUserID { get; set; }
        public string LastModifiedByUserID { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string ShortName { get; set; }
        public string MobilePin { get; set; }
        public int? Type { get; set; }
        public string ApplicationId { get; set; }
        public string FinanceCode { get; set; }
        public string ChucVu { get; set; }
        public List<Guid> RoleList { get; set; }
        public UserInArea UserInArea { get; set; }
        //HaNH
        public List<RightsOfUser> UserRights { get; set; }
        public UsersInOrganizationModel UserOrganization { get; set; }
        public DateTime Birthday { get; set; }
    }

    public class UserLockingModel
    {
        public Guid UserId { get; set; }
        public bool IsApproved { get; set; }
    }

    public class UpdateUserPasswordModel
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
        public bool? IsAdmin { get; set; }
    }

    public class UserFilterModel : PaginationRequest
    {
        public bool? Status { get; set; }
        public int? Month { get; set; }
        public int? Type { get; set; }
    }
    public class RoleAndRightOfUser
    {
        public List<RoleOfUser> Roles { get; set; }
        public List<RightsOfUser> FreeRights { get; set; }
    }

    public class RoleOfUser
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public Guid ApplicationId { get; set; }
        public string RoleCode { get; set; }
        public List<RightsOfUser> RightsInRole { get; set; }
    }
    public class RoleListOfUser
    {
        public Guid UserId { get; set; }
        public List<RoleOfUser> RoleList { get; set; }
        public Guid ApplicationId { get; set; }
    }
    public class UserInArea
    {
        public Guid? Id { get; set; }
        public Guid? UserId { get; set; }
        public Guid? ApplicationId { get; set; }
        public Guid ProvinceId { get; set; }
        public string ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public Guid? DistrictId { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }

        public Guid? WardId { get; set; }
        public string WardCode { get; set; }
        public string WardName { get; set; }

        public Guid? AddressId { get; set; }
        public string AddressCode { get; set; }
        public string AddressName { get; set; }
        public int Level { get; set; }
        public string FullAddress { get; set; }

        public Guid CreatedByUserId { get; set; }
        public string CreatedByUserFullName { get; set; }
    }

    //HaNH
    public class RightsOfUser
    {
        public long Id { get; set; }
        public Guid UserId { get; set; }
        public string RightCode { get; set; }
        public string InheritedFromRoles { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool Inherited { get; set; }
        public bool Enable { get; set; }
        public Guid ApplicationId { get; set; }
    }
    public class DeleteRoleOfUserModel
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public Guid ApplicationId { get; set; }
    }

    public class RightInUserModel
    {
        public Guid UserId { get; set; }
        public string RightCode { get; set; }
        public Guid? ApplicationId { get; set; }
        public List<string> LstRightCode { get; set; }

    }

    public class UsersInOrganizationModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public Guid? CreatedByUserId { get; set; }

        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }

        public DateTime? LastModifiedOnDate { get; set; }

        public string Role { get; set; }
    }
    // ManhNX
    public class UserRegisterModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsApproved { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string Mobile { get; set; }
        public DateTime Birthday { get; set; }
        public int? Type { get; set; } = 0;
        public string ApplicationId { get; set; }
        //base
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        //Workflow
        public string isFinished { get; set; }
        public string SchemeName { get; set; }
        public Guid? WorkflowProcessUserId { get; set; }
        public string WorkflowProcessUserName { get; set; }
    }
    public class UserCreateByQTHTModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsApproved { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string Mobile { get; set; }
        public DateTime Birthday { get; set; }
        public int? Type { get; set; } = 0;
        public string ApplicationId { get; set; }
        public List<Guid> RoleList { get; set; }
        //base
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        //Workflow
        public string isFinished { get; set; }
        public string SchemeName { get; set; }
        public Guid? WorkflowProcessUserId { get; set; }
        public string WorkflowProcessUserName { get; set; }
        public string FinanceCode { get; set; }
        public string ChucVu { get; set; }
    }
    public class UserRegisterFilterModel : PaginationRequest
    {
        public bool? IsLockedOut { get; set; }
        public int? Type { get; set; } = 0;
        public string WFStatus { get; set; }
    }
    public class UserUpdateByQTHTModel
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsApproved { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string Mobile { get; set; }
        public DateTime Birthday { get; set; }
        public int? Type { get; set; }
        public string ApplicationId { get; set; }
        public List<Guid> RoleList { get; set; }
        //base
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        //Workflow
        public string SchemeName { get; set; }
        public Guid? WorkflowProcessUserId { get; set; }
        public string WorkflowProcessUserName { get; set; }
    }
    public class CheckMailModel
    {
        public Guid UserId { get; set; }
        public string Email { get; set; }
    }


    public class EmailBodyModel
    {
        public string LAST_MODIFIED_ON_DATE { get; set; }
        public string NEW_PASSWORD { get; set; }

    }


    public class RegisterModel
    {
        public string USER_NAME { get; set; }


    }

    public class CreatePositionModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid PositionId { get; set; }
    }

}