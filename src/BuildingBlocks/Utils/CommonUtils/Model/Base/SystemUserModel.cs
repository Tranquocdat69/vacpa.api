﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonUtils
{
    public class BaseUserModel
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Role { get; set; }
        //new Model
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class SystemUserModel : BaseUserModel, IEquatable<SystemUserModel>
    {
        public string Password { get; set; }
        public string NickName { get; set; }
        public string ShortName { get; set; }
        public string Email { get; set; }
        public string OtherEmail { get; set; }
        public bool Active { get; set; }
        public string Avatar { get; set; }
        public string Mobile { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateBy { get; set; }
        public List<SystemRoleModel> Roles { get; set; }
        public List<SystemRightModel> Rights { get; set; }
        public List<UserProfileBaseModel> Profiles { get; set; }

        public UserOrganization UserOrganization { get; set; }
        public string OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string ParentOrganizationId { get; set; }
        public string ParentOrganizationName { get; set; }
        public string ChucVu { get; set; }
        public List<PositionModel> Positions { get; set; }
        //public ReaderProfileModel ReaderProfile { get; set; }
        public bool Result { get; set; }
        public string Message { get; set; }
        public string ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string FinanceCode { get; set; }
        //public ParentOrganizationModel ParentOrganization { get; set; }
        public bool Equals(SystemUserModel other)
        {
            return this.UserId == other.UserId &&
               this.UserName == other.UserName &&
               this.FullName == other.FullName;
        }
        public string SignFinalPath { get; set; }
        public int? OrgOrder { get; set; }
        public string Ca { get; set; }
        public int Type { get; set; }
        public bool IsLockedOut { get; set; }

        //base
        public string CreatedByFullName { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public string LastModifiedByFullName { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
    }

    public class SystemUserDetail
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string OUName { get; set; }
        public string DepartmentName { get; set; }
        public string ApplicationName { get; set; }
        public DateTime BirthDay { get; set; }
        public string Gender { get; set; }
        public string ChucVu { get; set; }
    }
    public class SystemUserProfile
    {
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public string PIN { get; set; }
        public string FinanceCode { get; set; }
    }

    public class ChangePassModel
    {
        public string OrganizationnalUnit { get; set; }
        public string NewPassword { get; set; }
        public string CurrentPassword { get; set; }
    }

    public class SystemUserQueryObject
    {
        public string RightCode { get; set; }
        public string RoleId { get; set; }
        public string RoleCode { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }

        public string ApplicationId { get; set; }

        public string TextSearch { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public bool? State { get; set; }
        public bool IsIncludeRightsAndRoles { get; set; }
        public Guid? OrganizationId { get; set; }
        public bool? IsDonVi { get; set; }
        public int? Type { get; set; }
    }

    public class SystemUserDeleteModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public bool Result { get; set; }
        public string Message { get; set; }
    }

    public class RoleOfSystemUserModel
    {
        public string UserId { get; set; }
        public string ApplicationId { get; set; }
        public Guid OrgId { get; set; }
        public List<SystemRoleModel> Roles { get; set; }
    }

    public class RolesOfUserFunctionModel
    {
        public string FunctionName { get; set; }
        public bool Result { get; set; }
        public string UserId { get; set; }
        public SystemRoleModel Role { get; set; }
        public string ApplicationId { get; set; }
    }

    public class RightOfUserFunctionModel
    {
        public string FunctionName { get; set; }
        public bool Result { get; set; }
        public string UserId { get; set; }
        public SystemRightModel Right { get; set; }
        public List<SystemRoleModel> RoleDepens { get; set; }
    }
    public class PositionOfUserFunctionModel
    {
        public string FunctionName { get; set; }
        public bool Result { get; set; }
        public string UserId { get; set; }
        public PositionModel Position { get; set; }
        public string ApplicationId { get; set; }
    }
    public class EnableDisableRightOfSystemRoleModel
    {
        public string UserId { get; set; }
        public string RightCode { get; set; }
        public bool Enable { get; set; }
    }

    public class CheckRoleOfUserModel
    {
        public string RoleId { get; set; }
        public string UserId { get; set; }
        public bool Result { get; set; }
        public string Message { get; set; }
        public string ApplicationId { get; set; }
    }

    public class SystemUserTreeView
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsCheck { get; set; }
        public bool IsChild { get; set; }
        public bool IsOrganization { get; set; }
        public int Level { get; set; }
        public SystemUserModel UserModel { get; set; }
        public List<SystemUserTreeView> SubChild { get; set; }
    }

    public partial class UserProfileBaseModel
    {
        public long Id { get; set; }
        public string PropertyNames { get; set; }
        public string PropertyValuesString { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public Guid UserId { get; set; }
    }
    //them chu ki vao day
    public class UserProfileSignatureModel
    {
        public int SignTypeFull { get; set; }
        public string SignFullPath { get; set; }
        public string SignFinalPath { get; set; }
        public int SignTypeShort { get; set; }
        public string SignShortPath { get; set; }
        public string LogoPath { get; set; }
        public int SignDisplay { get; set; }
        public int SignBy { get; set; }
        public string Ca { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public Guid UserId { get; set; }
        public string Base64 { get; set; }

    }


    public class PositionModelBase
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    /**Model */
    public class PositionModel : PositionModelBase
    {
        public string Note { get; set; }
        public bool Status { get; set; }
        public int Order { get; set; }
    }

    public class SystemRoleModel : IEquatable<SystemRoleModel>
    {
        public string RoleId { get; set; }
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
        public int RoleMember { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public List<SystemUserModel> Users { get; set; }
        public List<SystemRightModel> Rights { get; set; }
        public string Description { get; set; }
        public Guid ApplicationId { get; set; }
        public Guid CreatedByUserId { get; set; }
        public bool Equals(SystemRoleModel other)
        {
            return this.RoleId == other.RoleId &&
             this.RoleCode == other.RoleCode &&
             this.RoleName == other.RoleName;
        }
    }

    public class RightBaseModel
    {
        public string RightCode { get; set; }
        public string RightName { get; set; }
        public int Level { get; set; }
    }

    public class SystemRightModel : RightBaseModel, IEquatable<SystemRightModel>
    {

        public string Description { get; set; }
        public int Order { get; set; }
        public bool Enabled { get; set; }
        public bool Status { get; set; }
        public bool IsGroup { get; set; }
        public string GroupCode { get; set; }
        public List<SystemRightModel> SubRights { get; set; }
        public List<SystemUserModel> Users { get; set; }
        public List<SystemRoleModel> Roles { get; set; }

        public bool Equals(SystemRightModel target)
        {
            return RightCode == target.RightCode &&
              this.RightName == target.RightName &&
              this.Order == target.Order &&
              this.Enabled == target.Enabled;
        }
    }

    public class UserOrganization
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public Guid? CreatedByUserId { get; set; }

        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }

        public DateTime? LastModifiedOnDate { get; set; }

        public string Role { get; set; }
    }


}