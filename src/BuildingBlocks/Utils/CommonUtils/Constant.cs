﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CommonUtils
{
    public class ClaimConstants
    {
        public static string USER_ID = "x-userId";
        public static string USER_NAME = "x-userName";
        public static string APP_ID = "x-appId";
        public static string APPS = "x-app";
        public static string ROLES = "x-role";
        public static string RIGHT = "x-right";
        public static string ISSUED_AT = "x-iat";
        public static string EXPIRES_AT = "x-exp";
        public static string CHANNEL = "x-channel";
        public static string PHONE = "x-phone";

        public static string FULL_NAME = "x-fullName";
        public static string AVATAR = "x-avatar";
        public static string RIGHTS = "x-rights";
        public static string AREAS = "x-areas";
    }
    public class AppConstants
    {
        public static string EnvironmentName = "production";
        public static Guid HO_APP => new Guid("00000000-0000-0000-0000-000000000001");

        public static string SOCKETIO_STATUS_CONNECT = "42";
    }
    public class SocketEventConstants
    {
        public static string PRENEWS = "score-prenews";
        public static string SCORE = "score-update";
        public static string DGLD = "dgld-update";
    }
    public class ResApiTarget
    {
        public static string Self = "1";
        public static string Ipfm = "0";

        public static string Dhsx = "2";
    }
    public class AppChanel
    {
        public static int WebAdmin = 1;
        public static int Mobile = 0;
    }
    public class UserConstants
    {
        public static Guid AdministratorId => new Guid("00000000-0000-0000-0000-000000000001");
        public static Guid UserId => new Guid("00000000-0000-0000-0000-000000000002");
        public static int IsSuperAdmin = -1;
        public static int IsUser = 0;
        public static int IsStaff = 1;
    }
    public class QueueConstants
    {

    }
    public class RoleConstants
    {

    }

    public class RightConstants
    {
        public static Guid AccessAppId => new Guid("00000000-0000-0000-0000-000000000001");
        public static string AccessAppCode = "TRUY_CAP_HE_THONG";
        public static Guid DefaultAppId => new Guid("00000000-0000-0000-0000-000000000002");
        public static string DefaultAppCode = "TRUY_CAP_MAC_DINH";
        public static string VIEW = "VIEW";

    }
    public class NavigationConstants
    {
    }
    public class TypeOfSource
    {
        public static int TAPTIN = 1;
        public static int MIC = 2;
        public static int VANBAN = 3;
        public static int TIEPSONG = 4;
        public static int TBSH = 5;
    }
    public class WFCurrentState
    {
        public static string CHODUYET = "chờ duyệt";
        public static string DADUYET = "đã duyệt";
        public static string DAGUI = "đã gửi";
        public static string HUYDUYET = "hủy duyệt";
        public static string BITUCHOI = "bị từ chối";
        public static string SUALAI = "bị từ chối";
        public static string DANGSOANTHAO = "đang soạn thảo";
    }

    // truyền dữ liệu từ hệ thống SOA sang MSA
    public static class SyncDataSOAToMSA
    {
        #region QTHT
        public static string APPLICATION = "SOAToMSA.Application";
        public static string ORGANIZATION = "SOAToMSA.Organization";
        public static string RIGHT = "SOAToMSA.Rights2";
        public static string ROLE = "SOAToMSA.Roles2";
        public static string RIGHTINROLE = "SOAToMSA.RightInRoles2";
        public static string USER = "SOAToMSA.Users2";
        public static string RIGHTOFUSER = "SOAToMSA.RightOfUser2";
        public static string USERINORGANIZATION = "SOAToMSA.UserInOrganization";
        public static string USERINROLE = "SOAToMSA.UserInRole";
        public static string NAVIGATION = "SOAToMSA.Navigation";
        public static string NAVIGATIONROLE = "SOAToMSA.NavigationRole";
        public static string CREATE_USER = "SOAToMSA.CreateUser";
        public static string UPDATE_USER = "SOAToMSA.UpdateUser";
        #endregion

        #region
        public static string PRENEWS = "SOAToMSA.Prenews";
        public static string PRENEWSREL = "SOAToMSA.PrenewsRelationShip";
        public static string PRENEWSRELITEM = "SOAToMSA.PrenewsRelationShipItem";
        public static string PRENEWSRELPRODUCT = "SOAToMSA.PrenewsRelationShipProduct";

        public static string PRENEWSEKIP = "SOAToMSA.PrenewsEkip";
        public static string SCHEDULEPRENEWS = "SOAToMSA.SchedulePrenews";
        public static string SCHEDULEPRENEWSHUMAN = "SOAToMSA.SchedulePrenewsHuman";
        public static string SCHEDULEPRENEWSPRODUCT = "SOAToMSA.SchedulePrenewsProduct";

        #endregion
        #region Catalog
        public static string POSITION = "SOAToMSA.Position";
        public static string USERPOSITIONREL = "SOAToMSA.UserPositionRel";
        public static string TAXONOMYVOCABULARY = "SOAToMSA.TaxonomyVocabulary";
        public static string CATALOGITEM = "SOAToMSA.CatalogItem";
        public static string TAXONOMYTERM = "SOAToMSA.TaxonomyTerm";
        public static string RESOURCES = "SOAToMSA.Resources";
        public static string DUTY = "SOAToMSA.Duty";
        public static string DUTYTYPE = "SOAToMSA.DutyType";
        #endregion

        #region News
        public static string NEWS = "SOAToMSA.News";
        public static string NEWSREL = "SOAToMSA.NewsRelationShip";
        #endregion

        #region Plan
        public static string PRODUCTIONPLAN = "SOAToMSA.ProductionPlan";
        #endregion

        #region Cms
        public static string APPOINTMENT = "SOAToMSA.Appointment";
        #endregion
    }


    // truyền dữ liệu từ hệ thống MSA sang SOA
    public static class SyncDataMSAToSOA
    {
        #region QTHT
        public static string APPLICATION = "MSAToSOA.Application";
        public static string ORGANIZATION = "MSAToSOA.Organization";
        public static string RIGHT = "MSAToSOA.Rights";
        public static string ROLE = "MSAToSOA.Roles";
        public static string RIGHTINROLE = "Queue.RightInRoles";
        public static string USER = "Queue.Users";
        public static string RIGHTOFUSER = "MSAToSOA.RightOfUser";
        public static string USERINORGANIZATION = "MSAToSOA.UserInOrganization";
        public static string USERINROLE = "MSAToSOA.UserInRole";
        public static string NAVIGATION = "MSAToSOA.Navigation";
        public static string NAVIGATIONROLE = "MSAToSOA.NavigationRole";
        public static string CREATE_USER = "MSAToSOA.CreateUser";
        public static string UPDATE_USER = "MSAToSOA.UpdateUser";
        #endregion


        #region Catalog
        public static string POSITION = "MSAToSOA.Position";
        public static string USERPOSITIONREL = "MSAToSOA.UserPositionRel";
        public static string CATALOGITEM = "MSAToSOA.CatalogItem";


        public static string RESOURCES = "MSAToSOA.Resources";

        #endregion

        #region Plan
        public static string PRODUCTIONPLAN = "MSAToSOA.ProductionPlan";
        #endregion

        #region Cms
        public static string APPOINTMENT = "MSAToSOA.Appointment";
        #endregion
    }

    public static class RabbitTopic
    {
        public static string User = "USER_TOPIC";
    }

}