﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;



namespace CommonUtils
{
    public enum MembershipPasswordFormat
    {
        //
        // Summary:
        //     Passwords are not encrypted.
        Clear = 0,
        //
        // Summary:
        //     Passwords are encrypted one-way using the SHA1 hashing algorithm.
        Hashed = 1,
        //
        // Summary:
        //     Passwords are encrypted using the encryption settings determined by the machineKey
        //     Element (ASP.NET Settings Schema) element configuration.
        Encrypted = 2
    }

    public class PasswordRender
    {
        public MembershipPasswordFormat passwordFormat;

        
        public string RandomString(int size, bool lowerCase)
        {
            var builder = new StringBuilder();
            var random = new Random();
            for (int i = 0; i < size; i++)
            {
                char ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }
        public string CreateSalt512()
        {
            var message = RandomString(512, false);
            return BitConverter.ToString((new SHA512Managed()).ComputeHash(Encoding.ASCII.GetBytes(message))).Replace("-", "");
        }
        public string GenerateHMAC(string clearMessage, string secretKeyString)
        {
            var encoder = new ASCIIEncoding();
            var messageBytes = encoder.GetBytes(clearMessage);
            var secretKeyBytes = new byte[secretKeyString.Length / 2];
            for (int index = 0; index < secretKeyBytes.Length; index++)
            {
                string byteValue = secretKeyString.Substring(index * 2, 2);
                secretKeyBytes[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }
            var hmacsha512 = new HMACSHA512(secretKeyBytes);
            byte[] hashValue = hmacsha512.ComputeHash(messageBytes);
            string hmac = "";
            foreach (byte x in hashValue)
            {
                hmac += String.Format("{0:x2}", x);
            }
            return hmac.ToUpper();
        }

        public new MembershipPasswordFormat PasswordFormat
        {
            get
            {
                return passwordFormat;
            }
        }    
    }
}
