﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ApiUtils;
using CommonUtils;

namespace Business
{
    public class CacheService : ICacheService
    {
        private readonly ILogger<CacheService> _logger;

        public CacheService(ILogger<CacheService> logger)
        {
            _logger = logger;

        }

        public async Task<T> GetOrCreate<T>(string key, Func<Task<T>> func)
        {
            try
            {
                var cache = RedisConnectorHelper.Connection.GetDatabase();
                var startTime = DateTime.Now;
                var enable = Utils.GetConfig("Redis:Enable");
                if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(enable))
                {
                    var ci = await func();
                    //await cache.StringSetAsync(key, JsonConvert.SerializeObject(ci), TimeSpan.FromHours(24));
                    return ci;
                }
                else
                {
                    try
                    {
                        if (await cache.KeyExistsAsync(key))
                        {
                            var r = await Task.Run(() => JsonConvert.DeserializeObject<T>(cache.StringGet(key)));
                            Console.WriteLine("GetOrCreate cached " + (DateTime.Now - startTime).TotalMilliseconds);
                            return r;
                        }
                        else
                        {
                            Console.WriteLine("GetOrCreate non-cached " + (DateTime.Now - startTime).TotalMilliseconds);
                            var ci = await func();
                            try
                            {
                                await cache.StringSetAsync(key, JsonConvert.SerializeObject(ci), TimeSpan.FromHours(168));
                            }
                            catch { }
                            return ci;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("GetOrCreate error non-cached " + (DateTime.Now - startTime).TotalMilliseconds);
                        var ci = await func();
                        try
                        {
                            await cache.StringSetAsync(key, JsonConvert.SerializeObject(ci), TimeSpan.FromHours(168));
                        }
                        catch { }
                        return ci;
                    }

                }
            }
            catch { return await func(); }
        }

        public async Task<T> GetOrCreate<T>(string key, Func<Task<T>> func, int expireTime)
        {
            try
            {
                var cache = RedisConnectorHelper.Connection.GetDatabase();
                var startTime = DateTime.Now;
                var enable = Utils.GetConfig("Redis:Enable");
                if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(enable))
                {
                    var ci = await func();
                    //await cache.StringSetAsync(key, JsonConvert.SerializeObject(ci), TimeSpan.FromTicks(expireTime));
                    return ci;
                }
                else
                {
                    try
                    {
                        if (await cache.KeyExistsAsync(key))
                        {
                            var r = await Task.Run(() => JsonConvert.DeserializeObject<T>(cache.StringGet(key)));
                            Console.WriteLine("GetOrCreate cached " + (DateTime.Now - startTime).TotalMilliseconds);
                            return r;
                        }
                        else
                        {
                            Console.WriteLine("GetOrCreate non-cached " + (DateTime.Now - startTime).TotalMilliseconds);
                            var ci = await func();
                            try
                            {
                                await cache.StringSetAsync(key, JsonConvert.SerializeObject(ci), TimeSpan.FromHours(expireTime));
                            }
                            catch { }
                            return ci;
                        }
                    }
                    catch (Exception e)
                    {
                        //_logger.LogError(e)
                        var ci = await func();
                        try
                        {
                            await cache.StringSetAsync(key, JsonConvert.SerializeObject(ci), TimeSpan.FromHours(expireTime));
                        }
                        catch { }
                        Console.WriteLine("GetOrCreate error non-cached " + (DateTime.Now - startTime).TotalMilliseconds);
                        return ci;
                    }

                }
            }
            catch { return await func(); }
        }

        public async void Remove(string key)
        {
            try
            {
                var cache = RedisConnectorHelper.Connection.GetDatabase();
                try
                {
                    if (await cache.KeyExistsAsync(key))
                    {
                        await cache.KeyDeleteAsync(key);
                    }
                }
                catch (System.Exception)
                {
                    var enable = Utils.GetConfig("Redis:Enable");
                    if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(enable))
                    {

                    }
                    else if (await cache.KeyExistsAsync(key))
                    {
                        await cache.KeyDeleteAsync(key);
                    }
                }
            }
            catch
            { }
        }

        public async void Set<T>(string key, T cacheEntry, int expireTime = 0)
        {
            try
            {
                var cache = RedisConnectorHelper.Connection.GetDatabase();
                if (expireTime != 0)
                {
                    try
                    {
                        await cache.StringSetAsync(key, JsonConvert.SerializeObject(cacheEntry), TimeSpan.FromHours(expireTime));
                    }
                    catch { }

                }
                else
                {
                    try
                    {
                        await cache.StringSetAsync(key, JsonConvert.SerializeObject(cacheEntry));
                    }
                    catch { }
                }
            }
            catch
            { }
        }
    }
}
