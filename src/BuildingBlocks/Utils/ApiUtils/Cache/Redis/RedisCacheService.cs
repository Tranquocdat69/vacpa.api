﻿using ApiUtils;
using ApiUtils.Cache;
using Business;
using CommonUtils;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Serilog;
using StackExchange.Redis;
using System;
using System.Text.Json;
using System.Threading.Tasks;
using JsonSerializer = System.Text.Json.JsonSerializer;
//using JsonSerializer = Newtonsoft.Json.JsonSerializer;


namespace Business
{
    public class RedisCacheService : ICacheService, IDistributedCacheService
    {
        private readonly IDistributedCache _distributedCache;
        private readonly IConnectionMultiplexer _redisConnectionMultiplexer;
        private readonly IDatabase _redisDatabase;
        private readonly ILogger<RedisCacheService> _logger;

        // Cache expire trong 300s
        private int timeLive = 86400;

        public RedisCacheService(IDistributedCache distributedCache, ILogger<RedisCacheService> logger)
        {
            _distributedCache = distributedCache;
            _logger = logger;
            int.TryParse(Utils.GetConfig("redis:timeLive"), out timeLive);
            if (timeLive <= 0)
            {
                timeLive = 86400;
            }
        }

        public async Task<TItem> GetOrCreate<TItem>(string key, Func<Task<TItem>> createItem)
        {
            TItem cacheEntry;
            try
            {
                var itemString = _distributedCache.GetString(key);
                if (string.IsNullOrEmpty(itemString))
                {
                    // Cache expire trong 300s
                    var options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(timeLive));

                    cacheEntry = await createItem();
                    var item = JsonConvert.SerializeObject(cacheEntry);

                    // Nạp lại giá trị mới cho cache
                    _distributedCache.SetString(key, item, options);

                    // var dataCache = _distributedCache.GetString(key);
                }
                else
                {
                    try
                    {
                        cacheEntry = JsonConvert.DeserializeObject<TItem>(itemString);
                    }
                    catch (Exception)
                    {
                        var cache = RedisConnectorHelper.Connection.GetDatabase();
                        cacheEntry = JsonConvert.DeserializeObject<TItem>(cache.StringGet(key));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Có lỗi xảy ra khi lấy dữ liệu dừ redis 1");
                try
                {
                    var cache = RedisConnectorHelper.Connection.GetDatabase();
                    cacheEntry = JsonConvert.DeserializeObject<TItem>(cache.StringGet(key));
                }
                catch (Exception)
                {
                    Console.WriteLine($"Có lỗi xảy ra khi lấy dữ liệu dừ redis 2");
                    cacheEntry = await createItem();
                }
            }
            return cacheEntry;
        }
        public async Task<TItem> GetOrCreate<TItem>(string key, Func<TItem> createItem)
        {
            TItem cacheEntry;
            try
            {
                var itemString = _distributedCache.GetString(key);
                if (string.IsNullOrEmpty(itemString))
                {
                    // Cache expire trong 300s
                    var options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(timeLive));

                    cacheEntry = createItem();
                    var item = JsonConvert.SerializeObject(cacheEntry);

                    // Nạp lại giá trị mới cho cache
                    await _distributedCache.SetStringAsync(key, item, options);

                    // var dataCache = _distributedCache.GetString(key);
                }
                else
                {
                    cacheEntry = JsonConvert.DeserializeObject<TItem>(itemString);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Có lỗi xảy ra khi lấy dữ liệu dừ redis");
                cacheEntry = createItem();
            }
            return cacheEntry;
        }

        public async Task<T> GetOrCreate<T>(string key, Func<Task<T>> funcToRun, int expireTime)
        {
            T cacheEntry;
            try
            {
                var itemString = _distributedCache.GetString(key);
                if (string.IsNullOrEmpty(itemString))
                {
                    // Cache expire trong 300s
                    var options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(expireTime));

                    cacheEntry = await funcToRun();
                    var item = JsonConvert.SerializeObject(cacheEntry);

                    // Nạp lại giá trị mới cho cache
                    _distributedCache.SetString(key, item, options);

                    // var dataCache = _distributedCache.GetString(key);
                }
                else
                {
                    cacheEntry = JsonConvert.DeserializeObject<T>(itemString);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Có lỗi xảy ra khi lấy dữ liệu dừ redis");
                cacheEntry = await funcToRun();
            }
            return cacheEntry;
        }

        public T HashGet<T>(string key, string field)
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            try
            {
                RedisValue redisValue = cache.HashGet(key, field);
                if (redisValue.HasValue)
                    return JsonSerializer.Deserialize<T>(redisValue);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
            return default;
           
        }

        public void HashSet<T>(string key, string field, T value)
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            bool result = cache.HashSet(key, field, JsonSerializer.Serialize<T>(value));
            if (result)
                _logger.LogInformation($"Field {field} was added");
            else
                _logger.LogInformation($"Field: {field} was updated");
        }

        public void Remove(string key)
        {
            _distributedCache.Remove(key);
        }

        public void Set<T>(string key, T cacheEntry, int expireTime = 0)
        {
            throw new NotImplementedException();
        }
    }

}
