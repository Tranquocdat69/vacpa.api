﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using ApiUtils;
using Business;
using CommonUtils;
using Microsoft.Extensions.Caching.Distributed;

namespace ApiUtils
{
    public class UserCollection
    {
        public HashSet<SystemUserModel> Collection;
        private static ICacheService _cache;

        protected UserCollection()
        {
        }

        public void setServiceRunService(ICacheService cache)
        {
            _cache = cache;
        }

        public static UserCollection Instance { get; } = new UserCollection();

        public async void LoadToHashSet()
        {

            Collection = new HashSet<SystemUserModel>();
            var enable = Utils.GetConfig("Redis:Disable");
            try
            {
                Thread.Sleep(1000);
                getRedisCache("DHSX-USERS");
            }
            catch (Exception ex)
            {
                Console.WriteLine("DB cache.StringGet(DHSX-USERS, stringCache): " + ex.Message);

                var listResponse = await getFromEndpoint();

                setMemoryCache(listResponse);
                //setRedisCache("DHSX-USERS");
            }
        }

        private void setMemoryCache(List<SystemUserModel> list)
        {
            var unew = new HashSet<SystemUserModel>();
            if (list != null && list.Count > 0)
            {
                // Add to hashset

                foreach (var o in list)
                {
                    unew.Add(o);
                }

                Collection.Clear();
                Collection = unew;
            }
        }
        private void setRedisCache(string key, int count = 1)
        {
            if (count == 3)
            {
                return;
            }
            else
            {
                try
                {
                    var stringCache = System.Text.Json.JsonSerializer.Serialize(Collection);
                    var cache = RedisConnectorHelper.Connection.GetDatabase();
                    cache.StringSet(key, stringCache);
                    //cache.HashSetAsync(key+1, users);
                    Console.WriteLine("cache.StringSet(" + key + ", stringCache)");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error Set cache " + ex.Message + " " + count);
                    setRedisCache(key, count = count + 1);
                }
            }
        }
        private async void getRedisCache(string key)
        {
            var unew = new HashSet<SystemUserModel>();
            var rUsers = new List<SystemUserModel>();
            var time = DateTime.Now;

            if (_cache != null)
            {
                //var cr = await _distributedCache.GetStringAsync(key);
                rUsers = await _cache.GetOrCreate(key, async () =>
                {
                    return await getFromEndpoint();
                });
            }
            else
            {
                var cache = RedisConnectorHelper.Connection.GetDatabase(); 
                rUsers = JsonSerializer.Deserialize<List<SystemUserModel>>(cache.StringGet(key));
            }

            Console.WriteLine("cache.StringGet(" + key + ", stringCache)");
            foreach (var response in rUsers)
                unew.Add(response);

            Collection.Clear();
            Collection = unew;
        }

        // get cache from api local
        private async Task<List<SystemUserModel>> getFromEndpoint()
        {
            var resApiHandler = new ResApiHandler<List<SystemUserModel>, dynamic>(CommonUtils.ResApiTarget.Dhsx);
            var result = await resApiHandler.Get(Utils.GetConfig("adminservice:apiuri") + "api/users/byposition/0");
            return result;
        }

        public string GetName(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result?.Name;
        }

        public SystemUserModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            if (result == null)
            {
                result = new SystemUserModel();
            }
            return result;
        }

        public bool Check()
        {
            try
            {
                if (Collection != null && Collection.Count > 0)
                {
                    return true;
                }else{
                    LoadToHashSet();
                }
                return false;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public SystemUserModel GetUserByUserName(string sub)
        {
            var result = Collection.FirstOrDefault(u => u.UserName == sub);
            if (result == null)
            {
                result = new SystemUserModel();
            }
            return result;
        }

        public SystemUserModel GetUserByFinanceCode(string sub)
        {
            var result = Collection.FirstOrDefault(u => u.FinanceCode == sub);
            if (result == null)
            {
                result = new SystemUserModel();
            }
            return result;
        }

        public bool CheckUserActive(string sub)
        {
            var result = Collection.FirstOrDefault(u => u.UserName == sub);
            if (result != null && !result.Active)
            {
                return true;
            }else{
                return false;
            }
        }

        public SystemUserModel GetUserById(Guid userId)
        {
            var user = Collection.FirstOrDefault(u => u.UserId == userId);
            if (user == null)
            {
                user = new SystemUserModel();
            }
            return user;
        }

        public SystemUserModel GetUserByIdAllowNull(Guid? userId)
        {
            var user = Collection.FirstOrDefault(u => u.UserId == userId);
            if (user == null)
            {
                user = new SystemUserModel();
            }
            return user;
        }

        public List<SystemUserModel> GetAllUserByAppId(string appId)
        {
            var user = from s in Collection.ToList() where s.ApplicationId == appId && s.IsLockedOut == false select s;
             
            if (user == null)
            {
                user = new List<SystemUserModel>();
            }
            return user.ToList();
        }

        public bool CheckUserActiveV2(string sub)
        {
            var result = Collection.FirstOrDefault(u => u.UserName == sub);
            if (result != null && result.Active)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}