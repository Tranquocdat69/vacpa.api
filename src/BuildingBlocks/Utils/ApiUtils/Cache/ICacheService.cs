﻿using ApiUtils;
using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Business
{
    public interface ICacheService
    {
        void Remove(string key);
        void Set<T>(string key, T cacheEntry, int expireTime = 0);
        Task<T> GetOrCreate<T>(string key, Func<Task<T>> funcToRun);
        Task<T> GetOrCreate<T>(string key, Func<Task<T>> funcToRun, int expireTime);
    }
}
