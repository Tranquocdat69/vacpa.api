﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiUtils.Cache
{
    public interface IDistributedCacheService
    {
        T HashGet<T>(string key, string field);

        void HashSet<T>(string key, string field, T value);
    }
}
