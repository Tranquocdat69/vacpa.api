﻿using CommonUtils;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace ApiUtils
{
    public class RedisConnectorHelper
    {
        //static string redisCNN = Configuration.GetSection("Redis")["ConnectionString"];
        static RedisConnectorHelper()
        {
            try
            {
                RedisConnectorHelper.lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
                {
                    return ConnectionMultiplexer.Connect(Utils.GetConfig("Redis:ConnectionString"));
                });
            }
            catch (Exception)
            {
                //return null;
            }
            
        }

        private static Lazy<ConnectionMultiplexer> lazyConnection;

        public static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }
    }
}
