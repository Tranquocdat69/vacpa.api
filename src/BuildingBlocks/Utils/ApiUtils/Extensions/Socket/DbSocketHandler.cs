﻿using System;
using CommonUtils;
using Newtonsoft.Json;
using Socket.Io.Client.Core;
using Socket.Io.Client.Core.Model;
using WebSocket4Net;

namespace ApiUtils
{
    public class DbSocketHandler : ISocketHandler

    {
        private readonly string _socketUri = Utils.GetConfig("AppSettings:SocketServerUri");
        //private readonly ISocketIoClient _socketHandler;
        public DbSocketHandler()
        {
            //_socketHandler = socketHandle;

            //var r = new SocketIoClient();
            //var jwtHandler = new JWTHelper();
            //var token = jwtHandler.BuildStaticToken(new UserModel { UserId = Guid.NewGuid() });
            //var options = new SocketIoOpenOptions("");

            //var someEventSubscription = r.On("updatesignin")
            ////.Throttle(TimeSpan.FromSeconds(1)) //optional
            //.Subscribe(message =>
            //{
            //    Console.WriteLine($"Received event: {message.EventName}. Data: {message.Data}");
            //});

            ////var _socketUri = configuration["AppSettings:SocketServerUri"];
            //r.OpenAsync(new Uri("http://" + _socketUri + "?token=" + token), options).Wait();

        }
        public void Send(string[] data)
        {
            try
            {
                var startTime = DateTime.Now;
                var socketUri = _socketUri;
                var jwtHandler = new JWTHelper();
                var token = jwtHandler.BuildStaticToken(new UserModel { UserId = Guid.NewGuid() });

                using (var ws = new WebSocket("ws://" + socketUri + "/socket.io/?EIO=2&transport=websocket&token=" + token))
                {

                    ws.Opened += (sender, e) =>
                      Console.WriteLine("New message from controller: " + e);
                     
                    ws.Error += (sender, e) =>
                      Console.WriteLine("Error message from controller: " + e);

                    ws.DataReceived += (sender, e) =>
                     Console.WriteLine("DataReceived message from controller: " + e);

                    ws.Closed += (sender, e) =>
                     Console.WriteLine("Closed message from controller: " + e);

                    ws.MessageReceived += (sender, e) =>
                     Console.WriteLine("Server said: " + e);


                    ws.OpenAsync().Wait();
                    // model.id = id.ToString();

                    var r = data;
                    var mesage = AppConstants.SOCKETIO_STATUS_CONNECT + JsonConvert.SerializeObject(r);
                    ws.Send(mesage);
                    ws.CloseAsync().Wait();
                    // Serilog.Log.Information("UpdateSign_v2.6.Update Realtime" + (DateTime.Now - startTime).TotalMilliseconds);
                }
            }
            catch (System.Exception ex)
            {
                var x = ex;
            }
        }

        public void Send<TData>(string eventName, TData data)
        {
            try
            {
                //_socketHandler.Emit(eventName, data);
            }
            catch (System.Exception ex)
            {
                var x = ex;
                Console.WriteLine(ex);
                Serilog.Log.Error("", ex);
            }
        }
    }
}