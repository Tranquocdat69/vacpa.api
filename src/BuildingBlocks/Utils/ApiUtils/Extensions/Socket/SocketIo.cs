﻿using CommonUtils;
using Microsoft.Extensions.Configuration;
using Socket.Io.Client.Core;
using Socket.Io.Client.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiUtils
{
    public class SocketIo
    {
        public SocketIo() { }
        public static SocketIoClient Init(IConfiguration configuration)
        {
            var r = new SocketIoClient();
            var jwtHandler = new JWTHelper();
            var token = jwtHandler.BuildStaticToken(new UserModel { UserId = Guid.NewGuid() });
            var options = new SocketIoOpenOptions("");

            var someEventSubscription = r.On("updatesignin")
            //.Throttle(TimeSpan.FromSeconds(1)) //optional
            .Subscribe(message =>
            {
                Console.WriteLine($"Received event: {message.EventName}. Data: {message.Data}");
            });

            var _socketUri = configuration["AppSettings:SocketServerUri"];
            r.OpenAsync(new Uri("http://" + _socketUri + "?token=" + token), options).Wait();
            return r;
        }
    }
}
