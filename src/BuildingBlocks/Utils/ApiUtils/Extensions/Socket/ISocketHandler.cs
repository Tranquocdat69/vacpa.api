﻿namespace ApiUtils
{
    public interface ISocketHandler
    {
        void Send(string[] data);
        void Send<TData>(string eventName, TData data);
    }
}