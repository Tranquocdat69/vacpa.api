﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApiUtils {
    public interface IBus
    {
        Task SendAsync<T>(string queue, T message);
        Task ReceiveAsync<T>(string queue, Action<T> onMessage);
        Task SendTopicAsync<T>(string topic, T message);
        Task ReceiveTopicAsync<T>(string topic, Action<T> onMessage);
    }
    public class RabbitBus : IBus
    {
        private readonly IModel _channel;
        internal RabbitBus(IModel channel)
        {
            _channel = channel;
        }
        public async Task SendAsync<T>(string queue, T message)
        {
            await Task.Run(() =>
            {
                _channel.QueueDeclare(queue, true, false, false);
                var properties = _channel.CreateBasicProperties();
                properties.Persistent = false;
                var output = JsonConvert.SerializeObject(message);
                _channel.BasicPublish(string.Empty, queue, null,
                Encoding.UTF8.GetBytes(output));
            });
        }
        public async Task SendTopicAsync<T>(string topic, T message)
        {
            await Task.Run(() =>
            {
                _channel.ExchangeDeclare(exchange: topic,
                                    type: "topic");

                var routingKey = "anonymous.info";
                var output = JsonConvert.SerializeObject(message);
                var body = Encoding.UTF8.GetBytes(output);
                _channel.BasicPublish(exchange: topic,
                                     routingKey: routingKey,
                                     basicProperties: null,
                                     body: body);
                // Console.WriteLine(" [x] Sent '{0}':'{1}'", routingKey, message);
            });
        }
        public async Task ReceiveAsync<T>(string queue, Action<T> onMessage)
        {
            _channel.QueueDeclare(queue, true, false, false);
            var consumer = new AsyncEventingBasicConsumer(_channel);

            consumer.Received += async (s, e) =>
            {
                var jsonSpecified = Encoding.UTF8.GetString(e.Body);
                var item = JsonConvert.DeserializeObject<T>(jsonSpecified);
                onMessage(item);
                await Task.Yield();
            };
            _channel.BasicConsume(queue, true, consumer);
            await Task.Yield();
        }
        public async Task ReceiveTopicAsync<T>(string topic, Action<T> onMessage)
        {
            var routingKey = "anonymous.info";

            _channel.ExchangeDeclare(exchange: topic, type: "topic");
            var queueName = _channel.QueueDeclare().QueueName;

            _channel.QueueBind(queue: queueName,
                                  exchange: topic,
                                  routingKey: routingKey);

            Console.WriteLine(" [*] Waiting for messages. To exit press CTRL+C");

            var consumer = new AsyncEventingBasicConsumer(_channel);
            consumer.Received += async (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                var item = JsonConvert.DeserializeObject<T>(message);
                onMessage(item);
                //routingKey =;
                // Console.WriteLine(" [x] Received '{0}':'{1}'",
                //                    ea.RoutingKey,
                //                   message);
            };
            _channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);

            Console.WriteLine(" Press [enter] to exit.");
            await Task.Yield();
        }
    }

}
