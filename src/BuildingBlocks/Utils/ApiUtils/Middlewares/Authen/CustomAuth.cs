﻿using CommonUtils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace ApiUtils
{
    public static class CustomAuth
    {
        public static AuthenticationBuilder AddCustomAuth(this AuthenticationBuilder builder, Action<CustomAuthOptions> configureOptions)
        {
            return builder.AddScheme<CustomAuthOptions, CustomAuthHandler>("Custom Scheme", "Custom Auth", configureOptions);
        }
    }
    public class CustomAuthOptions : AuthenticationSchemeOptions
    {
        public CustomAuthOptions()
        {

        }
    }

    internal class CustomAuthHandler : AuthenticationHandler<CustomAuthOptions>
    {
        private readonly string _serviceValidate = Utils.GetConfig("Authentication:WSO2:Uri");
        private readonly string _clientId = Utils.GetConfig("Authentication:WSO2:Clientid");
        private readonly string _clientSecret = Utils.GetConfig("Authentication:WSO2:Secret");
        private readonly string _redirectUri = Utils.GetConfig("Authentication:WSO2:Redirecturi");
        private readonly string _timeToLive = Utils.GetConfig("Authentication:Jwt:TimeToLive");


        public CustomAuthHandler(IOptionsMonitor<CustomAuthOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
            // store custom services here...
        }
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            AuthenticateResult result = null;
            try
            {
                var encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(_clientId + ":" + _clientSecret));

                var startTime = DateTime.Now;
                string xpermissionHeader = Request.Headers["X-Permission-Basic"];
                if (Utils.GetConfig("Authentication:XPermission:Enable") == "true")
                {
                    if (xpermissionHeader != null)
                    {
                        // Get the token
                        var token = xpermissionHeader.Trim();
                        // validatetoken
                        var handerJwt = new JwtSecurityTokenHandler();
                        var tokenInfo = handerJwt.ReadJwtToken(token);
                        SecurityToken validatedToken;
                        handerJwt.ValidateToken(token, new TokenValidationParameters()
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = Utils.GetConfig("Authentication:Jwt:Issuer"),
                            ValidAudience = Utils.GetConfig("Authentication:Jwt:Issuer"),
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Utils.GetConfig(("Authentication:Jwt:Key"))))
                        }, out validatedToken);
                        if (validatedToken != null
                        && validatedToken.Issuer == Utils.GetConfig("Authentication:Jwt:Issuer")
                        && validatedToken.ValidFrom.CompareTo(DateTime.Now) != 1
                        && validatedToken.ValidTo.CompareTo(DateTime.Now) != -1)
                        {
                            var claimsJwt = new List<Claim>();
                            var userId = tokenInfo.Claims.Where(x => x.Type == ClaimConstants.USER_ID).FirstOrDefault().Value;
                            claimsJwt.Add(new Claim(ClaimConstants.USER_ID, userId.ToString()));
                            claimsJwt.AddRange(tokenInfo.Claims);
                            ClaimsIdentity claimsIdentityJwt = new ClaimsIdentity(claimsJwt, "Jwt");
                            ClaimsPrincipal claimsPrincipalJwt = new ClaimsPrincipal(claimsIdentityJwt);

                            result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipalJwt,
                                                new AuthenticationProperties(), "Jwt"));
                            return result;
                        }
                    }
                    return AuthenticateResult.Fail("Không xác thực");
                }
                string authHeader = Request.Headers["Authorization"];

                #region WSO2
                if (Utils.GetConfig("Authentication:WSO2:Enable") == "true")
                {
                    if (authHeader != null && authHeader.StartsWith("Bearer "))
                    {
                        string accessToken = Guid.NewGuid().ToString();
                        // Get the token
                        var token = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                        accessToken = token;
                        

                        var pertokenstring = accessToken.ToString().Trim();

                        bool isValid = Guid.TryParse(pertokenstring, out _);

                        if (!isValid)
                        {

                            #region check jwt
                            var atoken = "";
                            // validatetoken
                            var handerJwt = new JwtSecurityTokenHandler();
                            var tokenInfo = handerJwt.ReadJwtToken(pertokenstring);

                            var issuer = Utils.GetConfig("Authentication:Jwt:Issuer");
                            var key = Utils.GetConfig("Authentication:Jwt:Key");

                            SecurityToken validatedToken;
                            handerJwt.ValidateToken(pertokenstring, new TokenValidationParameters()
                            {
                                ValidateIssuer = true,
                                ValidateAudience = true,
                                ValidateLifetime = true,
                                ValidateIssuerSigningKey = true,
                                ValidIssuer = issuer,
                                ValidAudience = issuer,
                                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key))
                            }, out validatedToken);

                            if (validatedToken != null
                            && validatedToken.Issuer == issuer
                            && validatedToken.ValidFrom.CompareTo(DateTime.Now) != 1
                            && validatedToken.ValidTo.CompareTo(DateTime.Now) != -1)
                            {
                                var claimsJwt = new List<Claim>();
                                atoken = tokenInfo.Claims.Where(x => x.Type == "ACCESS_TOKEN").FirstOrDefault().Value;
                            }
                            else
                            {
                                Serilog.Log.Error("Mobile note BuildToken" + validatedToken.ValidFrom.ToString("HH:mm:ss dd/MM/yyy") + " --------- " + validatedToken.ValidTo.ToString("HH:mm:ss dd/MM/yyy"), "");

                                return AuthenticateResult.Fail("Chưa xác thực");
                            }

                            Serilog.Log.Information("I. HandleAuthenticateAsync success : " + (DateTime.Now - startTime).TotalMilliseconds + " ms");
                            #endregion

                            // // Get the token
                            // var token = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                            accessToken = atoken;

                            // if date time now - is haft of token time
                            var m = Convert.ToDouble(_timeToLive == "10" ? (int.Parse(_timeToLive)).ToString() : "10");
                            var eTime = DateTime.Now.AddMinutes(-m);

                            if (!isValid && validatedToken.ValidTo.CompareTo(eTime) != -1)
                            {
                                var claims = new List<Claim>();
                                claims.Add(new Claim(ClaimTypes.NameIdentifier, UserConstants.AdministratorId.ToString()));
                                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Bear");
                                ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                                result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal,
                                      new AuthenticationProperties(), "Bear"));
                                return result;
                            }
                            else
                            {
                                Uri refreshTokenUri = new Uri(_serviceValidate + "oauth2/userinfo")
                                                  .AddQuery("schema", "openid");

                                var webRequest = (System.Net.HttpWebRequest)WebRequest.Create(refreshTokenUri);
                                webRequest.Method = "POST";
                                webRequest.ContentType = "application/x-www-form-urlencoded";
                                webRequest.Accept = "application/json, text/javascript, */*";
                                webRequest.Headers.Add("Authorization", "Bearer " + accessToken);

                                using (var jsonResponse = await webRequest.GetResponseAsync())
                                {
                                    var jsonStream = jsonResponse.GetResponseStream();

                                    MemoryStream ms = new MemoryStream();
                                    jsonStream.CopyTo(ms);
                                    ms.Position = 0;
                                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                                    response.Content = new StreamContent(ms);
                                    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                                    await response.Content.ReadAsStringAsync();

                                    var claims = new List<Claim>();
                                    claims.Add(new Claim(ClaimTypes.NameIdentifier, UserConstants.AdministratorId.ToString()));
                                    ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Bear");
                                    ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                                    result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal,
                                             new AuthenticationProperties(), "Bear"));

                                    Serilog.Log.Information("I.1 W2 HandleAuthenticateAsync: " + (DateTime.Now - startTime).TotalMilliseconds + " ms");
                                    return result;
                                }
                            }
                        }
                        else
                        {
                            Uri refreshTokenUri = new Uri(_serviceValidate + "oauth2/userinfo");
                            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                            var webRequest = (System.Net.HttpWebRequest)WebRequest.Create(refreshTokenUri);
                            webRequest.Method = "POST";
                            webRequest.ContentType = "application/x-www-form-urlencoded";
                            webRequest.Accept = "application/json, text/javascript, */*";
                            webRequest.Headers.Add("Authorization", "Bearer " + accessToken);

                            using (var jsonResponse = await webRequest.GetResponseAsync())
                            {
                                var jsonStream = jsonResponse.GetResponseStream();

                                MemoryStream ms = new MemoryStream();
                                jsonStream.CopyTo(ms);
                                ms.Position = 0;
                                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                                response.Content = new StreamContent(ms);
                                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                                await response.Content.ReadAsStringAsync();

                                Console.WriteLine("BuildClaim:WSO2: " + (DateTime.Now - startTime).TotalMilliseconds);

                                var claimsWSO2 = new List<Claim>();
                                claimsWSO2.Add(new Claim(ClaimConstants.USER_ID, UserConstants.AdministratorId.ToString()));
                                ClaimsIdentity claimsIdentityWSO2 = new ClaimsIdentity(claimsWSO2, "Bear");
                                ClaimsPrincipal claimsPrincipalWSO2 = new ClaimsPrincipal(claimsIdentityWSO2);

                                result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipalWSO2,
                                        new AuthenticationProperties(), "Bear"));

                                Console.WriteLine("I.1 W2 HandleAuthenticateAsync: " + (DateTime.Now - startTime).TotalMilliseconds);

                                return result;
                            }
                        }
                    }
                }
                #endregion
                #region Basic
                if (Utils.GetConfig("Authentication:Basic:Enable") == "true")
                {
                    if (authHeader != null && authHeader.StartsWith("Basic "))
                    {
                        // Get the encoded username and password
                        var encodedUsernamePassword = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                        // Decode from Base64 to string
                        var decodedUsernamePassword = Encoding.UTF8.GetString(Convert.FromBase64String(encodedUsernamePassword));
                        // Split username and password
                        var username = decodedUsernamePassword.Split(':', 2)[0];
                        var password = decodedUsernamePassword.Split(':', 2)[1];
                        // Check if login is correct
                        if (username == Utils.GetConfig("Authentication:AdminUser") && password == Utils.GetConfig("Authentication:AdminPassWord"))
                        {

                            var claimsBasic = new List<Claim>();
                            claimsBasic.Add(new Claim(ClaimConstants.USER_ID, UserConstants.AdministratorId.ToString()));
                            ClaimsIdentity claimsIdentityBasic = new ClaimsIdentity(claimsBasic, "Basic");
                            ClaimsPrincipal claimsPrincipalBasic = new ClaimsPrincipal(claimsIdentityBasic);

                            result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipalBasic,
                                                    new AuthenticationProperties(), "Basic"));
                            return result;
                        }
                    }
                }
                #endregion
                #region JWT
                if (Utils.GetConfig("Authentication:Jwt:Enable") == "true")
                {
                    if (authHeader != null && authHeader.StartsWith("BearerJWT "))
                    {
                        string accessToken = Guid.NewGuid().ToString();
                        // Get the token
                        var token = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                        // validatetoken
                        var handerJwt = new JwtSecurityTokenHandler();
                        var tokenInfo = handerJwt.ReadJwtToken(token);
                        SecurityToken validatedToken;
                        handerJwt.ValidateToken(token, new TokenValidationParameters()
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = Utils.GetConfig("Authentication:Jwt:Issuer"),
                            ValidAudience = Utils.GetConfig("Authentication:Jwt:Issuer"),
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Utils.GetConfig(("Authentication:Jwt:Key"))))
                        }, out validatedToken);
                        if (validatedToken != null
                        && validatedToken.Issuer == Utils.GetConfig("Authentication:Jwt:Issuer")
                        && validatedToken.ValidFrom.CompareTo(DateTime.Now) != 1
                        && validatedToken.ValidTo.CompareTo(DateTime.Now) != -1)
                        {
                            var claimsJwt = new List<Claim>();
                            var userId = tokenInfo.Claims.Where(x => x.Type == ClaimConstants.USER_ID).FirstOrDefault().Value;
                            claimsJwt.Add(new Claim(ClaimConstants.USER_ID, userId.ToString()));
                            claimsJwt.AddRange(tokenInfo.Claims);
                            ClaimsIdentity claimsIdentityJwt = new ClaimsIdentity(claimsJwt, "Jwt");
                            ClaimsPrincipal claimsPrincipalJwt = new ClaimsPrincipal(claimsIdentityJwt);

                            result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipalJwt,
                                                new AuthenticationProperties(), "Jwt"));
                            return result;
                        }

                    }
                }
                #endregion

                return AuthenticateResult.Fail("Không xác thực");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                // Console.WriteLine(ex);
                return AuthenticateResult.Fail("Không xác thực");
            }
        }
    
    }

    
}
