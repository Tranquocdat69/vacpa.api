﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;

namespace CommonUtils
{

    public static class Helper
    {
        /// <summary>
        /// Transform data to http response
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static IActionResult TransformData(Response data)
        {
            var result = new ObjectResult(data) { StatusCode = (int)data.Code };
            return result;
        }

        /// <summary>
        /// Get user info in token and headder
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public static RequestUser GetRequestInfo(HttpRequest request, ClaimsPrincipal currentUser)
        {

            var result = new RequestUser
            {
                UserId = Guid.Empty,
                ApplicationId = Guid.Empty
            };
            request.Headers.TryGetValue("X-ApplicationId", out StringValues applicationId);
            if (!string.IsNullOrEmpty(applicationId) && Utils.IsGuid(applicationId))
            {
                result.ApplicationId = new Guid(applicationId);
            }
            if (currentUser.HasClaim(c => c.Type == ClaimTypes.NameIdentifier))
            {
                var userId = currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
                if (!string.IsNullOrEmpty(userId) && Utils.IsGuid(userId))
                {
                    result.UserId = new Guid(userId);
                }
            }
            request.Headers.TryGetValue("X-User", out StringValues userId1);
            if (!string.IsNullOrEmpty(userId1) && Utils.IsGuid(userId1))
            {
                result.UserId = new Guid(userId1);
            }
            return result;
        }

    }
    
    public class JWTHelper{

        #region jwt
        public string BuildToken(UserModel user)
        {
            var iat = DateTime.Now;
            var exp = iat.AddMinutes(Convert.ToDouble(Utils.GetConfig("Authentication:Jwt:TimeToLive")));
            var claims = new[]
            {
                            new Claim(ClaimConstants.USER_NAME, user.Email??""),
                            new Claim(ClaimConstants.FULL_NAME, user.Name??""),
                            new Claim(ClaimConstants.AVATAR, user.Avatar??""),
                            new Claim(ClaimConstants.USER_ID, user.UserId.ToString()),
                            //new Claim(ClaimConstants.APP_ID, applicationModel.Id.ToString()),
                            //new Claim(ClaimConstants.APPS, JsonConvert.SerializeObject(listApplication.Select(x=>x.Code))),
                            new Claim(ClaimConstants.ROLES, JsonConvert.SerializeObject(user.UserRights.Roles.Select(x=>x.RoleCode))),
                            new Claim(ClaimConstants.RIGHTS, JsonConvert.SerializeObject(user.UserRights.FreeRights.Where(x=> x.Enable).Select(x=>x.RightCode))),
                            new Claim(ClaimConstants.AREAS, JsonConvert.SerializeObject(user.UserInArea)),
                            new Claim(ClaimConstants.EXPIRES_AT, iat.ToUnixTime().ToString()),
                            new Claim(ClaimConstants.ISSUED_AT,  exp.ToUnixTime().ToString()),
                    };




            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Utils.GetConfig("Authentication:Jwt:Key")));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(Utils.GetConfig("Authentication:Jwt:Issuer"),
                           Utils.GetConfig("Authentication:Jwt:Issuer"),
                           claims,
                           expires: DateTime.Now.AddMinutes(Convert.ToDouble(Utils.GetConfig("Authentication:Jwt:TimeToLive"))),
                           signingCredentials: creds);
            // if (isRememberMe)
            // {
            //     token = new JwtSecurityToken(_config["Authentication:Jwt:Issuer"],
            //                  _config["Authentication:Jwt:Issuer"],
            //                  claims,
            //                  expires: DateTime.Now.AddMinutes(Convert.ToDouble(99999)),
            //                  signingCredentials: creds);
            // }
            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            return tokenString;
        }

        public string BuildStaticToken(UserModel user)
        {
            var iat = DateTime.Now;
            var exp = iat.AddMinutes(Convert.ToDouble(Utils.GetConfig("Authentication:Jwt:TimeToLive")));
            var claims = new[]
            {
                            //new Claim(ClaimConstants.USER_NAME, user.Email??""),
                            //new Claim(ClaimConstants.FULL_NAME, user.Name??""),
                            //new Claim(ClaimConstants.AVATAR, user.Avatar??""),
                            new Claim(ClaimConstants.USER_ID, user.UserId.ToString()),
                            //new Claim(ClaimConstants.APP_ID, applicationModel.Id.ToString()),
                            //new Claim(ClaimConstants.APPS, JsonConvert.SerializeObject(listApplication.Select(x=>x.Code))),
                            //new Claim(ClaimConstants.ROLES, JsonConvert.SerializeObject(user.UserRights.Roles.Select(x=>x.RoleCode))),
                            //new Claim(ClaimConstants.RIGHTS, JsonConvert.SerializeObject(user.UserRights.FreeRights.Where(x=> x.Enable).Select(x=>x.RightCode))),
                            //new Claim(ClaimConstants.AREAS, JsonConvert.SerializeObject(user.UserInArea)),
                            //new Claim(ClaimConstants.EXPIRES_AT, iat.ToUnixTime().ToString()),
                            //new Claim(ClaimConstants.ISSUED_AT,  exp.ToUnixTime().ToString()),
                    };




            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Utils.GetConfig("Authentication:Jwt:Key")));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(Utils.GetConfig("Authentication:Jwt:Issuer"),
                           Utils.GetConfig("Authentication:Jwt:Issuer"),
                           claims,
                           expires: DateTime.Now.AddMinutes(Convert.ToDouble(Utils.GetConfig("Authentication:Jwt:TimeToLive"))),
                           signingCredentials: creds);
            // if (isRememberMe)
            // {
            //     token = new JwtSecurityToken(_config["Authentication:Jwt:Issuer"],
            //                  _config["Authentication:Jwt:Issuer"],
            //                  claims,
            //                  expires: DateTime.Now.AddMinutes(Convert.ToDouble(99999)),
            //                  signingCredentials: creds);
            // }
            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            return tokenString;
        }

        public UserModel GetUserInfoByJWTToken(string token){
            try
            {
                var result = new UserModel();
                // Get the token
                token = token.Trim();
                // validatetoken
                var handerJwt = new JwtSecurityTokenHandler();
                var tokenInfo = handerJwt.ReadJwtToken(token);
                SecurityToken validatedToken;
                handerJwt.ValidateToken(token, new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Utils.GetConfig("Authentication:Jwt:Issuer"),
                    ValidAudience = Utils.GetConfig("Authentication:Jwt:Issuer"),
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Utils.GetConfig(("Authentication:Jwt:Key"))))
                }, out validatedToken);
                if (validatedToken != null
                && validatedToken.Issuer == Utils.GetConfig("Authentication:Jwt:Issuer")
                && validatedToken.ValidFrom.CompareTo(DateTime.Now) != 1
                && validatedToken.ValidTo.CompareTo(DateTime.Now) != -1)
                {
                    var claimsJwt = new List<Claim>();
                    var userId = tokenInfo.Claims.Where(x => x.Type == ClaimConstants.USER_ID).FirstOrDefault().Value;

                    result.UserId = new Guid(userId);
                    result.UserName = tokenInfo.Claims.Where(x => x.Type == ClaimConstants.USER_NAME).FirstOrDefault().Value;
                    result.FullName = tokenInfo.Claims.Where(x => x.Type == ClaimConstants.FULL_NAME).FirstOrDefault().Value;
                    // apps result.FullName = tokenInfo.Claims.Where(x => x.Type == ClaimConstants.FULL_NAME).FirstOrDefault().Value;
                    result.UserRights.Roles = JsonConvert.DeserializeObject<List<RoleOfUser>>(tokenInfo.Claims.Where(x => x.Type == ClaimConstants.ROLES).FirstOrDefault().Value);
                    result.UserRights.FreeRights = JsonConvert.DeserializeObject<List<RightsOfUser>>(tokenInfo.Claims.Where(x => x.Type == ClaimConstants.RIGHTS).FirstOrDefault().Value);
                    result.UserInArea = JsonConvert.DeserializeObject<UserInArea>(tokenInfo.Claims.Where(x => x.Type == ClaimConstants.AREAS).FirstOrDefault().Value);

                }
                return result;
            }
            catch (System.Exception ex)
            {
                Serilog.Log.Error(ex, "");
                    return null;
            }
            
        }
        
        #endregion


    }
    public class RequestUser
    {
        public Guid UserId { get; set; }
        public Guid ApplicationId { get; set; }
    }
    public static class ApiUtils
    {
        public static CustomRequestHeaderValue GetRequestFields(HttpRequest request, IPrincipal principal)
        {
            var result = new CustomRequestHeaderValue
            {
                UserId = AppConstants.HO_APP,
                ModuleId = Guid.Empty
            };

            string userId = request.Headers["X-User"];

            if (string.IsNullOrEmpty(userId) && principal != null) userId = principal.Identity.Name;

            string moduleId = request.Headers["X-Module"];

            if (!string.IsNullOrEmpty(userId))
            {
                result.UserId = new Guid(userId);
            }

            if (!string.IsNullOrEmpty(moduleId))
            {
                result.ModuleId = new Guid(moduleId);
            }

            return result;
        }

        public static CustomRequestHeaderValue GetRequestFields(HttpRequest request)
        {

            var result = new CustomRequestHeaderValue
            {
                UserId = Guid.Empty,
                ModuleId = Guid.Empty
            };

            string userId = request.Headers["X-User"];
            string moduleId = request.Headers["X-Module"];
            string applicationId = request.Headers["X-ApplicationId"];
            string branchUnit = request.Headers["X-BranchUnit"];
            string permission = request.Headers["X-Permission"];
            string authHeader = (request.Headers["Authorization"]);

            //if (!string.IsNullOrEmpty(permission) || !string.IsNullOrEmpty(authHeader))
            //{
            //    try
            //    {
            //        authHeader = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
            //        if (!string.IsNullOrEmpty(authHeader))
            //        {
            //            permission = authHeader;
            //        }
            //        // Get the token
            //        var token = permission.Trim();
            //        // validatetoken
            //        var handerJwt = new JwtSecurityTokenHandler();
            //        var tokenInfo = handerJwt.ReadJwtToken(token);

            //        var issuer = Utils.GetConfig("Authenticate:Jwt:Issuer");
            //        var key = Utils.GetConfig("Authenticate:Jwt:Key");

            //        SecurityToken validatedToken;
            //        handerJwt.ValidateToken(token, new TokenValidationParameters()
            //        {
            //            ValidateIssuer = true,
            //            ValidateAudience = true,
            //            ValidateLifetime = true,
            //            ValidateIssuerSigningKey = true,
            //            ValidIssuer = issuer,
            //            ValidAudience = issuer,
            //            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key))
            //        }, out validatedToken);
            //        if (validatedToken != null
            //        && validatedToken.Issuer == issuer
            //        && validatedToken.ValidFrom.CompareTo(DateTime.Now) != 1
            //        && validatedToken.ValidTo.CompareTo(DateTime.Now) != -1)
            //        {
            //            var claimsJwt = new List<Claim>();
            //            userId = tokenInfo.Claims.Where(x => x.Type == "USER_ID" || x.Type == "nameid").FirstOrDefault()?.Value;
            //            applicationId = tokenInfo.Claims.Where(x => x.Type == "APPLICATION_ID").FirstOrDefault()?.Value;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex);
            //        return result;
            //    }

            //}


            if (!string.IsNullOrEmpty(userId) && Utils.IsGuid(userId))
            {
                result.UserId = new Guid(userId);
            }

            if (!string.IsNullOrEmpty(moduleId))
            {
                result.ModuleId = new Guid(moduleId);
            }

            if (!string.IsNullOrEmpty(applicationId) && Utils.IsGuid(applicationId))
            {
                result.ApplicationId = new Guid(applicationId);
            }

            if (!string.IsNullOrEmpty(permission))
            {
                result.Permission = permission;
            }
            result.BranchUnit = branchUnit;
            return result;
        }

    }
    public class CustomRequestHeaderValue
    {
        public Guid UserId { get; set; }
        public Guid ModuleId { get; set; }
        public Guid ApplicationId { get; set; }
        public string BranchUnit { get; set; }
        public string Permission { get; set; }
    }

    public class TokenRequest
    {
        public string Token { get; set; }
        public string Password { get; set; }
    }
    public class TokenInfo
    {
        public Guid ObjectId { get; set; }
        public int Level { get; set; }
        public long Tick { get; set; }
        public DateTime DateTimeExpired { get; set; }
    }
    public static class TokenHelpers
    {
        #region basic token

        /// <summary>
        /// Tạo token theo key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string CreateBasicToken(string key)
        {
            try
            {
                string token = string.Empty;

                byte[] keyData = Encoding.UTF8.GetBytes(key);

                // Token chứa mã đối tượng tải về
                if (keyData != null) token = Convert.ToBase64String(keyData.ToArray());
                //Safe URl
                token = Base64UrlEncoder.Encode(token);
                return token;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Lấy key theo token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static string GetKeyFromBasicToken(string token)
        {
            try
            {
                //Safe URl
                token = Base64UrlEncoder.Decode(token);
                string key = string.Empty;

                if (IsBase64(token))
                {
                    byte[] dataToken = Convert.FromBase64String(token);

                    if (dataToken != null) key = Encoding.UTF8.GetString(dataToken);
                }
                return key;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion 

        #region token download

        /// <summary>
        /// Tạo token chứa mã đối tượng, thời gian hiệu lực
        /// </summary>
        /// <param name="objectId">mã đối tượng</param>
        /// <param name="ticks">thời gian hiệu lực</param>
        /// <param name="keyEncrypt">khóa mã hóa</param>
        /// <returns></returns>
        public static string CreateUniqueToken(string objectId, long ticks, string keyEncrypt)
        {
            try
            {
                string token = string.Empty;

                byte[] key = Encoding.UTF8.GetBytes(objectId);
                byte[] time = Encoding.UTF8.GetBytes(ticks.ToString());

                // Token chứa thông tin thời gian hết hạn và mã đối tượng tải về
                if (time.Concat(key) != null) token = Convert.ToBase64String(key.Concat(time).ToArray());

                // Mã hóa token
                if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(keyEncrypt)) token = Encrypt.EncryptText(token, keyEncrypt);
                //Safe URl
                token = Base64UrlEncoder.Encode(token);
                return token;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Lấy thời gian hết hạn theo token
        /// </summary>
        /// <param name="token">mã đối tượng</param>
        /// <param name="keyEncrypt">khóa mã hóa</param>
        /// <returns></returns>
        public static DateTime? GetDateTimeExpired(string token, string keyEncrypt)
        {
            try
            {
                //Safe URl
                token = Base64UrlEncoder.Decode(token);
                // Giải mã chuỗi token nếu dùng mã hóa
                if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(keyEncrypt)) token = Encrypt.DecryptText(token, keyEncrypt);
                token = token.Replace("\0", string.Empty);
                DateTime unixYear0 = new DateTime(1970, 1, 1, 0, 0, 1);
                DateTime dateTimeExpired = DateTime.Now;

                string timeTicksExpiredString = string.Empty;

                if (IsBase64(token))
                {
                    byte[] dataToken = Convert.FromBase64String(token);
                    if (dataToken != null)
                    {
                        byte[] dataTick = new byte[dataToken.Length - 36];

                        Array.Copy(dataToken, 36, dataTick, 0, dataToken.Length - 36);
                        if (dataTick != null) timeTicksExpiredString = Encoding.UTF8.GetString(dataTick);
                        if (!string.IsNullOrEmpty(timeTicksExpiredString))
                        {
                            long ticks = long.Parse(timeTicksExpiredString);
                            dateTimeExpired = new DateTime(unixYear0.Ticks + ticks);
                        }
                    }
                    return dateTimeExpired;
                }
                return null;

            }
            catch (Exception)
            {

                throw;
            }

        }
        /// <summary>
        /// Lấy mã đối tượng theo token
        /// </summary>
        /// <param name="token">mã đối tượng</param>
        /// <param name="keyEncrypt">khóa mã hóa</param>
        /// <returns></returns>
        public static Guid GetObjectId(string token, string keyEncrypt)
        {
            try
            {
                //Safe URl
                token = Base64UrlEncoder.Decode(token);
                // Giải mã chuỗi token nếu sử dụng mã hóa
                if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(keyEncrypt)) token = Encrypt.DecryptText(token, keyEncrypt);
                token = token.Replace("\0", string.Empty);
                Guid objectId = Guid.Empty;

                if (IsBase64(token))
                {
                    string objectStringId = string.Empty;
                    byte[] dataToken = Convert.FromBase64String(token);
                    byte[] dataGuid = new byte[36];
                    Array.Copy(dataToken, 0, dataGuid, 0, 36);
                    if (dataGuid != null) objectStringId = Encoding.UTF8.GetString(dataGuid);

                    if (!string.IsNullOrEmpty(objectStringId) && Utils.IsGuid(objectStringId))
                    {
                        objectId = new Guid(objectStringId);
                    }
                }


                return objectId;
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion
        #region token tokenInfo
        /// <summary>
        /// Tạo token
        /// </summary>
        /// <param name="tokenInfo"></param>
        /// <param name="keyEncrypt"></param>
        /// <returns></returns>
        public static string CreateToken(TokenInfo tokenInfo, string keyEncrypt)
        {
            try
            {
                string token = string.Empty;

                byte[] objectId = Encoding.UTF8.GetBytes(tokenInfo.ObjectId.ToString());
                byte[] level = Encoding.UTF8.GetBytes(tokenInfo.Level.ToString());
                byte[] tick = Encoding.UTF8.GetBytes(tokenInfo.Tick.ToString());

                // Token chứa thông tin thời gian hết hạn và mã đối tượng tải về
                if (level.Concat(objectId).Concat(tick) != null) token = Convert.ToBase64String(level.Concat(objectId).Concat(tick).ToArray());
                // Mã hóa token
                if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(keyEncrypt)) token = Encrypt.EncryptText(token, keyEncrypt);
                //Safe URl
                token = Base64UrlEncoder.Encode(token);
                return token;
            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Lấy token
        /// </summary>
        /// <param name="token"></param>
        /// <param name="keyEncrypt"></param>
        /// <returns></returns>
        public static TokenInfo GetToken(string token, string keyEncrypt)
        {
            try
            {
                //Safe URl
                token = Base64UrlEncoder.Decode(token);
                // Giải mã chuỗi token nếu dùng mã hóa
                if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(keyEncrypt)) token = Encrypt.DecryptText(token, keyEncrypt);
                token = token.Replace("\0", string.Empty);
                if (IsBase64(token))
                {
                    byte[] dataToken = Convert.FromBase64String(token);
                    if (dataToken != null)
                    {
                        var result = new TokenInfo();
                        byte[] dataLevel = new byte[1];
                        Array.Copy(dataToken, 0, dataLevel, 0, 1);
                        byte[] dataGuid = new byte[36];
                        Array.Copy(dataToken, 1, dataGuid, 0, 36);
                        byte[] dataTick = new byte[dataToken.Length - 37];
                        Array.Copy(dataToken, 37, dataTick, 0, dataToken.Length - 37);
                        if (dataLevel != null && dataGuid != null && dataTick != null)
                        {
                            result.ObjectId = new Guid(Encoding.UTF8.GetString(dataGuid));
                            result.Level = Convert.ToInt16(Encoding.UTF8.GetString(dataLevel));
                            result.Tick = long.Parse(Encoding.UTF8.GetString(dataTick));
                            DateTime unixYear0 = new DateTime(1970, 1, 1, 0, 0, 1);
                            DateTime dateTimeExpired = DateTime.Now;
                            string timeTicksExpiredString = string.Empty;
                            dateTimeExpired = new DateTime(unixYear0.Ticks + result.Tick);
                            result.DateTimeExpired = dateTimeExpired;
                        }
                        return result;
                    }
                }

                return null;
            }
            catch (Exception)
            {
                return null;
            }

        }
        #endregion
        public static bool IsBase64(this string base64String)
        {
            if (base64String == null || base64String.Length == 0 || base64String.Length % 4 != 0
               || base64String.Contains(" ") || base64String.Contains("\t") || base64String.Contains("\r") || base64String.Contains("\n"))
                return false;

            try
            {
                Convert.FromBase64String(base64String);
                return true;
            }
            catch (Exception)
            {
                // Handle the exception
            }
            return false;
        }

    }


    public static class Encrypt
    {
        #region Encrypt Function

        public static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;
                    AES.Padding = PaddingMode.Zeros;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }
        public static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;
                    AES.Padding = PaddingMode.Zeros;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }
        public static string DecryptText(string input, string password)
        {
            // Get the bytes of the string
            byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            string result = Encoding.UTF8.GetString(bytesDecrypted);

            return result;
        }
        public static string EncryptText(string input, string password)
        {
            // Get the bytes of the string
            byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            // Hash the password with SHA256
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            string result = Convert.ToBase64String(bytesEncrypted);

            return result;
        }

        #endregion   
    }
    public static class Security
    {
        #region Check sum
        public static class Algorithms
        {
            public static readonly HashAlgorithm MD5 = new MD5CryptoServiceProvider();
            public static readonly HashAlgorithm SHA1 = new SHA1Managed();
            public static readonly HashAlgorithm SHA256 = new SHA256Managed();
            public static readonly HashAlgorithm SHA384 = new SHA384Managed();
            public static readonly HashAlgorithm SHA512 = new SHA512Managed();
            //public static readonly HashAlgorithm RIPEMD160 = new RIPEMD160Managed();
        }
        public static string GetHashFromFile(string fileName, HashAlgorithm algorithm)
        {
            using (var stream = new BufferedStream(File.OpenRead(fileName), 100000))
            {
                return BitConverter.ToString(algorithm.ComputeHash(stream)).Replace("-", string.Empty);
            }
        }
        public static bool VerifyHashFromFile(string fileName, HashAlgorithm algorithm, string hashInput)
        {
            bool verify = false;
            string hashResult = "";

            using (var stream = new BufferedStream(File.OpenRead(fileName), 100000))
            {
                hashResult = BitConverter.ToString(algorithm.ComputeHash(stream)).Replace("-", string.Empty);
                if (hashResult.SequenceEqual(hashInput)) verify = true;
            }

            return verify;
        }
        #endregion

    }

    public static class DateUtil
    {
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static DateTime FromUnixTime(this long unixTime)
        {
            return epoch.AddMilliseconds(unixTime);
        }

        public static long ToUnixTime(this DateTime datetime)
        {
            return (long)(datetime - epoch).TotalMilliseconds;
        }

    }
    
}
