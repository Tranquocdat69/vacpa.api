using System;
using System.Collections.Generic;
using System.Text;

namespace ApiUtils
{
    public class MdcResponse
    {
        public string Result { get; set; }
        public int UserId { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }

    }
}