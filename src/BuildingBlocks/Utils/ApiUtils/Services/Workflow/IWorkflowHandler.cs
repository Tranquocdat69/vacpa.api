﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiUtils
{
    public interface IWorkflowHandler
    {

        #region Code WF new for WF 3.5

        Task<Response> CreateDocument(Document newDocument);
        Task<Response> GetCommands(InputCommand dataCommand);
        Task<Response> GetNextUserProcess(InputCommand dataCommand);
        Task<Response> ExcuteCommand(InputCommand dataCommand);
        Task<Response> DeleteDocument(Document document);
        Task<Response> GetDocumentHistory(Guid id);
        Task<Response> GetStateNameOfDocuments(Guid id);
        // ManhNX
        Task<Response> GetWFName();
        #endregion
    }
}
