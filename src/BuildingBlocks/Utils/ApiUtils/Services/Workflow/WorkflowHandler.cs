﻿using CommonUtils;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ApiUtils
{
    public class DbWorkflowHandler : IWorkflowHandler
    {
        //ApplicationLog logger = new ApplicationLog();
        private readonly string workFlowApiUri;
        private readonly IConfiguration _configure;

        #region Contructor

        public DbWorkflowHandler(IConfiguration configure)
        {
            _configure = configure;
            workFlowApiUri = _configure["workflow:apiuri"];
        }


        public DbWorkflowHandler() { }

        #endregion

        #region Workflow engine version 3.5

        public async Task<Response> CreateDocument(Document newDocument)
        {
            try
            {
                // Post data
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new System.Uri(workFlowApiUri);

                    // Set the header of request
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var uri = "api/documents";
                    // Post data

                    string json = JsonConvert.SerializeObject(newDocument, Formatting.Indented);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(client.BaseAddress + uri, httpContent);

                    if (response.IsSuccessStatusCode)
                    {
                        var result = JsonConvert.DeserializeObject<ResponseWorkflow<Document>>(response.Content.ReadAsStringAsync().Result);
                        if (result.Status == 1)
                        {
                            return new ResponseObject<Document>(result.Data, response.ReasonPhrase, Code.Success);
                        }
                        else
                        {
                            return new ResponseObject<Document>(result.Data, response.ReasonPhrase, Code.Forbidden);
                        }
                    }
                    else
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        return new ResponseObject<Document>(null, response.ReasonPhrase, Code.ServerError);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> DeleteDocument(Document document)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new System.Uri(workFlowApiUri);

                    // Set the header of request
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var data = JsonConvert.SerializeObject(document);
                    var utfBytes = Encoding.UTF8.GetBytes(data);
                    var uri = "api/deletedocument";
                    // Post data
                    string json = JsonConvert.SerializeObject(document, Formatting.Indented);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(client.BaseAddress + uri, httpContent);
                    if (response.IsSuccessStatusCode)
                    {
                        return new ResponseObject<bool>(true, response.ReasonPhrase, Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<bool>(false, response.ReasonPhrase, Code.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> ExcuteCommand(InputCommand dataCommand)
        {
            try
            {
                // Post data
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new System.Uri(workFlowApiUri);
                    // Set the header of request
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var data = JsonConvert.SerializeObject(dataCommand);
                    var utfBytes = Encoding.UTF8.GetBytes(data);
                    var uri = "api/excutecommand";
                    // Post data
                    string json = JsonConvert.SerializeObject(dataCommand, Formatting.Indented);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(client.BaseAddress + uri, httpContent);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = JsonConvert.DeserializeObject<ResponseWorkflow<DocumentStateModel>>(response.Content.ReadAsStringAsync().Result);

                        return new ResponseObject<DocumentStateModel>(result.Data, response.ReasonPhrase, Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<DocumentStateModel>(null, response.ReasonPhrase, Code.Forbidden);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetCommands(InputCommand dataCommand)
        {
            try
            {
                // Post data
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new System.Uri(workFlowApiUri);
                    // Set the header of request
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var data = JsonConvert.SerializeObject(dataCommand);
                    var utfBytes = Encoding.UTF8.GetBytes(data);
                    var uri = "api/commands";
                    // Post data
                    string json = JsonConvert.SerializeObject(dataCommand, Formatting.Indented);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(client.BaseAddress + uri, httpContent);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = JsonConvert.DeserializeObject<ResponseWorkflow<List<string>>>(response.Content.ReadAsStringAsync().Result);
                        //using (var unitOfWork = new UnitOfWork(new DatabaseFactory()))
                        //{

                        //}
                        return new ResponseObject<List<string>>(result.Data, response.ReasonPhrase, Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<List<string>>(new List<string>(), response.ReasonPhrase, Code.Forbidden);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetDocumentHistory(Guid id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new System.Uri(workFlowApiUri);

                    // Set the header of request
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var uri = "api/history/" + id;
                    // Post data
                    //var response = await client.PostAsync(client.BaseAddress + uri, new StringContent("{}", Encoding.UTF8, "application/json"));
                    var response = await client.GetAsync(client.BaseAddress + uri);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = JsonConvert.DeserializeObject<ResponseWorkflow<List<DocumentHistoryViewModel>>>(response.Content.ReadAsStringAsync().Result);
                        return new ResponseObject<List<DocumentHistoryViewModel>>(result.Data, response.ReasonPhrase, Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<List<DocumentHistoryViewModel>>(null, response.ReasonPhrase, Code.Forbidden);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetNextUserProcess(InputCommand dataCommand)
        {
            try
            {
                // Post data
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new System.Uri(workFlowApiUri);
                    // Set the header of request
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var data = JsonConvert.SerializeObject(dataCommand);
                    var utfBytes = Encoding.UTF8.GetBytes(data);
                    var uri = "api/nextuser";
                    // Post data
                    string json = JsonConvert.SerializeObject(dataCommand, Formatting.Indented);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(client.BaseAddress + uri, httpContent);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = JsonConvert.DeserializeObject<ResponseWorkflow<List<UserInfo>>>(response.Content.ReadAsStringAsync().Result);
                        return new ResponseObject<List<UserInfo>>(result.Data, response.ReasonPhrase, Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<List<UserInfo>>(null, response.ReasonPhrase, Code.Forbidden);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        public async Task<Response> GetStateNameOfDocuments(Guid id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new System.Uri(workFlowApiUri);

                    // Set the header of request
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var uri = "api/statenameofdocument/" + id;
                    // Post data
                    var response = await client.PostAsync(client.BaseAddress + uri, new StringContent("{}", Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        var result = JsonConvert.DeserializeObject<ResponseWorkflow<DocumentStateModel>>(response.Content.ReadAsStringAsync().Result);
                        return new ResponseObject<DocumentStateModel>(result.Data, response.ReasonPhrase, Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<DocumentStateModel>(null, response.ReasonPhrase, Code.Forbidden);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }
        // ManhNX
        public async Task<Response> GetWFName()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new System.Uri(workFlowApiUri);
                    // Set the header of request
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var uri = workFlowApiUri + "api/allscheme";// Thay đổi url ở đây
                    // Post data
                    var httpContent = new StringContent("{}", Encoding.UTF8, "application/json");
                    var response = await client.GetAsync(uri);

                    if (response.IsSuccessStatusCode)
                    {
                        var result = JsonConvert.DeserializeObject<ResponseWorkflow<List<WorkflowScheme>>>(response.Content.ReadAsStringAsync().Result);
                        return new ResponseObject<List<WorkflowScheme>>(result.Data, response.ReasonPhrase, Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<List<WorkflowScheme>>(null, response.ReasonPhrase, Code.Forbidden);
                    }
                }
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return new ResponseError(Code.ServerError, ex.Message);
            }
        }

        #endregion
    }
}
