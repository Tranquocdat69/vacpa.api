﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiUtils
{
    #region Model danh mục workflow

    public class DMWorkflowModel
    {
        public Guid WorkflowId { get; set; }
        public string WorkflowCode { get; set; }
        public string WorkflowName { get; set; }
        public bool Status { get; set; }
    }

    public class DMWorkflowQueryFilter
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string TextSearch { get; set; }
        public bool Status { get; set; }
        public DMWorkflowQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
        }
    }

    #endregion

    #region Model danh muc workflowCommand

    public class DMWorkflowCommandModel
    {
        public Guid CommandID { get; set; }
        public string CommandCode { get; set; }
        public string CommandName { get; set; }
        public int? Order { get; set; }
    }

    public class DMWorkflowCommandQueryFilter
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string TextSearch { get; set; }
        public DMWorkflowCommandQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
        }
    }

    #endregion

    #region Model danh muc workflowState

    public class DMWorkflowStateModel
    {
        public Guid StateID { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
    }
    public class DMWorkflowStateQueryFilter
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string TextSearch { get; set; }
        public DMWorkflowStateQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;

        }
    }
    #endregion

    #region Model workflow new for WF 3.5

    public class Document
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int? Number { get; set; }
        public string Comment { get; set; }
        public Guid AuthorId { get; set; }
        public Guid? ManagerId { get; set; }
        public decimal Sum { get; set; }
        public string State { get; set; }
        public string StateName { get; set; }
        public string SchemeName { get; set; }
    }

    public class InputCommand
    {
        public Guid DocumentId { get; set; }
        public string UserId { get; set; }
        public string UserFullName { get; set; }
        public string NextUserId { get; set; }
        public string CommandName { get; set; }
        public string StateNameToSet { get; set; }
        public string Comment { get; set; }
        public List<FileAttach> DataFileAttach { get; set; }
        public List<Guid> AllowIdentityIds { get; set; }
    }

    public class FileAttach
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }

    public class DocumentHistoryViewModel
    {
        public string NguoiXuLy { get; set; }
        public Guid? UserIdNguoiXuLy { get; set; }
        public string NguoiXuLyTiepTheo { get; set; }
        public Guid? UserIdNguoiXuLyTiepTheo { get; set; }
        public string HanhDong { get; set; }
        //public DateTime? ThoiGian { get; set; }
        //public DateTime? ThoiGianDuKien { get; set; }
        public DateTime? ThoiGianKetThuc { get; set; }
        public string GhiChu { get; set; }
        //public int TrangThai { get; set; }
        //public List<FileAttach> TepDinhKem { get; set; }
    }

    public class UserInfo
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string RoleName { get; set; }
        public string OrgName { get; set; }
    }

    public class DocumentStateModel
    {
        public string StateName { get; set; }
        public bool? IsFinished { get; set; }
        public string SchemeCode { get; set; }
    }
    //ManhNX
    public class WorkflowScheme
    {
        public string Code { get; set; }
        public string Scheme { get; set; }
    }
    #endregion

    #region Model workflow old
    public class WorkflowSchemeModel
    {
        public string Code { get; set; }
        public string Scheme { get; set; }

    }
    public class WorkflowCreateDocumentModel
    {
        public string DocumentId { get; set; }
        public string AuthorId { get; set; }
        public string DocumentName { get; set; }
        public string WorkflowCode { get; set; }
    }
    public class WorkflowDocumentModel
    {
        public string DocumentId { get; set; }
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }
        public string State { get; set; }
        public string StateName { get; set; }

    }
    public class WorkflowDocumentTransitionHistoryModel
    {
        public string DocumentId { get; set; }
        public string AuthorId { get; set; }
        public string AuthorName { get; set; }
        public string InitialState { get; set; }
        public string DestinationState { get; set; }
        public string AllowedEmployee { get; set; }
        public string Comment { get; set; }
        public DateTime? TransitionTime { get; set; }


        public bool? IsDenial { get; set; }
    }
    public class WorkflowCreateDocumentResult
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string DocumentId { get; set; }

    }

    public class WorkflowProcessDocumentModel
    {
        public string DocumentId { get; set; }
        public string DocumentType { get; set; }
        public string Command { get; set; }
        public string Comment { get; set; }
        public string UserId { get; set; }
        public string ActorId { get; set; }
        public string State { get; set; }
    }

    public class WorkflowProcessDocumentResult
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string DocumentId { get; set; }
        public string DocumentState { get; set; }
    }

    public class WorkflowSendMailModel
    {
        public string ActorName { get; set; }
        public string AuthorName { get; set; }
        public string Message { get; set; }
    }

    public enum WorkflowQueryOrder
    {
        NGAY_TAO_DESC,
        NGAY_TAO_ASC,
        ID_DESC,
        ID_ASC
    }

    public class WorkflowQueryFilter
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string TextSearch { get; set; }
        public string UserId { get; set; }
        public WorkflowQueryOrder Order { get; set; }

        public WorkflowQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
            Order = WorkflowQueryOrder.NGAY_TAO_DESC;
        }
    }
    #endregion
}
