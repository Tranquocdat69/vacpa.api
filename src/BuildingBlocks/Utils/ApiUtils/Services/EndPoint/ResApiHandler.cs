﻿using CommonUtils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ApiUtils
{
    public class ResApiHandler<TResult, TParam>
        where TResult : class
        where TParam : class
    {
        private readonly HttpClient _apiClient;

        public static ResApiHandler<TResult, TParam> Instance { get; }

        private static string _token = string.Empty;
        private static DateTime _tokenExpireDate = DateTime.Now;
        public ResApiHandler(string systemTarget)
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            _apiClient = new HttpClient(clientHandler);
            _apiClient.DefaultRequestHeaders.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36");

            if (systemTarget == CommonUtils.ResApiTarget.Ipfm)
            {
                _apiClient.DefaultRequestHeaders.Add("API-Key", Utils.GetConfig("Authentication:IPFM:API-Key"));
                Console.WriteLine("API-Key:" + Utils.GetConfig("Authentication:IPFM:API-Key"));

                if (string.IsNullOrEmpty(_token) || _tokenExpireDate.AddMinutes(-10) <= DateTime.Now)
                {
                    var userName = Utils.GetConfig("Authentication:IPFM:Username");
                    var passWord = Utils.GetConfig("Authentication:IPFM:PassWord");

                    var token = GetIPFMToken(userName, passWord).Result;
                    if (token != null && !string.IsNullOrEmpty(token.Token))
                    {
                        Console.WriteLine("token is not null");
                        _apiClient.DefaultRequestHeaders.Authorization =
                            new AuthenticationHeaderValue(
                                "Authorization", "Bearer " + token.Token);
                        _token = token.Token;
                        _tokenExpireDate = token.ExpireDate.ToLocalTime();
                    }
                    else
                    {
                        Console.WriteLine("token is null");
                        _apiClient.DefaultRequestHeaders.Authorization =
                            new AuthenticationHeaderValue(
                                "Authorization", "Bearer " + token.User.Token);
                        _token = token.User.Token;
                        _tokenExpireDate = token.User.ExpireDate.ToLocalTime();
                    }
                }
                else
                {
                    Console.WriteLine("----------------- _token is not null");
                    _apiClient.DefaultRequestHeaders.Authorization =
                            new AuthenticationHeaderValue(
                                "Authorization", "Bearer " + _token);
                }

            }
            else if (systemTarget == CommonUtils.ResApiTarget.Self)
            {
                _apiClient.DefaultRequestHeaders.Add("X-ApplicationId", Utils.GetConfig("Authentication:appId"));
                _apiClient.DefaultRequestHeaders.Add("X-Permission", Utils.GetConfig("Authentication:XPermission"));
            }
            else if(systemTarget == CommonUtils.ResApiTarget.Dhsx)
            {
                _apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var authenticationBytes = Encoding.ASCII.GetBytes(Utils.GetConfig("Authentication:AdminUser") + ":" + Utils.GetConfig("Authentication:AdminPassWord"));

                _apiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(authenticationBytes));
            }

        }

        public ResApiHandler(string username, string password)
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            _apiClient = new HttpClient(clientHandler);
            //_apiClient.DefaultRequestHeaders.Add("Authorization", Utils.GetConfig("Authentication:Jwt:Anonymous"));
            _apiClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue(
                        "Basic", Convert.ToBase64String(
                            System.Text.ASCIIEncoding.ASCII.GetBytes(
                               username + ":" + password)));
            //_apiClient.DefaultRequestHeaders.Add("X-ApplicationId", Utils.GetConfig("Authentication:appId"));
        }

        static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        private async Task<IPFMLoginResponseModel> GetIPFMToken(string username, string password)
        {
            try
            {
                Console.WriteLine("username:" + username);
                Console.WriteLine("password:" + password);
                var uri = Utils.GetConfig("Authentication:IPFM:Uri") + "account/login";
                var paswordHash = ComputeSha256Hash(password);

                Console.WriteLine("uri:" + uri);
                Console.WriteLine("paswordHash:" + paswordHash);

                //var s = await _apiClient.GetAsync("http://183.81.32.66:9512/api/v-1/cat/province");

                //var sstringResponse = await s.Content.ReadAsStringAsync();

                //Console.WriteLine("stringResponse:" + sstringResponse);

                Console.WriteLine("stringResponse:--------------------------------------------");

                /// string json = JsonConvert.SerializeObject(new IPFMLoginModel() {Username=username,Password=paswordHash}, Formatting.Indented);
                // var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _apiClient.PostAsJsonAsync(uri, new IPFMLoginModel() { Username = username, Password = paswordHash });
                var stringResponse = await response.Content.ReadAsStringAsync();

                Console.WriteLine("stringResponse:-------------------------------------------- Done");

                // Console.WriteLine("stringResponse:" + stringResponse);

                return JsonConvert.DeserializeObject<IPFMLoginResponseModel>(stringResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine("stringResponse:-------------------------------------------- ERROR:" + ex);
                Serilog.Log.Error(ex, "");
                return null;
            }
        }

        public async Task<TResult> Get(string uri)
        {
            var starttime = DateTime.Now;
            try
            {
                var response = await _apiClient.GetAsync(uri);
                var stringResponse = await response.Content.ReadAsStringAsync();
                Console.WriteLine("MDC - Get Response Time:-----------------:" + (DateTime.Now - starttime).Milliseconds);
                return JsonConvert.DeserializeObject<TResult>(stringResponse);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return null;
            }
        }


        public async Task<TResult> Post(string uri, TParam param = null)
        {
            var starttime = DateTime.Now;
            try
            {
                // string json = JsonConvert.SerializeObject(param, Formatting.Indented);
                // var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _apiClient.PostAsJsonAsync(uri, param);
                var stringResponse = await response.Content.ReadAsStringAsync();
                Console.WriteLine("MDC - Post Response Time:-----------------:" + (DateTime.Now - starttime).Milliseconds);
                return JsonConvert.DeserializeObject<TResult>(stringResponse);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                Serilog.Log.Error(JsonConvert.SerializeObject(param));
                return null;
            }
        }

        public async Task<TResult> Put(string uri, TParam param = null)
        {
            try
            {
                //string json = JsonConvert.SerializeObject(param, Formatting.Indented);
                //var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _apiClient.PostAsJsonAsync(uri, param);
                var stringResponse = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TResult>(stringResponse);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                Serilog.Log.Error(JsonConvert.SerializeObject(param));
                return null;
            }
        }

        public async Task<TResult> Delete(string uri)
        {
            try
            {
                var response = await _apiClient.DeleteAsync(uri);
                var stringResponse = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TResult>(stringResponse);
            }
            catch (Exception ex)
            {
                Serilog.Log.Error(ex, "");
                return null;
            }
        }

    }
}
