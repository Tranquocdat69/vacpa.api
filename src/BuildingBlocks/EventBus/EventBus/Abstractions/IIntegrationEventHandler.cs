﻿using SV.Core.FW.MSA.BuildingBlocks.EventBus.Events;
using System.Threading.Tasks;

namespace SV.Core.FW.MSA.BuildingBlocks.EventBus.Abstractions
{
    public interface IIntegrationEventHandler<in TIntegrationEvent> : IIntegrationEventHandler 
        where TIntegrationEvent: IntegrationEvent
    {
        Task Handle(TIntegrationEvent @event);
    }

    public interface IIntegrationEventHandler
    {
    }
}
